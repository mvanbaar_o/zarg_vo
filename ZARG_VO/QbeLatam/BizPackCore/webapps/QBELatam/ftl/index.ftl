<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<#if RequestParameters.platform??>
<meta name="viewport" content="width=device-width, initial-scale=1">
</#if>
<title>VP/MS Styler - QBELatam</title>
		
<#if RequestParameters.platform??>
<#include "_header_ios.ftl">
<#else>
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/jquery__default/jquery-ui.css'}" type="text/css" />
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/ui.jqgrid.css'}" type="text/css" />
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/jquery.treeview.css'}" type="text/css" />
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/all.css'}" type="text/css" />
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/custom.css'}" type="text/css" />

<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery-ui.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/json2.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jookie.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/grid.locale-en.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.jqGrid.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/excanvas.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.cookie-modified.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.numbers.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/amplify.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.ba-bbq.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.tmpl.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/knockout-1.2.1.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/date.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.treeview.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.custom.properties.js'}"></script> <!-- custom properties' template definitions -->
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.widgets.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.custom.widgets.js'}"></script> <!-- custom widgets' template definitions -->
</#if>


<script type="text/javascript">		

var styler_ui_model = "ID_UI_QbeLatam_FLO";

jQuery(document).ready(function($){
	var quoteId = "${quoteId!''}";

	<#if RequestParameters.debug??>
		jQuery("#debugQuoteInput").attr("value", quoteId);
		jQuery("#debugModelInput").attr("value", styler_ui_model);
		jQuery("#debugContainer").show();
	</#if>

	// product
	var settings = {
		defaultModel: styler_ui_model,
		quoteId: quoteId
    };
	$('#styler').vpmsBindViewModel(settings);
});
</script>

</head>
<body>
	<div id="styler" url="${springMacroRequestContext.getContextPath() + '/process'}" resurl="${springMacroRequestContext.getContextPath() + '/images'}" pdfurl="pdf.pdf"></div>		
	<script id="ID_UI_QbeLatam_FLO" type="text/html">
	<div vpms_element="{name: 'ID_UI_QbeLatam_FLO', widget: 'pages'}">
		<div vpms_group="{name: 'PG_FLO_IN', widget: 'superWidget', settings:{colNumber:1}}">
			<div vpms_element="{name: 'SC_Basic_Data', widget: 'border', container:false, settings:{colNumber:4,css:'bold-caption '}}">
				<div vpms_element="{name: 'fld_Country', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Currency', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Contract_Type', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Risk_Mode', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Transaction_Code', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_User_Type', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_System_Date', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Anzsic_Code', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
			</div>
			
			<div vpms_element="{name: 'SC_Quote', widget: 'border', container:false, settings:{colNumber:3,css:'bold-caption '}}">
				<div vpms_element="{name: 'fld_Policy_Commencement_Date', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Policy_Expiry_dat', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Rating_dat', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Sum_Insured', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Province', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Post_Code', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
			</div>
			
			<div vpms_element="{name: 'SC_Vehicle_Data', widget: 'border', container:false, settings:{colNumber:3,css:'bold-caption '}}">
				<div vpms_element="{name: 'fld_Vehicle_Category', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Vehicle_Type', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Oth_Brand', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Is_Zero_Km', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Manuf_Year_key', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Vehicle_Age', widget:'edit', format:'number', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Cover_Plan', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Origin', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Usage', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Deductible', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Oilfield_Airport', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Windscreen', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Liability', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Cleaning_Costs', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Hail', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Environmental_Damage', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Environmental_Remediation_Costs', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Nof_Accessory', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'ET_Nof_Accessory', widget:'grid', settings:{'height':115}, multiple: true}">
					<div vpms_group="{name: 'ET_Nof_Accessory', widget: 'superWidget', settings:{colNumber:2}}">
						<div vpms_element="{name: 'fld_Accessory_Type', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}}"></div>
						
						<div vpms_element="{name: 'fld_Accessory_Type_Description', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
						
						<div vpms_element="{name: 'fld_Accessory_Type_Sum_Insured', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
					</div>
				</div>
			</div>
			
			<div vpms_element="{name: 'SC_Premiums_Commission', widget: 'border', container:false, settings:{colNumber:3,css:'bold-caption '}}">
				<div vpms_element="{name: 'fld_Comm_Agreed', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Comm_Adj_Pcnt', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Campaign_Discount_Pcnt', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_004_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_024_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_030_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_091_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_092_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_093_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_094_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_096_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_185_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_095_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_971_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_972_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
			</div>
		</div>
		
		<div vpms_group="{name: 'PG_FLO_OUT', widget: 'superWidget', settings:{colNumber:1}}">
			<div vpms_element="{name: 'fld_Risk_No', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
			
			<div vpms_element="{name: 'fld_Version', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
			
			<div vpms_element="{name: 'fld_Validate_Risk_Out', widget:'memo', labeled:true, autocommit:true, rows:'12', container:false, settings:{labelAlign:'left'}}"></div>
			
			<div vpms_element="{name: 'ET_Risk_Out', widget:'grid', settings:{'height':506}, multiple: true}">
				<div vpms_group="{name: 'ET_Risk_Out', widget: 'superWidget', settings:{colNumber:2,css:'bold-caption '}}">
					<div vpms_element="{name: 'fld_Risk_Out_Cov', widget:'edit', labeled:true, autocommit:true, container:false, settings:{width:100,labelAlign:'left',align:'left',css:'bold-caption '}}"></div>
					
					<div vpms_element="{name: 'fld_Risk_Out_Art', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}}"></div>
					
					<div vpms_element="{name: 'fld_Risk_Out_Tpr', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}}"></div>
					
					<div vpms_element="{name: 'fld_Risk_Out_Trt', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}}"></div>
					
					<div vpms_element="{name: 'fld_Risk_Out_Pp', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}}"></div>
					
					<div vpms_element="{name: 'fld_Risk_Out_Rt', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}}"></div>
					
					<div vpms_element="{name: 'fld_Risk_Out_Si', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}}"></div>
					
					<div vpms_element="{name: 'fld_Risk_Out_Af', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}}"></div>
				</div>
			</div>
		</div>
		<div vpms_element="{name: 'pagesbuttons', widget:'buttonbar'}">
			<div vpms_element="{name: 'Save', caption: 'Save', widget:'button', pos:'left', role:'save', isElement:false}"></div>
			<div vpms_element="{name: 'Previous', caption: 'Previous', widget:'button', pos:'right', role:'previous', isElement:false}"></div>
			<div vpms_element="{name: 'Next', caption: 'Next', widget:'button', pos:'right', role:'next', isElement:false}"></div>
		</div>
	</div>
	</script>
	<!-- debug section: enable debug with ?debug attached to URL after quote ID --> 
	<div id="debugContainer" style="display:none;">
		<a href="#" onclick="if(jQuery('#debug').is(':visible')) jQuery('#debug').hide(); else jQuery('#debug').show();">Debug</a> 
		<div id="debug" style="display:none;">
			<form method="post" action="${springMacroRequestContext.getContextPath() + '/debug'}" enctype="multipart/form-data">
				<input id="debugQuoteInput" type="hidden" name="quoteId"/> 
				<input id="debugModelInput" type="hidden" name="modelName"/>
				<p><button type="submit" name="action" value="getState">Get state</button></p>
				<p><button type="submit" name="action" value="setState">Set state</button> File: <input type="file" name="vpmsState" /> </p>
			</form>
		</div>
	</div>
</body>

</html>

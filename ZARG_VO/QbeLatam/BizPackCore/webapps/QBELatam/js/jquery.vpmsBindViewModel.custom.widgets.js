$.vpmsBindViewModel.pushTemplate({
	name: 'sliderTypeId',
	template: '\
	<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error)}">\
		<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
		<div class="vpms-slider-jquery-widget" data-bind="visible:ko.utils.unwrapObservable($data.relevant)!==false, attr: {tooltip:tooltip}" >\
			<span class="range" data-bind="text:min_custom_property_id"></span>\
			<div class="{{if autocommit}}vpms-autocommit{{/if}}" data-bind="attr: {id:id, name:id}"/>\
			<span class="range" data-bind="text:max_custom_property_id"></span>\
			<input class="ui-slider-value-input {{if autocommit}}vpms-autocommit{{/if}}" id="sliderValue" data-bind="value:value" />\
		</div>\
	</div>\
			',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = true;	
		if(!model.name) model.name = "slider";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, true);
		model.visible = $.vpmsBindViewModel.initModelItem(model.visible, true);

		model.value = $.vpmsBindViewModel.initModelItem(model.value, 50);

		model.min_custom_property_id = $.vpmsBindViewModel.initModelItem(model.min_custom_property_id, 0);
		model.max_custom_property_id = $.vpmsBindViewModel.initModelItem(model.max_custom_property_id, 100);
		// width - via CSS!
		// validation message - the element is wrapped in a new <div> with one more element for the image!
		// caption alignment - works
		// layout data - works
		
	},
	updateModel: function(model, data, settings){
		$.vpmsBindViewModel.updateModelItem(model, 'min_custom_property_id', data.min_custom_property_id!==undefined?data.min_custom_property_id:0);
		$.vpmsBindViewModel.updateModelItem(model, 'max_custom_property_id', data.max_custom_property_id!==undefined?data.max_custom_property_id:100);
		//Get the updated model values
		var id = $.vpmsBindViewModel.unwrapObservable(model.id);
		var min = $.vpmsBindViewModel.unwrapObservable(model.min_custom_property_id);
		var max = $.vpmsBindViewModel.unwrapObservable(model.max_custom_property_id);
		var value = $.vpmsBindViewModel.unwrapObservable(model.value);
		if (isNaN(value)) value = 0;
		
		//Update the view
		var widget = $('#' + id);
		if(widget.children().length > 0){
			widget.slider('option',{value:Number(value), min:Number(min), max:Number(max)});
		}else{
			widget.slider({value:Number(value), min:Number(min), max:Number(max), step:1});
			widget.on('slidestop', model, function(event, ui) {
				var observable = event.data.value;
				if (ui.value != $.vpmsBindViewModel.unwrapObservable(observable)) {
					observable(ui.value.toString());
					$('#' + $.vpmsBindViewModel.unwrapObservable(event.data.id)).change();
				}
			});
		}
	}
}); 

//SectionContainerElement widget  - defaults container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'SectionContainerElement',
	template: '\
	<div class="vpms-accordion-element-header" data-bind="visible:relevant() && !$item.group.nocaption, attr:{id:id}, click:function(event){$item.group.selectGroup($item.group, $data);}"><div style="display:block;">\
			<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: required"><span class="ui-icon ui-icon-bullet"></span></div>\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error {{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<div class="vpms-accordion-caption" data-bind="text:caption"></div>\
			<div style="clear:both"></div>\
		</div>\
	</div>\
	<div class="vpms-accordion-element" data-bind="attr: {id:id}, visible:relevant">\
		{{if $data.customWidget && $.vpmsBindViewModel.templateExists($data.customWidget)}}\
		<div class="vpms-elements-container vpms-${$data.settings.colNumber}-col" data-bind="template: {name:customWidget, data:$data}"></div>\
	{{else}}\
		<div class="vpms-elements-container vpms-${$data.settings.colNumber}-col" data-bind="template: {name:\'element\', foreach:elements}"></div>\
	{{/if}}\
		<div style="clear:both"></div>\
	</div>',
});

$.vpmsBindViewModel.pushTemplate({
	name: 'SectionContainer',
	template: '<div class="vpms-accordion-container" data-bind="attr:{id:id, selectedId:selectedId}">\
				<div class="vpms-accordion" data-bind="template:{name:\'SectionContainerElement\', foreach:elements, templateOptions: {group:$data}	}"></div>\
				</div>\
	',
	initModel: function(model){
		if(!model.name) model.name = "SectionContainer";
		//accordion has no selected Page - get and update all groups
		model.getAllGroupsData = true;
		model.setAllGroupsData = true;
		if(model.container==undefined)
			model.container = true;	
		if(model.noaccordion==undefined)
			model.noaccordion = false;	
		if(model.nocaption==undefined || !model.noaccordion)
			model.nocaption = false;			
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.selectedId = $.vpmsBindViewModel.initModelItem(model.selectedId, "");
		
		if(!model.selectGroup) model.selectGroup =  function(model, group){
			if(model.insertAfterSelected){
				if(ko.utils.unwrapObservable(group.addCaption) && model.addCaption)
					model.addCaption(ko.utils.unwrapObservable(group.addCaption));
			}
		} ;
	},
	updateModel: function(model, data){
		$.vpmsBindViewModel.updateModelItem(model, 'selectedId', data.sp);
	}
});

//pages widget - defaults container=true, tabbed=true
$.vpmsBindViewModel.pushTemplate({
	name: 'PageContainer',
	template: '<div class="ui-tabs ui-widget ui-widget-content ui-corner-all">\
	<div class="vpms-tabs-widget">\
		<ul class="vpms-tab-menu ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" data-bind="template: {name:\'pagesTabs\', foreach:groups, templateOptions:{model:$data} }">\
		</ul>\
	</div>\
	<div class="vpms-pages-container"><div class="vpms-clear-float"></div>\
		<div data-bind="template: {name:\'pagesSections\', foreach:groups, templateOptions:{model:$data} }"></div></div>\
	<div class="vpms-clear-float"></div></div>\
',		
	initModel :function(model){
		if(model.container==undefined)
			model.container = true;
		
		if(!model.name) model.name = "PageContainer";	
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);

		if(model.groups!==undefined){
			model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		}else{
			var collectedGroups = [];
			for(var element in model.elements){
				for(var group in model.elements[element].groups){
					collectedGroups.push(model.elements[element].groups[group]);
				}
			}
			model.groups = $.vpmsBindViewModel.initModelItem(collectedGroups, []);
		}

		model.elements = $.vpmsBindViewModel.initModelItem([], []);
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
			
		if(!model.selectedPageInt) model.selectedPageInt = ko.observable();
		
		if(!model.selectedPage) model.selectedPage = ko.dependentObservable({
			read: function () {
				return this.selectedPageInt();
			},
			write: function (value) {
				if(this.selectedPageInt()!=value){
					if($.vpmsBindViewModel.isModelUpdating('#' + this.containerId)){
						this.selectGroupUpdateRequest = value;
						return;
					}					
					if(!model.syncUpdate){
						this.selectedPageInt(value);
						$.vpmsBindViewModel.pushState(this.containerId, ko.utils.unwrapObservable(this.name), value);						
					}else{
						//findout group
						var group = undefined;
						var groupsArray = ko.utils.unwrapObservable(model.groups);
						for(var i=0; i<groupsArray.length; i++)
							if(ko.utils.unwrapObservable(groupsArray[i].id)==value){
								group = groupsArray[i];
								break;
							}
						//check if there are elements or groups to restore, restore them else act as async update						
						if(group && (group._elements || group._groups)){
							var elementsHasMutated=false, groupsHasMutated=false;
							if(group._elements){
								ko.utils.arrayPushAll(group.elements(), group._elements);								
								group._elements = undefined;
								elementsHasMutated = true;
							}
							if(group._groups){
								ko.utils.arrayPushAll(group.groups(), group._groups);
								group._groups = undefined;
								groupsHasMutated = true;
							}
							var rootModel = $('#' + this.containerId).data('model');
							$.vpmsBindViewModel.blockUI(rootModel, "", true);
							rootModel.applyingBinding = true;
							setTimeout(function(){
								try{
									$.vpmsBindViewModel.debug("syncUpdate - start bind cached elements");
									if(elementsHasMutated)
										group.elements.valueHasMutated();
									if(groupsHasMutated)
										group.groups.valueHasMutated();
									$('#' + rootModel.containerId).find(".vpms-pages-section").filter('#' + value).addClass('vpms-afterRender');									
									$.vpmsBindViewModel.debug("syncUpdate - end bind cached elements");
									rootModel.applyingBinding = false;									
								}catch(e){
									rootModel.applyingBinding = false;
									rootModel.applyingBindingError = "Error applying binding: " + e.message;
									throw rootModel.applyingBindingError;
								}
							}, 1);							
							this.requestedPage = value;							
							jQuery("#" + this.containerId).vpmsBindViewModel("updateModel");
						}else{
							this.selectedPageInt(value);
							$.vpmsBindViewModel.pushState(this.containerId, ko.utils.unwrapObservable(this.name), value);													
						}
					}
				}
			},
			owner: model
		});
		if(!model.tabClick) model.tabClick = function(event, model, group){
			if($(event.target).hasClass('vpms-pages-remove-button'))
				jQuery(event.target).vpmsBindViewModel('removeFromGroup', model, group);
			else{				
				if(model.selectedPage()!=ko.utils.unwrapObservable(group.id))		
					model.selectedPage(ko.utils.unwrapObservable(group.id));
				else
					jQuery(event.target).vpmsBindViewModel("updateModel");
			}
		};
		
		if(!model.nextPage) model.nextPage = ko.dependentObservable(function(){
			if(!this.groups)
				return;
			var groupsArray = ko.utils.unwrapObservable(this.groups);
			var found = false;
			for(var i=0; i<groupsArray.length; i++){
			if(!found)
					//find current
					found = (ko.utils.unwrapObservable(groupsArray[i].id)==ko.utils.unwrapObservable(this.selectedPage));
				else
					//next relevant
					if(groupsArray[i].relevant==undefined || ko.utils.unwrapObservable(groupsArray[i].relevant))
						return ko.utils.unwrapObservable(groupsArray[i].id);
			}
		}, model);
		
		if(!model.prevPage) model.prevPage = ko.dependentObservable(function(){
			if(!this.groups)
				return;
			var groupsArray = ko.utils.unwrapObservable(this.groups);
			var found = false;
			for(var i=groupsArray.length-1; i>=0; i--){
				if(!found)
					//find current
					found = (ko.utils.unwrapObservable(groupsArray[i].id)==ko.utils.unwrapObservable(this.selectedPage));
				else
					//prev relevant
					if(groupsArray[i].relevant==undefined || ko.utils.unwrapObservable(groupsArray[i].relevant))
						return ko.utils.unwrapObservable(groupsArray[i].id);
			}
		}, model);			
		
		//populate targetModel
		var elementsArray = ko.utils.unwrapObservable(model.elements);
		for(var i=0; i<elementsArray.length; i++){
			if(elementsArray[i].widget=='buttonbar')
				elementsArray[i].targetModel = model;
		}
		
		if(!model.selectedGroup) model.selectedGroup = ko.dependentObservable(function () {
			var groupsArray = ko.utils.unwrapObservable(this.groups);			
			for(var i=0; i<groupsArray.length; i++){
				if(ko.utils.unwrapObservable(groupsArray[i].id) == this.selectedPage()){
					return groupsArray[i];
				}
			}
		}, model);
			
		if(model.multiple){
			if(!model.addClick) model.addClick = function(event, model){
				if(model.insertAfterSelected){
					var selectedGroup = model.selectedGroup();
					if(!selectedGroup.canAdd || selectedGroup.canAdd())
						jQuery(event.target).vpmsBindViewModel('addToGroup', model, model.selectedPage());
				}
				else
					jQuery(event.target).vpmsBindViewModel('addToGroup', model);
			};
			
			model._addCaption = ko.dependentObservable(function () {
					var activeGroup = this.selectedGroup();
					if(this.insertAfterSelected && activeGroup)
						return ko.utils.unwrapObservable(activeGroup.addCaption);
					else
						return ko.utils.unwrapObservable(this.addCaption);
				}, model);			
		}
		
	},
	beforeUpdateModel: function(model, data){
		if(data && data.g_){			
			//if selected Page comes from server - set it 			
			if(!data.sp){
				if(model.requestedPage){
					data.sp = model.requestedPage;
					model.requestedPage = undefined;
				}else{
					//if bbq has selected page - take it
					var tab = $.vpmsBindViewModel.getState(model.containerId, ko.utils.unwrapObservable(model.name));
					if(tab!==undefined && tab!="")
						data.sp = tab;
				}			
			}	
			var groupsArray = ko.utils.unwrapObservable(model.groups);
			//if data.sp is set - check if this page will be relevant after update
			if(data.sp){
				for(var i=0; i<data.g_.length; i++){
					if(data.g_[i].id == data.sp){
						//findout group
						var group=undefined;
						for(var k=0; k<groupsArray.length; k++){
							if(groupsArray[k].name == data.g_[i]._n){
								group = groupsArray[k];
								break;
							}
						}
						if(group){
							if(group.relevant) group.relevant(data.g_[i]._r!==undefined?data.g_[i]._r:true);
							if(group.relevant!=undefined && ko.utils.unwrapObservable(group.relevant)==false)
								data.sp=undefined;							
						}else{
							if(!(data.g_[i]._r===undefined || data.g_[i]._r==true))
								data.sp=undefined;
						}
						break;
					}
				}
				//reset selected page
				var resetSelectedPage = (data.sp==undefined);
			}					
				
			//if data.sp still not set else take first relevant		
			var i=0;
			while(i<data.g_.length && !data.sp){
				var group;
				if(model.multiple){
					if(data.g_[i]._r===undefined || data.g_[i]._r==true)
						data.sp = data.g_[i].id;
				}else{
					for(var k=0; k<groupsArray.length; k++){
						if(ko.utils.unwrapObservable(groupsArray[k].name)==data.g_[i]._n){
							group = groupsArray[k];
							if(group.relevant) group.relevant(data.g_[i]._r!==undefined?data.g_[i]._r:true);
							if(group.relevant===undefined || ko.utils.unwrapObservable(group.relevant))
								data.sp = data.g_[i].id;
							break;
						}
					}		
				}
				i++;
			};
			//reset selected page if data.sp was not relevant, but do this only once
			if(model.resetSelectedPage)
				model.resetSelectedPage = undefined;
			else
				if(data.sp && resetSelectedPage){
					model.resetSelectedPage = true;
					var _sp = data.sp;
					data.sp = undefined;
					setTimeout(function(){
						model.selectedPage(_sp);
					}, 1);
				}
			//if no relevant page - set first as selectedPage - better than get everytime all pages and wait till one will be relevant
			if(!data.sp && data.g_.length>0)
				data.sp = data.g_[0].id;

			//syncUpdate on init - clear content and cache elements for not selected
			if(model.syncUpdate && $.vpmsBindViewModel.isModelIniting('#' + model.containerId)){					 
				for(var i=0; i<data.g_.length; i++){
					if(data.sp!=data.g_[i].id){
						//cash all groups content for not selected page
						for(var k=0; k<groupsArray.length; k++){
							if(ko.utils.unwrapObservable(groupsArray[k].name)==data.g_[i]._n){
								if(ko.utils.unwrapObservable(groupsArray[k].elements).length>0)
									groupsArray[k]._elements = groupsArray[k].elements.splice(0, ko.utils.unwrapObservable(groupsArray[k].elements).length);
								if(ko.utils.unwrapObservable(groupsArray[k].groups).length>0)
									groupsArray[k]._groups = groupsArray[k].groups.splice(0, ko.utils.unwrapObservable(groupsArray[k].groups).length);
								break;
							}
						}
						//clear all content for not selected page			
						data.g_[i].k_=undefined;
						data.g_[i].e_=undefined;
						data.g_[i].g_=undefined;
					}					
				}
			}
		}		
	},
	updateModel: function(model, data){
		if(data && data.sp){			
			model.selectedPageInt(data.sp);
			$.vpmsBindViewModel.pushState(model.containerId, ko.utils.unwrapObservable(model.name), model.selectedPage(), true);
		}
		if(!model.setAllGroupsData)
			model.prevSelectedGroup = model.selectedGroup();
		if(model.selectGroupUpdateRequest){
			var _sp = model.selectGroupUpdateRequest;
			model.selectGroupUpdateRequest = undefined;
			setTimeout(function(){
				model.selectedPage(_sp);
			}, 1);			
		}		
	}
});
//using
//<#include "selectQuote.ftl">
//<#include "Singapore_ZCare.ftl">
//<#include "Singapore_ZProtect.ftl">
//<#include "Singapore_ZLink.ftl">
//<#include "Singapore_ZSaver.ftl">
//<#include "Singapore_ZInvest.ftl">
//<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.modelCompiler.js'}"></script>
//<button onclick="javascript:$.vpmsBindViewModel.compileModels(['singSelectQuote','display*']);">compile models</button>

(function($){
	function serialize(_obj) { 
		// Let Gecko browsers do this the easy way 
		   if (typeof _obj.toSource !== 'undefined' && typeof _obj.callee === 'undefined') 
		   { 
		      return _obj.toSource(); 
		   } 
		     return _serialize(_obj, 2); 
		}; 
		function _serialize(_obj, level) 
		{    
		     var pre=Array(level+1).join(' '); 
		   // Other browsers must do it the hard way 
		   switch (typeof _obj) 
		   {           
		      // numbers, booleans, and functions are trivial: 
		      // just return the object itself since its default .toString() 
		      // gives us exactly what we want 
		      case 'number': 
		      case 'boolean': 
		      case 'function': 
		         return _obj; 
		         break; 
		 
		      // for JSON format, strings need to be wrapped in quotes 
		      case 'string': 
		         return '\'' + _obj + '\''; 
		         break; 
		 
		      case 'object': 
		         var str; 
		         if (_obj.constructor === Array || typeof _obj.callee !== 'undefined') 
		         { 
		            str = '[\n'; 
		            var i, len = _obj.length; 
		            for (i = 0; i < len-1; i++) { str += pre + _serialize(_obj[i], level+1) + ',\n'; } 
		            str += pre + serialize(_obj[i], level+1) + '\n' + pre + ']'; 
		         } 
		         else 
		         { 
		            str = pre + '{\n'; 
		            var key; 
		            var pre1 = Array(level+2).join(' ');
		            for (key in _obj) { str += pre1 + key + ':' + _serialize(_obj[key], level+2) + ',\n'; } 
		            str = str.replace(/\,\n$/, '') + '\n' + pre + '}'; 
		         } 
		         return str; 
		         break; 
		 
		      default: 
		         return 'UNKNOWN'; 
		         break; 
		   } 
		};
		
		$.vpmsBindViewModel.compileModels = function(id){
			if(!id)
				return;
			var idList = [];
			if($.isArray(id))
				idList = id;
			else
				idList.push(id);
			for(var i=0; i<idList.length; i++){
				var name = idList[i];
				if(!name)
					continue;				
				if(name.indexOf('*')==idList[i].length-1)
					var selector = 'script[id^="' + name.substr(0, name.length-1) + '"]';
				else
					var selector = 'script[id="' + name + '"]';
				$("body").find(selector).each(function(){
					var model = $.vpmsBindViewModel.generateModelFromHtml(undefined, $(this).attr('id'), true, true);
					if(model){						
						$("body").append("<div>" + $(this).attr('id') + "</div>");
						$("body").append("<textarea cols=100 rows=20>" + "$.vpmsBindViewModel.models.push({\n name: '" + $(this).attr('id') +"',\n model: function(){return " + serialize(model) +";\n}});</textarea>");
					}				
				});								
			}
		};	
})(jQuery);
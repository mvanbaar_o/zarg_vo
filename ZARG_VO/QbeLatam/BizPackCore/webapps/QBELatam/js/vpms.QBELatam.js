$.vpmsBindViewModel.models.push({
 name: 'ID_UI_QbeLatam_FLO',
 model: function() { return   {name: 'ID_UI_QbeLatam_FLO', widget: 'pages',
  elements: [
     {name: 'pagesbuttons', widget:'buttonbar',
     elements: [
        {name: 'Save', caption: 'Save', widget:'button', pos:'left', role:'save', isElement:false},
        {name: 'Previous', caption: 'Previous', widget:'button', pos:'right', role:'previous', isElement:false},
        {name: 'Next', caption: 'Next', widget:'button', pos:'right', role:'next', isElement:false}
     ]
     }
  ],
  groups: [
     {name: 'PG_FLO_IN', widget: 'superWidget', settings:{colNumber:1},
     elements: [
        {name: 'SC_Basic_Data', widget: 'border', container:false, settings:{colNumber:4,css:'bold-caption '},
        elements: [
           {name: 'fld_Country', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Currency', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Contract_Type', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Risk_Mode', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Transaction_Code', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_User_Type', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_System_Date', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_Anzsic_Code', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}
        ]
        },
        {name: 'SC_Quote', widget: 'border', container:false, settings:{colNumber:3,css:'bold-caption '},
        elements: [
           {name: 'fld_Policy_Commencement_Date', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_Policy_Expiry_dat', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_Rating_dat', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_Sum_Insured', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_Province', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Post_Code', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}
        ]
        },
        {name: 'SC_Vehicle_Data', widget: 'border', container:false, settings:{colNumber:3,css:'bold-caption '},
        elements: [
           {name: 'fld_Vehicle_Category', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Vehicle_Type', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Oth_Brand', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Is_Zero_Km', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Manuf_Year_key', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Vehicle_Age', widget:'edit', format:'number', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_Cover_Plan', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Origin', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Usage', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Deductible', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Oilfield_Airport', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Windscreen', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Liability', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Cleaning_Costs', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Hail', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Environmental_Damage', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Environmental_Remediation_Costs', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Nof_Accessory', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'ET_Nof_Accessory', widget:'grid', settings:{'height':115}, multiple: true,
           template: 
              {name: 'ET_Nof_Accessory', widget: 'superWidget', settings:{colNumber:2},
              elements: [
                 {name: 'fld_Accessory_Type', widget:'combobox', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left'}},
                 {name: 'fld_Accessory_Type_Description', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
                 {name: 'fld_Accessory_Type_Sum_Insured', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}
              ]
              }
           }
        ]
        },
        {name: 'SC_Premiums_Commission', widget: 'border', container:false, settings:{colNumber:3,css:'bold-caption '},
        elements: [
           {name: 'fld_Comm_Agreed', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_Comm_Adj_Pcnt', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_Campaign_Discount_Pcnt', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_004_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_024_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_030_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_091_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_092_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_093_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_094_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_096_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_185_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_095_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_971_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_972_Old_Annl_Prem', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}
        ]
        }
     ]
     },
     {name: 'PG_FLO_OUT', widget: 'superWidget', settings:{colNumber:1},
     elements: [
        {name: 'fld_Risk_No', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
        {name: 'fld_Version', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
        {name: 'fld_Validate_Risk_Out', widget:'memo', labeled:true, autocommit:true, rows:'12', container:false, settings:{labelAlign:'left'}},
        {name: 'ET_Risk_Out', widget:'grid', settings:{'height':506}, multiple: true,
        template: 
           {name: 'ET_Risk_Out', widget: 'superWidget', settings:{colNumber:2,css:'bold-caption '},
           elements: [
              {name: 'fld_Risk_Out_Cov', widget:'edit', labeled:true, autocommit:true, container:false, settings:{width:100,labelAlign:'left',align:'left',css:'bold-caption '}},
              {name: 'fld_Risk_Out_Art', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}},
              {name: 'fld_Risk_Out_Tpr', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}},
              {name: 'fld_Risk_Out_Trt', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}},
              {name: 'fld_Risk_Out_Pp', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}},
              {name: 'fld_Risk_Out_Rt', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}},
              {name: 'fld_Risk_Out_Si', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}},
              {name: 'fld_Risk_Out_Af', widget:'edit', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left',css:'bold-caption '}}
           ]
           }
        }
     ]
     }
  ]
  };}
});
//jQuery UI tree view PlugIn
//
// Version 1.00
//
// Alex Dobrushin (http://dobrushin.de/)
//
(function($){
	$.treeview = {
		defaults : {
			speed: 300,
			closedSign: '[+]',
			openedSign: '[-]',
			blancSign:'&nbsp;&nbsp;&nbsp',
			expand:false
		}		
	};
	
  var methods = {
     init : function(options) {
	     var settings = {};
		 $.extend(settings, $.treeview.defaults);
		 if(options)
			$.extend(settings, options);		
		return this.each(function(){
			var $this = $(this);
			$this.data('settings', settings);
			$this.addClass('ui-treeview');
			changed($this, $this);
			//select selected
			selectItem($this.find('li a.selected:first'), $this);
		});
	},
	select : function() { 	
		var $treeview = $(this).closest('.ui-treeview');
		if($treeview.length>0)	
			selectItem(this, $treeview);		
	},
	changed : function(){
		var $treeview = $(this).closest('.ui-treeview');
		if($treeview.length>0)
			changed(this, $treeview);
	}
  };

  function selectItem(item, $treeview){
		var $item = $(item);
		if($item.length>0){
			var $a = $($item.closest('li').find('a:first'));
			var settings = $treeview.data('settings');
//			$treeview.find('li a.selected').removeClass('selected');		
//			$a.addClass('selected');
			//open parents before
			$a.parents("ul:hidden").slideDown(settings.speed, function(){
				$(this).parent("li").find(".open-close-button:first").delay(settings.speed).html(settings.openedSign);
			});					
			//open children
			var $ul = $a.parent().find("ul:first");
			if($ul.length>0 && $ul.children('li').length>0 && !$ul.is(":visible")){
				$a.parent().find("ul:first").slideDown(settings.speed, function(){
					$(this).parent("li").find(".open-close-button:first").delay(settings.speed).html(settings.openedSign);
				});						
			}				
		}  
  }

  function changed(item, $treeview){
		var $item = $(item);
		if($item.length>0){
			var settings = $treeview.data('settings');	
			var $li = $item.find('li').add($item.filter('li'));
			$li.each(function() {		
				var $button = $(this).children("a:first").children('.open-close-button,.open-close-button-spacer');
				if($button.length>0)
					$button.remove();
				//check parent li if it has +- sign
				var $parentli = $(this).parent('ul').parent('li');
				if($parentli){
					$button = $parentli.children("a:first").children('.open-close-button,.open-close-button-spacer');
					if($button.hasClass('open-close-button-spacer')){
						$button.remove();
						$button = $('<span class="open-close-button">'+ settings.closedSign +'</span>');
						$parentli.children('a:first').prepend($button);
					}
					if($parentli.find('ul:first').is(':visible'))
						$button.html(settings.openedSign);
					else
						$button.html(settings.closedSign);
				}
				var $ul = $(this).children("ul:first");				
				if($ul.length > 0 && $ul.children('li').length>0){
					if(!settings.expand){
						$(this).children("ul").hide();
						$(this).children("a:first").prepend('<span class="open-close-button">'+ settings.closedSign +'</span>');
					}else
						$(this).children("a:first").prepend('<span class="open-close-button">'+ settings.openedSign +'</span>');
				}
				else{
					if($ul.length > 0)
						$ul.hide();
					$(this).children("a:first").prepend('<span class="open-close-button-spacer">'+ settings.blancSign +'</span>');
				}
				$(this).children("a:first").addClass('ui-state-default');
				$(this).children("a:first").bind('mouseover mouseout', function (event) {				
	                if (event.type == 'mouseover') {
	                    if(!$(this).hasClass('ui-state-active')) $(this).addClass("ui-state-hover");
	                } else {
	                    $(this).removeClass("ui-state-hover");
	                }
	            });			
				
			});
			//click
			$li.children('a').click(function(evt) {
				if($(evt.target).closest('.open-close-button').length>0){
					if($(this).parent().find("ul:first").is(":visible")){
						$(this).parent().find("ul").slideUp(settings.speed, function(){
							$(this).parent("li").find("ul").parent('li').find(".open-close-button:first").delay(settings.speed).html(settings.closedSign);
						});							
					}
				}
				selectItem(this, $treeview);
			});		
		}
  }  
  
  $.fn.treeview = function(method) {    
    if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.numbers' );
    }      
  };
})(jQuery);
 
//overwrite core functions
$.vpmsBindViewModel.isMobile = true;
$.vpmsBindViewModel.mobileHeaderTheme = "b";
$.vpmsBindViewModel.mobileContentTheme = "c";
$.mobile.loadingMessageTextVisible = true;
$.mobile.loadingMessageTheme = $.vpmsBindViewModel.mobileContentTheme;

$.vpmsBindViewModel.adjustElementsWidth = function(){
};
    
$.vpmsBindViewModel.blockUI = function(model, text){
	$.mobile.loadingMessage = text;
	if(!model.blockUI)
		model.blockUI = ko.observable(true);
	else
		model.blockUI(true);
	$.mobile.showPageLoadingMsg();
};

$.vpmsBindViewModel.unblockUI = function(model){
	$.mobile.hidePageLoadingMsg();
	model.blockUI(false);
};    

$.vpmsBindViewModel.afterRenderInit = function($this){
	$this.on('change', '.vpms-autocommit', function(){
		$.vpmsBindViewModel.autoCommit(this);
	});
	//iOS Safari bug - native date picker does not call change
	$this.on('blur', 'input[type="date"]', function(){
		$(this).trigger('change');
	});
};

$.vpmsBindViewModel.dialog = function(id, title, content, options){
		var $dialog = $('#' + id);
		if($dialog.length==0){		
			$dialog = $('<div id="' + id + '" data-role="dialog" data-overlay-theme="' + $.vpmsBindViewModel.mobileContentTheme +'" data-theme="' + $.vpmsBindViewModel.mobileHeaderTheme + '" data-header-theme="' + $.vpmsBindViewModel.mobileHeaderTheme + '"><div data-role="header"><h1></h1></div><div data-role="content"></div></div>');
			$('body').append($dialog);
			$dialog.page();
		}
		$dialog.find('div[data-role="header"]>h1').html(title);
        var $content = $dialog.find('div[data-role="content"]');
        if(typeof content == 'string')
            $content.html(content);
        else{
            $content.empty();
            $content.append(content);
            $dialog.page('destroy').page();
        }
		$.mobile.changePage($dialog, 'pop', true, true);
};

$.vpmsBindViewModel.afterRender = function($this, containerId){
//    	$.mobile.initializePage();
	if($this.hasClass($.vpmsBindViewModel.options.containerClass)){
		$this.attr('data-header-theme', $.vpmsBindViewModel.mobileHeaderTheme);
		$this.attr('data-footer-theme', $.vpmsBindViewModel.mobileHeaderTheme);
		$this.attr('data-content-theme', $.vpmsBindViewModel.mobileContentTheme);
		$this.page('destroy').page();
	}else{
		$this.trigger('create');
	};
	//mask
	if($.fn.mask)
		$this.findAndSelf('.vpms-edit-widget input[mask]').each(function(){
			var mask = $(this).attr('mask');
			if(mask)
				$(this).mask(mask);
		})	
};

$.vpmsBindViewModel.options.afterModelUpdate=function(container, settings){
	//pages widget icons
	$(container).find(".vpms-pages-widget:visible").each(function(index){
		var $last = undefined; var $first = undefined; 
		$(this).children(".vpms-pages-tabs-container").find("a:jqmData(role='button')").each(function(index){
			var $this = $(this);
			if($this.is(":visible")){
				var icon = $this.attr('data-icon'); 
				if(icon){
					$this.addClass('ui-btn-icon-left');
					$this.find('.ui-icon').remove();
					$this.find('.ui-btn-inner').append("<span class='ui-icon ui-icon-shadow ui-icon-" + icon +"' >");
				}else{
					$this.removeClass('ui-btn-icon-left');
					$this.find('.ui-icon').remove();    		
				}
				if($first == undefined){
					$first = $this;
					$this.addClass("ui-corner-left");
					$this.children(".ui-btn-inner").addClass("ui-corner-left");
				}else{
					$this.removeClass("ui-corner-left");
					$this.children(".ui-btn-inner").removeClass("ui-corner-left");				
				}
				$this.removeClass("ui-corner-right");
				$this.children(".ui-btn-inner").removeClass("ui-corner-right");				
				$last = $this;
			}
		});
		if($last!=undefined){
			$last.addClass("ui-corner-right");
			$last.children(".ui-btn-inner").addClass("ui-corner-right");							
		}
	});
	//collapsible accordions
	$(container).find(".vpms-accordion-widget:visible").children(".vpms-accordion-container").each(function(index){
		var collapsibles = $(this).children(':jqmData(role="collapsible-set")').children(':jqmData(role="collapsible")');
		collapsibles.jqmRemoveData('collapsible-last');
		collapsibles.last().jqmData('collapsible-last', true);
		collapsibles.children('.ui-collapsible-heading').children('a').removeClass('ui-corner-top ui-corner-bottom').children('.ui-btn-inner').removeClass('ui-corner-top ui-corner-bottom');
		collapsibles.first().children('.ui-collapsible-heading').children('a').addClass('ui-corner-top').children('.ui-btn-inner').addClass('ui-corner-top');
		if(collapsibles.last().hasClass('ui-collapsible-collapsed'))
			collapsibles.last().children('.ui-collapsible-heading').children('a').addClass('ui-corner-bottom').children('.ui-btn-inner').addClass('ui-corner-bottom');
		//content
		collapsibles.children('.ui-collapsible-content').removeClass('ui-corner-bottom').last().addClass('ui-corner-bottom');
	});
	//buttons visible
	$(container).find(".ui-btn-hidden").each(function(index){
		if(this.style.display == "none")
			$(this).closest('.ui-btn').hide();
		else
			$(this).closest('.ui-btn').show();
	});
}            

//overwrite templates 
//element widget
$.vpmsBindViewModel.pushTemplate({
	name: 'element',
	template: '\
			{{if $data.container}}\
			<div data-bind="visible:ko.utils.unwrapObservable($data.relevant)!==false">\
				{{if $data.widget && $.vpmsBindViewModel.templateExists($data.widget)}}\
					<div class="vpms-${widget}-widget {{if $data.settings}}${$data.settings.css}{{/if}}" data-bind="template: {name:widget, data:$data}"> </div>\
				{{/if}}\
			</div>\
			{{else}}\
			<div data-role="fieldcontain" data-bind="visible:ko.utils.unwrapObservable($data.relevant)!==false">\
				{{if $data.hasLabel}}\
					<span class="vpms-label-widget {{if $data.settings}}${$data.settings.css}{{/if}}" data-bind="template: {name:\'label\', data:$data}"></span>\
				{{/if}}\
				{{if $data.widget && $.vpmsBindViewModel.templateExists($data.widget)}}\
					<span class="vpms-${widget}-widget {{if $data.settings}}${$data.settings.css}{{/if}}" data-bind="template: {name:widget, data:$data}"> </span>\
				{{/if}}\
			</div>\
			{{/if}}\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'border',
	template: '\
		<ul data-role="listview" data-inset="true" data-theme="${$.vpmsBindViewModel.mobileContentTheme}" data-divider-theme="${$.vpmsBindViewModel.mobileHeaderTheme}">\
	        <li data-role="list-divider" data-bind="text: caption"></li>\
			{{if $data.customWidget && $.vpmsBindViewModel.templateExists($data.customWidget)}}\
			<li class="vpms-elements-container" data-bind="template: {name:customWidget, data:$data}"></li>\
			{{else}}\
			<li class="vpms-elements-container" data-bind="template: {name:\'element\', foreach:elements}"></li>\
			{{/if}}\
		</ul>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'label',
	template: '\
		<label data-bind="attr:{\'for\':id}">\
			{{if $data.labeled}}<span data-bind="text:caption"></span>{{/if}}\
			<span class="ui-state-required" data-bind="visible:required">&bull;</span>\
		</label>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'value',
	template:  '\
		<label class="ui-controlgroup-label" data-bind="text:caption"></label>\
		<div class="ui-controlgroup-controls" data-bind="attr:{id:id, title:tooltip}, text:displayValue" />\
		<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
	',
	initModel: function(model){
		this.parent.initModel(model);
		model.labeled = false;
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'edit',
	template: '\
		<input class="{{if autocommit}}vpms-autocommit{{/if}}" data-bind="attr:{id:id, name:id, title:tooltip}, typedValue:value, disable:disabled" {{if settings}}settings="${JSON.stringify(settings)}"{{/if}} {{if format}}type="${format}"{{/if}} {{if $data.settings && $data.settings.mask}}mask="${$data.settings.mask}"{{/if}}/>\
		<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
	',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = true;			
		if(!model.name) model.name = "edit";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		if(!model.displayValue)
			model.displayValue = ko.dependentObservable({
			read: function () {
				return $.vpmsBindViewModel.readFormattedValue(this,ko.utils.unwrapObservable(this.value));				
			},
			write: function (value) {
				$.vpmsBindViewModel.writeFormattedValue(this, value);				
			},
			owner: model
		});
	}
});		

//Memo widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'memo',
	template: '\
		<textarea class="{{if autocommit}}vpms-autocommit{{/if}}" data-bind="attr:{id:id, name:id, title:tooltip, rows:rows}, value:value, readonly:disabled" />\
		<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
	'
});	

$.vpmsBindViewModel.pushTemplate({
	name: 'combobox',
	template: '\
		<span data-bind="childOptions:choices, optionsText:\'_x\', optionsValue:\'_v\'"><select class="combobox {{if autocommit}}vpms-autocommit{{/if}}"  data-bind="attr:{id:id, name:id, title:tooltip, size:size},  value:value, disable:disabled"></select></span>\
		<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
	'
});		

$.vpmsBindViewModel.pushTemplate({
	name: 'checkbox',
	template: '\
			<input class="{{if autocommit}}vpms-autocommit{{/if}}" type="checkbox" data-bind="attr:{id:id, name:id}, checked:checked, disable:disabled" />\
			<label data-bind="text:caption, attr:{\'for\':id}"></label>\
			<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
	'
});

//radiogroup widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'radiogroupElement',
	template: '\
		<input class="{{if $item.autocommit}}vpms-autocommit{{/if}}" type="radio" data-bind="value:_v, disable:$item.disabled, attr:{name:$item.id, id:ko.utils.unwrapObservable($item.id)+\'val\'+ko.utils.unwrapObservable(_v)}, checked:$item.value" />\
		<label data-bind="text:_x, attr:{\'for\':ko.utils.unwrapObservable($item.id)+\'val\'+ko.utils.unwrapObservable(_v)}"></label>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'radiogroup',
	template: '\
		<fieldset data-role="controlgroup" data-bind="template: {name:\'radiogroupElement\', foreach:choices, templateOptions: {id:id, value:value, disabled:disabled, autocommit:autocommit} }">\
		<legend data-bind="text:caption">\
		</fieldset>\
		<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
	',
	initModel: function(model){
		this.parent.initModel(model);
		model.labeled = false;
	}
});	

//accordionElement widget  - defaults container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'accordionElement',
	template: '\
		<div class="vpms-accordion-element" data-role="collapsible" data-bind="visible:relevant, attr:{id:id, \'data-collapsed\':ko.utils.arrayIndexOf($item.group.groups, $data)==0?\'false\':\'true\'}">\
			<h3 data-bind="click:function(event){$item.group.selectGroup($item.group, $data);}">\
				<span data-bind="text:caption, css:{\'vpms-accordion-caption-error\':error}"></span>\
				<span class="ui-icon ui-icon-shadow ui-icon-alert" style="left:0px" data-bind="visible:error"></span>\
			</h3>\
			<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
			{{if $.vpmsBindViewModel.templateExists($data.widget) }}<div class="vpms-${$data.widget}-widget" data-bind="template: {name:widget, data:$data}"></div>{{/if}}\
			{{if $item.group.multiple}}\
			<div class="vpms-remove-button" data-bind="visible:!$item.group.disabled() && !$item.group.staticPageCount()">\
				<div data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'delete\', enable:canRemove, caption:removeCaption, click:function(event){jQuery(event.target).vpmsBindViewModel(\'removeFromGroup\', $item.group, $data);} } }"></div>\
			</div>\
			{{/if}}\
		</div>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'accordion',
	template: '<div class="vpms-accordion-container" data-bind="attr:{id:id}">\
		<div data-role="collapsible-set" data-theme="${$.vpmsBindViewModel.mobileHeaderTheme}" data-content-theme="${$.vpmsBindViewModel.mobileContentTheme}" class="vpms-accordion" data-bind="template:{name:\'accordionElement\', foreach:groups, \
		afterAdd:function(elem){\
			if(!$.vpmsBindViewModel.isModelIniting(elem) && $(elem).hasClass(\'vpms-accordion-element\')){\
				$(elem).attr(\'data-collapsed\', \'false\');\
				$.vpmsBindViewModel.afterRender($(elem).closest(\'.vpms-accordion-container\'), $data.containerId);\
			}\
		},\
		beforeRemove:function(elem){\
			$(elem).remove();\
		}, templateOptions: {group:$data}\
		}"></div>\
		{{if multiple}}\
			<div class="vpms-add-button" data-bind="visible:!disabled() && !staticPageCount()">\
				<div data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'add\', caption:addCaption, enable:canAdd, click:function(event){$data.addClick(event, $data);} } }"></div>\
			</div>\
		{{/if}}\
		</div>\
	',
	initModel: function(model){
		this.parent.initModel(model);
		
	}
});		

$.vpmsBindViewModel.pushTemplate({
	name: 'button',
	template: '\
		<button type="button" data-inline="true" data-bind="visibleui:$item.visible?$item.visible:$data.visible?$data.visible:true, enableui:$item.enable?$item.enable:$data.enable?$data.enable:true, attr:{\'id\':$item.id?$item.id:$data.id}, textui:$item.caption?$item.caption:$data.caption?$data.caption:\'NA\', click:function(event){if($item.click) $item.click(event); else if($data.click) $data.click(event);}"></button>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'buttonbar',
	template: '\
		<div class="ui-grid-b">\
			<div class="ui-block-a" style="text-align:left">\
			{{each elements}}\
				{{if $value.pos==\'left\'}}\
					<span data-bind="template: {name:\'button\', data:$value }"></span>\
				{{/if}}\
			{{/each}}\
			</div>\
			<div class="ui-block-b" style="text-align:center">\
			{{each elements}}\
				{{if $value.pos==\'center\'}}\
					<span data-bind="template: {name:\'button\', data:$value }"></span>\
				{{/if}}\
			{{/each}}\
			</div>\
			<div class="ui-block-c" style="text-align:right">\
			{{each elements}}\
				{{if $value.pos==\'right\'}}\
					<span data-bind="template: {name:\'button\', data:$value }"></span>\
				{{/if}}\
			{{/each}}\
			</div>\
		</div>\
'
});


$.vpmsBindViewModel.pushTemplate({
	name: 'pagesTabs',
	template: '\
		<a class="vpms-pages-tab" data-role="button" data-bind="visible:relevant, click: function(event){ $item.model.tabClick(event, $item.model, $data) }, attr: {id: id, \'data-icon\':(ko.utils.unwrapObservable($data.error)!=undefined && ko.utils.unwrapObservable($data.error)!=\'\' && (ko.utils.unwrapObservable($data.visited)==true || $item.model.visitable!=true))?\'alert\':((ko.utils.unwrapObservable($data.error)==undefined || ko.utils.unwrapObservable($data.error)==\'\') && ko.utils.unwrapObservable($data.visited)==true && $item.model.visitable==true)?\'check\':\'\'}, css: { \'ui-btn-active\': $item.model.selectedPage() == id() }">\
			<span data-bind="text:caption"></span>\
		</a>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'pagesSections',
	template: '\
		<div class="vpms-pages-section" data-bind="attr: {id: id}, visible:relevant() && $item.model.selectedPage()==id()">\
			<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
			{{if $.vpmsBindViewModel.templateExists($data.widget) }}\
				<div class="vpms-${widget}-widget"><div data-bind="template: {name:widget, data:$data}"></div></div>\
			{{/if}}\
			{{if $item.model.multiple}}\
				<div class="vpms-remove-button" data-bind="visible:canRemove() && !$item.model.disabled() && !$item.model.staticPageCount()">\
					<div data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'delete\', caption:removeCaption, click:function(event){jQuery(event.target).vpmsBindViewModel(\'removeFromGroup\', $item.model, $data);} } }"></div>\
				</div>\
			{{/if}}\
		</div>\
	'
});	


$.vpmsBindViewModel.pushTemplate({
	name: 'pages',
	template: '\
		<div class="vpms-pages-tabs-container" data-role="controlgroup" data-type="horizontal" data-theme="${$.vpmsBindViewModel.mobileHeaderTheme}" data-content-theme="${$.vpmsBindViewModel.mobileContentTheme}" data-bind="template: {name:\'pagesTabs\', foreach:groups, templateOptions:{model:$data}, \
		afterAdd:function(elem){\
			if(!$.vpmsBindViewModel.isModelIniting(elem) && $(elem).hasClass(\'vpms-pages-tab\')){\
				$.vpmsBindViewModel.afterRender($(elem), $data.containerId);\
			}\
		} }">\
			{{if multiple}}\
				<a class="vpms-pages-tab" data-role="button" data-bind="visible:canAdd() && !disabled() && !staticPageCount(), click:function(event){$data.addClick(event, $data)}">\
					<span class="vpms-pages-add-button" data-bind="text:_addCaption">&#10010;</span>\
				</a>\
			{{/if}}\
		</div>\
		<div class="vpms-pages-container ui-body ui-body-${$.vpmsBindViewModel.mobileContentTheme} ui-corner-bottom ui-corner-right">\
			<div data-bind="template: {name:\'pagesSections\', foreach:groups, templateOptions:{model:$data}, \
				afterAdd:function(elem){\
					if(!$.vpmsBindViewModel.isModelIniting(elem) && $(elem).hasClass(\'vpms-pages-section\')){\
						setTimeout(function(){$.vpmsBindViewModel.afterRender($(elem), $data.containerId);},1);\
					}\
				} }"></div>\
		</div>\
		<div class="vpms-elements-container" data-bind="template: {name:\'element\', foreach:elements}"></div>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name : "product",
	template: '\
		<div data-role="header" data-bind="visible:!blockUI()"><h1 data-bind="text: $.vpmsBindViewModel.element(captions, \'productCaption\').caption"></h1></div>\
		<div data-role="content" data-bind="visible:!blockUI()"><div data-bind="template: {name:\'element\', foreach:elements}"></div></div>\
		<div data-role="footer" data-bind="visible:!blockUI()">\
			<div class="ui-grid-b">\
				<div class="ui-block-a">&nbsp;</div>\
				<div class="ui-block-b" style="text-align:center">Powered by VP/MS</div>\
				<div class="ui-block-c" style="text-align:right"><span data-bind="text: $.vpmsBindViewModel.element(captions, \'text.version\').caption"></span>&nbsp;<span data-bind="text: $.vpmsBindViewModel.element(captions, \'contextVersion\').caption"></span></div>\
			</div>\
		</div>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name : "selectQuote",
	template: '\
		<ul data-role="listview" data-bind="template: {name: \'selectQuoteChoicesWidget\', foreach: $.vpmsBindViewModel.element(elements, ko.utils.unwrapObservable(productKey)).choices}"></ul>\
	',
	initModel: function(model){
		if (this.parent)
			this.parent.initModel(model);	
	}
});		

//table widget  - defaults container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'tableRow',
	template: '\
	<tr class="ui-body-${$.vpmsBindViewModel.mobileContentTheme}" data-bind="attr: {id:id}, visible:relevant">\
	{{each elements}}\
		<td class="ui-body-${$.vpmsBindViewModel.mobileContentTheme} ${$value.format} ${$value.css}" data-bind="text:$value.displayValue"></td>\
	{{/each}}\
	</tr>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'table',
	template: '<table class="vpms-table-widget" data-bind="attr:{id:id}">\
		<colgroup>\
		{{each settings.colSettings}}\
			<col ${$value} />\
		{{/each}}\
		</colgroup>\
		<thead><tr>\
		{{each settings.colNames}}\
			<th class="ui-bar-${$.vpmsBindViewModel.mobileHeaderTheme}" data-bind="text:$value"></th>\
		{{/each}}\
		</tr></thead>\
		<tbody data-bind="template:{name:\'tableRow\', foreach:groups, templateOptions: {group:$data} }"></tbody>\
		</table>\
	'
});		

$.vpmsBindViewModel.pushTemplate({
	name: 'grid',
	template: function(){
		var t = $.vpmsBindViewModel.getTemplate('table');
		if(t){
			if($.isFunction(t.template))
				return t.template();
			else
				return t.template;
		}
		else
			return '<p>grid template is not defined</p>';		
	},
	initModel: function(model){
		$.vpmsBindViewModel.initModel_widget(model, 'table');
	},
	updateModel: function(model, data, settings){
		$.vpmsBindViewModel.updateModel_widget(model, data, settings, 'table');
	}
});	

$.vpmsBindViewModel.pushTemplate({
	name: 'treeview',
	template: function(){
		var t = $.vpmsBindViewModel.getTemplate('pages');
		if(t){
			if($.isFunction(t.template))
				return t.template();
			else
				return t.template;
		}
		else
			return '<p>treeview template is not defined</p>';		
	},
	initModel: function(model){
		$.vpmsBindViewModel.initModel_widget(model, 'pages');
	},
	updateModel: function(model, data, settings){
		$.vpmsBindViewModel.updateModel_widget(model, data, settings, 'pages');
	}
});	

$.vpmsBindViewModel.pushTemplate({
	name: 'elementgroup',
	template: function(){
		var t = $.vpmsBindViewModel.getTemplate('superWidget');
		if(t){
			if($.isFunction(t.template))
				return t.template();
			else
				return t.template;
		}
		else
			return '<p>elementgroup template is not defined</p>';		
	},
	initModel: function(model){
		model.isGroup = false;
		$.vpmsBindViewModel.initModel_widget(model, 'superWidget');
	},
	updateModel: function(model, data, settings){
		$.vpmsBindViewModel.updateModel_widget(model, data, settings, 'superWidget');
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'autocomplete',
	template: function(){
		var t = $.vpmsBindViewModel.getTemplate('combobox');
		if(t){
			if($.isFunction(t.template))
				return t.template();
			else
				return t.template;
		}
		else
			return '<p>autocomplete template is not defined</p>';		
	},
	initModel: function(model){
		$.vpmsBindViewModel.initModel_widget(model, 'combobox');
	},
	updateModel: function(model, data, settings){
		$.vpmsBindViewModel.updateModel_widget(model, data, settings, 'combobox');
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'checkboxAfterLabel',
	template: function(){
		var t = $.vpmsBindViewModel.getTemplate('checkbox');
		if(t){
			if($.isFunction(t.template))
				return t.template();
			else
				return t.template;
		}
		else
			return '<p>checkboxAfterLabel template is not defined</p>';		
	},
	initModel: function(model){
		$.vpmsBindViewModel.initModel_widget(model, 'checkbox');
	},
	updateModel: function(model, data, settings){
		$.vpmsBindViewModel.updateModel_widget(model, data, settings, 'checkbox');
	}
});
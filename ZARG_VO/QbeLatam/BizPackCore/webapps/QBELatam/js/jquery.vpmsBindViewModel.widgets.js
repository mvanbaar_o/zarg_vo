$.vpmsBindViewModel.widgets_previousAdjustElementsWidth = $.vpmsBindViewModel.adjustElementsWidth;
$.vpmsBindViewModel.adjustElementsWidth = function(container){
	if($.vpmsBindViewModel.widgets_previousAdjustElementsWidth)
		$.vpmsBindViewModel.widgets_previousAdjustElementsWidth(container);
	jQuery(container).find('.vpms-tabs-widget.vpms-slide-tabs:visible').each(function(index, value){ 
		$.vpmsBindViewModel.widgets_updateSliderPagesWidget($(this));
	});
};

//element widget
$.vpmsBindViewModel.pushTemplate({
	name: 'element',
	template: '\
			<div class="{{if $data.container}}vpms-container{{else}}vpms-element{{/if}} {{if $data.settings && $data.settings.span}}vpms-span${$data.settings.span}{{/if}}" data-bind="visible:ko.utils.unwrapObservable($data.relevant)!==false">\
				{{if $data.hasLabel}}\
					<div class="ui-widget vpms-label-widget{{if !$data.labeled}} vpms-notlabeled{{/if}}{{if $data.settings}} ${$data.settings.css}{{/if}}{{if $data.settings && $data.settings.labelAlign}} vpms-align-${$data.settings.labelAlign}{{/if}}" data-bind="template: {name:\'label\', data:$data}"></div>\
				{{/if}}\
				{{if $data.widget && $.vpmsBindViewModel.templateExists($data.widget)}}\
					<div class="ui-widget vpms-${widget}-widget ui-corner-all {{if !$data.labeled}}vpms-notlabeled{{/if}} {{if $data.settings}}${$data.settings.css}{{/if}} {{if $data.settings && $data.settings.align}}vpms-align-${$data.settings.align}{{/if}}" data-bind="template: {name:widget, data:$data}"> </div>\
				{{/if}}\
			</div>\
			'
});

//button widget - defaults: isElement=false
//options: id, caption, enable, visible click (function)
$.vpmsBindViewModel.pushTemplate({
	name: 'button',
	template: '<div class="vpms-button-widget" data-bind="visibleui:$item.visible?$item.visible:$data.visible?$data.visible:true">\
				<button type="button" data-bind="enableui:$item.enable?$item.enable:$data.enable?$data.enable:true, attr:{\'id\':$item.id?$item.id:$data.id, tooltip:$item.tooltip?$item.tooltip:$data.tooltip}, textui:$item.caption?$item.caption:$data.caption?$data.caption:\'NA\', click:function(event){if($item.click) $item.click(event); else if($data.click) $data.click(event);}"></button>\
			</div>',
	initModel: function(model){
		model.hasLabel = false;
		if(model.isElement===undefined)
			model.isElement = true;
		if(model.setValue===undefined)
			model.setValue="1";

		if(!model.pos) model.pos = "left";
		if(!model.name) model.name = "btn." + model.role;
		if(!model.role){
			if($.vpmsBindViewModel.roleBindings[model.name])
				model.role = model.name;
			else
				model.role = 'default';
		}
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "btn." + model.role);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		//caption
		if(!model.isElement){
			//if not element - get button's caption from resources
			model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
			var caption = $.vpmsBindViewModel.element(model.captions, model.id(), false);
			if(caption===false){
				caption  = $.vpmsBindViewModel.initModel_caption({name:model.id(), caption:model.caption});
				model.captions.push(caption);
			}
			model.caption = caption.caption;
		}else
			model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);

		//visible
		if(!model.visible){
			if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role].visible){
				model.visible = function(){
					return $.vpmsBindViewModel.roleBindings[model.role].visible(model); 
				};
			}else
			if(model.isElement)
				model.visible = function(){
					return model.relevant();
				};
		}
		model.visible = $.vpmsBindViewModel.initModelItem(model.visible, true);
		
		//enable
		if(!model.enable){
			if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role].enable){
				model.enable = function(){
					return $.vpmsBindViewModel.roleBindings[model.role].enable(model); 
				};
			}else
			if(model.isElement){
				model.enable = function(){
					return !model.disabled();
				};
			}else
				model.enable = $.vpmsBindViewModel.initModelItem(model.enable, true);
		}
		//click
		if(model.click)
			model._click = model.click;
		model.click = function(event){
			//set value
			model.value(model.setValue);
			//call user defined function if set
			if(model._click)
				model._click(event);
			else
				if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role].click){
					var $container = $(event.target).closest("." + jQuery.vpmsBindViewModel.options.containerClass);
					var rootmodel = $container.data('model');
					if(!rootmodel.actions)
						rootmodel.actions = {};
					var action = {};
					//action to be filled with parameters
					rootmodel.actions[model.role] = action;
					if(!rootmodel.actionsModels)
						rootmodel.actionsModels = {};
					//action button to pass in updater
					rootmodel.actionsModels[model.role] = model;
					$.vpmsBindViewModel.roleBindings[model.role].click(event.target, model, rootmodel, action);
				}
		};
		if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role].initModel){
			$.vpmsBindViewModel.roleBindings[model.role].initModel(model);
		}
	},
	updateModel: function(model, data){	
		if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role].updateModel){
			$.vpmsBindViewModel.roleBindings[model.role].updateModel(model, data);
		}
	}	
});

//enable, visible, click, update
jQuery.vpmsBindViewModel.roleBindings.next = {
		'visible' : function(model){
			return jQuery.vpmsBindViewModel.roleBindings.next.enable(model) || jQuery.vpmsBindViewModel.roleBindings.previous.enable(model);
		},
		'enable' : function(model){
			var widget = $.vpmsBindViewModel.getParentHasItem(model, "nextPage");
			if(widget)
				return (widget.nextPage()!==undefined);
			else
				return false;
		},
		'click' : function(element, model, rootmodel, action){
			var widget = $.vpmsBindViewModel.getParentHasItem(model, "nextPage");
			if(widget){
				var id = widget.nextPage();
				widget.nextPage(id);
			}			
		}
};

jQuery.vpmsBindViewModel.roleBindings.previous = {
		'visible' : function(model){
			return jQuery.vpmsBindViewModel.roleBindings.next.enable(model) || jQuery.vpmsBindViewModel.roleBindings.previous.enable(model);
		},
		'enable' : function(model){
			var widget = $.vpmsBindViewModel.getParentHasItem(model, "prevPage");
			if(widget)
				return (widget.prevPage()!==undefined);
			else
				return false;			
		},
		'click' : function(element, model, rootmodel, action){
			var widget = $.vpmsBindViewModel.getParentHasItem(model, "prevPage");
			if(widget){
				var id = widget.prevPage();
				widget.prevPage(id);
			}			
		}
};

jQuery.vpmsBindViewModel.roleBindings.save = {
		'enable' : function(model){
			return !$.vpmsBindViewModel.unwrapObservable(model,'disabled');
		},
		'click' : function(element, model, rootmodel, action){
			jQuery(element).vpmsBindViewModel("updateModel");
		}
};

jQuery.vpmsBindViewModel.roleBindings.cancel = {
		'enable' : function(model){
			return !$.vpmsBindViewModel.unwrapObservable(model,'disabled');
		},
		'click' : function(element, model, rootmodel, action){
			jQuery(element).vpmsBindViewModel("changeModel");
		}
};

jQuery.vpmsBindViewModel.roleBindings.pdf = {
		'initModel' : function(model){
			if(!model.settings) model.settings={};
			model.settings.ignoreDisabled = true;
		},
		'enable' : function(model){
			return !$.vpmsBindViewModel.unwrapObservable(model,'disabled');
		},		
		'click' : function(element, model, rootmodel, action){
			jQuery(element).vpmsBindViewModel("updateModel");
		},
		'update' : function(model, rootmodel, action){
			//if button still enabled
			var container = jQuery('#' + rootmodel.containerId); 
			if(model){
				if(($.isFunction(model.enable) && model.enable()===true) || model.enable===true)
					$.vpmsBindViewModel.openPdf(container);
			}else{
				//actually should not happen : if pdf button  is not set open pdf anyway?
			}
		}
};

jQuery.vpmsBindViewModel.roleBindings['default'] = {
		'enable' : function(model){
			return !$.vpmsBindViewModel.unwrapObservable(model,'disabled');	
		},		
		'click' : function(element, model, rootmodel, action){
			jQuery.vpmsBindViewModel.updateVPMSModel(jQuery(element).closest("." + $.vpmsBindViewModel.options.containerClass), true);
		}
};

//text widget - defaults: isElement=false
$.vpmsBindViewModel.pushTemplate({
	name: 'text',
	template: '<div class="vpms-text-widget-text" data-bind="attr: {id: id}, text:displayValue"></div>',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = false;	
		if(!model.name) model.name = "text";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.text = $.vpmsBindViewModel.initModelItem(model.text, "");
		model.displayValue = $.vpmsBindViewModel.initModelItem($.vpmsBindViewModel.unwrapObservable(model, 'text'), "");
	},
	updateModel: function(model, data){
		if(model.isElement)
			$.vpmsBindViewModel.updateModelItem(model, 'displayValue', $.vpmsBindViewModel.unwrapObservable(model, 'caption'));
	}
});

//image widget - defaults: isElement=false
$.vpmsBindViewModel.pushTemplate({
	name: 'image',
	template: '<img class="vpms-image-widget-image" data-bind="attr: {\'src\':src, tooltip:tooltip, id:id}" {{if $data.width}}width="${$data.width}"{{/if}} {{if $data.height}}height="${$data.height}"{{/if}} />',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = false;
		if(!model.name) model.name = "image";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, true);
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model._src = model.src;
		model.src = $.vpmsBindViewModel.initModelItem("", "");
	},
	updateModel: function(model, data, settings){
		var src = model._src;
		if(model.isElement){
			src = $.vpmsBindViewModel.unwrapObservable(model, 'value');			
		}
		$.vpmsBindViewModel.updateModelItem(model, 'src', (settings.resurl?(settings.resurl + "/"):"") + src);
	}
});

//Border widget - defaults: isGroup=true, composite=false, container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'border',
	template: '<fieldset class="vpms-border-widget-fieldset ui-widget-content ui-corner-all" data-bind="attr: {id: id}">\
				<legend class="vpms-border-widget-caption ui-widget-header">\
					<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: required"><span class="ui-icon ui-icon-bullet"></span></div>\
					<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error {{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
					<span data-bind="text: caption, attr:{tooltip:tooltip}"></span>\
				</legend>\
				{{if $data.customWidget && $.vpmsBindViewModel.templateExists($data.customWidget)}}\
					<div class="vpms-elements-container vpms-${$data.settings.colNumber}-col" data-bind="template: {name:customWidget, data:$data}"></div>\
				{{else}}\
					<div class="vpms-elements-container vpms-${$data.settings.colNumber}-col" data-bind="template: {name:\'element\', foreach:elements}"></div>\
				{{/if}}\
				</fieldset>',
	initModel: function(model){
		model.hasLabel = false;
		if(model.isGroup===undefined)
			model.isGroup = true;
		if(!model.isGroup && model.isElement===undefined)
			model.isElement = true;
		if(model.container===undefined)
			model.container = true;
		if(!model.settings)
			model.settings = {};
		if(model.settings.colNumber===undefined)
			model.settings.colNumber = 'no';
		if(!model.name) model.name = "border";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
	}
});

//Label widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'label',
	template: '<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: required"><span class="ui-icon ui-icon-bullet"></span></div>\
				<label class="vpms-label-widget-label" data-bind="attr:{\'for\':id}, text:caption"></label>',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(!model.name) model.name = "label";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
	}	
});

//value widget - defaults: isElement=true	
$.vpmsBindViewModel.pushTemplate({
	name: 'value',
	template:  '\
		<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error)}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<div class="vpms-value-widget-value ${format}" data-bind="attr:{id:id, tooltip:tooltip}, text:displayValue"></div>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(model.disabled===undefined)
			model._disabled = true;
		if(!model.name) model.name = "value";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.displayValue = $.vpmsBindViewModel.initModelItem($.vpmsBindViewModel.readFormattedValue(model,ko.utils.unwrapObservable(model.value)), ""); 
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, true);
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
	},
	updateModel: function(model, data){
		model.displayValue($.vpmsBindViewModel.readFormattedValue(model,ko.utils.unwrapObservable(model.value))); 
	}
});

//Edit widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'edit',
	template: '\
		<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error)}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<input class="${format} {{if autocommit}}vpms-autocommit{{/if}}" data-bind="event:{change:onChange}, attr:{id:id, name:id, tooltip:tooltip}, typedValue:value, disable:disabled" {{if settings}}settings="${JSON.stringify(settings)}"{{/if}} {{if $data.settings && $data.settings.mask}}mask="${$data.settings.mask}"{{/if}} {{if $data.settings && $data.settings.maxlength}}maxlength="${$data.settings.maxlength}"{{/if}}/>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(!model.name) model.name = "edit";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		if(model.format && model.format.indexOf("date")>=0)
			model.value = $.vpmsBindViewModel.initModelItem(model.value, null);
		else
			model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		if(model.generateDisplayValue)
			model.displayValue = $.vpmsBindViewModel.initModelItem($.vpmsBindViewModel.readFormattedValue(model,ko.utils.unwrapObservable(model.value)), "");
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(model, e);
			return true;
		};
	},
	updateModel: function(model, data){
		if(model.generateDisplayValue)
			model.displayValue($.vpmsBindViewModel.readFormattedValue(model,ko.utils.unwrapObservable(model.value))); 
	}
});
$.vpmsBindViewModel.pushTemplate({
	name: 'file',
	template: '\
		<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error)}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<label class="vpms-file-widget-name" data-bind="text:value, visible:!settings.upload"></label>\
			<form method="post" enctype="multipart/form-data">\
				<input name="fileContent" type="file" data-bind="event:{change:onChange}, disable:disabled" {{if !settings.upload}}style="display:none"{{/if}}/><input name="quoteId" type="hidden" value=""/><input name="fileNodeId" type="hidden" data-bind="value:settings.fileId"/><input name="fileInstanceId" type="hidden" data-bind="value:id"/>\
			</form>\
			<div class="vpms-file-button" data-bind="visible:settings.download"><span data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'download\', enable:downloadEnabled, caption:settings.downloadCaption, click:downloadFile } }"></span></div>\
			<div class="vpms-file-button" data-bind="visible:settings.upload"><span data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'upload\', enable:uploadEnabled, caption:settings.uploadCaption, click:uploadFile } }"></span></div>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(!model.name) model.name = "file";
		if(!model.settings) model.settings = {};
		if(model.settings.upload === undefined)
			model.settings.upload = true;
		if(model.settings.download === undefined)
			model.settings.download = true;
		if(model.settings.downloadCaption === undefined)
			model.settings.downloadCaption = "Download";
		if(model.settings.uploadCaption === undefined)
			model.settings.uploadCaption = "Upload";
		if(model.settings.fileId === undefined)
			model.settings.fileId = model.name;		
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.uploadEnabled = $.vpmsBindViewModel.initModelItem(undefined, false);
		model.downloadEnabled = $.vpmsBindViewModel.initModelItem(undefined, false);
		model.onChange = function(e){
			model.uploadEnabled(jQuery(e.target).val()!=="");
			return true;
		};
		model.afterUpload = function(form, message){
			var $container = $(form).closest("." + $.vpmsBindViewModel.options.containerClass);
			if(message!==""){
				$.vpmsBindViewModel.dialog($container.attr('id') + '_error_dialog', "Uploading error", message, {width:$container.width()-100});
			}else 
				$.vpmsBindViewModel.updateVPMSModel($(form).closest("." + $.vpmsBindViewModel.options.containerClass), true);
		};
		model.uploadFile = function(event){
			$.vpmsBindViewModel.uploadFile(jQuery(event.target).closest('.vpms-file-widget').find('form'), model);
		};
		model.downloadFile = function(event){
			var $container = $(event.target).closest("." + $.vpmsBindViewModel.options.containerClass);
			$.vpmsBindViewModel.downloadFile($container, model);
		};
	},
	updateModel: function(model, data){
		model.downloadEnabled(model.value()!=="" && model.value()!==undefined);
	}
});
//Memo widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'memo',
	template: '\
		<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error)}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<textarea class="{{if autocommit}}vpms-autocommit{{/if}}" data-bind="event:{change:onChange}, attr:{id:id, name:id, tooltip:tooltip, rows:rows}, value:value, disable:disabled" {{if $data.settings && $data.settings.maxlength}}maxlength="${$data.settings.maxlength}"{{/if}}/>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(model.rows===undefined)
			model.rows = 2;
		if(!model.name) model.name = "memo";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(model, e);
			return true;
		};
	}
});

//combobox widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'combobox',
	template: '\
		<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error), \'vpms-height-auto\':$data.size && ko.utils.unwrapObservable($data.size)>1 }">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<span data-bind="childOptions:choices, optionsText:\'_x\', optionsValue:\'_v\'"><select class="{{if autocommit}}vpms-autocommit{{/if}}"  data-bind="event:{change:onChange}, attr:{id:id, name:id, tooltip:tooltip, size:size},  value:value, disable:disabled"></select></span>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(model.size===undefined)
			model.size = 1;	
		if(!model.name) model.name = "combobox";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(model, e);
			return true;
		};
	},
	updateModel: function(model, data){
		//IE8 disabled Listbox fix: selected item not highlighted  
		if(model.size>1){
			$('#' + model.containerId).find('#' + ko.utils.unwrapObservable(model.id)).each(function(){
				$(this).children('option').removeClass("vpms-combobox-widget-selected-option");
				if($(this).is(":disabled"))
					$(this).children('option:selected').addClass("vpms-combobox-widget-selected-option");
			});
		}
	}
});
//combobox widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'autocomplete',
	template: '\
		<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error) }">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<input class="{{if autocommit}}vpms-autocommit{{/if}}" data-bind="autocomplete:choices, attr:{id:id, name:id, tooltip:tooltip}, value:displayValue, disable:disabled" {{if settings}}settings="${JSON.stringify(settings)}"{{/if}}/>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;	
		if(!model.name) model.name = "autocomplete";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		if(!model.displayValue)
			model.displayValue = ko.dependentObservable({
			read: function () {
				if(this._value!=ko.utils.unwrapObservable(this.value) || this._hash!=this.hash){
					var value = ko.utils.unwrapObservable(this.value);
					var choices = ko.utils.unwrapObservable(this.choices);
					this._displayValue = "";
					if(choices)
						for(var i=0; i<choices.length; i++){
							if(value==choices[i]._v){
								this._displayValue = choices[i]._x;
								break;
							}
						}
					this._hash=this.hash;
					this._value=ko.utils.unwrapObservable(this.value);
				}
				return this._displayValue;
			},
			write: function (value) {
				//
			},
			owner: model
		});	
		model.onChange = function(element){
			var text = $(element).val();
			var choices = ko.utils.unwrapObservable(this.choices);
			if(choices && choices.length>0){
				for(var i=0; i<choices.length; i++){
					if(text==choices[i]._x){
						this.value(choices[i]._v);
						$(element).trigger('change');
						return;
					}
				}
				this.value(choices[0]._v);
				$(element).val(choices[0]._x);
			}else{
				this.value("");
				$(element).val("");
			}
			$(element).trigger('change');
		};
	}
});

ko.bindingHandlers.autocomplete = {
		init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
			if(jQuery.fn.autocomplete){
				var $element = jQuery(element);
				$element.data('viewModel', viewModel);
				var choices =  ko.utils.unwrapObservable(valueAccessor());
				var source = $.map(choices, function(item){return {label:item._x, val:item._v}; });
				$element.autocomplete({
					source:source,
					minLength: 0,
					select: function( event, ui ) {
						var model = $(this).data('viewModel');
						model.value(ui.item.val);
						$(this).blur(); //to prevent call change handler;
					},
					change : function( event, ui){
						var model = $(this).data('viewModel');
						if(!ui.item)
							model.onChange(this);
					}
				});
				var $button = jQuery('<a class="vpms-autocomplete-button" tabIndex="-1"></a>');
				$button.insertAfter($element);
				$button.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				});
				$button.click(function(){
					var $input = jQuery(this).closest('.vpms-autocomplete-widget').find('input'); 
					$input.autocomplete( "search", "" );
					$input.focus();
				});
			}			
		},
		update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
			if(jQuery.fn.autocomplete){
				var choices =  ko.utils.unwrapObservable(valueAccessor());
				var source = $.map(choices, function(item){return {label:item._x, val:item._v}; });
				jQuery(element).autocomplete("option", "source", source);
			}
		}
};


//checkbox widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'checkbox',
	template: '\
		<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error)}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<input class="{{if autocommit}}vpms-autocommit{{/if}}" type="checkbox" data-bind="event:{click:onChange},attr:{id:id, name:id, tooltip:tooltip}, checked:checked, disable:disabled" />\
			<label data-bind="text:caption, attr:{\'for\':id}"></label>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		model.format = "boolean";
		model.labeled = false; //label will be built in widget itself
		if(!model.name) model.name = "checkbox";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "0");
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		if($.vpmsBindViewModel.element(model.captions, 'text.yes', false)===false)
			model.captions.push($.vpmsBindViewModel.initModel_caption({name:'text.yes', caption:'yes'}));
		if($.vpmsBindViewModel.element(model.captions, 'text.no', false)===false)
			model.captions.push($.vpmsBindViewModel.initModel_caption({name:'text.no', caption:'no'}));

		model.checked = ko.dependentObservable({
			read: function () {
				return (ko.utils.unwrapObservable(this.value)=="1"); 
			},
			write: function (value) {
				if(value)
					this.value("1");
				else
					this.value("0");
			},
			owner: model
		});
		if(model.generateDisplayValue){
			model.readDisplayValue = function(){
				if(this.checked())
					return ko.utils.unwrapObservable($.vpmsBindViewModel.element(this.captions, 'text.yes').caption);
				else
					return ko.utils.unwrapObservable($.vpmsBindViewModel.element(this.captions, 'text.no').caption);
			};
			model.displayValue = $.vpmsBindViewModel.initModelItem(model.readDisplayValue(), "");
		}
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(model, e);	
			return true;
		};
	},
	updateModel: function(model, data){
		if(model.generateDisplayValue)
			model.displayValue(model.readDisplayValue()); 
	}
});
//checkbox widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'checkboxAfterLabel',
	template: '\
	<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error)}">\
		<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
		<input class="{{if autocommit}}vpms-autocommit{{/if}}" type="checkbox" data-bind="event:{click:onChange}, attr:{id:id, name:id, tooltip:tooltip}, checked:checked, disable:disabled" />\
	</div>\
	',	
	initModel: function(model){
		var labeled = model.labeled;
		$.vpmsBindViewModel.initModel_widget(model, 'checkbox');
		model.labeled = labeled;
	}
});


//radiogroup widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'radiogroupElement',
	template: '\
	<div class="vpms-radiogroup-item-widget">\
	<input class="{{if $item.autocommit}}vpms-autocommit{{/if}}" type="radio" data-bind="event:{click:$item.onChange},value:_v, disable:$item.disabled, attr:{tooltip:$item.tooltip, name:$item.id, id:ko.utils.unwrapObservable($item.id)+\'val\'+ko.utils.unwrapObservable(_v)}, checked:$item.value" /><label data-bind="text:_x, attr:{\'for\':ko.utils.unwrapObservable($item.id)+\'val\'+ko.utils.unwrapObservable(_v)}"></label>\
	</div>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'radiogroup',
	template: '\
		<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error)}, attr: {id: id}">\
		<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
		<div class="vmps-radiogroup-items vpms-${$data.settings.colNumber}-col" data-bind="template: {name:\'radiogroupElement\', foreach:choices, templateOptions: {id:id, tooltip:tooltip, value:value, disabled:disabled, autocommit:autocommit, onChange:onChange} }"></div>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(!model.name) model.name = "radiogroup";
		if(!model.settings)
			model.settings = {};
		if(model.settings.colNumber===undefined)
			model.settings.colNumber = 'no';
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.autocommit = $.vpmsBindViewModel.initModelItem(model.autocommit, false);
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(model, e);
			return true;
		};
	}	
});

$.vpmsBindViewModel.pushTemplate({
	name: 'elementgroup',
	template: '\
		<div data-bind="css: {\'vpms-widget_with_error_container\':ko.utils.unwrapObservable(error)}, attr: {id: id}">\
		<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
		<div class="vpms-elementgroup-elements" data-bind="template: {name:\'superWidget\', data:$data }"></div></div>\
	',
	initModel: function(model){
		if(!model.labeled)
			model.hasLabel = false;
		if(model.isGroup===undefined)
			model.isGroup = true;
		if(!model.name) model.name = "elementgroup";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);
	}
});

//accordionElement widget  - defaults container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'accordionElement',
	template: '\
	{{if !$item.group.settings.nocaption}}\
	<div class="vpms-accordion-element-header{{if $item.group.settings.noaccordion}} ui-state-active ui-accordion-header{{/if}}" data-bind="attr:{id:id, relevant:relevant()+\'\'}, click:function(event){$item.group.selectGroup($item.group, $data);}"><div style="display:block;">\
			<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: required"><span class="ui-icon ui-icon-bullet"></span></div>\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error {{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<div class="vpms-accordion-caption" data-bind="text:caption, attr:{tooltip:tooltip}"></div>\
			<div style="clear:both"></div>\
		</div>\
	</div>\
	{{/if}}\
	<div class="vpms-accordion-element {{if !$item.group.settings.nocaption}}ui-widget-content ui-accordion-content{{/if}}" data-bind="attr: {id:id, relevant:relevant()+\'\'}">\
		{{if $.vpmsBindViewModel.templateExists($data.widget) }}<div class="vpms-${$data.widget}-widget" data-bind="template: {name:widget, data:$data}"></div>{{/if}}\
		<div style="clear:both"></div>\
		{{if $item.group.multiple}}\
		<div class="vpms-remove-button" data-bind="visible:!$item.group.disabled() && !$item.group.staticPageCount()">\
			<div data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'delete\', enable:canRemove, caption:removeCaption, click:function(event){jQuery(event.target).vpmsBindViewModel(\'removeFromGroup\', $item.group, $data);} } }"></div>\
		</div>\
		{{/if}}\
		<div style="clear:both"></div>\
	</div>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'accordion',
	template: '<div class="vpms-accordion-container" data-bind="attr:{id:id, selectedId:selectedId}, visible:relevant">\
		<div class="{{if !$data.settings.noaccordion}}vpms-accordion{{else}}ui-accordion{{/if}}" data-bind="template:{name:\'accordionElement\', foreach:groups, afterAdd:afterAdd, beforeRemove:beforeRemove, templateOptions: {group:$data} }"></div>\
		{{if multiple}}\
			<div class="vpms-add-button" data-bind="visible:!disabled() && !staticPageCount()">\
				<div data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'add\', caption:addCaption, enable:canAdd, click:function(event){$data.addClick(event, $data);} } }"></div>\
			</div>\
		{{/if}}\
		</div>\
	',
	initModel: function(model){
		if(!model.name) model.name = "accordion";
		//accordion has no selected Page - get and update all groups
		model.getAllGroupsData = true;
		model.setAllGroupsData = true;
		if(model.onlyRelevant === undefined && model.getAllGroupsData)
			model.onlyRelevant = true;		
		if(model.container===undefined)
			model.container = true;	
		if(!model.settings) model.settings={};
		if(model.settings.noaccordion===undefined)
			model.settings.noaccordion = false;
		if(model.settings.nocaption===undefined || !model.settings.noaccordion)
			model.settings.nocaption = false;
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.selectedId = $.vpmsBindViewModel.initModelItem(model.selectedId, "");
		if(!model.afterAdd) model.afterAdd = function(elem, index, data){
			if(!$.vpmsBindViewModel.isModelIniting(elem) && $(elem).hasClass('vpms-accordion-element')){
				var $accordion = $(elem).closest('.vpms-accordion');
				if(!$accordion.hasClass('vmps-recreate-accordion-pending')){
					$accordion.addClass('vmps-recreate-accordion-pending');
					setTimeout(function(){
						var activeIndex = 0; 
						if($accordion.hasClass('ui-accordion')){
							activeIndex = $accordion.accordion('option', 'active');
							$accordion.accordion('destroy');
						}
						var activeId = $accordion.closest('.vpms-accordion-container').attr('selectedId');
						if(activeId){
							var accordionHeaders = $accordion.children(".vpms-accordion-element-header");
							activeIndex =accordionHeaders.index(accordionHeaders.filter('#' + activeId));
						}
						$accordion.accordion($.vpmsBindViewModel.getAccordionOptions(activeIndex));
						$accordion.removeClass('vmps-recreate-accordion-pending');
					}, 1);
				}
				setTimeout(function(){$.vpmsBindViewModel.afterRender($([$(elem).prev()[0], elem]), model.containerId);}, 1);
			}
		};
		if(!model.beforeRemove) model.beforeRemove = function(elem, index, data){
			if($(elem).hasClass('vpms-accordion-element')){
					var $accordion = $(elem).closest('.vpms-accordion');
					setTimeout(function(){
						var activeId = $accordion.closest('.vpms-accordion-container').attr('selectedId');
						if(activeId){
							var accordionHeaders = $accordion.children(".vpms-accordion-element-header");
							$accordion.accordion('option', 'active',accordionHeaders.index(accordionHeaders.filter('#' + activeId)));
						}
					}, 1);
			}
			ko.removeNode(elem);
		};
		if(model.multiple){
			if(!model.addClick) model.addClick = function(event, model){
				if(model.insertAfterSelected){
					var selectedPage;
					var $accordion = jQuery(event.target).closest('.vpms-accordion-widget').children('.vpms-accordion-container').children('.ui-accordion');
					if($accordion.length>0){
						var active = $accordion.accordion( "option", "active" );
						if(active!==undefined){
							$headers = $accordion.children('.vpms-accordion-element-header');
							if($headers.length>active)
								selectedPage = jQuery($headers[active]).attr('id');
						}
					}
					jQuery(event.target).vpmsBindViewModel('addToGroup', model, selectedPage);
				}
				else
					jQuery(event.target).vpmsBindViewModel('addToGroup', model);
			};
		}
		
		if(!model.selectGroup) model.selectGroup =  function(model, group){
			if(model.insertAfterSelected){
				if(ko.utils.unwrapObservable(group.addCaption) && model.addCaption)
					model.addCaption(ko.utils.unwrapObservable(group.addCaption));
			}
		} ;
	},
	updateModel: function(model, data){
		$.vpmsBindViewModel.updateModelItem(model, 'selectedId', data.sp);
	}
});

//superWidget - defaults container=true, isGroup=true, composite=true
$.vpmsBindViewModel.pushTemplate({
	name: 'superWidget',
	template: '{{if $data.customWidget && $.vpmsBindViewModel.templateExists($data.customWidget)}}\
					<div class="vpms-elements-container vpms-${$data.settings.colNumber}-col" data-bind="template: {name:customWidget, data:$data}"></div>\
				{{else}}\
					<div class="vpms-elements-container vpms-${$data.settings.colNumber}-col" data-bind="template: {name:\'element\', foreach:elements}"></div>\
				{{/if}}',
	initModel: function(model){
		if(model.container===undefined)
			model.container = true;
		if(!model.settings)
			model.settings = {};
		if(model.settings.colNumber===undefined)
			model.settings.colNumber = 'no';
		if(!model.name) model.name = "superWidget";
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
	}
});

$.vpmsBindViewModel.widgets_updateSliderPagesWidget = function($tabContainer){
	var updateMargin = function($tabContainer, $ul, selectedTab, marginLeft){
		$tabContainer.data("selectedTab", selectedTab);
		$tabContainer.data("marginLeft", marginLeft);
		$ul.css("margin-left", marginLeft + "px");	
		var navbuttonleft = $tabContainer.find(".vpms-slider-tabs-nav-left");
		if(selectedTab===0)
			navbuttonleft.addClass("ui-state-disabled");
		else
			navbuttonleft.removeClass("ui-state-disabled");
		var navbuttonright = $tabContainer.find(".vpms-slider-tabs-nav-right");
		if(($ul.width() + marginLeft)<($tabContainer.width() - navbuttonright.width() - navbuttonleft.width()))
			navbuttonright.addClass("ui-state-disabled");
		else
			navbuttonright.removeClass("ui-state-disabled");
	};
	var selectedTab = $tabContainer.data("selectedTab");
	if(selectedTab===undefined)
		selectedTab = 0;
	var $ul = $tabContainer.find('ul.vpms-tab-menu');
	var navbuttons = $tabContainer.find(".vpms-slider-tabs-nav");
	var tabsWidth = 0;
	var marginLeft = 0;
	for(var i=0; i<$ul.children().length; i++){
		var w = jQuery($ul.children()[i]).outerWidth(true);
		tabsWidth += w;
		if(selectedTab>i)
			marginLeft +=w;
	}
	tabsWidth += 1; //IE rounding issue
	if($tabContainer.width()<tabsWidth){
		$ul.width(tabsWidth + "px");
		if(navbuttons.length===0){
			$tabContainer.append($('<div class="ui-state-default vpms-slider-tabs-nav vpms-slider-tabs-nav-left">&#9668;</div>'));
			$tabContainer.append($('<div class="ui-state-default vpms-slider-tabs-nav vpms-slider-tabs-nav-right">&#9658;</div>'));
			navbuttons = $tabContainer.find(".vpms-slider-tabs-nav");
			navbuttons.height($tabContainer.height()); 
			navbuttons.css("line-height", $tabContainer.height() + "px");
			navbuttons.click(function(event){
				var $button = $(event.target);
				if($button.hasClass("ui-state-disabled"))
					return;
				var $tabContainer = $(event.target).closest(".vpms-tabs-widget.vpms-slide-tabs");
				var $ul = $tabContainer.find('ul.vpms-tab-menu');
				var selectedTab = $tabContainer.data("selectedTab");
				var marginLeft = $tabContainer.data("marginLeft");
				if(selectedTab===undefined)
					selectedTab = 0;
				if($button.hasClass("vpms-slider-tabs-nav-left")){
					selectedTab--;
					updateMargin($tabContainer, $ul, selectedTab, marginLeft + jQuery($ul.children()[selectedTab]).outerWidth(true));
				}else
				if($button.hasClass("vpms-slider-tabs-nav-right")){
					selectedTab++;
					updateMargin($tabContainer, $ul, selectedTab, marginLeft - jQuery($ul.children()[selectedTab-1]).outerWidth(true));
				}
			});
		}
		updateMargin($tabContainer, $ul, selectedTab, $(navbuttons[0]).width() - marginLeft);
		navbuttons.show();
	}else{
		$ul.width("auto");
		$ul.css("margin-left", 0);
		navbuttons.hide();
	}
};

//pages widget - defaults container=true, tabbed=true
$.vpmsBindViewModel.pushTemplate({
	name: 'pagesTabs',
	template: '\
	<li class="ui-state-default ui-corner-top" data-bind="visible:relevant, attr:{id:id}, css: { \'ui-state-active\': $item.model.selectedPage() == id() }, click: function(event){ $item.model.tabClick(event, $item.model, $data) }">\
		<a class="vpms-tab-link" data-bind="attr: {id: id}">\
			<span class="vpms-tab-link-opened-icon ui-icon ui-icon-carat-1-s"></span>\
			<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: required"><span class="ui-icon ui-icon-bullet"></span></div>\
			<div class="vpms-tab-icon ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<div class="vpms-tab-icon" data-bind="visible: (ko.utils.unwrapObservable($data.error)==undefined || ko.utils.unwrapObservable($data.error)==\'\') && ko.utils.unwrapObservable($data.visited)==true"><span class="ui-icon ui-icon-check"></span></div>\
			<span class="vpms-tab-link-text" data-bind="text:caption, attr:{tooltip:tooltip}"></span>\
			{{if $item.model.multiple}}\
				<span class="vpms-pages-remove-button" data-bind="visible: canRemove() && !$item.model.disabled() && !$item.model.staticPageCount(), attr:{tooltip:removeCaption}">&#10006;</span>\
			{{/if}}\
		</a>\
	</li>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'pagesSections',
	template: '<div class="vpms-pages-section" data-bind="attr: {id: id}, visible:relevant() && $item.model.selectedPage()==id()">{{if $.vpmsBindViewModel.templateExists($data.widget) }}<div class="vpms-${widget}-widget"><div data-bind="template: {name:widget, data:$data}"></div></div>{{/if}}</div>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'pages',
	template: '<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" data-bind="visible:relevant">\
	{{if !settings.suppresstabs}}\
		<div class="vpms-tabs-widget{{if settings.slidetabs}} vpms-slide-tabs{{/if}}" data-bind="attr: {id: id}">\
			<ul class="vpms-tab-menu ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" data-bind="template: {name:\'pagesTabs\', foreach:groups, templateOptions:{model:$data},\
			afterAdd:function(elem){\
				if(!$.vpmsBindViewModel.isModelIniting(elem) && $(elem).filter(\'li\').length>0){\
					setTimeout(function(){$.vpmsBindViewModel.afterRender($(elem), $data.containerId);},1);\
				}\
			} }">\
				{{if multiple}}\
					<li class="ui-state-default ui-corner-top" data-bind="visible:canAdd() && !disabled() && !staticPageCount(), click:function(event){$data.addClick(event, $data)}"><a class="vpms-tab-link">\
					<span class="vpms-pages-add-button" data-bind="attr:{tooltip:_addCaption}">&#10010;</span>\
					</a></li>\
				{{/if}}\
			</ul>\
		</div>\
	{{/if}}\
	<div class="vpms-pages-container" data-bind="visible:relevant"><div class="vpms-clear-float"></div>\
		<div data-bind="template: {name:\'pagesSections\', foreach:groups, templateOptions:{model:$data}, \
			afterAdd:function(elem){\
				if(!$.vpmsBindViewModel.isModelIniting(elem) && $(elem).hasClass(\'vpms-pages-section\')){\
					setTimeout(function(){$.vpmsBindViewModel.afterRender($(elem), $data.containerId);},1);\
				}\
			} }"></div></div>\
	<div class="vpms-clear-float"></div></div>\
	<div class="vpms-elements-container" data-bind="template: {name:\'element\', foreach:elements}"></div>\
',
	initModel :function(model){
		if(model.container===undefined)
			model.container = true;
		if(model.getAllGroupsData)
			 model.syncUpdate = false;
		if(!model.settings) model.settings={};
		if(model.settings.slidetabs===undefined)	model.settings.slidetabs = false;
		
		if(model.onlyRelevant === undefined && model.getAllGroupsData)
			model.onlyRelevant = true;		
		if(!model.name) model.name = "pages";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);

		if(!model.selectedPageInt) model.selectedPageInt = ko.observable();
		
		if(!model.selectedPage) model.selectedPage = ko.dependentObservable({
			read: function () {
				return this.selectedPageInt();
			},
			write: function (value) {
				if(this.selectedPageInt()!=value){
					if($.vpmsBindViewModel.isModelUpdating('#' + this.containerId)){
						this.selectGroupUpdateRequest = value;
						return;
					}
					if(!model.syncUpdate){
						this.selectedPageInt(value);
						$.vpmsBindViewModel.pushState(this.containerId, ko.utils.unwrapObservable(this.id), value, this.getAllGroupsData);
					}else{
						//findout group
						var group;
						var groupsArray = ko.utils.unwrapObservable(model.groups);
						for(var i=0; i<groupsArray.length; i++)
							if(ko.utils.unwrapObservable(groupsArray[i].id)==value){
								group = groupsArray[i];
								break;
							}
						//check if there are elements or groups to restore, restore them else act as async update
						if(group && (group._elements || group._groups)){
							var elementsHasMutated=false, groupsHasMutated=false;
							if(group._elements){
								ko.utils.arrayPushAll(group.elements(), group._elements);
								group._elements = undefined;
								elementsHasMutated = true;
							}
							if(group._groups){
								ko.utils.arrayPushAll(group.groups(), group._groups);
								group._groups = undefined;
								groupsHasMutated = true;
							}
							var rootModel = $('#' + this.containerId).data('model');
							$.vpmsBindViewModel.blockUI(rootModel, "", true);
							rootModel.applyingBinding = true;
							setTimeout(function(){
								try{
									$.vpmsBindViewModel.debug("syncUpdate - start bind cached elements");
									if(elementsHasMutated)
										group.elements.valueHasMutated();
									if(groupsHasMutated)
										group.groups.valueHasMutated();
									$('#' + rootModel.containerId).find(".vpms-pages-section").filter('#' + value).addClass('vpms-afterRender');
									$.vpmsBindViewModel.debug("syncUpdate - end bind cached elements");
									rootModel.applyingBinding = false;
								}catch(e){
									rootModel.applyingBinding = false;
									rootModel.applyingBindingError = "Error applying binding: " + e.message;
									throw rootModel.applyingBindingError;
								}
							}, 1);
							this.requestedPage = value;
							jQuery.vpmsBindViewModel.updateVPMSModel($('#' + this.containerId), true);
						}else{
							this.selectedPageInt(value);
							$.vpmsBindViewModel.pushState(this.containerId, ko.utils.unwrapObservable(this.id), value, this.getAllGroupsData);
						}
					}
				}
			},
			owner: model
		});
		if(!model.tabClick) model.tabClick = function(event, model, group){
			if($(event.target).hasClass('vpms-pages-remove-button')){
				jQuery(event.target).vpmsBindViewModel('removeFromGroup', model, group);
			}else{
				var targetId = jQuery.vpmsBindViewModel.unwrapObservable(group, 'id');
				if(model.selectedPage()!=targetId){
					model.selectedPage(targetId);
				}else{
					if(!model.getAllGroupsData)
						jQuery.vpmsBindViewModel.updateVPMSModel($('#' + this.containerId), true);
				}
			}
		};
		
		if(!model.nextPage) model.nextPage = function(pageId){
			if(pageId){
				this.selectedPage(pageId);
				return;
			}						
			if(!this.groups)
				return;
			var groupsArray = jQuery.vpmsBindViewModel.unwrapObservable(this, "groups");
			var found = false;
			for(var i=0; i<groupsArray.length; i++){
				var _id = jQuery.vpmsBindViewModel.unwrapObservable(groupsArray[i], "id");
				if(!found){
					//find current
					found = (_id==jQuery.vpmsBindViewModel.unwrapObservable(this, "selectedPage"));
				}else{
					//next relevant
					if(groupsArray[i].relevant===undefined || jQuery.vpmsBindViewModel.unwrapObservable(groupsArray[i],"relevant"))
						return _id;
				}
			}
		};
		
		if(!model.prevPage) model.prevPage = function(pageId){
			if(pageId){
				this.selectedPage(pageId);
				return;
			}						
			if(!this.groups)
				return;
			var groupsArray = jQuery.vpmsBindViewModel.unwrapObservable(this, "groups");
			var found = false;
			for(var i=groupsArray.length-1; i>=0; i--){
				var _id = jQuery.vpmsBindViewModel.unwrapObservable(groupsArray[i], "id");
				if(!found)
					//find current
					found = (_id==jQuery.vpmsBindViewModel.unwrapObservable(this, "selectedPage"));
				else
					//prev relevant
					if(groupsArray[i].relevant===undefined || jQuery.vpmsBindViewModel.unwrapObservable(groupsArray[i], "relevant"))
						return _id;
			}
		};
		
		if(!model.selectedGroup) model.selectedGroup = ko.dependentObservable(function () {
			var groupsArray = ko.utils.unwrapObservable(this.groups);
			for(var i=0; i<groupsArray.length; i++){
				if(ko.utils.unwrapObservable(groupsArray[i].id) == this.selectedPage()){
					return groupsArray[i];
				}
			}
		}, model);
			
		if(model.multiple){
			if(!model.addClick) model.addClick = function(event, model){
				if(model.insertAfterSelected){
					var selectedGroup = model.selectedGroup();
					if(!selectedGroup.canAdd || selectedGroup.canAdd())
						jQuery(event.target).vpmsBindViewModel('addToGroup', model, model.selectedPage());
				}
				else
					jQuery(event.target).vpmsBindViewModel('addToGroup', model);
			};
			
			model._addCaption = ko.dependentObservable(function () {
					var activeGroup = this.selectedGroup();
					if(this.insertAfterSelected && activeGroup)
						return ko.utils.unwrapObservable(activeGroup.addCaption);
					else
						return ko.utils.unwrapObservable(this.addCaption);
				}, model);
			if(model.syncUpdate){
				model.beforeGroupAdded = function(model, modelGroup, data, dataGroup){
					//syncUpdate - clear content and cache elements for not selected
					if(data.sp){
						if(data.sp!=dataGroup.id){
							if(ko.utils.unwrapObservable(modelGroup.elements).length>0)
								modelGroup._elements = modelGroup.elements.splice(0, ko.utils.unwrapObservable(modelGroup.elements).length);
							if(ko.utils.unwrapObservable(modelGroup.groups).length>0)
								modelGroup._groups = modelGroup.groups.splice(0, ko.utils.unwrapObservable(modelGroup.groups).length);
							//clear all content for not selected page
							dataGroup.k_=undefined;
							dataGroup.e_=undefined;
							dataGroup.g_=undefined;
						}
					}
				};
			}
		}
	},
	beforeUpdateModel: function(model, data){
		var resetSelectedPage = false;
		var group;
		if(data && data.g_){
			//if selected Page comes from server - set it
			if(!data.sp){
				if(model.requestedPage){
					data.sp = model.requestedPage;
				}else{
					//if bbq has selected page - take it
					var tab = $.vpmsBindViewModel.getState(model.containerId, ko.utils.unwrapObservable(model.id));
					if(tab!==undefined && tab!=="")
						data.sp = tab;
				}
			}
			model.requestedPage = undefined;
			var groupsArray = ko.utils.unwrapObservable(model.groups);
			var i, k;
			//if data.sp is set - check if this page will be relevant after update
			if(data.sp){
				for(i=0; i<data.g_.length; i++){
					if(data.g_[i].id == data.sp){
						//findout group
						group=undefined;
						for(k=0; k<groupsArray.length; k++){
							if(groupsArray[k].name == data.g_[i]._n){
								group = groupsArray[k];
								break;
							}
						}
						if(group){
							if(group.relevant) group.relevant(data.g_[i]._r!==undefined?data.g_[i]._r:true);
							if(group.relevant!==undefined && ko.utils.unwrapObservable(group.relevant)===false)
								data.sp=undefined;
						}else{
							if(!(data.g_[i]._r===undefined || data.g_[i]._r===true))
								data.sp=undefined;
						}
						break;
					}
				}
				//reset selected page
				resetSelectedPage = (data.sp===undefined);
			}
			
			i=0;
			//if data.sp still not set else take first relevant
			while(i<data.g_.length && !data.sp){

				if(model.multiple){
					if(data.g_[i]._r===undefined || data.g_[i]._r===true)
						data.sp = data.g_[i].id;
				}else{
					for(k=0; k<groupsArray.length; k++){
						if(ko.utils.unwrapObservable(groupsArray[k].name)==data.g_[i]._n){
							group = groupsArray[k];
							if(group.relevant) group.relevant(data.g_[i]._r!==undefined?data.g_[i]._r:true);
							if(group.relevant===undefined || ko.utils.unwrapObservable(group.relevant))
								data.sp = data.g_[i].id;
							break;
						}
					}
				}
				i++;
			}
			//reset selected page if data.sp was not relevant, but do this only once
			if(model.resetSelectedPage)
				model.resetSelectedPage = undefined;
			else
				if(data.sp && resetSelectedPage){
					model.resetSelectedPage = true;
					var _sp = data.sp;
					data.sp = undefined;
					setTimeout(function(){
						model.selectedPage(_sp);
					}, 1);
				}
			//if no relevant page - set first as selectedPage - better than get everytime all pages and wait till one will be relevant
			if(!data.sp && data.g_.length>0)
				data.sp = data.g_[0].id;

			//syncUpdate on init - clear content and cache elements for not selected
			if(model.syncUpdate && !model.syncUpdateInited){
				for(i=0; i<data.g_.length; i++){
					if(data.sp!=data.g_[i].id){
						//cash all groups content for not selected page
						for(k=0; k<groupsArray.length; k++){
							if(ko.utils.unwrapObservable(groupsArray[k].name)==data.g_[i]._n){
								if(ko.utils.unwrapObservable(groupsArray[k].elements).length>0)
									groupsArray[k]._elements = groupsArray[k].elements.splice(0, ko.utils.unwrapObservable(groupsArray[k].elements).length);
								if(ko.utils.unwrapObservable(groupsArray[k].groups).length>0)
									groupsArray[k]._groups = groupsArray[k].groups.splice(0, ko.utils.unwrapObservable(groupsArray[k].groups).length);
								break;
							}
						}
						//clear all content for not selected page
						data.g_[i].k_=undefined;
						data.g_[i].e_=undefined;
						data.g_[i].g_=undefined;
					}
				}
				model.syncUpdateInited = true;
			}
		}		
	},
	updateModel: function(model, data){
		if(data && data.sp){
			model.selectedPageInt(data.sp);
			$.vpmsBindViewModel.pushState(model.containerId, ko.utils.unwrapObservable(model.id), model.selectedPage(), true);
		}
		if(!model.setAllGroupsData)
			model.prevSelectedGroup = model.selectedGroup();
		if(model.selectGroupUpdateRequest){
			var _sp = model.selectGroupUpdateRequest;
			model.selectGroupUpdateRequest = undefined;
			setTimeout(function(){
				model.selectedPage(_sp);
			}, 1);
		}
		if(model.settings.slidetabs){
			var _id = $.vpmsBindViewModel.unwrapObservable(model, "id");
			setTimeout(function(){
				var $tabContainer = jQuery("#" + _id + ".vpms-slide-tabs:visible");
				if($tabContainer.length>0){
					$.vpmsBindViewModel.widgets_updateSliderPagesWidget($tabContainer);
				}
			}, 1);			
		}
	}
});


//Button Bar widget - defaults: container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'buttonbar',
	template: '\
	<div class="vpms-button-bar-left">\
	{{each elements}}\
		{{if $value.pos==\'left\'}}\
			<div data-bind="template: {name:\'button\', data:$value }"></div>\
		{{/if}}\
	{{/each}}&nbsp;\
	</div>\
	<div class="vpms-button-bar-center">&nbsp;\
	{{each elements}}\
		{{if $value.pos==\'center\'}}\
			<div data-bind="template: {name:\'button\', data:$value }"></div>\
		{{/if}}\
	{{/each}}&nbsp;\
	</div>\
	<div class="vpms-button-bar-right">&nbsp;\
	{{each elements}}\
		{{if $value.pos==\'right\'}}\
			<div data-bind="template: {name:\'button\', data:$value }"></div>\
		{{/if}}\
	{{/each}}\
	</div>\
',
	initModel :function(model){
		if(model.container===undefined)
			model.container = true;
		if(!model.name) model.name = "buttonbar";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);		
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);
	}
});

//product - defaults: container=true
$.vpmsBindViewModel.pushTemplate({
	name : "product",
	template: '\
	<div>\
		<div class="ui-widget-header ui-corner-all vpms-product-widget-header">\
			<span data-bind="text: caption"></span>\
			{{if $data.navigateBack!=undefined}}<span class="vpms-button-widget" style="float:right; font-size:0.65em"><button data-bind="click:navigateBack, textui:backcaption"></button></span><div style="clear:both;"></div>{{/if}}\
		</div>\
		<div data-bind="template: {name:\'element\', foreach:elements}"></div>\
	</div>\
	',
	initModel: function(model){
		if(model.container===undefined)
			model.container = true;
		if(!model.name) model.name = "product";
		if(!model.id) model.id = model.name;
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);		
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.id);
		var caption  = $.vpmsBindViewModel.initModel_caption({name:'btn.back', caption:'Back'});
		model.captions.push(caption);
		model.backcaption = caption.caption;
	},
	updateModel: function(model, data){
		var caption = $.vpmsBindViewModel.element(model.captions, 'productCaption', false);
		if(caption!==false)
			$.vpmsBindViewModel.updateModelItem(model, 'caption', $.vpmsBindViewModel.unwrapObservable(caption, 'caption'));
	}
});

//selectQuote - defaults: container=true
$.vpmsBindViewModel.pushTemplate({
	name : "selectQuoteChoicesWidget",
	template: '<li><a href="#" data-bind="text:_x,attr:{name:_v},click:function(event){jQuery(event.target).vpmsBindViewModel(\'changeModel\', ko.utils.unwrapObservable(_v))}"></a></li>'
});

$.vpmsBindViewModel.pushTemplate({
	name : "selectQuote",
	template: '\
	<div class="ui-widget-content ui-corner-all">\
		<ul data-bind="template: {name: \'selectQuoteChoicesWidget\', foreach: $.vpmsBindViewModel.element(elements, ko.utils.unwrapObservable(productKey)).choices}"></ul>\
		<div style="clear:both;"></div>\
	</div>\
	<div class="vpms-footer">\
		<div class="vpms-footer-part">&nbsp;</div>\
		<div class="vpms-footer-part"><div class="vpms-powered-by-text">Powered by&nbsp;</div><div class="vpms-powered-by-img">&nbsp;</div></div>\
		<div class="vpms-version-info vpms-footer-part" style="float:right"><span data-bind="text: $.vpmsBindViewModel.element(captions, \'text.version\').caption"></span>&nbsp;<span data-bind="text: $.vpmsBindViewModel.element(captions, \'contextVersion\').caption"></span></div>\
	</div>',
	initModel: function(model){
		if(model.container===undefined)
			model.container = true;
		if(!model.name) model.name = "selectQuote";
		model.version = $.vpmsBindViewModel.initModelItem(model.version, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.productKey = $.vpmsBindViewModel.initModelItem(model.settings?model.settings.productKey:undefined, "A_Product_key");
	}
});

//treeview widget - defaults: container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'treeviewTabs',
	template: '\
	<li data-bind="visible:relevant, attr:{id:id}">\
		<a class="vpms-clearfix ui-corner-left" data-bind="css: { \'ui-state-active\': $item.rootmodel.selectedPage() == id() }, click: function(event){ $item.rootmodel.tabClick(event, $item.rootmodel, $item.model, $data)}">\
			<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: required"><span class="ui-icon ui-icon-bullet"></span></div>\
			<div class="vpms-tab-icon ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error{{if !jQuery.vpmsBindViewModel.isMobileAgent}} ui-icon ui-icon-alert{{/if}}" data-bind="attr: {error: error}">&#10033;</span></div>\
			<div class="vpms-tab-icon" data-bind="visible: (ko.utils.unwrapObservable($data.error)==undefined || ko.utils.unwrapObservable($data.error)==\'\') && ko.utils.unwrapObservable($data.visited)==true"><span class="ui-icon ui-icon-check"></span></div>\
			<span class="vpms-treeview-link-text" data-bind="text:caption, attr:{tooltip:tooltip}"></span>\
			{{if $item.model.multiple}}\
			<span class="vpms-treeview-remove-button" data-bind="visible:canRemove() && !$item.model.disabled() && !$item.model.staticPageCount(), attr:{tooltip:removeCaption}">&#10006;</span>\
			{{/if}}\
			{{if multiple}}\
			<span class="vpms-treeview-add-button" data-bind="visible:canAdd() && !disabled() && !staticPageCount(), attr:{tooltip:addCaption}">&#10010;</span>\
			{{/if}}\
			{{if $.vpmsBindViewModel.isIe7}}\
			<img style="display:none;float:none;"/>\
			{{/if}}\
		</a>\
		<ul data-bind="template: {name:\'treeviewTabs\', foreach:groups, templateOptions:{rootmodel:$item.rootmodel, model:$data}, \
		afterAdd:function(elem){\
			if(!$.vpmsBindViewModel.isModelIniting(elem) && $(elem).filter(\'li\').length>0){\
				setTimeout(function(){$(elem).treeview(\'changed\'); $.vpmsBindViewModel.afterRender($(elem), $data.containerId);},1);\
			}\
		} }">\
		</ul>\
	</li>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'treeviewSection',
	template: '\
	<div class="vpms-treeview-section ui-widget-content" data-bind="attr: {id: id}, visible:$item.rootmodel.selectedPage() == id()">\
		{{if $.vpmsBindViewModel.templateExists($data.widget) }}\
			<div class="vpms-${widget}-widget" data-bind="template: {name:widget, data:$data}"></div>\
		{{/if}}\
	</div>\
	<div data-bind="template: {name:\'treeviewSection\', foreach:groups, templateOptions:{rootmodel:$item.rootmodel}, \
		afterAdd:function(elem){\
			if(!$.vpmsBindViewModel.isModelIniting(elem) && $(elem).hasClass(\'vpms-treeview-section\')){\
				setTimeout(function(){$.vpmsBindViewModel.afterRender($(elem).parent(), $data.containerId);},1);\
			}\
		} }">\
	</div>\
'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'treeview',
	template: '<div class="vpms-treeview-container ui-widget-content {{if settings.suppresstabs}}vpms-suppress-tabs{{/if}}">\
	{{if !settings.suppresstabs}}\
	<ul class="vpms-treeview ui-widget-content" data-bind="template: {name:\'treeviewTabs\', foreach:groups, templateOptions:{rootmodel:$data, model:$data}, \
	afterAdd:function(elem){\
		if(!$.vpmsBindViewModel.isModelIniting(elem) && $(elem).filter(\'li\').length>0){\
			setTimeout(function(){$(elem).treeview(\'changed\'); $.vpmsBindViewModel.afterRender($(elem), $data.containerId);},1);\
		}\
	} }">\
	</ul>\
	{{/if}}\
	<div data-bind="template: {name:\'treeviewSection\', foreach:groups, templateOptions:{rootmodel:$data}, \
		afterAdd:function(elem){\
			if(!$.vpmsBindViewModel.isModelIniting(elem) && $(elem).hasClass(\'vpms-treeview-section\')){\
				setTimeout(function(){$.vpmsBindViewModel.afterRender($(elem).parent(), $data.containerId);},1);\
			}\
		} }">\
	</div><div class="vpms-clear-float"></div></div>\
	<div class="vpms-elements-container" data-bind="template: {name:\'element\', foreach:elements}"></div>',		
	initModel :function(model){
		model.getGroupsTree = true;
		
		if(model.container===undefined)
			model.container = true;
		if(!model.settings)
			model.settings = {};
		if(!model.name) model.name = "treeview";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.getModelGroupById = function(_model, _id){
			var groupsArray = (_model.groups)?ko.utils.unwrapObservable(_model.groups):_model.g_;
			for(var i=0; i<groupsArray.length; i++){
				if(ko.utils.unwrapObservable(groupsArray[i].id) == _id)
					return groupsArray[i];
				else
					if(groupsArray[i].groups || groupsArray[i].g_){
						var page = this.getModelGroupById(groupsArray[i], _id);
						if(page)
							return page;
					}
			}
			return undefined;
		};
		model.getDataModelGroupById = function(_data, _model, _id){
			var groupsArray;
			if(_model)
				groupsArray = ko.utils.unwrapObservable(_model.groups);
			for(var i=0; i<_data.g_.length; i++){
				var modelGroup = undefined;
				if((_data.g_[i].id==_id || _data.g_[i].g_) && groupsArray){
					for(var k=0; k<groupsArray.length; k++)
						if(groupsArray[k].name==_data.g_[i]._n){
							modelGroup = groupsArray[k];
							break;
						}
				}
				if(_data.g_[i].id == _id){
					return {
						data:_data.g_[i],
						model: modelGroup
					};
				}
				else
					if(_data.g_[i].g_){
						var result = this.getDataModelGroupById(_data.g_[i], modelGroup, _id);
						if(result)
							return result;
					}
			}
			return undefined;
		};
		model.updateGroupsTree = function(_model, selectedGroup){
			if(_model.groups){
				var groupsArray = ko.utils.unwrapObservable(_model.groups);
				for(var i=0; i<groupsArray.length; i++){
					if(ko.utils.unwrapObservable(selectedGroup.id)==ko.utils.unwrapObservable(groupsArray[i].id))
						return true;
					else
						if(groupsArray[i].multiple && groupsArray[i].insertAfterSelected!==false && selectedGroup.name==groupsArray[i].name){
							if(selectedGroup.addCaption && groupsArray[i].addCaption)
								groupsArray[i].addCaption(selectedGroup.addCaption());
							return true;
						}
						
					if(this.updateGroupsTree(groupsArray[i], selectedGroup)===true)
						return true;
				}
			}
		};
		if(!model.selectedPageInt) model.selectedPageInt = ko.observable();
		
		if(!model.selectedPage) model.selectedPage = ko.dependentObservable({
			read: function () {
				return this.selectedPageInt();
			},
			write: function (value) {
				if(this.selectedPageInt()!=value){
					if($.vpmsBindViewModel.isModelUpdating('#' + this.containerId)){
						this.selectGroupUpdateRequest = value;
						return;
					}
					if(!this.syncUpdate){
						this.selectedPageInt(value);
						$.vpmsBindViewModel.pushState(this.containerId, ko.utils.unwrapObservable(this.id), value);
					}else{
						//findout group
						var group = this.getModelGroupById(this, value);
						//check if there are elements, restore them else act as async update
						if(group && group._elements){
							ko.utils.arrayPushAll(group.elements(), group._elements);
							group._elements = undefined;
							var rootModel = $('#' + this.containerId).data('model');
							$.vpmsBindViewModel.blockUI(rootModel);
							rootModel.applyingBinding = true;
							setTimeout(function(){
								try{
									$.vpmsBindViewModel.debug("syncUpdate - start bind cached elements");
									group.elements.valueHasMutated();
									$('#' + rootModel.containerId).find(".vpms-treeview-section").filter('#' + value).addClass('.vpms-afterRender');
									$.vpmsBindViewModel.debug("syncUpdate - end bind cached elements");
									rootModel.applyingBinding = false;
								}catch(e){
									rootModel.applyingBinding = false;
									rootModel.applyingBindingError = "Error applying binding: " + e.message;
									throw rootModel.applyingBindingError;
								}
							}, 1);
							this.requestedPage = value;
							jQuery("#" + this.containerId).vpmsBindViewModel("updateModel");
						}else{
							this.selectedPageInt(value);
							$.vpmsBindViewModel.pushState(this.containerId, ko.utils.unwrapObservable(this.id), value);
						}
					}
				}
			},
			owner: model
		});
		if(!model.selectedGroup) model.selectedGroup = ko.dependentObservable(function () {
			return this.getModelGroupById(this, this.selectedPage());
		}, model);				
		if(!model.tabClick) model.tabClick = function(event, rootmodel, model, group){
			if($(event.target).hasClass('vpms-treeview-remove-button')){
				rootmodel.setSelectedPage=ko.utils.unwrapObservable(model.id);
				jQuery(event.target).vpmsBindViewModel('removeFromGroup', model, group);
			}else{
				if($(event.target).hasClass('vpms-treeview-add-button')){
					rootmodel.setSelectedPage=ko.utils.unwrapObservable(group.id);
					var groupAfterId;
					var activeGroup = rootmodel.selectedGroup();
					if(rootmodel.insertAfterSelected && model.insertAfterSelected!==false && activeGroup && activeGroup.name == group.name && ko.utils.unwrapObservable(activeGroup.canAdd)===true)
						groupAfterId = ko.utils.unwrapObservable(activeGroup.id);
					jQuery(event.target).vpmsBindViewModel('addToGroup', group, groupAfterId);
				}else{
					if(rootmodel.selectedPage()!=ko.utils.unwrapObservable(group.id))
						rootmodel.selectedPage(ko.utils.unwrapObservable(group.id));
					else
						jQuery(event.target).vpmsBindViewModel("updateModel");
				}
			}
		};
		
		//Treeview navigation		
		function findPrevPage(_model, params){
			if(!_model.groups)
				return;
			var groupsArray = $.vpmsBindViewModel.unwrapObservable(_model.groups);
			for(var i=0; i<groupsArray.length; i++){
				var _group = $.vpmsBindViewModel.getArrayElement(groupsArray[i]);
				var _id= $.vpmsBindViewModel.unwrapObservable(_group, "id");
				if(_id==params.id)
					return params.prev;
				else{
					if(_group.relevant===undefined || $.vpmsBindViewModel.unwrapObservable(_group, "relevant"))
						params.prev = _id;
					var result = findPrevPage(_group, params);
					if(result)
						return result;
				}
			}
		}
		function findNextPage(_model, params){
			if(!_model.groups)
				return;
			var groupsArray = $.vpmsBindViewModel.unwrapObservable(_model.groups);			
			for(var i=groupsArray.length-1; i>=0; i--){
				var _group = $.vpmsBindViewModel.getArrayElement(groupsArray[i]);
				var result = findNextPage(_group, params);
				if(result)
					return result;
				var _id= $.vpmsBindViewModel.unwrapObservable(_group, "id");
				if(_id==params.id)
					return params.next;
				else{
					if(_group.relevant===undefined || $.vpmsBindViewModel.unwrapObservable(_group, "relevant"))
						params.next = _id;
				}
			}
		}		
		model.prevPage = function(pageId){
			if(pageId){
				this.selectedPage(pageId);
				return;
			}	
			var params = {
				id : $.vpmsBindViewModel.unwrapObservable(this, "selectedPage")
			};
			return findPrevPage(this, params);
		};
		model.nextPage = function(pageId){
			if(pageId){
				this.selectedPage(pageId);
				return;
			}
			var params = {
				id : $.vpmsBindViewModel.unwrapObservable(this, "selectedPage")
			};
			return findNextPage(this, params);
		};		
	},
	beforeUpdateModel: function(model, data){
		if(data && data.g_){
			var resetSelectedPage = false;
			//1: determine future selected page
			//model.setSelectedPage contains groupId where group was added or removed from
			if(model.setSelectedPage){
				var selectedGroup = model.getModelGroupById(data, model.setSelectedPage);
				if(selectedGroup && selectedGroup.sp)
					data.sp = selectedGroup.sp; 
				model.setSelectedPage = undefined;
			}else
				//model.requestedPage on syncUpdate
				if(model.requestedPage){
					data.sp = model.requestedPage;
					model.requestedPage = undefined;
				}else{
					//otherwise if state has selected page - take it
					var tab = $.vpmsBindViewModel.getState(model.containerId, ko.utils.unwrapObservable(model.id));
					if(tab!==undefined && tab!=="")
						data.sp = tab;
				}
			//2: if future selected page exists - check if this page will be relevant after update
			if(data.sp){
				var dataModelGroup = model.getDataModelGroupById(data, model, data.sp);
				if(dataModelGroup){
					if(dataModelGroup.model){
						if(dataModelGroup.model.relevant) dataModelGroup.model.relevant(dataModelGroup.data._r!==undefined?dataModelGroup.data._r:true);
						if(dataModelGroup.model.relevant!==undefined && ko.utils.unwrapObservable(dataModelGroup.model.relevant)===false)
							data.sp=undefined;
					}else
						if(!(dataModelGroup.data._r===undefined || dataModelGroup.data._r===true))
							data.sp=undefined;
					//reset selected page
					resetSelectedPage = (data.sp===undefined);
				}
			}
			var groupsArray = ko.utils.unwrapObservable(model.groups);
			//3: if data.sp still not set else take first relevant on top level
			var i=0;
			while(i<data.g_.length && !data.sp){
				var group;
				if(model.multiple){
					if(data.g_[i]._r===undefined || data.g_[i]._r===true)
						data.sp = data.g_[i].id;
				}else{
					for(var k=0; k<groupsArray.length; k++){
						if(ko.utils.unwrapObservable(groupsArray[k].name)==data.g_[i]._n){
							group = groupsArray[k];
							if(group.relevant) group.relevant(data.g_[i]._r!==undefined?data.g_[i]._r:true);
							if(group.relevant===undefined || ko.utils.unwrapObservable(group.relevant))
								data.sp = data.g_[i].id;
							break;
						}
					}
				}
				i++;
			}
			//4: reset selected page if data.sp was not relevant, but do this only once
			if(model.resetSelectedPage)
				model.resetSelectedPage = undefined;
			else
				if(data.sp && resetSelectedPage){
					model.resetSelectedPage = true;
					var _sp = data.sp;
					data.sp = undefined;
					setTimeout(function(){
						model.selectedPage(_sp);
					}, 1);
				}
			//if no relevant page - set first as selectedPage - better than get everytime all pages and wait till one will be relevant
			if(!data.sp && data.g_.length>0)
				data.sp = data.g_[0].id;

			//syncUpdate on init - clear content and cache elements for not selected
			if(model.syncUpdate && !model.syncUpdateInited){
				var clearContentFunction = function(_data, _model, _id){
					var groupsArray = ko.utils.unwrapObservable(_model.groups);
					for(var i=0; i<_data.g_.length; i++){
						var modelGroup = undefined;
						for(var k=0; k<groupsArray.length; k++){
							if(ko.utils.unwrapObservable(groupsArray[k].name)==_data.g_[i]._n){
								modelGroup = groupsArray[k];
								break;
							}
						}
						if(_id!=data.g_[i].id){
							//cash all groups content for not selected page
							if(modelGroup && ko.utils.unwrapObservable(modelGroup.elements).length>0)
								modelGroup._elements = modelGroup.elements.splice(0, ko.utils.unwrapObservable(modelGroup.elements).length);
							//clear all content for not selected page
							_data.g_[i].e_=undefined;
						}
						if(_data.g_[i].g_)
							clearContentFunction(_data.g_[i], modelGroup, _id);
					}
					
				};
				if(data.sp)
					clearContentFunction(data, model, data.sp);
				model.syncUpdateInited = true;
			}
		}
	},
	updateModel: function(model, data){
		if(data.sp){
			model.selectedPageInt(data.sp);
			$.vpmsBindViewModel.pushState(model.containerId, ko.utils.unwrapObservable(model.id), model.selectedPage(), true);
			if(model.insertAfterSelected)
				model.updateGroupsTree(model, model.selectedGroup());
			setTimeout(function(){jQuery('li#' + model.selectedPage()).treeview('select'); }, 1);
		}
		if(model.syncUpdate)
			$.vpmsBindViewModel.unblockUI(model);
		if(!model.setAllGroupsData)
			model.prevSelectedGroup = model.selectedGroup();
		if(model.selectGroupUpdateRequest){
			var _sp = model.selectGroupUpdateRequest;
			model.selectGroupUpdateRequest = undefined;
			setTimeout(function(){
				model.selectedPage(_sp);
			}, 1);
		}
	}
});


//table widget  - defaults container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'tableRow',
	template: '\
	<tr class="${$item.group.settings.contentClass} vpms-afterRender" data-bind="attr: {id:id}, visible:relevant">\
	{{each elements}}\
		<td class="${$item.group.settings.contentClass}" {{if $value.settings && $value.settings.span}}span="${$value.settings.span}"{{/if}}>\
		{{if $value.widget && $.vpmsBindViewModel.templateExists($value.widget)}}\
			<div class="vpms-${$value.widget}-widget {{if $value.settings}}${$value.settings.css}{{/if}} {{if $value.settings && $value.settings.align}}vpms-align-${$value.settings.align}{{/if}}" data-bind="visible:$value.relevant, template: {name:$value.widget, data:$value}"></div>\
		{{/if}}\
		</td>\
	{{/each}}\
	{{if $item.group.multiple}}\
		<td class="${$item.group.settings.contentClass} vpms-table-remove-column" data-bind="visible:!$item.group.disabled() && !$item.group.staticPageCount()">\
			<div data-bind="attr:{enabled:canRemove}">\
				<div data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'delete\', enable:canRemove, caption:removeCaption, click:function(event){jQuery(event.target).vpmsBindViewModel(\'removeFromGroup\', $item.group, $data);} } }"></div>\
			</div>\
		</td>\
	{{/if}}\
	</tr>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'table',
	template: '{{if $data.navigation}}\
		<div class="vpms-table-navigation" data-bind="visible:navigation.visible">\
			<span class="vpms-table-navigation-prev" data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'prevPage\', caption:\'<\', enable:navigation.hasPrev, click:function(event){$data.navigationPrevPage(event, $data);} } }"></span>\
			<span class="vpms-table-navigation-label" data-bind="text:navigation.label"></span>\
			<span class="vpms-table-navigation-next" data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'nextPage\', caption:\'>\', enable:navigation.hasNext, click:function(event){$data.navigationNextPage(event, $data);} } }"></span>\
		</div>{{/if}}\
		<div {{if $data.settings.height}}style="overflow-y:auto;height:${$data.settings.height}"{{/if}}><table class="${$data.settings.contentClass} vpms-table-widget vpms-single-column" data-bind="attr:{id:id}">\
		{{if !$data.settings.noheader}}\
		<thead><tr class="${$data.settings.contentClass}">\
		{{each settings.colSettings}}\
			<th class="${$data.settings.headerClass} {{if $value.settings}}${$value.settings.css}{{/if}} {{if $value.settings && $value.settings.labelAlign}} vpms-align-${$value.settings.labelAlign}{{/if}}" {{if $value.settings && $value.settings.width}}style="width:${$value.settings.width};"{{/if}}>\
				<span data-bind="text:$value.caption"></span>\
				<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: $value.required"><span class="ui-icon ui-icon-bullet"></span></div>\
			</th>\
		{{/each}}\
		{{if multiple}}\
			<th class="${$data.settings.headerClass} vpms-table-remove-column" data-bind="visible:!disabled() && !staticPageCount()"></th>\
		{{/if}}\
		</tr></thead>\
		{{/if}}\
		<tbody data-bind="template:{name:\'tableRow\', foreach:groups, templateOptions: {group:$data} }"></tbody>\
		</table></div>\
		{{if multiple}}\
		<div class="vpms-add-button" data-bind="visible:!disabled() && !staticPageCount()">\
			<div data-bind="template: {name:\'button\', data:$data, templateOptions: {id:\'add\', caption:addCaption, enable:canAdd, click:function(event){$data.addClick(event, $data);} } }"></div>\
		</div>\
		{{/if}}\
	',
	initModel: function(model){
		if(!model.name) model.name = "table";
		//table has no selected Page - get and update all groups
		model.getAllGroupsData = true;
		if(model.onlyRelevant === undefined && model.getAllGroupsData)
			model.onlyRelevant = true;				
		if(!model.readonly)
			model.setAllGroupsData = true;
		if(model.container===undefined)
			model.container = true;
		if(!model.settings)
			model.settings={};
		if(model.settings.noborder===undefined)
			model.settings.noborder = false;
		if(model.settings.headerClass===undefined)
			model.settings.headerClass=model.settings.noborder?"":"ui-widget-header";
		if(model.settings.contentClass===undefined)
			model.settings.contentClass=model.settings.noborder?"":"ui-widget-content";
		if(model.settings.noheader===undefined)
			model.settings.noheader = false;
		if(model.settings.visibleLines){
			var intVisibleLines = parseInt(model.settings.visibleLines);
			if(!isNaN(intVisibleLines) && intVisibleLines>0)
				model.settings.height = (intVisibleLines+1) * 2.6 + "em";
		}
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		//init columns from template
		if(!model.settings)
			model.settings = {};
		if(!model.settings.colSettings)
			model.settings.colSettings = [];
		//colNames in model root to get column captions from VPMS
		model.colNames = [];
		model.initColumn = function(element, index){
			var colModel = {
				settings: element.settings?$.vpmsBindViewModel.cloneObject(element.settings):{},
				caption : $.vpmsBindViewModel.initModelItem(undefined, element.name),
				required: $.vpmsBindViewModel.initModelItem(undefined, false)
			};
			if(this.settings.colSettings.length>index)
				this.settings.colSettings[index] = colModel;
			else
				this.settings.colSettings.push(colModel);
			this.colNames.push(element.name);
		};
		var i;
		if(model.multiple){
			//if model is multiple - get column captions and settings from template
			var template = $.vpmsBindViewModel.getGroupTemplate(model);
			if(template)
				for(i=0; i<template.elements.length; i++)
					model.initColumn(template.elements[i], i);
			if(!model.addClick) model.addClick = function(event, model){
				jQuery(event.target).vpmsBindViewModel('addToGroup', model);
			};
		}else{ 
			//else - from first group
			var groupsArray =  $.vpmsBindViewModel.unwrapObservable(model.groups);
			if(groupsArray && groupsArray.length>0){
				var elementsArray = $.vpmsBindViewModel.unwrapObservable(groupsArray[0].elements);
				if(elementsArray)
					for(i=0; i<elementsArray.length; i++)
						model.initColumn(elementsArray[i], i);
			}
		}
		//navigation
		$.vpmsBindViewModel.initGroupsNavigation(model); 
	},
	updateModel: function(model, data){
		if(data){
			var i;
			if(model.multiple){
				for(i=0; i<model.settings.colSettings.length; i++){
					if(data.cn_ && data.cn_.length>i)
						$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'caption', data.cn_[i]);
					if(data.cr_ && data.cr_.length>i)
						$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'required', (data.cn_[i]=="1"));
				}
			}else{
				var groupsArray =  $.vpmsBindViewModel.unwrapObservable(model.groups);
				if(groupsArray && groupsArray.length>0){
					var elementsArray = $.vpmsBindViewModel.unwrapObservable(groupsArray[0].elements);
					if(elementsArray)
						for(i=0; i<model.settings.colSettings.length; i++){
							$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'caption', $.vpmsBindViewModel.unwrapObservable(elementsArray[i], 'caption'));
							$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'required', $.vpmsBindViewModel.unwrapObservable(elementsArray[i], 'required'));
						}
				}
			}
		}
		//navigation
		if(model.navigationUpdate)
			model.navigationUpdate(data);
	}
});

//grid widget
//*settings.colModel build from template.elements (including format, choices etc.)
//*setting.navigation = false - if true server navigation loading pages
//*settings.readonly = false - if true not editable grid
//*settings.colNames - array of strings width captions (translatable)
//translatable means: model.settings.colNames = ["grid.firstName"], model.captions = [{name:"grid.firstName", id:"grid.firstName"}] 
//in this case resources translated string with id grid.firstName will be returned.
//*if settings.colNames is not defined, will be filled from template.elements[i].name, is still translatable, example template.elements[i].name="firstName"
//model.captions = [{name:"firstName", id:"grid.firstName"}]
$.vpmsBindViewModel.pushTemplate({
	name: 'grid',
	template: '<table data-bind="attr:{tableId:\'table_\' + $data.containerId + $data.id()}"></table>',
	initModel: function(model){
		if(!model.name) model.name = "grid";
		if(!model.settings)
			model.settings = {};
		if(!model.settings.height && model.settings.visibleLines){
			var intVisibleLines = parseInt(model.settings.visibleLines);
			if(!isNaN(intVisibleLines) && intVisibleLines>0)
				model.settings.height = intVisibleLines * 23;
		}
		//grid has no selected Page - get and update all groups
		model.getAllGroupsData = true;
		if(model.onlyRelevant === undefined && model.getAllGroupsData)
			model.onlyRelevant = true;				
		if(!model.readonly)
			model.setAllGroupsData = true;
		if(model.container===undefined)
			model.container = true;
		if(model.insertAfterSelected===undefined)
			model.insertAfterSelected=true;
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		if(!model.selectedPage) model.selectedPage = ko.observable();
		if(!model.selectedGroup) model.selectedGroup = ko.dependentObservable(function () {
			var groupsArray = ko.utils.unwrapObservable(this.groups);
			for(var i=0; i<groupsArray.length; i++){
				if(ko.utils.unwrapObservable(groupsArray[i].id) == this.selectedPage()){
					return groupsArray[i];
				}
			}
		}, model);
		
		//init columns from template if columns is empty
		if(!model.settings.colNames || model.settings.colNames.length===0){
			model.settings.colNames = [];
			if(model.template){
				var template = (jQuery.isFunction(model.template))?model.template():model.template;
				model.settings.colNames.push("id");
				for(var i=0; i<template.elements.length; i++){
					model.settings.colNames.push(template.elements[i].name);
					if(template.elements[i].generateDisplayValue === undefined)
						template.elements[i].generateDisplayValue = true;
				}
			}
			//copy to model root to get captions from VPMS
			model.colNames = model.settings.colNames;
		}	
		if(model.settings.dynamicLoad){
			if(!model.settings.rowsPerPage){
				if(!model.settings.rowNum)					
					model.settings.rowNum = 20;
				model.settings.rowsPerPage = model.settings.rowNum;
			}
			$.vpmsBindViewModel.initGroupsNavigation(model);
		}else{
			model.selectedGridPage = 1;
		}
	},
	updateModel: function(model, data){
		function createGrid(model){
			jQuery('[tableId="' + $.vpmsBindViewModel.escapeSelector('table_' + model.containerId + model.id()) + '"]').each(function(){
				var table = jQuery(this);
				var tableId = $.vpmsBindViewModel.uniqueSelector(model.containerId + model.id());
				var pagerId = 'pager_' + tableId;
				table.attr('id', tableId);
				table.after('<div id="' + pagerId + '"></div>');
				//create colModel
				if(!model.settings.colModel){
					model.settings.colModel = [];
					var commonSettings = jQuery('#' + model.containerId).data('settings'); 
					if(model.template){
						var template = (jQuery.isFunction(model.template))?model.template():model.template;
						model.settings.colModel.push({name:'id',index:'id', hidden:true});
						for(var i=0; i<template.elements.length; i++){
							var col = {name:template.elements[i].name, index:template.elements[i].name};
							if(template.elements[i].settings){
								jQuery.extend(col, template.elements[i].settings);
							}
							if(template.elements[i].format=='date'){
								col.sorttype = 'date';
								col.datefmt = template.elements[i].settings && template.elements[i].settings.dateFormat?template.elements[i].settings.dateFormat:
												(commonSettings && commonSettings.dateFormat)?commonSettings.dateFormat:
												(commonSettings && commonSettings.locale && commonSettings.locale.dateFormat)?commonSettings.locale.dateFormat:'ISO';
							}else
								if(template.elements[i].format=='number'){
									if(template.elements[i].settings && template.elements[i].settings.integer===true)
										col.sorttype = 'int';
									else
										col.sorttype = 'float';
									col.align = 'right';
								}
							if(template.elements[i].settings && template.elements[i].settings.align){
								col.align = template.elements[i].settings.align;
							}
							if(!template.elements[i].disabled && !model.readonly){
									col.editable=true;
									if(template.elements[i].widget=='checkbox')
										col.edittype='checkbox';
									else
										if($.vpmsBindViewModel.isChoicesWidget(template.elements[i].widget))
											col.edittype='select';
										else
											col.edittype='text';
									//datepicker
									if(template.elements[i].format=='date'){
										var dateOptions = jQuery.vpmsBindViewModel.getDatepickerOptions(template.elements[i].settings, commonSettings);
										col.editoptions = {dataInit: function(elem){
												setTimeout(function(){jQuery(elem).datepicker(dateOptions);}, 10);
											}
										};
									}else
									//numbers
									if(template.elements[i].format=='number'){
										var numbersOptions = jQuery.vpmsBindViewModel.getNumbersOptions(template.elements[i].settings, commonSettings);
										col.editoptions = {dataInit: function(elem){
											jQuery(elem).numbers(numbersOptions);
										}};
									}
							}
							//cellattr
							col.cellattr = function(rowId, cellValue, rawObject, cm, rdata) { 
								if(rawObject["_" + cm.name + "_error"])
									return ' class="ui-state-error ui-state-error-text" title="' + rawObject["_" + cm.name + "_error"] + '"';
								else
									return ' title="' +  rawObject["_" + cm.name + "_tooltip"] + '"';
							};
							model.settings.colModel.push(col);
						}//for template.elements
					}//if(model.template)
				} //if(!model.colModel)	
				//caption
				if(!model.settings.caption){
					model.settings.caption = model.caption?ko.utils.unwrapObservable(model.caption):model.name;
				}
				//jqGrid settings
				var settings = {
					datatype: "local",
					height: "100%",
					pager: '#'+ pagerId,
					viewrecords:true
				};
				jQuery.extend(settings, model.settings);
				//colnames
				if(data.cn_)
					settings.colNames = data.cn_;
				if(settings.hiddengrid===true){
					model.hidden = true;
				}
				settings.onHeaderClick = function(gridstate){
					if(gridstate=='visible'){
						model.hidden = false;
						jQuery('#' + model.containerId).vpmsBindViewModel("updateModel");
					}else{
						model.hidden = true;
					}
				};			
				//server navigation enabled
				if(model.settings.dynamicLoad){
					settings.localReader = {
							root: function(obj) {
								return model.navigation.rows;
							},
							page: function(obj) { 
								return model.navigation.pageNumber; 
							},
							total: function(obj) {
								return model.navigation.totalPages; 
							},
							records: function(obj) { 
								return model.navigation.totalRecords; 
							}
					};
				}
					
				settings.onPaging = function(pgButton){
					if(model.settings.dynamicLoad){
						model.navigation.pageNumber = parseInt(jQuery('#' + tableId).getGridParam('page'));				
						jQuery('#' + tableId).vpmsBindViewModel("updateModel");
					}else{
						model.selectedGridPage = parseInt(jQuery('#' + tableId).getGridParam('page'));
					}
				};
				//editable grid functions
				var bindKeysSettings;
				if(!model.readonly){
					model.updateNavCaptions = function(id){
						var group;
						if(id)
							group = jQuery.vpmsBindViewModel.elementById(model.groups, id);
						var $addButton = jQuery('#add_table_' + tableId);
						if($addButton.length>0)
							if(group && model.insertAfterSelected && ko.utils.unwrapObservable(group.addCaption))
								$addButton.attr('title', ko.utils.unwrapObservable(group.addCaption));
							else
								$addButton.attr('title', ko.utils.unwrapObservable(model.addCaption));
						var $removeButton = jQuery('#del_table_' + tableId);
						if($removeButton.length>0)
							if(group && ko.utils.unwrapObservable(group.removeCaption))
								$removeButton.attr('title', ko.utils.unwrapObservable(group.removeCaption));
							else
								$removeButton.attr('title', ko.utils.unwrapObservable(model.removeCaption));
					};
					//reflect changes in grid to viewModel
					var reselectRow = function(id){
						  if(model.selid){
							  jQuery('#' + tableId).jqGrid('setSelection', model.selid, false);
							  model.updateNavCaptions(model.selid);
							  model.selid=undefined;
						  }else{
							  jQuery('#' + tableId).jqGrid('setSelection', id, false);
							  model.updateNavCaptions(id);
						  }
						  jQuery('#' + tableId).focus();
					};
					var afterSave = function(rowid, response){
						model.lastselid = undefined;
						var group = jQuery.vpmsBindViewModel.elementById(model.groups, rowid);
						var row = jQuery('#'+ tableId).jqGrid('getRowData', rowid);
						var autocommit = false;
						if(group && row){
							for(var name in row){
								if(name!='id'){
									var element = jQuery.vpmsBindViewModel.element(group.elements, name, false);
									if(element!==false){
										if(element.autocommit===true)
											autocommit = true;
										if(element.widget=="checkbox"){
											var displayValue = jQuery.vpmsBindViewModel.unwrapObservable(element, 'displayValue');
											var checked = jQuery.vpmsBindViewModel.unwrapObservable(element, 'checked');
											if(row[name]!=displayValue)
												jQuery.vpmsBindViewModel.updateModelItem(element, 'checked', !checked);
										}else
										if($.vpmsBindViewModel.isChoicesWidget(element.widget)){
											var choicesArray = jQuery.vpmsBindViewModel.unwrapObservable(element, 'choices');
											for(var i=0;i<choicesArray.length;i++){
												if(jQuery.vpmsBindViewModel.unwrapObservable(choicesArray[i]._x)==row[name]){
													jQuery.vpmsBindViewModel.updateModelItem(element, 'value', jQuery.vpmsBindViewModel.unwrapObservable(choicesArray[i], '_v'));
													break;
												}
											}
										}else
											$.vpmsBindViewModel.writeFormattedValue(element, row[name]);
									}
								}
							}
							if(autocommit){
								//set selectedPage to update only changed row
								model.selectedPage(rowid);
								model.prevSelectedGroup = model.selectedGroup();
								jQuery('#' + tableId).vpmsBindViewModel("updateModel");
								var updatingTimerTimeout = 0;
								var updatingTimer = window.setInterval(function () {
									  if (!model.updating) {
										  window.clearInterval(updatingTimer);
										  reselectRow(rowid);
									  }else
									  if(updatingTimerTimeout>=3000)
										  window.clearInterval(updatingTimer);
									  else
										  updatingTimerTimeout += 200;
								}, 200);
							}
							else
								reselectRow(rowid);
						}
					};
					var afterRestore = function(rowid){
						model.lastselid = undefined;
						reselectRow(rowid);
					};
					var editRow = function(id, e){
						if(model.lastselid && id!=model.lastselid)
							jQuery('#'+ tableId).jqGrid('saveRow',model.lastselid, false, 'clientArray', undefined, afterSave, afterRestore, afterRestore);
						//wait for model ends updating  (5 seconds timeout)
						var updatingTimerTimeout = 0;
						var updatingTimer = window.setInterval(function () {
								if (!model.updating) {
									window.clearInterval(updatingTimer);
									//get choices && checkbox
									var group = jQuery.vpmsBindViewModel.elementById(model.groups, id);
									if(group){
										var result;
										var elementsArray = jQuery.vpmsBindViewModel.unwrapObservable(group.elements);
										for(var i=0; i<elementsArray.length; i++){
											if(elementsArray[i].disabled!==undefined){
												jQuery('#'+ tableId).jqGrid('setColProp', elementsArray[i].name, {editable: !jQuery.vpmsBindViewModel.unwrapObservable(elementsArray[i].disabled)});
											}
											if(elementsArray[i].widget=='combobox' || elementsArray[i].widget=='radiogroup'){
												var choices = jQuery.vpmsBindViewModel.unwrapObservable(elementsArray[i].choices);
												result = '';
												for(var k=0; k<choices.length; k++)
													result += jQuery.vpmsBindViewModel.unwrapObservable(choices[k]._v) + ":" + jQuery.vpmsBindViewModel.unwrapObservable(choices[k]._x) + ";";
												if(result.length>0)
													result = result.substring(0, result.length-1);
												jQuery('#'+ tableId).jqGrid('setColProp', elementsArray[i].name, {editoptions: {value:result} });
											}else
											if(elementsArray[i].widget=='checkbox'){
												result = jQuery.vpmsBindViewModel.unwrapObservable(jQuery.vpmsBindViewModel.element(elementsArray[i].captions, 'text.yes').caption) + ":" + ko.utils.unwrapObservable(jQuery.vpmsBindViewModel.element(elementsArray[i].captions, 'text.no').caption);
												jQuery('#'+ tableId).jqGrid('setColProp', elementsArray[i].name, {editoptions: {value:result} });
											}
										}
									}
									//reselectRow
									jQuery('#' + tableId).jqGrid('setSelection', id, false);
									//edit row
									jQuery('#' + tableId).jqGrid('editRow',id,true, function(){if(e) setTimeout(function(){jQuery("input, select",e.target).focus();}, 10);
									}, false, 'clientArray', undefined, afterSave, afterRestore, afterRestore);
									model.lastselid=id;
								}else
								if(updatingTimerTimeout>=3000)
									window.clearInterval(updatingTimer);
								else
									updatingTimerTimeout += 200;
						}, 200);
					};
					settings.onSelectRow = function(id){
						if(model.lastselid && id!=model.lastselid){
							model.selid = id;
							jQuery('#'+ tableId).jqGrid('saveRow',model.lastselid, false, 'clientArray', undefined, afterSave, afterRestore, afterRestore);
						}else
							model.updateNavCaptions(id);
					};
					
					//inline edit - open row for editing on click
					settings.ondblClickRow = function(id, iRow, iCol, e){
						editRow(id, e);
					};//ondblClickRow
					bindKeysSettings = {"onEnter": function(rowid){ editRow(rowid); }};
				}//!model.readonly
				//createGrid
				table.jqGrid(settings).jqGrid("bindKeys", bindKeysSettings);
				if(!model.readonly && ko.utils.unwrapObservable(model.staticPageCount)!==true){
					//nav grid
					table.jqGrid('navGrid', settings.pager,
						{edit:true,add:true,del:true,search:false,view:false,refresh:false,
							deltitle:ko.utils.unwrapObservable(model.removeCaption),
							addtitle:ko.utils.unwrapObservable(model.addCaption),
							addfunc:function(){
								if(model.insertAfterSelected){
									var selectedId = table.getGridParam('selrow');
									var group = selectedId?jQuery.vpmsBindViewModel.elementById(model.groups, selectedId):undefined;
									if((!group || !group.canAdd || group.canAdd()) && (!model.canAdd || model.canAdd()))
										table.vpmsBindViewModel('addToGroup', model, selectedId);
								}
								else
									if(!model.canAdd || model.canAdd())
										table.vpmsBindViewModel('addToGroup', model);
							},
							delfunc:function(id){
								var group = jQuery.vpmsBindViewModel.elementById(model.groups, id);
								if(!group.canRemove || group.canRemove())
									table.vpmsBindViewModel('removeFromGroup', model, group);
							},
							editfunc:function(id){
								if(model.lastselid!=id)
									editRow(id, null);
								else
									table.jqGrid('saveRow',model.lastselid, false, 'clientArray', undefined, afterSave, afterRestore, afterRestore);
							}
						},
						{}, // edit options
						{}, // add options
						{} // del options
					);
				}
			});
			model.inited = true;
		}	
		
		setTimeout(function(){
			//server side navigation
			if(model.navigationUpdate)
				model.navigationUpdate(data);
			jQuery('[tableId="' + $.vpmsBindViewModel.escapeSelector('table_' + model.containerId + model.id()) + '"]').each(function(){
				var table = jQuery(this);

				if(!model.inited){
					createGrid(model);
				}
				//reset selectedpage
				model.selectedPage(undefined);
				//merge data
				var groupsArray = ko.utils.unwrapObservable(model.groups);
				var selectedIndex;
				var rows = [];
				var count=0;
				for(var i=0; i<groupsArray.length; i++){
					if(ko.utils.unwrapObservable(groupsArray[i].relevant)===false)
						continue;
					var row = {id:groupsArray[i].id()};
					var elements = ko.utils.unwrapObservable(groupsArray[i].elements);
					for(var k=0; k<elements.length; k++){
						if(elements[k].displayValue)
							row[elements[k].name] = ko.utils.unwrapObservable(elements[k].displayValue);
						else
							row[elements[k].name] = ko.utils.unwrapObservable(elements[k].value);
						row["_" + elements[k].name + "_error"] = ko.utils.unwrapObservable(elements[k].error);
						var tooltip = ko.utils.unwrapObservable(elements[k].tooltip);
						row["_" + elements[k].name + "_tooltip"] = tooltip?tooltip:"";
					}
					if(data.sp==row.id)
						selectedIndex = count;
					count++;
					rows.push(row);
				}
				//set table caption and table colnames
				if(data.cn_){
					for(i=0; i<model.settings.colModel.length; i++)
						if(i<data.cn_.length){
							var css = undefined;
							if(model.settings.colModel[i].labelAlign)
								css = {'text-align':model.settings.colModel[i].labelAlign};
							var colname = data.cn_[i];
							if(data.cr_ && data.cr_[i] =="1")
								colname += '<span class="ui-state-highlight vpms-widget-required-icon"><span class="ui-icon ui-icon-bullet"></span></span>';
							table.setLabel( model.settings.colModel[i].name, colname, css);
						}
				}				
				table.setCaption(ko.utils.unwrapObservable(model.caption));
				if(model.settings.dynamicLoad){
					model.navigation.rows = rows;
				}
				else{
					table.clearGridData();
					table.setGridParam({data: rows});
					if(selectedIndex){
						var rowNum = table.getGridParam('rowNum');
						table.setGridParam({page: Math.floor(selectedIndex/rowNum)+1});						
					}else{
						table.setGridParam({page: model.selectedGridPage});												
					}
				}
				table.trigger('reloadGrid');
				//enable-disable edit buttons
				if(ko.utils.unwrapObservable(model.disabled)===true)
					jQuery('#pager_' + table.attr('id')).find('.ui-pg-button').addClass('ui-state-disabled');
				else
					jQuery('#pager_' + table.attr('id')).find('.ui-pg-button').removeClass('ui-state-disabled');
				//select new added row
				if(data.sp){
					setTimeout(function(){table.jqGrid('setSelection', data.sp); table.focus();},100);
				}else
					model.updateNavCaptions();
			});
		}, 1);
	}
});
// jQuery Numbers Plugin
//
// Version 1.01
//
// Alex Dobrushin (http://dobrushin.de/)
//
(function($){
	var	decimalSeparator = Number("1.2").toLocaleString().substr(1,1);
	var	thousandsSeparator = Number("1234").toLocaleString().substr(1,1);
	//toLocaleString() does not work in Chrome any other Solution?
	if(thousandsSeparator=='2')
		thousandsSeparator = (decimalSeparator==".")?",":".";	
	
	$.numbers = {
		defaults : {
			integer : false,
			positive : false,
			nothousands : false,
			removeTrailingZeros : true,
//			removeLeadingZeros : true
//			max,
//			min,
//			hiddenId,
//			decimals,	
			decimalSeparator : decimalSeparator,
			thousandsSeparator : thousandsSeparator	
		},
		toNumber: function(value, options){
		    var opt = {};
			$.extend(opt, $.numbers.defaults);
			if(options)
				$.extend(opt, options);
			//parse string			
			if((typeof value)!="number"){
				//remove all except decimal separator, numbers and minus
				value = value.replace(new RegExp("[^\\-\\d\\" + opt.decimalSeparator + "]", "g"), "");
				//sign should be only first
				var neg = value.substring(0,1)=="-";
				value = value.replace(/[\-]/g, "");
				//decimalSeparator only once and not first and not last
				var i = value.indexOf(opt.decimalSeparator);
				var decSeparatorNeeded = (i>0 && !opt.integer && i<value.length-1);
				value = value.replace(new RegExp("[\\" + opt.decimalSeparator + "]", "g"), "");
				if(decSeparatorNeeded)		
					value = value.substring(0, i) + opt.decimalSeparator + value.substring(i);
				//replace leading zeros
				if(opt.removeLeadingZeros || !opt.nothousands)
					value = value.replace(new RegExp("^0*(\\d|\\d\\" + opt.decimalSeparator + "\\d|0\\" + opt.decimalSeparator + "\\d)", ""), "$1");	   
				//replace trailing zeros
				if(opt.removeTrailingZeros)
					value = value.replace(new RegExp("(?:(\\" + opt.decimalSeparator + "\\d*[1-9])|\\" + opt.decimalSeparator + ")0*$", ""), "$1");
				//restore minus sign
				if(neg && !opt.positive)	value = "-" + value;
			}
			//max
			if(opt.max && validAndLess(opt.max, value, opt))
				value = opt.max;
			//min
			if (opt.min && validAndLess(value, opt.min, opt))	
				value = opt.min;
			//fixed decimals
			if(opt.decimals && !opt.integer){
				var num = getNumberValue(value, opt);
				if((typeof num)=='number')
					value = num.toFixed(opt.decimals);
			}
			//make string again
			value = value + ""; 
			//replace decimalSeparator with '.' 
			return value.replace(new RegExp("[\\" + opt.decimalSeparator + "]", "g"), ".");
		},
		toString: function(value, options){
			
		    var opt = {};
			$.extend(opt, $.numbers.defaults);
			if(options)
				$.extend(opt, options);			
			//fixed decimals
			if(opt.decimals && !opt.integer){
				var num = Number(value);				
				if(!isNaN(num) && (typeof num)=='number')
					value = num.toFixed(opt.decimals);
			}
			//make string again
			value = value + ""; 
			 
			//replace "." with decimal separator
			value = value.replace(new RegExp("[\\" + "." + "]", "g"), opt.decimalSeparator);
			//thousand separate
			if(!opt.nothousands)
				value = thousandSeparateValue(value, opt);
			return value;
		}
	};
	
function getCursorPosition($this) {
	var pos = 0;
	var input = $this.get(0);
	// IE Support
	if (document.selection) {
		input.focus();
		var sel = document.selection.createRange();
		var selLen = document.selection.createRange().text.length;
		sel.moveStart('character', -input.value.length);
		pos = sel.text.length - selLen;
	}
	// Firefox support
	else if (input.selectionStart || input.selectionStart == '0')
		pos = input.selectionStart;

	return pos;
}

function validateNumber(obj, opt, oldValue) {
	var $this = $(obj);
	var value = $this.val();
	if(oldValue===undefined)
		oldValue = value;
	//filter value
	var num = $.numbers.toNumber(value, opt);
	if(opt.hiddenId && ($('#' + hiddenId).length!==0))
		$('#' + opt.hiddenId).val(num);
	//thousands separator
	value = $.numbers.toString(num, opt);
	$this.val(value);
	if(oldValue!=value)
		$this.trigger('change');
}

function getNumberValue(strValue, opt){
	if(!strValue)
		return 'NaN';	//not defined
	else
		if((typeof strValue)=="number")
			return strValue;
	//remove thousands separators, replace decimal with "."
	strValue = strValue.replace(new RegExp("[\\" + opt.thousandsSeparator + "]", "g"), "").replace(new RegExp("[\\" + opt.decimalSeparator + "]", "g"), ".");
    return Number(strValue);
}	
	
function validAndLess(strValue1, strValue2, opt){
	var value1 = getNumberValue(strValue1, opt);
	var value2 = getNumberValue(strValue2, opt);
    return ('NaN' != value1 && 'NaN' != value2 && value1 < value2);
}	
	
function thousandSeparateValue(value, opt)
{
    value += '';
    value = value.replace(new RegExp("[\\" + opt.thousandsSeparator + "]", "g"), "");
    x = value.split(opt.decimalSeparator);
    x1 = x[0];
    x2 = x.length > 1 ? opt.decimalSeparator + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + opt.thousandsSeparator + '$2');
    }
    return x1 + x2;
}

function getEventCharCode(event){
	var evt=(event)?event:(window.event)?window.event:null;
	if (evt.which === null)
		return evt.keyCode;    // IE
	else if (evt.which !== 0 && evt.charCode !== 0)
		return event.which;	  // All others
	else
		return -1;// special key	
}

  var methods = {
     init : function(options) {
	     var settings = {};
		 $.extend(settings, $.numbers.defaults);
		 if(options)
			$.extend(settings, options);
		return this.each(function(){
				var $this = $(this);
				validateNumber($this, settings);		
				
				$this.keypress(function(event) {
					var e = event?event:window.event;
					var charCode = getEventCharCode(event); 
					if (charCode!=-1 && //special char - don't prevent
						e.ctrlKey!==true && // firefox fix - don't prevent if ctrl key holding
						!((charCode>=48 && charCode <=57) ||
						//decimal separator only once and not on the first position and not for integers
						(settings.decimalSeparator.charCodeAt(0)==charCode && !settings.integer && $this.val().indexOf(settings.decimalSeparator)===-1 && getCursorPosition($this)!==0) ||
						//minus only on the first position
						("-".charCodeAt(0)==charCode && !settings.positive && getCursorPosition($this)===0) ||
						//thousand separator before decimal and not on first position
						(settings.thousandsSeparator.charCodeAt(0)==charCode && !settings.nothousands) && getCursorPosition($this)!==0 && ($(this).val().indexOf(settings.decimalSeparator)===-1 || $(this).val().indexOf(settings.decimalSeparator)>getCursorPosition($this))) 
					){
						e.preventDefault();
					}
				});

				//paste 
				$this.bind('paste', function(e){
					var $this = $(this);
					var oldValue = $this.val();
					setTimeout(function(){validateNumber($this, settings, oldValue);}, 1);
				});
				//change
				$this.bind('change', function(e){
					validateNumber($(this), settings);
				});					
			});
	}
  };

  $.fn.numbers = function(method) {    
    if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.numbers' );
    }      
  };
})(jQuery);
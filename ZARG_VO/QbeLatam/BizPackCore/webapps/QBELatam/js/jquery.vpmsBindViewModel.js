//parameters:
//actionUrlAttr - attribute name that contains action url
//model - model name to render to this element (model.widget = widget name if 'widget' not set)
//widget - widget name to render
//actionUrl - if given will overwrite attribute actionUrlAttr
/*
 * JSON encoding
 * boolean
 * isElement=true : 	e=1
 * isGroup=true : 		g=1
 * isChoices=true : 	c=1
 * isTree=true : 		t=1
 * multiple=true :		m=1
 * isNavigation :		n=1
 * hidden=true: 		h=1
 * format=date,number: 	f=date,number
 * composite=true : 	s=1
 * get all data : 		a=1
 * caption isParam: 	p=1
 * group is visited: 	v=1
 * can add: 			ca=1
 * can remove: 			cr=1
 * sync updated: 		su=1
 * 
 *
 * lists
 * elements : e_
 * groups : g_
 * captions : k_
 * choices : c_
 * template : t_
 * column names: cn_
 * column required: cr_
 * data: d_
 * 
 * values
 * name : _n
 * caption : _k
 * tooltip : _t
 * error : _e
 * editable : _a
 * relevant : _r
 * required : _q
 * value : _v
 * text : _x
 * parent instance (array): _p
 * can add: _ca=1
 * can remove: _cr=1
 * add caption: _ac
 * remove caption: _rc
 * hash: _h
 * navigation page: _np
 * 
 */
(function($){
	 jQuery.fn.findAndSelf = function(selector) {
		    return this.find(selector).add(this.filter(selector));
		  };
	 $.vpmsBindViewModel = {
		tmplScriptType : "text/html",
		options : {	
					containerClass : "vpms-mvvm-container",
					actionUrlAttr : "url",
					pdfUrlAttr : "pdfurl",
					resourceUrlAttr : "resurl",
					fileUrlAttr : "fileurl",
					storeKey : "vpmsBindViewModelStore",
					defaultDateFormat: 'dd/mm/yy',
					defaultVpmsDateFormat: 'dd/mm/yy',					
					isOfflineMode : false, 
					oneColContainerWidth : 400,
					blockUIDelay: 500, 
					bindAfterUpdate: false				 
		},
		debug:   function(msg, restart){
				 	if($.vpmsBindViewModel.isDebug===true){
				 		if(restart===true)
							lastDebugTime = 0;
						var interval = 0;
						var now = (new Date()).getTime();
						if(lastDebugTime)
							interval = now - lastDebugTime;
						lastDebugTime = now;
						if(typeof console != "undefined" && typeof console.log != "undefined"){
							try{
								console.log(interval + ": " + msg);
								return  true;
							}catch(e){
								return false;
							}
						} 
					}
		},	
		cloneObject: function(obj) {
			var newObj = (obj instanceof Array) ? [] : {};
			for(var i in obj) {
				if(obj[i] && typeof obj[i]=="object")
					newObj[i] = $.vpmsBindViewModel.cloneObject(obj[i]);
				else
					newObj[i] = obj[i];
			}
			return newObj;
		},
		isDebug : true,
		setVisitedOnAutocommit: true,
		roleBindings : [],
		properties : [],
		templates : [],
		models : [],
		urlState : {},
		pushState : function(containerId, elementName, value, dontUpdateModel){
			var name = containerId;
			if(elementName)
				name = name + "_" + elementName;
			var state = {};
			state[name] = value;
			if($.bbq){
				if(dontUpdateModel)
					//this hack will set value of previous hash as it was before 
					$.vpmsBindViewModel.urlState[name] = value;
				$.bbq.pushState( state );
			}else
			if(window.amplify){
				if(amplify.store.sessionStorage(name)!=value && !dontUpdateModel){
					amplify.store.sessionStorage(name, value);
					$.vpmsBindViewModel.updateVPMSModel($('#' + containerId), true);
				}else
					amplify.store.sessionStorage(name, value);				
			}else
			{
				if($.vpmsBindViewModel.urlState[name]!=value && !dontUpdateModel){
					$.vpmsBindViewModel.urlState[name] = value;
					$.vpmsBindViewModel.updateVPMSModel($('#' + containerId), true);
				}else
					$.vpmsBindViewModel.urlState[name] = value;
			}
		},
		getState : function(containerId, elementName){
			var name = containerId;
			if(elementName)
				name = name + "_" + elementName;			
			if($.bbq)
				return $.bbq.getState(name);
			else
			if(window.amplify)
				return amplify.store.sessionStorage(name);
			else
				return $.vpmsBindViewModel.urlState[name];
		},
		getAllStates : function(containerId){
			var result = [];
			var states = $.vpmsBindViewModel.urlState;
			if($.bbq)
				states = $.deparam.fragment();
			else if(window.amplify)
				states = amplify.store.sessionStorage();
			
			for(var name in states){
				if(name.indexOf(containerId)===0){
					result.push(name);
				}
			}				
			return result;
		},
		removeState : function(states, dontUpdateModel){
			if(!$.bbq || dontUpdateModel){
				if($.isArray(states)){
					for(var i=0; i<states.length; i++){
						if(!$.bbq && window.amplify)
							amplify.store.sessionStorage(states[i], null);
						else						
							$.vpmsBindViewModel.urlState[states[i]] = undefined;
					}
				}else{
					if(!$.bbq && window.amplify)
						amplify.store.sessionStorage(states, null);
					else
						$.vpmsBindViewModel.urlState[states] = undefined;
				}
			}
			if($.bbq)
				$.bbq.removeState(states);
		},
		htmlEncode : function(value){
		    if (value) {
		    	return String(value)
	            	.replace(/&/g, '&amp;')
	            	.replace(/"/g, '&quot;')
		    	    .replace(/'/g, '&#39;')
		    	    .replace(/</g, '&lt;')
		    	    .replace(/>/g, '&gt;')
		    	    .replace(/\r\n/g, '<br>').replace(/\n/g, '<br>').replace(/\r/g, '<br>');
		    } else {
		        return '';
		    }			
		},
		templateExists : function(id){
			return ($("body").find("script[id='" + id + "']").length!==0);
		},
		getTemplate : function(name){
			if(name)
				for(var i=0; i<this.templates.length; i++){
					if(this.templates[i].name==name){
						return this.templates[i];
					}
				}				
		},
		pushTemplate : function(template){
			if(template && template.name){
				var index = -1;
				for(var i=0; i<this.templates.length; i++){
					if(this.templates[i].name==template.name){
						index = i;
						break;
					}
				}
				if(index>=0){
					template.parent = this.templates[index];
					this.templates[index] = template;
				}else
					this.templates.push(template);				
			}
		},
		getGroupTemplate : function(model){			
			if(model.template){
				if($.isFunction(model.template))
					return model.template();
				else
					return $.vpmsBindViewModel.cloneObject(model.template);
			}						
			else
				return undefined;			
		},
		getProperty : function(id){
			if(id)
				for(var i=0; i<this.properties.length; i++){
					if(this.properties[i].id==id){
						return this.properties[i];
					}
				}				
		},
		pushProperty : function(property){
			if(property && property.id){
				var index = -1;
				for(var i=0; i<this.properties.length; i++){
					if(this.properties[i].id==property.id){
						index = i;
						break;
					}
				}
				if(index>=0){
					this.properties[index] = property;
				}else
					this.properties.push(property);				
			}
		},		
		getModel : function(name){
			if(name)
				for(var i=0; i<this.models.length; i++){
					if(this.models[i].name==name){
						return this.models[i];
					}
				}				
		},	
		getFullPath : function(model, path){
			if(model.isElement || model.isGroup){
				var name = $.vpmsBindViewModel.unwrapObservable(model, 'name');
				if(model.index!==undefined)
					name = name + "[" + $.vpmsBindViewModel.unwrapObservable(model, 'index') + "]";
				if(!path)
					path = name;
				else
					path = name + "|" + path;
			}
			var parent = $.vpmsBindViewModel.getParent(model);
			if(!parent)
				return path;
			else
				return $.vpmsBindViewModel.getFullPath(parent, path);
		},		
		autoCommit : function(elem){
			if($(elem).is(":disabled"))
				return;
			var $container = $(elem).closest("." + $.vpmsBindViewModel.options.containerClass);
			if($container){
				var settings = $container.data('settings');
				if(!settings || settings.updating)
					return;								
				setTimeout(function(){
					$.vpmsBindViewModel.updateVPMSModel($container, true);					
				}, 1);
			}						
		},
		isModelUpdating : function(elem){
			var $container = $(elem).closest('.' + $.vpmsBindViewModel.options.containerClass);
			if($container){
				var settings = $container.data('settings');
				if(settings)
					return settings.updating;
			}			
		},
		isModelIniting : function(elem){
			var $container = $(elem).closest('.' + $.vpmsBindViewModel.options.containerClass);
			if($container){
				var model = $container.data('model');
				if(model)
					return model.init;
			}			
		},
		isChoicesWidget : function (widget){
			return widget=="combobox" || widget=="radiogroup" || widget=="autocomplete";
		},
		blockUI : function(model, text, isOverlay){
			var $container =  $('#' + model.containerId);
			if($container.length>0){				
				var $blockUI = $container.next('.vpms-block-ui');
				if($blockUI.length===0)
					$blockUI = $container.children('.vpms-block-ui');
				if($blockUI.length>0){
					if($blockUI.is(":visible"))
						return;
					$blockUI.find('.vpms-block-ui-text').text(text);
				}else{
					$blockUI = $('<div class="vpms-block-ui"><div style="height:50%"></div><div class="vpms-block-ui-text">' + text + '</div></div>');
					$blockUI.insertAfter($container);
				}
				
				if(isOverlay){
					$blockUIOverlay = $container.children('.vpms-block-ui-overlay');
					if($blockUIOverlay.length===0){
						$blockUIOverlay = $('<div class="vpms-block-ui-overlay"></div>');
						$container.append($blockUIOverlay);
					}						
					$container.append($blockUI);
					$blockUIOverlay.show();
				}else{
					$blockUI.insertAfter($container);
					$container.hide();
				}
				$blockUI.show();	
			}
		},
		unblockUI : function(model){
			var $container =  $('#' + model.containerId);
			if($container.length>0){
				var $blockUI = $container.next('.vpms-block-ui');
				if($blockUI.length===0)
					$blockUI = $container.children('.vpms-block-ui');				
				$blockUI.hide();				
				var $blockUIOverlay = $container.children('.vpms-block-ui-overlay');
				$blockUIOverlay.hide();				
				$container.show();			
			}
		},
		convertTreeviewToPages: function(model, rootWidget, internalWidget){
			if(model.widget=="treeview" && model.groups && model.groups.length>0){
				if(!internalWidget)
					internalWidget = "pages";
				if(!rootWidget)
					rootWidget = "pages";				
				var convertGroup = function(_model){
					if(_model.groups && _model.groups.length>0){
						if(!_model.elements)
							_model.elements = [];
						var staticPages = {
							name : _model.name + "_" + internalWidget,
							widget : internalWidget,
							groups : []		
						};
						for(var i = 0; i<_model.groups.length; i++){
							if(_model.groups[i].multiple){
								_model.groups[i].widget = internalWidget;
								_model.elements.push(_model.groups[i]);
								if(_model.groups[i].template)
									$.vpmsBindViewModel.convertTreeviewToPages(_model.groups[i].template);						
							}else{
								if(!_model.groups[i].widget)
									_model.groups[i].widget = "superWidget";
								staticPages.groups.push(_model.groups[i]);
								convertGroup(_model.groups[i]);
							}
						}
						if(staticPages.groups.length>0){
							_model.elements.push(staticPages);
						}				
					}
					_model.groups = undefined;					
				};
				for(var groupsCount=0; groupsCount<model.groups.length; groupsCount++){
					convertGroup(model.groups[groupsCount]);
				}
				model.widget = rootWidget;
			}
		},		
		applyBindings : function(container){
			var model = $(container).data('model');
			if($.vpmsBindViewModel.isMobile){
				//for mobile version bind template in the same div, otherwise it will brake mobile page structure
				$(container).addClass("vpms-" + model.widget +"-widget");
				//this is a hack to avoid memory leak causing jQuery Template plugin
				$(container).removeData("tmpl");$(container).removeData("tmplItem");					
				$(container).attr('data-bind', "template: { name: \"" + model.widget + "\" }");
			}else{
				var $product = $("<div class=\" ui-widget vpms-" + model.widget + "-widget\" data-bind='template: { name: \"" + model.widget + "\" }'></div>");
				$(container).append($product);					
			}
			ko.applyBindings(model, container);
		},
		initModelObservable : function(model, container){
			return model;
		},
		initModelItem : function(item, defaultValue){
			if(item!==undefined){
				if($.isFunction(item))
					return item;
			}else
				item = defaultValue;
			if($.isArray(item))
				return ko.observableArray(item);
			else
				return ko.observable(item);
		},
		initGroupsNavigation : function(model){
			if(!model.settings || !model.settings.rowsPerPage)
				return;
			model.onlyRelevant = true;
			model.navigation = {
				pageNumber : 1,
				rowsPerPage : model.settings.rowsPerPage,
				totalRecords : 0,
				visible : $.vpmsBindViewModel.initModelItem(undefined, false),
				hasNext : $.vpmsBindViewModel.initModelItem(undefined, false),
				hasPrev : $.vpmsBindViewModel.initModelItem(undefined, false),
				label : $.vpmsBindViewModel.initModelItem(undefined, "")					
			};
			model.navigationUpdate = function(data){
				this.navigation.pageNumber = data.page;
				this.navigation.totalRecords = data.records;
				this.navigation.totalPages = data.total;
				$.vpmsBindViewModel.updateModelItem(this.navigation, 'visible',this.navigation.totalRecords > this.navigation.rowsPerPage);
				var startIndex = (this.navigation.pageNumber-1)*this.navigation.rowsPerPage+1;
				var endIndex = Math.min(this.navigation.totalRecords, this.navigation.pageNumber*this.navigation.rowsPerPage);
				$.vpmsBindViewModel.updateModelItem(this.navigation, 'label', startIndex + '-' + endIndex + '/' +  this.navigation.totalRecords);
				$.vpmsBindViewModel.updateModelItem(this.navigation, 'hasNext', this.navigation.rowsPerPage*this.navigation.pageNumber < this.navigation.totalRecords);
				$.vpmsBindViewModel.updateModelItem(this.navigation, 'hasPrev', this.navigation.pageNumber > 1);
			};			
			model.navigationNextPage = function(event){
				if($.vpmsBindViewModel.unwrapObservable(this.navigation.hasNext)){					
					this.navigation.pageNumber++;
					this.spliceGroups = true;
					jQuery.vpmsBindViewModel.updateVPMSModel(jQuery(event.target).closest('.' + $.vpmsBindViewModel.options.containerClass), true, true);
				}
			};
			model.navigationPrevPage = function(event){
				if($.vpmsBindViewModel.unwrapObservable(this.navigation.hasPrev)){
					this.navigation.pageNumber--;
					this.spliceGroups = true;
					jQuery.vpmsBindViewModel.updateVPMSModel(jQuery(event.target).closest('.' + $.vpmsBindViewModel.options.containerClass), true, true);
				}
			};			
		},
		updateModelItem : function(model, name, value){
			if($.isFunction(model[name]))
				model[name](value);
			else
				model[name] = value;
		},
		unwrapObservable : function(model, name){
			if(name)
				return ko.utils.unwrapObservable(model[name]);
			else
				return ko.utils.unwrapObservable(model);				
		},		
		dependentObservable : function(model, functionName, read, write){
			model[functionName] = ko.dependentObservable({read:read, write:write, owner:model});
		}, 
		initArrayElement : function(element){
			return element;
		},		
		getArrayElement : function(element){
			return element;		
		},		
		initModel_widget : function(model, widget){
			if(!widget)
				widget = model.widget;
			var t = this.getTemplate(widget);
			while(t){
				if(t.initModel){
					t.initModel(model);
					break;
				}
				t = t.parent;
			}
		},	
		beforeUpdateModel_widget : function(model, data, settings, widget){
			if(!widget)
				widget = model.widget;
			var t = $.vpmsBindViewModel.getTemplate(widget);
			while(t){
				if(t.beforeUpdateModel){
					t.beforeUpdateModel(model, data, settings);
					break;
				}
				t = t.parent;
			}
		},		
		updateModel_widget : function(model, data, settings, widget){
			if(!widget)
				widget = model.widget;
			var t = $.vpmsBindViewModel.getTemplate(widget);
			while(t){
				if(t.updateModel){
					t.updateModel(model, data, settings);
					break;
				}
				t = t.parent;
			}
		},
		initModel_customProperties : function(model){
			if(model.dynamics){
				model._dynamics = model.dynamics;
				model.dynamics = undefined;
				for(var key in model._dynamics){
					if(model[key] === undefined){
						var property = $.vpmsBindViewModel.getProperty(key);
						model[key] = $.vpmsBindViewModel.initModelItem(model[key], (property&&property.defaultValue!==undefined)?property.defaultValue:model._dynamics[key]!==undefined?model._dynamics[key]:"");
					}
				}				
			}
		},
		applyBehavior_customDynamicProperties : function(model, data){
			if (model._dynamics && data){
				for (var key in model._dynamics){
					if(data[key] != $.vpmsBindViewModel.unwrapObservable(model, key)){
						$.vpmsBindViewModel.updateModelItem(model, key, data[key]?data[key]:"");
						var property = $.vpmsBindViewModel.getProperty(key);
						if(property && property.applyBehavior !== undefined && $.isFunction(property.applyBehavior)){
							property.applyBehavior(model);
						}
					}
				}
			}
		},
		applyBehavior_customStaticProperties : function(model){
			if (model.settings){
				for (var key in model.settings){
					var property = $.vpmsBindViewModel.getProperty(key);
					if(property && property.applyBehavior !== undefined && $.isFunction(property.applyBehavior)){
						property.applyBehavior(model);
					}
				}
			}
		},
		updateModel_dependencies : function(model){
			if(model.settings && model.settings.clientVisible && !model._clientVisibleHandler){			
				var id = model.settings.clientVisible;
				var value = "1";
				var idx = model.settings.clientVisible.indexOf('|');
				if(idx>0){
					id = model.settings.clientVisible.substring(0, idx);
					value = model.settings.clientVisible.substring(idx+1);
				}
				model._clientVisibleData = {name: id, value: value};
				model._clientVisibleHandler = function(name, value){
					if(!name || !this._clientVisibleData.name)
						return;
					if((this._clientVisibleData.name.toUpperCase() == name.toUpperCase()) && (this.relevant!==undefined))
						$.vpmsBindViewModel.updateModelItem(this, 'relevant', this._clientVisibleData.value==value);
				}; 
				var triggerModel = $.vpmsBindViewModel.findModelByName(model, id);
				if(triggerModel){
					if(!triggerModel._dependencies)
						triggerModel._dependencies = [];
					triggerModel._dependencies.push({property:'visible', model:model});
				}			
			}			
		},
		updateModel_afterGroupAdded : function(model, group, data){
			if(model.afterGroupAdded)
				model.afterGroupAdded(model, group, data);
			afterMultipleGroupAdded(group, data);
		},		
		updateModel_beforeGroupAdded : function(model, modelGroup, data, dataGroup){
			if(model.beforeGroupAdded)
				model.beforeGroupAdded(model, modelGroup, data, dataGroup);
		},		
		initModel_captionsList: function(model, list){
			var captions = $.vpmsBindViewModel.unwrapObservable(model.captions);
			for(var i=0; i<captions.length; i++){
				for(var k=0; k<list.length; k++){
					if(list[k]==$.vpmsBindViewModel.unwrapObservable(list[i].id)){
						list.splice(k, 1);
						break;
					}
				}
			}
			for(i=0; i<list.length; i++){
				model.captions.push($.vpmsBindViewModel.initModel_caption({id:list[i], name:list[i]}));
			}					
		},		
		initModel_caption : function(model){	
			if(!model.name) model.name = "undefined";
			if(!model.id) model.id = model.name;
			model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.id);
			return model;
		},
		initModelRoot : function(model, containerId, settings){
		},
		initModel : function(model, containerId){
				model.containerId = containerId;
				if(model.html)
					model = $.vpmsBindViewModel.generateModelFromHtml(model, model.html);
				//save stattic disabled declaration
				if(model.disabled!==undefined)
					model._disabled = model.disabled;					
				if(model.multiple)
					initModel_multiple(model);
				this.initModel_widget(model);
				this.initModel_customProperties(model);
				//set disabled for any case if it is not set yet
				model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
				if($.vpmsBindViewModel.isChoicesWidget(model.widget))
					initModel_choices(model);
				initModel_groups(model, containerId);
				initModel_elements(model, containerId);
				initModel_captions(model);
				if(model.initModel)
					model.initModel();
				if(model.settings!==undefined && model.settings.autocommit!==undefined)
					model.autocommit = model.settings.autocommit;
				if(model.hasLabel===undefined && model.caption!==undefined && model.required!==undefined)
					model.hasLabel = true;
				return model;
		},
		onModelValueChange: function(model, event){
			if(model._dependencies && model._dependencies.length){
				//onModelValueChange - value is still previous - deferred call			
				setTimeout(function(){
					for(var i=0; i<model._dependencies.length; i++){
						var dependency = model._dependencies[i];
						if(dependency.model && dependency.model._clientVisibleHandler){
								dependency.model._clientVisibleHandler($.vpmsBindViewModel.unwrapObservable(model, 'name'), $.vpmsBindViewModel.unwrapObservable(model, 'value'));
						}
					}
				}, 1);											
			}
		},		
		initModel_parent : function(model, parent){
			model.parent = parent;
		},
		getParent : function(model){
			return model.parent;
		},
		getParentHasItem : function(model, item, value){
			var parent = $.vpmsBindViewModel.getParent(model);
			if(parent){
				if(parent[item]!==undefined){
					if(value!==undefined){
						if($.vpmsBindViewModel.unwrapObservable(parent, item)==value)
							return parent;
					}else{
						return parent;
					}
				}
				return $.vpmsBindViewModel.getParentHasItem(parent, item, value);
			}else{
				return undefined;
			}
		},
		getParentWidget : function(model, widget){
			return $.vpmsBindViewModel.getParentHasItem(model, "widget", widget);
		},
		findModelByName : function(model, name, excludeName){
			if(!model)
				return undefined;
			var modelName = $.vpmsBindViewModel.unwrapObservable(model, 'name');
			if(modelName){
				var result = $.vpmsBindViewModel.findNextModelByName(model, name, excludeName);
				if(result)
					return result;
			}
			var _parent=$.vpmsBindViewModel.getParent(model);
			if(_parent)
				return $.vpmsBindViewModel.findModelByName(_parent, name, modelName?modelName:excludeName);
		},
		findNextModelByName : function(model, name, excludeName){
			if(name == excludeName)
				return undefined;
			var _model = $.vpmsBindViewModel.getArrayElement(model);
			var _modelName = $.vpmsBindViewModel.unwrapObservable(_model, 'name'); 
			if(_modelName && _modelName.toUpperCase()==name.toUpperCase())
				return _model;
			var modelElements = $.vpmsBindViewModel.unwrapObservable(_model, 'elements');
			var i, result;
			if(modelElements)
				for(i=0;i<modelElements.length;i++){
					result = $.vpmsBindViewModel.findNextModelByName(modelElements[i], name, excludeName);
					if(result)
						return result;
				}
			var modelGroups = $.vpmsBindViewModel.unwrapObservable(_model, 'groups');
			if(modelGroups)
				for(i=0;i<modelGroups.length;i++){
					result = $.vpmsBindViewModel.findNextModelByName(modelGroups[i], name, excludeName);
					if(result)
						return result;
				}
			return undefined;
		},
		updateSettings : function(container, data){
			if(data){
				var $container = $(container);
				var settings = $container.data('settings');
				if(!settings) settings = {};
				if(!settings.locale)
					settings.locale = {};
				if(data.ds)
					settings.locale.decimalSeparator = data.ds;
				if(data.ts)
					settings.locale.thousandsSeparator = data.ts;				
				if(data.pc)
					settings.pdfcookiename = data.pc;
				settings.locale.dateFormat = data.df?$.vpmsBindViewModel.simpleDateFormatToDateFormat(data.df):$.vpmsBindViewModel.options.defaultDateFormat;
				settings.locale.vpmsDateFormat = data.vdf?$.vpmsBindViewModel.simpleDateFormatToDateFormat(data.vdf):$.vpmsBindViewModel.options.defaultVpmsDateFormat;
				if(data.quoteId){
					settings.quoteId = data.quoteId;
				}
				if(data.version){
					settings.version = data.version;
				}				
				if(data.loc){
					settings.locale.locale = data.loc;
					if(Date.changeCultureInfo)
						Date.changeCultureInfo(data.loc);
				}
				$container.data('settings', settings);
				return settings;
			}
		},			
		updateModel : function(model, data, settings, _disabled){
			if(!settings.detached)
				$.vpmsBindViewModel.updateModel_dependencies(model);			
			if(data){
				$.vpmsBindViewModel.beforeUpdateModel_widget(model, data, settings);
				if(model.id!==undefined){
					if(data.id)
						$.vpmsBindViewModel.updateModelItem(model,'id', data.id);
					else
						$.vpmsBindViewModel.updateModelItem(model,'id', model.name);
				}				
				//save hash
				if(data._h)
					model.hash = data._h;					
				if($.vpmsBindViewModel.isChoicesWidget(model.widget)){
					updateModel_mergeChoices(model, data);
				}			
				if(model.isElement)
					updateModel_element(model, data, settings);				
				if(model.isGroup)
					updateModel_group(model, data, settings);	
				updateModel_data(model, data);
				updateModel_captions(model, data);
				//disabled - update disabled from model always, and global disabled only if !ignoreDisabled				
				if(!model.settings || !model.settings.ignoreDisabled)
					$.vpmsBindViewModel.updateModelItem(model, 'disabled', (_disabled===true || model._disabled===true || (data._a===false)));
				else
					$.vpmsBindViewModel.updateModelItem(model, 'disabled', data._a===false);
				model.elements = updateModel_list(model, model.elements, data.e_, settings);
				if(model.multiple)
					updateModel_mergeGroups(model, data, settings);
				else
					model.groups = updateModel_list(model, model.groups, data.g_, settings);
				$.vpmsBindViewModel.updateModel_widget(model, data, settings);
				if(!settings.detached){
					if(!model.initialUpdated)
						$.vpmsBindViewModel.applyBehavior_customStaticProperties(model);
					$.vpmsBindViewModel.applyBehavior_customDynamicProperties(model, data);
				}
				model.initialUpdated = true;
			}
			return model;
		},		
		element : function(elements, name, def){
			if(elements){
				var list = $.vpmsBindViewModel.unwrapObservable(elements);
				for(var i=0; i<list.length; i++){
					var element = $.vpmsBindViewModel.getArrayElement(list[i]);
					if($.vpmsBindViewModel.unwrapObservable(element, 'name')==name)
						return element;
				}
			}
			if(def===undefined)
				return {name:name, caption:name, value:name, text:name};
			else
				return def;
		},	
		elementById : function(elements, id){
			if(elements){
				var list = $.vpmsBindViewModel.unwrapObservable(elements);
				for(var i=0; i<list.length; i++){
					var element = $.vpmsBindViewModel.getArrayElement(list[i]);
					if($.vpmsBindViewModel.unwrapObservable(element, 'id')==id)
						return element;
				}
			}
		},			
		readFormattedValue : function(model, value){
			if(model.format && model.format.indexOf("number")>=0 && jQuery.numbers){
				return jQuery.numbers.toString(value, model.settings);
			}
			else
			if(value instanceof Date){
				var dateFormat = (model.settings && model.settings.dateFormat)?model.settings.dateFormat: $.vpmsBindViewModel.options.defaultDateFormat;
				if (jQuery.datepicker && !$.vpmsBindViewModel.isMobile)
					return jQuery.datepicker.formatDate(dateFormat, value);
				else
					return  $.vpmsBindViewModel.FormatShortDate(value,dateFormat);				
			}
			else
				return value;
		},
		writeFormattedValue : function(model, value){
			if(model.format && model.format.indexOf("number")>=0 && jQuery.numbers){
				$.vpmsBindViewModel.updateModelItem(model, 'value', jQuery.numbers.toNumber(value, model.settings));
			}
			else
			if(model.format && model.format.indexOf("date")>=0){				
				var dateFormat = (model.settings && model.settings.dateFormat)?model.settings.dateFormat: $.vpmsBindViewModel.options.defaultDateFormat;
				try{
					if (jQuery.datepicker && !$.vpmsBindViewModel.isMobile){
						var date = jQuery.datepicker.parseDate(dateFormat, value);
		    			var strDate = jQuery.datepicker.formatDate(dateFormat, date);
		    			//forgiving format
		    			if(strDate!=value && Date )
		    				date = Date.parse(value);
		    			if(!date)
		    				date=""; //nulls will be not commited
						$.vpmsBindViewModel.updateModelItem(model, 'value', date);
					} else
						$.vpmsBindViewModel.updateModelItem(model, 'value', new Date(value));
				}catch(err){
					$.vpmsBindViewModel.updateModelItem(model, 'value', value);
				}											
			}				
			else
				$.vpmsBindViewModel.updateModelItem(model, 'value', value);			
		},
		hideToolTip : function(elem){
			$('.ui-tooltip').hide();
		},
		alphanumericSelector : function(str){
			if(str)
				return str.replace(/[^a-zA-Z0-9_]/g,'_');
			else
				return str;			
		},
		escapeSelector : function(str){
			if(str)
				return str.replace(/([ #;?&,.+*~\':"!^$[\]()=>|\/@])/g,'\\$1');
			else
				return str;
		},
		uniqueSelector : function(str){
			if(!str)
				str="";
			str = str.replace(/([ #;?&,.+*~\':"!^$[\]()=>|\/@])/g,'_');
			var newDate = new Date();
		    return str + newDate.getTime(); 
		},
		getElementSettings : function(element){
			var $element=$(element);
			var settingsJSON = $element.attr('settings');
			if(settingsJSON)
				return eval('(' + settingsJSON + ')');			
		},
		getDatepickerOptions : function(modelSettings, settings){
			var result = {	
					changeMonth: true,
					changeYear: true
			};
			if(modelSettings)
				$.extend(result, modelSettings);
			if(!result.dateFormat)
				result.dateFormat = (settings && settings.dateFormat)?settings.dateFormat:
									(settings && settings.locale && settings.locale.dateFormat)?settings.locale.dateFormat:
									$.vpmsBindViewModel.options.defaultDateFormat;
			result.beforeShow = function(input, inst) {
				if($.vpmsBindViewModel.isMobileAgent)
					$(inst.dpDiv).addClass('vpms-datepicker-mobile');
				else
					$(inst.dpDiv).addClass('vpms-datepicker');				
				$.vpmsBindViewModel.hideToolTip(input);				
				$(input).addClass('vpms-no-tooltip');
			};
			result.onClose = function(dateText, inst){
				$(inst.input).removeClass('vpms-no-tooltip');
			};
			result.onSelect = function(dateText, inst){			
				inst.input.trigger('change');
				inst.input.focus();
				setTimeout(function(){inst.input.datepicker('hide');}, 1);
			};
			result.hideOnSelect = false;
			return result;			
		},
		initDateEditor : function($container, settings){
			if ($.vpmsBindViewModel.isMobile) {
				//No jQuery DatePicker on jQuery mobile
			} else 			
				$container.find('.vpms-edit-widget .date').each(function(){
					var $date=$(this);
					var dateSettings = $.vpmsBindViewModel.getElementSettings($date);
					var datePickerOptions = $.vpmsBindViewModel.getDatepickerOptions(dateSettings, settings);
					$date.removeClass('hasDatepicker').datepicker(datePickerOptions);
					if($date.data('date_value')){
	    				$date.datepicker('setDate', $date.data('date_value'));
						$date.data('date_value', null);					
					}

				});			
		},	
		simpleDateFormatToDateFormat : function(dateFormat){
			if($.datepicker && !$.vpmsBindViewModel.isMobile){
				//day in year D -> o
				dateFormat = dateFormat.replace('DDD', 'oo');
				dateFormat = dateFormat.replace('D', 'o');
				//day in week E -> D
				dateFormat = dateFormat.replace('EEEE', 'DD');
				dateFormat = dateFormat.replace('EEE', 'D');
				//year
				dateFormat = dateFormat.replace('yyyy', '00');
				dateFormat = dateFormat.replace('yy', 'y');
				dateFormat = dateFormat.replace('00', 'yy');
				//month M -> m
				dateFormat = dateFormat.replace('MMMM', '00');
				dateFormat = dateFormat.replace('MMM', '0');
				dateFormat = dateFormat.replace('MM', 'mm');
				dateFormat = dateFormat.replace('M', 'm');
				dateFormat = dateFormat.replace(/0/g, 'M');
			}
			return dateFormat;
		},
		DateToString: function(d) {
		    function pad(n){
		        return n<10 ? '0'+n : n;
		    }
		    if(d){
			    return d.getFullYear()+'-'+pad(d.getMonth()+1)+'-'+pad(d.getDate());
		    }
		    return "";
		},
		FormatShortDate: function(d,pattern) {
			function pad(n){
		        return n<10 ? '0'+n : n;
		    }
			function shortMonth(n){
				var monthShortNames = ["Jan", "Feb", "!ar", "Apr", "!ay", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				return monthShortNames[n-1];
			}
			if(!pattern)pattern='yyyy-MM-dd';
		    if(d) {
		    	var _y=''+d.getFullYear(),_m=d.getMonth()+1,_d=d.getDate();
		    	return pattern.replace('yyyy', _y)
		        .replace('yy', _y.substring(2))
		        .replace('MMM', shortMonth(_m))
		        .replace('MM', pad(_m))
		        .replace('M', _m)
		        .replace('!', 'M')
		        .replace('dd', pad(_d))
		        .replace('d', _d);
		    }
		    return "";
		},
		StringToDate: function(s) {
			if(s instanceof Date)
				return s;
			if (typeof(s)!="string" || s.length != 10)
				return null;
		    return new Date(s.substr(0,4),s.substr(5,2)-1,s.substr(8,2));	    
		},
		getAccordionOptions : function(active){
			var result = {autoHeight : false};
			if(active)
				result.active = active;
			return result;			
		},		
		getNumbersOptions : function(modelSettings, settings){
			var result = {};
			if(modelSettings)
				$.extend(result, modelSettings);
			if(!result.decimalSeparator)
				result.decimalSeparator = (settings && settings.decimalSeparator)?settings.decimalSeparator:
					(settings && settings.locale && settings.locale.decimalSeparator)?settings.locale.decimalSeparator:
					undefined;
			if(!result.thousandsSeparator)
				result.thousandsSeparator = (settings && settings.thousandsSeparator)?settings.thousandsSeparator:
					(settings && settings.locale && settings.locale.thousandsSeparator)?settings.locale.thousandsSeparator:
					undefined;			
			return result;
		},		
		adjustElementsWidth : function(container){
			$(container).each(function(index, value){
				var settings = $(this).data('settings');
				if(settings && settings.oneColContainerWidth && !settings.singleColumn){
					var width = $(this).width();
					var $treeview = $(this).find('ul.vpms-treeview:first');
					if($treeview.length>0)
						width = width - $treeview.width();
					if (width < settings.oneColContainerWidth)
						$(this).addClass('vpms-single-column');//one column
					else 
						$(this).removeClass('vpms-single-column');//two and more columns
				}
			});
		},
		afterModelUpdate : function(container, model){			
			$(container).find('.vpms-afterRender').each(function(){
				$.vpmsBindViewModel.afterRender($(this), model.containerId);
				$(this).removeClass('vpms-afterRender');
			});
			if(model.actions){
				for(var name in model.actions){
					if($.vpmsBindViewModel.roleBindings[name] && $.vpmsBindViewModel.roleBindings[name].update)
						$.vpmsBindViewModel.roleBindings[name].update(model.actionsModels?model.actionsModels[name]:undefined, model, model.actions[name]);
				}				
				model.actions = undefined;
				model.actionsModels = undefined;
			}
			var settings = $(container).data('settings');
         	if(settings && settings.afterModelUpdate)
         		settings.afterModelUpdate(container, model);
         	
		},
		afterRenderInit: function($this){
			//tooltip	
			if($.vpmsBindViewModel.isMobileAgent){
				$this.on('click','.vpms-tooltip-error',function(event){ 
			    	$('<div class="ui-widget vpms-tooltip-error-text">' + $.vpmsBindViewModel.htmlEncode($(this).attr("error")) + '</div>').dialog({ 
					width:500, 
					closeOnEscape: true, 
					modal: true, 
					open: function(){$('.ui-widget-overlay').click(function() { $(".vpms-tooltip-error-text").dialog("close"); });},
					buttons: { "Close": function() { $(this).dialog("close"); } } });
			        return false; 
				});				
			}
			else{
				$( $this ).tooltip({
				      items: ".vpms-tooltip-error:not([disabled]):not(.vpms-no-tooltip),[tooltip]:not([disabled]):not(.vpms-no-tooltip)",
				      content: function() {
				        var element = $( this );
						   if($(this).attr("error"))
							   return '<div class="vpms-tooltip-error-text">' + $.vpmsBindViewModel.htmlEncode($(this).attr("error")) + '</div>';
						   else
							   if($(this).attr("tooltip"))
								   return '<div class="vpms-tooltip-text">' + $.vpmsBindViewModel.htmlEncode($(this).attr("tooltip")) + '</div>';				        
				      }
				});	
			}
			
			if(!$this.data("vpms-container-events-inited")){
				$this.on('change', '.vpms-autocommit', function(){
					$.vpmsBindViewModel.autoCommit(this);
				});
				//pages
				$this.on('mouseover', '.vpms-tabs-widget ul li', function(){
					if(!$(this).hasClass('ui-state-active'))
						$(this).addClass('ui-state-hover');
				});
				$this.on('mouseout', '.vpms-tabs-widget ul li', function(){				
						$(this).removeClass('ui-state-hover');
				});
				$this.data("vpms-container-events-inited", true);
			}
		},
		afterRender : function($this, containerId){
			var $container = containerId?$('#' + containerId):$this.closest('.' + $.vpmsBindViewModel.options.containerClass);
			var settings = $container.data('settings');
			var updating = settings?settings.updating:false;
			try{
				if(settings){
					settings.updating = true;
				}
				if($.vpmsBindViewModel.isMobileAgent)
					$container.addClass("vpms-mvvm-container-mobile");
				$.vpmsBindViewModel.adjustElementsWidth($container);

				//button
				$this.find('.vpms-button-widget > button').button();
				if($.vpmsBindViewModel.isMobileAgent)
					$this.find('.vpms-tooltip-error').button(); 			
				//datepicker
				if($.vpmsBindViewModel.initDateEditor)
					$.vpmsBindViewModel.initDateEditor($this, settings);
				//numbers
				$this.find('.vpms-edit-widget .number').each(function(){
					var $num=$(this);
					var numSettings = $.vpmsBindViewModel.getElementSettings($num);
					$num.numbers($.vpmsBindViewModel.getNumbersOptions(numSettings, settings));
				});
				//accordion
				$this.find('.vpms-accordion').each(function(){
					var $accordion = $(this);
					var activeChildren = $accordion.children('.vpms-accordion-element-header[relevant="true"]');
					var active = activeChildren.length===0?undefined:activeChildren[0];
					$accordion.accordion($.vpmsBindViewModel.getAccordionOptions(active));			
				});
				//treeview
				if($.fn.treeview)
					$this.findAndSelf('.vpms-treeview-widget').find('ul.vpms-treeview:first').treeview();
				if(settings.customAfterRender)
					settings.customAfterRender($this, containerId);
				//mask
				if($.fn.mask)
					$this.findAndSelf('.vpms-edit-widget input[mask]').each(function(){
						var mask = $(this).attr('mask');
						if(mask)
							$(this).mask(mask);
					});
			}finally{
				if(settings)
					settings.updating = updating;
	 		}
		},
		//functions to update and commit model could be extern redefined
		//container is a jQuery element in which model is rendered, contains as data 'model' and 'settings'
		//each element could contain flags, which means 
		//'setValues' - if false don't set values for this element (group) and all children, if true or undefined - set values 	
		updateVPMSModel : function(container, isAutocommit, isBlockUI){							
			$.vpmsBindViewModel.debug("UpdateVPMSModel start", true);
			var settings = $(container).data('settings');
			var model = $(container).data('model');
			if(settings.updating)
				return;			
			else
				settings.updating = true;
			$.vpmsBindViewModel.hideToolTip(container);
			var plainModel = $.vpmsBindViewModel.getPlainModelForUpdate(container, isAutocommit);
			if(model.init && settings.initParams)
				plainModel.initParams = settings.initParams; 
			plainModel.quoteId = settings.quoteId;
			plainModel.version = settings.version;
			if(model.init)
				plainModel.init = true;
			$.vpmsBindViewModel.debug("updateVPMSModel_AjaxCall start");
			if(isBlockUI)
				$.vpmsBindViewModel.blockUI(model, "", true);
			$.vpmsBindViewModel.updateVPMSModel_AjaxCall(container, "data="+encodeURIComponent(JSON.stringify(plainModel)));
			if(!isBlockUI && (typeof settings.blockUIDelay=="number") && settings.blockUIDelay>0 && !model.init){
				settings.blockUIDelayFunction = setTimeout(function(){
					if(settings.updating===true)
						$.vpmsBindViewModel.blockUI(model, "", true);
				}, settings.blockUIDelay);
			}			
		},
		updateVPMSModel_AjaxCall : function(container, data){
			var settings = $(container).data('settings');
			$.ajax({type: "POST", url: settings.url, data: data ,datatype: "json",		    	 
		    	 success: function(result, textStatus, jqXHR){
		    		$.vpmsBindViewModel.updateVPMSModel_AjaxSuccess(container, result);		    
		         },
		         error: function(error, text, thrown){		        	
		        	var err = $.vpmsBindViewModel.parseAjaxError(error, text, thrown);
		        	$.vpmsBindViewModel.updateVPMSModel_AjaxError(container, err.errMsg, err.title);
		         }
		    });
		},
		updateVPMSModel_AjaxSuccess : function(container, result){
			$.vpmsBindViewModel.debug("updateVPMSModel_AjaxSuccess start");
			var model = $(container).data('model');
			if(model.applyingBinding){
				setTimeout(function(){$.vpmsBindViewModel.updateVPMSModel_AjaxSuccess(container, result);}, 1);
				return;
			}				
			if(model.applyingBindingError){
				$.vpmsBindViewModel.debug("updateVPMSModel_AjaxSuccess cancelled - bind error: " + model.applyingBindingError);
				return;
			}
			var settings = $.vpmsBindViewModel.updateSettings(container, result);
			//clear states before model update
			model.actions = result.actions;
			if(model.init){
				var state = $.vpmsBindViewModel.getState(model.containerId, settings.quoteId);
				if(state!=settings.model){
					$.vpmsBindViewModel.removeState($.vpmsBindViewModel.getAllStates(model.containerId), true);
					//save current model name				
					$.vpmsBindViewModel.pushState(model.containerId, settings.quoteId,  settings.model, true);					
				}				
			}
			//update model
			$.vpmsBindViewModel.updateModel(model, result, settings);
			$.vpmsBindViewModel.debug("updateVPMSModel_AjaxSuccess - model updated");
			if(model.init){
				if(settings.bindAfterUpdate){
					_applyBindings($(container)[0]);
					model = $(container).data('model');
				}
				$.vpmsBindViewModel.unblockUI(model);
				model.init = false;
				$.vpmsBindViewModel.afterRenderInit($(container));
				$.vpmsBindViewModel.afterRender($(container));					
				settings.initParams = undefined;
				$.vpmsBindViewModel.debug("updateVPMSModel_AjaxSuccess - afterRender");
			}				
         	$.vpmsBindViewModel.afterModelUpdate(container, model);    
         	resetUpdatingStatus(container);
         	$.vpmsBindViewModel.unblockUI(model);
			$.vpmsBindViewModel.debug("updateVPMSModel_AjaxSuccess - complete");
			//fix for autocommit and buttons Add/Remove group press. Button press sets model.values during ajax call, clear model values fixes also multiple pressing on add/remove group 
			model.values = undefined;
			if(result.error){
				setTimeout(function(){
					$.vpmsBindViewModel.updateVPMSModel_AjaxError(container, result.error.message, result.error.title);
				});
			}
		},
		updateVPMSModel_AjaxError : function(container, errMsg, title){
			var $container =  $(container);			
			var dialog_id = $container.attr('id') + '_error_dialog';
			$.vpmsBindViewModel.dialog(dialog_id, title, errMsg, {width:$container.width()-100});
			resetUpdatingStatus(container);
			$.vpmsBindViewModel.unblockUI($container.data('model'));
		},		
		parseAjaxError : function(error, text, thrown, getContent){
       	 	var title = text;
       	 	var errMsg = thrown;
       	 	if(error){
	       	 	//status is for IE only - http://msdn.microsoft.com/en-us/library/aa385465(v=vs.85).aspx
	       	 	//readyState for FF, Chrome etc 0 - means UNSET http://www.w3.org/TR/XMLHttpRequest/#states
	       	 	if (error.readyState === 0 || error.status == 12029 || error.status == 12007) {
	       	 		errMsg = "Connection Error! Please make sure you are connected to the server and then reload the page!";
	       	 	}	       	 	
       	 		if(getContent && error && error.responseText){
	       	 		var data = error.responseText; 
	       	 		data = data.replace(/<html/g, '<div id="_html"').replace(/<\/html/g, '</div').
	       	 					replace(/<head/g, '<div id="_head"').replace(/<\/head/g, '</div').
	       	 					replace(/<title/g, '<div id="_title"').replace(/<\/title/g, '</div').
	       	 					replace(/<body/g, '<div id="_body"').replace(/<\/body/g, '</div').
	       	 					replace(/<meta/g, '<div id="_meta"').replace(/<\/meta/g, '</div');
	       	 		$data = $('<div>' + data + '</div>');		                 
	       	 		var $title = $data.find('#_title');
	       	 		if($title.length>0) 
	       	 			title = $title.html();
	       	 		var $errMsg = $data.find('#_body');
	       	 		if($errMsg.length>0)
	       	 			errMsg = $errMsg.html();
	       	 	}
       	 	}
       	 	return {title:title, errMsg:errMsg};
		},		
		dialog : function(id, title, message, options){
			if(jQuery().dialog){
				var $dialog = $('#' + id);
				if($dialog.length===0){		
					$dialog = $('<div id="' + id + '" style="display:none;" class="vpms-error-dialog"></div>');
					$dialog.html(message);
					$('body').append($dialog);
					if(!options)
						options = {};
					$.extend(options, {						
						modal:true,
						show:'clip',
						hide:'clip',
						closeOnEscape:true,
						title:title
					});
					$dialog.dialog(options);					
				}else{
					$dialog.dialog('option', 'title', title);
					$dialog.html(message);
					$dialog.dialog('open');
				}
			}else{				
				alert(title + ' : ' +errMsg.replace(/<\/?[^>]+>/gi, ''));
			}
		},		
		openPdf : function(container, params){
			var settings = $(container).data('settings');
			var model = $(container).data('model');			
			if(!settings.pdfurl)
				return;
			var pdfCookieName = settings.pdfcookiename + settings.quoteId;
			$.cookie(pdfCookieName, "0", { path: "/"});
			setTimeout(function(){
				$.vpmsBindViewModel.blockUI(model, $.vpmsBindViewModel.unwrapObservable($.vpmsBindViewModel.element(model.captions, "text.documentbeinggenerated"), 'caption'));
				$.vpmsBindViewModel.openPdf_Call(container, settings.quoteId + (params?"?"+params:""));
				
				if(pdfCookieName){
					var pdfFileDownloadCheckTimerTimeout = 0;
					var pdfFileDownloadCheckTimer = window.setInterval(function () {
					      if ($.cookie(pdfCookieName) == "2" || pdfFileDownloadCheckTimerTimeout>=30000) {
					    	  window.clearInterval(pdfFileDownloadCheckTimer);
					    	  $.cookie(pdfCookieName, null);
					    	  $.vpmsBindViewModel.unblockUI(model);
					      }else
					    	  pdfFileDownloadCheckTimerTimeout += 500;
					}, 500);				
				}
			}, 1);					
		},
		openPdf_Call : function(container, quoteId){
			var settings = $(container).data('settings');
			// -> /illustration/<quoteId>
			location.href = settings.pdfurl + '/' + quoteId;
		},
		uploadFile : function(form, model){
			var container = $(form).closest("." + $.vpmsBindViewModel.options.containerClass);
			var settings = $(container).data('settings');
			var frameId = "uploadFrame" + $.vpmsBindViewModel.unwrapObservable(model, 'id');
			$('<iframe name="' + frameId + '"/>').appendTo("body").attr({'id':frameId, 'style':'width: 0; height: 0; border: none;'});
		    window.frames[frameId].name = frameId;
		    var iframeId = document.getElementById(frameId);
		    var eventHandler = function () {
		    	$.vpmsBindViewModel.unblockUI(model);
	            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
	            else iframeId.removeEventListener("load", eventHandler, false);	 
	            // Message from server...
	            var document = iframeId.contentDocument?iframeId.contentDocument:iframeId.contentWindow?iframeId.contentWindow:iframeId.document;
		    	var content = document.body.innerHTML;
		    	var text = document.body.innerText?document.body.innerText:document.body.textContent;
	            var newVersion = parseInt(text);
	            if(!isNaN(newVersion)){
	            	settings.version = newVersion;
	            	content = "";
	            }
	            if(model.afterUpload)
	            	model.afterUpload(form, content);
	            setTimeout(function(){jQuery(iframeId).remove();}, 250);
	        };
		    if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
		    if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);
		    $(form).attr("target", frameId);
		    $(form).attr("action", settings.fileurl);
		    jQuery(form.find('input[name="quoteId"]')).val(settings.quoteId);
			$.vpmsBindViewModel.blockUI(model, "", true);
		    $.vpmsBindViewModel.uploadFile_Call(form);
		},		
		uploadFile_Call : function(form){
		    form.submit();			
		},
		downloadFile : function(container, downloadModel){
			var settings = $(container).data('settings');
			var model = $(container).data('model');			
			if(!settings.fileurl)
				return;
			var pdfCookieName = settings.pdfcookiename + settings.quoteId;
			$.cookie(pdfCookieName, "0", { path: "/"});
			setTimeout(function(){
				$.vpmsBindViewModel.blockUI(model, "", true);
				$.vpmsBindViewModel.downloadFile_Call(container, settings.quoteId, $.vpmsBindViewModel.unwrapObservable(downloadModel.settings, 'fileId'));
				
				if(pdfCookieName){
					var pdfFileDownloadCheckTimerTimeout = 0;
					var pdfFileDownloadCheckTimer = window.setInterval(function () {
					      if ($.cookie(pdfCookieName) == "2" || pdfFileDownloadCheckTimerTimeout>=30000) {
					    	  window.clearInterval(pdfFileDownloadCheckTimer);
					    	  $.cookie(pdfCookieName, null);
					    	  $.vpmsBindViewModel.unblockUI(model);
					      }else
					    	  pdfFileDownloadCheckTimerTimeout += 500;
					}, 500);				
				}
			}, 1);					
		},
		downloadFile_Call : function(container, quoteId, fileId){
			var settings = $(container).data('settings');
			location.href = settings.fileurl + '/' + quoteId + "?fileId=" + fileId;
		},		
		getPlainModelForUpdate : function(container, isAutocommit){
			var model = $(container).data('model');
			var product = model.product?model.product:model.name;
			//JSON encode model
			var result = prepareModelForUpdate_getData(model, model.init);
			//restore product name;
			result.name = product;
			//values
			if(model.init!==true)
				result.values = prepareModelForUpdate_setValues(model, model.values?model.values: [], isAutocommit);
			model.values = undefined;
			//actions
			result.actions = model.actions;
			return result;
		},
		getStoredValue : function(key, def){
			if(window.amplify && $.vpmsBindViewModel.options.storeKey){
				var values = amplify.store($.vpmsBindViewModel.options.storeKey);
				if(values){
					for(var i=0; i<values.length; i++){
						if(values[i].key == key)
							return values[i].value;
					}
				}
			}
			return def;
		},
		setStoredValue : function(key, value){
			if(window.amplify && $.vpmsBindViewModel.options.storeKey){
				var values = amplify.store($.vpmsBindViewModel.options.storeKey);
				if(!values)
					values = [];
				var found = false;
				for(var i=0; i<values.length; i++){
					if(values[i].key == key){
						values[i].value = value;
						found = true;	
					}
				}
				if(!found)
					values.push({key:key, value:value});
				amplify.store($.vpmsBindViewModel.options.storeKey, values);
			}			
		},
		changeModel_parseName : function(name, settings){
			return name;
		},
		generateModelFromHtml : function(model, id, dontGenerateCustomWidgets, forceGenerateModel){
			var $htmlWidget = $("body").find("script[id='" + id + "']");
			if($htmlWidget.length>0){
				if($htmlWidget.data('model') && !forceGenerateModel)
					model = $.vpmsBindViewModel.cloneObject($htmlWidget.data('model'));
				else{
					//script (template) should have root element
					var $customWidget = $($($htmlWidget.get(0)).html());
					model = generateModelFromWidget($customWidget, dontGenerateCustomWidgets);
					$htmlWidget.data('model', $.vpmsBindViewModel.cloneObject(model));
				}
			}
			return model;
		}		
	};	
	 
	function resetUpdatingStatus(container){
		var settings = $(container).data('settings');
		if(settings.blockUIDelayFunction){
			clearTimeout(settings.blockUIDelayFunction);
			settings.blockUIDelayFunction = undefined;
		}
     	settings.updating = false;
     	$(container).removeAttr('updating');
	}

	var a = navigator.userAgent||navigator.vendor||window.opera;
	$.vpmsBindViewModel.isMobileAgent = (/android|avantgo|playbook|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))); 
	$.vpmsBindViewModel.isIe6 = /MSIE 6/i.test(a);
	$.vpmsBindViewModel.isIe7 = /MSIE 7/i.test(a);
	$.vpmsBindViewModel.isIe8 = /MSIE 8/i.test(a);
	
	var lastDebugTime;
	 
	function parseHTML(html, tagName) {
		  var root = document.createElement("div");
		  root.innerHTML = html;
		  // Get all child nodes of root div
		  var allChilds = root.childNodes;
		  for (var i = 0; i < allChilds.length; i++) {
		    if (allChilds[i].nodeName == tagName) {
		      return allChilds[i];
		    }
		  }
		  return false;
	}
	   
    var templatesInited = false;
	function initTemplates(){
		if(templatesInited)
			return;
		for(var i=0; i<$.vpmsBindViewModel.templates.length; i++){
			var t = $.vpmsBindViewModel.templates[i];
			//find out template from self and parents
			while(t){
				if(t.template){
					break;
				}
				t = t.parent;
			}		
			if(t && t.template && !$.vpmsBindViewModel.templateExists(t.name)){
				var templateText = $.isFunction(t.template)?t.template():t.template;
				$("body").append("<script id=\"" + t.name + "\" type=\"" + $.vpmsBindViewModel.tmplScriptType + "\">" + templateText + "<\/script>");
			}
		}
		templatesInited = true;
	}
	
	function uniqueWidgetId(name){
		var i = 0;
		while($.vpmsBindViewModel.templateExists("custom_" + name + "_" + i + ""))
			i++;
		return "custom_" + name + "_" + i;
	}
	function generateModelFromWidget($customWidget, dontGenerateCustomWidgets){
		var model, i;
		if($customWidget.attr('vpms_group'))
			model = eval("(" + $customWidget.attr('vpms_group') + ")");
		else
			if($customWidget.attr('vpms_element'))
				model = eval("(" + $customWidget.attr('vpms_element') + ")");
			else
				model = {};
		//process all groups
		var $groups = $customWidget.children('[vpms_group]');
		for(i=0; i<$groups.length; i++){
			var $group = $($groups.get(i));
			var group = generateModelFromWidget($group, dontGenerateCustomWidgets);
			if(model.multiple){
				//make from first group template, other groups ignore
				model.template = group;
				break;
			}else{
				if(!model.groups)
					model.groups = [];
				model.groups.push(group);
			}
		}
		//remove groups from DOM
		$customWidget.children('[vpms_group]').remove();
		//process all elements
		var $elements = $customWidget.children('[vpms_element]');
		var $newCustomWidget = $customWidget.clone(); 
		var $newElements = $newCustomWidget.children('[vpms_element]');
		for(i=0; i<$elements.length; i++){
			var $element = $($elements.get(i));
			var $newElement = $($newElements.get(i));
			var element = generateModelFromWidget($element, dontGenerateCustomWidgets);
			if(!model.elements)
				model.elements = [];
			model.elements.push(element);
			//replace this div with 'element' binding
			if($.vpmsBindViewModel.templateExists(element.widget) && !dontGenerateCustomWidgets){	
				 $("<div data-bind=\"template:{name:'element', data:$.vpmsBindViewModel.element(elements, '" + element.name + "')}\"></div>").insertBefore($newElement);
			}
			$element.remove();
			$newElement.remove();
		}	
		//save what is left as custom widget
		if($.trim($customWidget.html()) && !dontGenerateCustomWidgets){
			var newCustomWidgetId = uniqueWidgetId(model.name);			
			$("body").append("<script id=\"" + newCustomWidgetId + "\" type=\"text/html\">" + $newCustomWidget.html() + "<\/script>");
			model.customWidget = newCustomWidgetId;
		}
		return model;
	}	
	function updateModel_captions(model, data){
		if(data.k_){
			model.captions =  $.vpmsBindViewModel.initModelItem(model.captions, []);
			var captions = $.vpmsBindViewModel.unwrapObservable(model, 'captions');
			for(var i=0; i<data.k_.length; i++){
				var caption = undefined;
				for(var k=0; k<captions.length; k++){
					if(captions[k].id==data.k_[i].id){
						caption = captions[k];
						break;
					}
				}
				if(!caption){
					caption = $.vpmsBindViewModel.initModel_caption({name : data.k_[i]._n});
					captions.push(caption);
				}
				//if caption same as id (not found) and caption is set - don't update
				if(data.k_[i]._k!=data.k_[i].id || $.vpmsBindViewModel.unwrapObservable(caption, 'caption')==="")
					$.vpmsBindViewModel.updateModelItem(caption, "caption", data.k_[i]._k);
			}
		}
	}
	function initModel_choices(model){
		model.choices = $.vpmsBindViewModel.initModelItem(model.choices, []);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		if(model.generateDisplayValue){
			model.readDisplayValue = function(){
				var choicesArray = $.vpmsBindViewModel.unwrapObservable(this, 'choices');
				for(var i=0;i<choicesArray.length;i++){					
					if($.vpmsBindViewModel.unwrapObservable(choicesArray[i]._v)==$.vpmsBindViewModel.unwrapObservable(this, 'value'))
						return $.vpmsBindViewModel.unwrapObservable(choicesArray[i], '_x');
				}					
				return "";				
			};
			model.displayValue = $.vpmsBindViewModel.initModelItem(model.readDisplayValue(), "");
		}
	}
	function initModel_multiple(model){
		model.canAdd = $.vpmsBindViewModel.initModelItem(model.canAdd, true);
		model.staticPageCount = $.vpmsBindViewModel.initModelItem(model.staticPageCount, false);
		model.addCaption = $.vpmsBindViewModel.initModelItem(model.addCaption, "Add " + model.name);
		model.removeCaption = $.vpmsBindViewModel.initModelItem(model.removeCaption, "Remove " + model.name);		
	}	
	function initModel_group(model, containerId){
		if(!model.name) model.name = "unknownGroup";
		if(model.isGroup===undefined)
			model.isGroup = true;
		if(model.multiple)
			model.isGroup = false;
		if(!model.isGroup)
			model.composite=false;
		else
			if(model.composite===undefined)
				model.composite = true;
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);		
		model = $.vpmsBindViewModel.initModel(model, containerId);
		model.id = $.vpmsBindViewModel.initModelItem(model.id, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);		
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.canAdd = $.vpmsBindViewModel.initModelItem(model.canAdd, true);
		model.canRemove = $.vpmsBindViewModel.initModelItem(model.canRemove, true);
		model.addCaption = $.vpmsBindViewModel.initModelItem(model.addCaption, "Add " + model.name);
		model.removeCaption = $.vpmsBindViewModel.initModelItem(model.removeCaption, "Remove " + model.name);
		model.visited = $.vpmsBindViewModel.initModelItem(model.visited, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		return model;
	}
	function initModel_groups(model, containerId){
		if(model.groups){
			model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []); 
			var groupsArray = $.vpmsBindViewModel.unwrapObservable(model, 'groups');			
			for(var i=0; i<groupsArray.length; i++){
				var _group = initModel_group(groupsArray[i], containerId);				
			    $.vpmsBindViewModel.initModel_parent(_group, model);
			    groupsArray[i] = $.vpmsBindViewModel.initArrayElement(_group);
			}
		}
	}	
	function initModel_elements(model, containerId){
		if(model.elements){
			model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
			var elementsArray = $.vpmsBindViewModel.unwrapObservable(model, 'elements');			
			for(var i=0; i<elementsArray.length; i++){
				var _element = $.vpmsBindViewModel.initModel(elementsArray[i], containerId);				
				$.vpmsBindViewModel.initModel_parent(_element, model);
				elementsArray[i] = $.vpmsBindViewModel.initArrayElement(_element);
			}
		}
	}
	function initModel_captions(model){
		if(model.captions){
			model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
			var captions = $.vpmsBindViewModel.unwrapObservable(model, 'captions');
			for(var i=0; i<captions.length; i++){
				$.vpmsBindViewModel.initModel_caption(captions[i]);
			}
		}
	}
	function afterMultipleGroupAdded(model, data){
		var _model = $.vpmsBindViewModel.getArrayElement(model);
		$.vpmsBindViewModel.updateModel_dependencies(_model);
		$.vpmsBindViewModel.applyBehavior_customStaticProperties(_model);
		$.vpmsBindViewModel.applyBehavior_customDynamicProperties(_model, data);
		var i;
		var modelElements = $.vpmsBindViewModel.unwrapObservable(_model, 'elements');
		if(modelElements)
			for(i=0;i<modelElements.length;i++){
				afterMultipleGroupAdded(modelElements[i], data&&data.e_&&data.e_.length>i?data.e_[i]:undefined);
			}
		var modelGroups = $.vpmsBindViewModel.unwrapObservable(_model, 'groups');
		if(modelGroups)
			for(i=0;i<modelGroups.length;i++){
				afterMultipleGroupAdded(modelGroups[i], data&&data.g_&&data.g_.length>i?data.g_[i]:undefined);
			}
	}
	function updateModel_data(model, data){
		if(data.d_){
			for(var key in data.d_){
				$.vpmsBindViewModel.updateModelItem(model, key, data.d_[key]);
			}
		}
	}
	function updateModel_element(model, data, settings){
		var modelValue =data._v!==undefined?data._v:"";		
		if(model.format && model.format.indexOf('date')>=0){
			if(!model.settings)
				model.settings = {};
			if(!model.settings.dateFormat && (settings.dateFormat || (settings.locale && settings.locale.dateFormat)))
				model.settings.dateFormat = settings.dateFormat?settings.dateFormat:settings.locale.dateFormat;
			if(!model.settings.vpmsDateFormat && (settings.vpmsDateFormat || (settings.locale && settings.locale.vpmsDateFormat)))
				model.settings.vpmsDateFormat = settings.vpmsDateFormat?settings.vpmsDateFormat:settings.locale.vpmsDateFormat;
			modelValue = $.vpmsBindViewModel.StringToDate(data._v);
		}
		else
		if(model.format && model.format.indexOf('number')>=0){		
			if(!model.settings)
				model.settings = {};
			if(!model.settings.decimalSeparator && (settings.decimalSeparator || (settings.locale && settings.locale.decimalSeparator)))
				model.settings.decimalSeparator = settings.decimalSeparator?settings.decimalSeparator:settings.locale.decimalSeparator;
			if(!model.settings.thousandsSeparator && (settings.thousandsSeparator || (settings.locale && settings.locale.thousandsSeparator)))
				model.settings.thousandsSeparator = settings.thousandsSeparator?settings.thousandsSeparator:settings.locale.thousandsSeparator;
			modelValue = data._v;			
		}		
		$.vpmsBindViewModel.updateModelItem(model, 'caption', data._k!==undefined?data._k:"");
		$.vpmsBindViewModel.updateModelItem(model, 'relevant', data._r!==undefined?data._r:true);	
		$.vpmsBindViewModel.updateModelItem(model, 'required', data._q!==undefined?data._q:false);
		model._previousValue = data._v;
		$.vpmsBindViewModel.updateModelItem(model, 'value', modelValue);		
		$.vpmsBindViewModel.updateModelItem(model, 'error', data._e!==undefined?data._e:"");
		$.vpmsBindViewModel.updateModelItem(model, 'tooltip', data._t!==undefined?data._t:"");
		$.vpmsBindViewModel.updateModelItem(model, 'enabled', data._a!==undefined?data._a:true);
	}
	function updateModel_group(model, data, settings){
		$.vpmsBindViewModel.updateModelItem(model, 'caption', data._k!==undefined?data._k:"");
		$.vpmsBindViewModel.updateModelItem(model, 'relevant', data._r!==undefined?data._r:true);	
		$.vpmsBindViewModel.updateModelItem(model, 'required', data._q!==undefined?data._q:false);
		$.vpmsBindViewModel.updateModelItem(model, 'error', data._e!==undefined?data._e:"");
		$.vpmsBindViewModel.updateModelItem(model, 'canAdd', data._ca!==undefined?data._ca:true);
		$.vpmsBindViewModel.updateModelItem(model, 'canRemove', data._cr!==undefined?data._cr:true);
		$.vpmsBindViewModel.updateModelItem(model, 'tooltip', data._t!==undefined?data._t:"");
		if(data._ac!==undefined) $.vpmsBindViewModel.updateModelItem(model, 'addCaption', data._ac);
		if(data._rc!==undefined) $.vpmsBindViewModel.updateModelItem(model, 'removeCaption', data._rc);		
		if(data.i!==undefined) $.vpmsBindViewModel.updateModelItem(model, 'index', data.i);
	}
	function updateModel_list(model, modelList, dataList, settings){
		if(dataList){
			modelList = $.vpmsBindViewModel.initModelItem(modelList, []);
			var modelArray = $.vpmsBindViewModel.unwrapObservable(modelList);
			var index = 0;var mixed = false;
			for(var i=0; i<dataList.length; i++){
				var found = false;
				for(var k=index; k<modelArray.length; k++){
					var modelItem = $.vpmsBindViewModel.getArrayElement(modelArray[k]);
					if(dataList[i]._n==modelItem.name){
						$.vpmsBindViewModel.updateModel(modelItem, dataList[i], settings, $.vpmsBindViewModel.unwrapObservable(model ,'disabled'));
						if(i==k && !mixed)
							index = i+1;
						else{
							mixed = true;
							index = 0;
						}
						found = true;
						break;
					}					
				}
				if(!found){
					var item = $.vpmsBindViewModel.initModel({name : dataList[i].name, widget: dataList[i].widget}, model.containerId);
					$.vpmsBindViewModel.updateModel(item, dataList[i], settings, $.vpmsBindViewModel.unwrapObservable(model, 'disabled'));
					modelList.splice(i, 0, $.vpmsBindViewModel.initArrayElement(item));
					modelArray = $.vpmsBindViewModel.unwrapObservable(modelList);
					mixed = true; index=0;
				}								
			}			
		}
		return modelList;
	}
	function updateModel_mergeGroups(model, data, settings){
		if(model.canAdd!==undefined)  $.vpmsBindViewModel.updateModelItem(model, 'canAdd', data._ca!==undefined?data._ca:true);
		if(model.staticPageCount!==undefined)  $.vpmsBindViewModel.updateModelItem(model, 'staticPageCount', data._st!==undefined?data._st:false);		
		if(model.addCaption!==undefined && data._ac!==undefined)  $.vpmsBindViewModel.updateModelItem(model, 'addCaption', data._ac);
		if(model.removeCaption!==undefined && data._rc!==undefined)  $.vpmsBindViewModel.updateModelItem(model, 'removeCaption', data._rc);
		if(model.caption!==undefined && data._k!==undefined)  $.vpmsBindViewModel.updateModelItem(model, 'caption', data._k);
		
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		var groupsArray = $.vpmsBindViewModel.unwrapObservable(model, 'groups');
		if(model.spliceGroups){
			groupsArray.splice(0, groupsArray.length);
			model.spliceGroups = undefined;
		}		
		var idList = ",";
		//merge and remove existing elements
		var index=(data.g_ && data.g_.length)?data.g_.length-1:0;
		var mixed=false;
		var i;
		for(i=groupsArray.length-1; i>=0; i--){
			var groupElement = $.vpmsBindViewModel.getArrayElement(groupsArray[i]);
			var id = $.vpmsBindViewModel.unwrapObservable(groupElement, 'id');
			var dataGroup = undefined;
			if(data.g_ && id)
				for(var j=index; j>=0; j--){
					if(data.g_[j].id==id){
						dataGroup = data.g_[j];
						if(i==j && !mixed)
							index=j-1;
						else{
							index = data.g_.length-1;
							mixed = true;
						}						
						break;
					}
				}
			//remove
			if(dataGroup === undefined || dataGroup._r===false){	
				if(model.beforeGroupRemove)
					model.beforeGroupRemove(groupsArray[i]);
				if(model.groups.remove)
					model.groups.remove(groupsArray[i]);
				else
					model.groups.splice(i,1);
				if(data.g_)
					index = data.g_.length-1;
				mixed = true;
			}
			//merge
			else{
				$.vpmsBindViewModel.updateModel(groupElement,dataGroup, settings, $.vpmsBindViewModel.unwrapObservable(model, 'disabled'));
			}
			if(dataGroup !== undefined)
				idList += dataGroup.id + ",";
		}		
			
		//insert new groups
		var removedCount = 0;
		if(data.g_)			
			for(i=0; i<data.g_.length; i++){
				if(idList.indexOf("," + data.g_[i].id + ",")<0){
					if(data.g_[i]._r===false)
						removedCount--;
					else{
						var group = $.vpmsBindViewModel.getGroupTemplate(model);
						if(!group) group={};
						if(model._dynamics)
							group.dynamics = model._dynamics;
						$.vpmsBindViewModel.initModel_parent(group, model);
						group = $.vpmsBindViewModel.initArrayElement(initModel_group(group, model.containerId));
						if(!settings.detached)
							settings.detached = 0;
						settings.detached++;
						var _group = $.vpmsBindViewModel.getArrayElement(group);
						$.vpmsBindViewModel.updateModel_beforeGroupAdded(model, _group, data, data.g_[i]);
						$.vpmsBindViewModel.updateModel(_group, data.g_[i], settings, $.vpmsBindViewModel.unwrapObservable(model, 'disabled'));
						settings.detached--;
						var idx = i + removedCount;
						var _groups = $.vpmsBindViewModel.unwrapObservable(model, 'groups');
						if(idx>=_groups.length){
							model.groups.push(group);							
							_group = _groups[_groups.length-1];
						}else{
							model.groups.splice(idx, 0, group);
							_group = _groups[idx];
						}
						if(!settings.detached)
							$.vpmsBindViewModel.updateModel_afterGroupAdded(model, _group, data.g_[i]);
					}
				}
			}
		return model;
	}
	function insertItemsToObservableArray (obsarr, arr, index) {
		index = index || $.vpmsBindViewModel.unwrapObservable(obsarr).length;
	    var args = [index, 0].concat(arr);
	    obsarr.splice.apply(obsarr, args);	
	}
	function updateModel_mergeChoices(model, data){
		if(data.c_){
			model.choices = $.vpmsBindViewModel.initModelItem(model.choices, []);
			$.vpmsBindViewModel.updateModelItem(model, 'choices', data.c_);
			$.vpmsBindViewModel.updateModelItem(model, 'value', null); //since default value is "", this will make selectedIndex = -1 for SELECT
		}		
		$.vpmsBindViewModel.updateModelItem(model, 'value', data._v!==undefined?data._v:"");
		data._v = $.vpmsBindViewModel.unwrapObservable(model, 'value');
		if(model.generateDisplayValue)
			$.vpmsBindViewModel.updateModelItem(model, 'displayValue', model.readDisplayValue());
		return model;
	}
	function prepareModelForUpdate_setValues(model, values, isAutocommit){
		var i;
		//dont process readonly and disabled elements 
		if(model.isElement && $.vpmsBindViewModel.unwrapObservable(model, 'readonly')!==true && $.vpmsBindViewModel.unwrapObservable(model, 'disabled')!==true){
			var modelId = $.vpmsBindViewModel.unwrapObservable(model, 'id');
			var modelValue=$.vpmsBindViewModel.unwrapObservable(model, 'value');
			//set value to vpms only if id is not empty and value not null 
			if(modelId && modelValue!==undefined && modelValue!==null){
				//date - convert to ISO format			
				if (modelValue instanceof Date)
					modelValue=$.vpmsBindViewModel.DateToString(modelValue);
				else{
					if(modelValue!==null && modelValue!==undefined)
						modelValue=modelValue+"";
				}
				if(model._previousValue!=modelValue){
					var valueObject = {id:$.vpmsBindViewModel.unwrapObservable(model,'id'),_v:modelValue};
					if(model.format)
						valueObject.f = model.format;
					valueObject.p = 0;
					if(model.settings && model.settings.priority)
						valueObject.p = model.settings.priority;
					for(i=values.length; i>=0; i--){
						if(i===0){
							values.push(valueObject);
						}else{
							if(values[i-1].p===undefined)
								values[i-1].p = 0;
							if(values[i-1].p<=valueObject.p){
								values.splice(i, 0, valueObject);
								break;
							}
						}
					}					
				}
			}				
		}
		if($.vpmsBindViewModel.unwrapObservable(model, 'relevant')!==false){
			if(model.elements){
				var elementsArray = $.vpmsBindViewModel.unwrapObservable(model, 'elements');
				for(i=0; i<elementsArray.length; i++)
					prepareModelForUpdate_setValues($.vpmsBindViewModel.getArrayElement(elementsArray[i]), values, isAutocommit);			
			}
			if(model.groups){
				if(model.prevSelectedGroup){
					var prevSelectedId = $.vpmsBindViewModel.unwrapObservable(model.prevSelectedGroup, 'id');
					var nextSelectedId = model.requestedPage?model.requestedPage:$.vpmsBindViewModel.unwrapObservable(model, 'selectedPage');
					if(prevSelectedId!=nextSelectedId || !isAutocommit || $.vpmsBindViewModel.setVisitedOnAutocommit){
						values.push({setData:true,id:$.vpmsBindViewModel.getFullPath(model.prevSelectedGroup),key:"visited",_v:"1"});
					}
					prepareModelForUpdate_setValues(model.prevSelectedGroup, values, isAutocommit);
				}
				else{
					if(model.setAllGroupsData){
						var groupsArray = $.vpmsBindViewModel.unwrapObservable(model, 'groups');
						for(i=0; i<groupsArray.length; i++)
							prepareModelForUpdate_setValues($.vpmsBindViewModel.getArrayElement(groupsArray[i]), values, isAutocommit);
					}
				}
			}
		}
		return values;
	}
	
	function prepareModelForUpdate_getRoot(model, init, selectedPages){
		var result = {};
		//don't send empty id and id for elements
		if(selectedPages && $.vpmsBindViewModel.unwrapObservable(model, 'id')) {
			result.id=$.vpmsBindViewModel.unwrapObservable(model, 'id');
			prepareModelForUpdate_getSelectedPages(model, selectedPages, model.multiple);
		}
		if(model.name) { result._n = $.vpmsBindViewModel.unwrapObservable(model, 'name'); }
		if($.vpmsBindViewModel.unwrapObservable(model, 'isGroup')) { result.g = 1; }
		if($.vpmsBindViewModel.unwrapObservable(model, 'isElement')) { result.e = 1; }
		if($.vpmsBindViewModel.unwrapObservable(model, 'composite')) { result.s = 1; }
		if($.vpmsBindViewModel.unwrapObservable(model, 'multiple')) { result.m = 1; }
		if(model.onlyRelevant) {result.n = 1;}
		if($.vpmsBindViewModel.unwrapObservable(model, 'hidden')) { result.h = 1; }
		if(model.format) { result.f = model.format; }
		if($.vpmsBindViewModel.unwrapObservable(model, 'getAllGroupsData')) { result.a = 1; }		
		if(model.parentInstance) { result._p = $.vpmsBindViewModel.unwrapObservable(model, 'parentInstance'); }
		if($.vpmsBindViewModel.isChoicesWidget(model.widget))
			result.c = 1;		
		if(model.getGroupsTree===true)
			result.t = 1;
		if(init && model.captions){		
			var captionsArray = $.vpmsBindViewModel.unwrapObservable(model, 'captions');
			if(captionsArray.length>0){
				result.k_ = [];
				for(var i=0; i<captionsArray.length; i++){
					var caption = {id:$.vpmsBindViewModel.unwrapObservable(captionsArray[i], 'id')};
					if(captionsArray[i].isParam===true)
						caption.p = 1;
					result.k_.push(caption);
				}
			}
		}
		var template = $.vpmsBindViewModel.getGroupTemplate(model);
		if(template)
			result.t_ = prepareModelForUpdate_getData(initModel_group(template), true);
		if(model.navigation){
			result.page = model.navigation.pageNumber;
			result.rowNum = model.navigation.rowsPerPage;
		}			
		if(model.colNames){
			result.cn_ = model.colNames;
		}
		if(model._dynamics){
			result.dynamics = model._dynamics;
		}		
		return result;
	}

	function prepareModelForUpdate_getData(model, init, getGroupsTree, selectedPages){
		var selectedPagesInited = false;
		var i;
		if(!selectedPages && !init){
			selectedPagesInited = true;
			selectedPages = [];
		}
		//root
		var result = prepareModelForUpdate_getRoot(model, init, selectedPages);
		//elements
		if(model.elements){
			var elementsArray = $.vpmsBindViewModel.unwrapObservable(model, 'elements');
			if(elementsArray.length>0){
				result.e_ = [];
				for(i=0; i<elementsArray.length; i++)					
					result.e_.push(prepareModelForUpdate_getData($.vpmsBindViewModel.getArrayElement(elementsArray[i]), init, false, selectedPages));
			}
		}					
		//groups		
		if(model.groups){			
			var selectedPage;
			if(model.selectedPage!==undefined){
				selectedPage = $.vpmsBindViewModel.unwrapObservable(model, 'requestedPage');
				if(!selectedPage)
					selectedPage = $.vpmsBindViewModel.unwrapObservable(model, 'selectedPage');
				//hash history change check
				//if selectedPage undefined - model not inited, get all pages
				//else if tab is undefined - get all pages also, first relevant page will be shown, but 
				//before update is impossible to detect if first relevant page now will be relevant after update
				//else - if current selectedPage != url tab id - history change case - set selectedPage from history
				if(selectedPage!==undefined){
					var tab = $.vpmsBindViewModel.getState( model.containerId, $.vpmsBindViewModel.unwrapObservable(model, 'name') );
					if(tab && tab!= $.vpmsBindViewModel.unwrapObservable(model, 'selectedPage')){
						selectedPage = tab;
						model.requestedPage = tab;
						//update selectedPages
						for(i=0; i<selectedPages.length; i++){
							if(selectedPages[i].id==$.vpmsBindViewModel.unwrapObservable(model, 'id')){
								selectedPages[i].sp=selectedPage;
								break;
							}
						}
					}							
				}
			}
			if(model.syncUpdate)
				result.su = 1;
			if(!model.multiple){
				var groupsArray = $.vpmsBindViewModel.unwrapObservable(model, 'groups');
				if(groupsArray.length>0){					
					result.g_ = prepareModelForUpdate_getGroups(model.groups, (!model.initialUpdated||model.getAllGroupsData)?true:selectedPage, init, (model.getGroupsTree===true) || getGroupsTree, selectedPages);
				}
			}
		}
		
		if(selectedPagesInited)
			result.sp = selectedPages;
		return result;
	}
	
	function prepareModelForUpdate_getGroups(groups, selectedPage, init, getGroupsTree, selectedPages){
		var result;
		if(groups){
			result = [];			
			var groupsArray = $.vpmsBindViewModel.unwrapObservable(groups);
			for(var i=0; i<groupsArray.length; i++){
				var item;
				var group = $.vpmsBindViewModel.getArrayElement(groupsArray[i]);
				if(init || selectedPage===true || (selectedPage == $.vpmsBindViewModel.unwrapObservable(group, 'id')))
					item = prepareModelForUpdate_getData(group, init, getGroupsTree, selectedPages);
				else{
					item = prepareModelForUpdate_getRoot(group, init, selectedPages);
					//tree view
					if(getGroupsTree && !group.multiple)
						item.g_ = prepareModelForUpdate_getGroups(group.groups, selectedPage, init, getGroupsTree, selectedPages); 				
				}
				result.push(item);
			}
		}
		return result;
	}
	
	function prepareModelForUpdate_getSelectedPages(model, selectedPages, recursive){
		//use selectedPages for hash also
		if($.vpmsBindViewModel.unwrapObservable(model, 'id')){
			var selectedPage = {id:$.vpmsBindViewModel.unwrapObservable(model, 'id')};
			if(model.hash && model.ignoreHash!==true)
				selectedPage._h = model.hash;
			if(model.navigation)
				selectedPage._np = model.navigation.pageNumber;

			if($.vpmsBindViewModel.unwrapObservable(model, 'requestedPage'))
				selectedPage.sp = $.vpmsBindViewModel.unwrapObservable(model, 'requestedPage');
			else
				if($.vpmsBindViewModel.unwrapObservable(model, 'selectedPage')) 
					selectedPage.sp = $.vpmsBindViewModel.unwrapObservable(model, 'selectedPage');
			
			if(model.getGroupsTree)
				selectedPage.t=1;
			if(selectedPage._h || selectedPage._np || model.requestedPage!==undefined || model.selectedPage!==undefined)
				selectedPages.push(selectedPage);
		}

		if(recursive){
			var i;
			if(model.groups){
				var groupsArray = $.vpmsBindViewModel.unwrapObservable(model, 'groups');
				for(i=0; i<groupsArray.length; i++)
					prepareModelForUpdate_getSelectedPages($.vpmsBindViewModel.getArrayElement(groupsArray[i]), selectedPages, true);				
			}
			if(model.elements){
				var elementsArray = $.vpmsBindViewModel.unwrapObservable(model, 'elements');
				for(i=0; i<elementsArray.length; i++)
					prepareModelForUpdate_getSelectedPages($.vpmsBindViewModel.getArrayElement(elementsArray[i]), selectedPages, true);				
			}			
		}
	}
	
	function _applyBindings(container){
		$.vpmsBindViewModel.debug("Init - start apply binding");
		$.vpmsBindViewModel.applyBindings(container);
		$.vpmsBindViewModel.debug("Init - end apply binding");							
	}
	
	var methods = {
		init : function(options) {
			$.vpmsBindViewModel.debug("Init start", true);
			initTemplates();
			$.vpmsBindViewModel.debug("Init - templates inited");
			var settings = {};
			$.extend(settings, $.vpmsBindViewModel.options);
			if(options)
				$.extend(settings, options);
			return this.each(function(){
				var $this = $(this);
				if(!$this.attr('id'))
					$this.attr('id', 'vpms');
				var containerId = $this.attr('id'); 
				var modelName = settings.model;
				if(!modelName)
					modelName = $.vpmsBindViewModel.getState(containerId, settings.quoteId);
				if(!modelName)
					modelName = settings.defaultModel;
				var modelTemplate = $.vpmsBindViewModel.getModel(modelName);
				var model = (modelTemplate && modelTemplate.model)?modelTemplate.model():$.vpmsBindViewModel.generateModelFromHtml(undefined, modelName);
				if(model===undefined){
					throw "Model " + modelName + " undefined";
				}
				$.vpmsBindViewModel.debug("Init - model " + modelName + " generated");
				settings.model = modelName;
				if(!$this.hasClass(settings.containerClass))
					$this.addClass(settings.containerClass);
				//read url from actionUrlAttr attribute
				if(!settings.url && settings.actionUrlAttr)
					settings.url = $this.attr(settings.actionUrlAttr);				
				//read pdfurl from pdfUrlAttr attribute
				if(!settings.pdfurl && settings.pdfUrlAttr)
					settings.pdfurl = $this.attr(settings.pdfUrlAttr);
				//read resurl from resourceUrlAttr attribute
				if(!settings.resurl && settings.resourceUrlAttr)
					settings.resurl = $this.attr(settings.resourceUrlAttr);
				//read fileurl from fileUrlAttr attribute
				if(!settings.fileurl && settings.fileUrlAttr)
					settings.fileurl = $this.attr(settings.fileUrlAttr);
				//init flag
				model.init = true;
				//it is possible to rewrite widget from settings (e.g. same model but different widgets)
				if(settings.widget)
					model.widget = settings.widget;
				$.vpmsBindViewModel.initModelRoot(model, containerId, settings);
				$.vpmsBindViewModel.initModel(model, containerId);	
				$.vpmsBindViewModel.debug("Init - model inited");
				if($this.hasClass('vpms-single-column'))
					settings.singleColumn = true;
				//save options for this div
				model = $.vpmsBindViewModel.initModelObservable(model, $this);
				$this.data('settings', settings);
				$this.data('model', model);				
				$.vpmsBindViewModel.blockUI(model, settings.blockUIText?settings.blockUIText:'Please wait...');
				
				//clear container and apply binding during ajax call
				var _this = this;
				model.applyingBinding = true;
				setTimeout(function(){
					var _model;
					try{
						//clear div before render - do not clear root div since it could have external events or data 			
						if(window.ko){
							var nodes = $(_this).children();
							for(var i=0; i<nodes.length; i++)
								ko.removeNode(nodes[i]);
						}else
							$(_this).empty();
						$.vpmsBindViewModel.debug("Init - container cleared");
								
						if(!settings.bindAfterUpdate)
							_applyBindings(_this);
						
						_model = $(_this).data('model');
						_model.applyingBindingError = undefined;
						_model.applyingBinding = false;	
					}catch(e){
						_model = $(_this).data('model');
						_model.applyingBinding = false;
						_model.applyingBindingError = "Error applying binding: " + e.message;
						throw model.applyingBindingError;
					}
				}, 1);
				//call ajax to fill model deferred if offlineMode
				$.vpmsBindViewModel.updateVPMSModel($this);
			});
		},
		updateModel : function() { 	
			var $container = $(this).closest("." + $.vpmsBindViewModel.options.containerClass);
			$.vpmsBindViewModel.updateVPMSModel($container);
		},
		addToGroup : function(group, groupAfterId){
			var $container = $(this).closest("." + $.vpmsBindViewModel.options.containerClass);
			var model = $container.data('model');
			var settings = $container.data('settings');
			if(!model.values)
				model.values = [];
			var nodeId = $.vpmsBindViewModel.unwrapObservable(group, 'name');
			var parentId = $.vpmsBindViewModel.unwrapObservable(group, 'id').replace(nodeId, "");
			model.values.push({addGroup:true, parentId:parentId, nodeId: nodeId, groupAfterId:groupAfterId});
			if(group.prevSelectedGroup){
				model.values.push({setData:true,id: $.vpmsBindViewModel.getFullPath(group.prevSelectedGroup),key:"visited",_v:"1"});
			}
			$.vpmsBindViewModel.updateVPMSModel($container, true);
			if(settings.isOfflineMode){
				var item = ($.isFunction(group.template))?initModel_group(group.template(), group.containerId):initModel_group($.vpmsBindViewModel.cloneObject(group.template), group.containerId);
				item.id(item.id() + internalCounter++);
				group.groups.push(item);
			}
		},		
		removeFromGroup : function(group, item){
			var removeConfirmationText = $.vpmsBindViewModel.unwrapObservable(group, "removeConfirmationText");
			if(!removeConfirmationText && group.settings)
				removeConfirmationText = group.settings.removeConfirmationText;
			if(removeConfirmationText && !confirm(removeConfirmationText)){
				return false;
			}
			var $container = $(this).closest("." + $.vpmsBindViewModel.options.containerClass);
			var model = $container.data('model');
			var settings = $container.data('settings');
			if(!model.values)
				model.values = [];
			if(group.prevSelectedGroup){
				model.values.push({setData:true,id: $.vpmsBindViewModel.getFullPath(group.prevSelectedGroup),key:"visited",_v:"1"});
			}
			var nodeId = $.vpmsBindViewModel.unwrapObservable(group, 'name');
			var parentId = $.vpmsBindViewModel.unwrapObservable(group, 'id').replace(nodeId, "");
			model.values.push({removeGroup:true, parentId:parentId, nodeId: nodeId, id: $.vpmsBindViewModel.unwrapObservable(item, 'id'), fullPath:$.vpmsBindViewModel.getFullPath(item)});
			$.vpmsBindViewModel.updateVPMSModel($container, true);
			if(settings.isOfflineMode)
				group.groups.remove(item);
		},
		changeModel : function(name){
			var $container = $(this).closest("." + $.vpmsBindViewModel.options.containerClass);
			if($container.length===0) return;
			var settings = $container.data('settings');
			var model = $container.data('model');
			settings = $.extend({}, settings);
			name = $.vpmsBindViewModel.changeModel_parseName(name, settings);
			var modelName = name?name:settings.defaultModel;

			if(!modelName)				
				return;			
			if(settings.model != modelName){
				if($.vpmsBindViewModel.beforeChangeModel)
					$.vpmsBindViewModel.beforeChangeModel($container, modelName);
				$.vpmsBindViewModel.removeState($.vpmsBindViewModel.getAllStates(model.containerId), true);
				settings.model = modelName;
				//leave quoteId settings.quoteId = "";
				$container.vpmsBindViewModel(settings);
			}
		}
	};
	
	var internalCounter = 0;
	
	$.fn.vpmsBindViewModel = function(method) {    
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.vpmsBindViewModel' );
		}      
	};
	if($.bbq){
		$(window).bind( "hashchange", function(e) {
			var state = $.deparam.fragment();
			$("." + $.vpmsBindViewModel.options.containerClass).each(function(){
				var $this = $(this);
				var containerId = $this.attr('id');
				var settings = $this.data('settings');
				var modelName = $.bbq.getState(containerId, settings.quoteId);				
				if(modelName && settings.model!=modelName){
					setTimeout(function(){
						$this.vpmsBindViewModel("changeModel", modelName);
					}, 1);
				}else{		
					//check if any state changed or added for this model (name stated with container id)
					var changed = false;
					for(var name in state){
						if(name.indexOf(containerId)===0){
							if($.vpmsBindViewModel.urlState[name]!=state[name]){
								changed = true;
								break;
							}
						}
					}
					//check if any removed
					if(!changed)
						for(name in $.vpmsBindViewModel.urlState){
							if(name.indexOf(containerId)===0){
								if($.vpmsBindViewModel.urlState[name]!=state[name]){
									changed = true;
									break;
								}
							}
						}						
					if(changed)
						$this.vpmsBindViewModel("updateModel");
				}
			});
			$.vpmsBindViewModel.urlState = state;
		});	
	}
})(jQuery);

if(window.ko){
	ko.bindingHandlers.enableui = {
	    'update': function (element, valueAccessor) {
	    	var varValueAccessor = valueAccessor();
	    	var value = (ko.isObservable(varValueAccessor))?ko.utils.unwrapObservable(varValueAccessor):(jQuery.isFunction(varValueAccessor))?varValueAccessor():varValueAccessor;

    		var $element = jQuery(element);
	        if (value && element.disabled){
				if($element.hasClass("ui-button") || $element.hasClass("ui-btn-hidden"))
					$element.button("enable");
				else
					element.removeAttribute("disabled");
			}
	        else if ((!value) && (!element.disabled)){
				if($element.hasClass("ui-button") || $element.hasClass("ui-btn-hidden"))
					$element.button("disable");
				else
					element.disabled = true;
			}
	    }
	};
	
	ko.bindingHandlers.visibleui = {
	    'update': function (element, valueAccessor) {
	    	var varValueAccessor = valueAccessor();
	    	var value = (ko.isObservable(varValueAccessor))?ko.utils.unwrapObservable(varValueAccessor):(jQuery.isFunction(varValueAccessor))?varValueAccessor():varValueAccessor;
	        var isCurrentlyVisible = element.style.display != "none";
	        if (value && !isCurrentlyVisible)
	            element.style.display = "";
	        else if ((!value) && isCurrentlyVisible)
	            element.style.display = "none";
	    }
	};
	
	
	ko.bindingHandlers.textui = { 
	    'update': function (element, valueAccessor) {
	    	var varValueAccessor = valueAccessor();
	    	var value = (ko.isObservable(varValueAccessor))?ko.utils.unwrapObservable(varValueAccessor):(jQuery.isFunction(varValueAccessor))?varValueAccessor():varValueAccessor;
	    	if ((value === null) || (value === undefined))
	            value = "";
			var $element = jQuery(element);
			if($element.children().length===0){
				if(typeof element.innerText == "string"){ 
					element.innerText = value;			
				}else{
					element.textContent = value;
				}
			}else{
				$element.children().each(function(){
					if(typeof this.innerText == "string"){
						this.innerText = value;			
					}else{
						this.textContent = value;
					}
				});
			}
	    }
	}; 
	
	ko.bindingHandlers.childOptions = {
		    update: function(element, valueAccessor, allBindingsAccessor, context) {
		    	var select = jQuery(element).find('select');
		    	if(select.length>0)
		        ko.bindingHandlers.options.update(select[0], valueAccessor, allBindingsAccessor, context);
		    }
	};
	
	ko.bindingHandlers.typedValue = {
		    init: function (element, valueAccessor, allBindingsAccessor, viewModel) { 
		    	ko.utils.registerEventHandler(element, 'change', function (event) {
		    		//TODO handle all types
		            var modelValue = valueAccessor();
		            if(viewModel.format && viewModel.format.indexOf("number")>=0 && jQuery.numbers){	            	
	                    var elementValue = ko.selectExtensions.readValue(element);
	                    //Don't format numbers on jQuery mobile (it will use HTML5 input fields) 
	                    if (!$.vpmsBindViewModel.isMobile) 
	                    	elementValue=jQuery.numbers.toNumber(elementValue, viewModel.settings);
	                    modelValue(elementValue);		    	
			    	}
			    	else
			    	if(viewModel.format && viewModel.format.indexOf("date")>=0){
			    		var date = null;
			    		if (jQuery.datepicker && !$.vpmsBindViewModel.isMobile){
			    			date = jQuery(element).datepicker('getDate');
			    			var strDate = jQuery.datepicker.formatDate(jQuery(element).datepicker( "option", "dateFormat" ), date, jQuery(element).datepicker("option", "all"));
			    			//forgiving format
			    			if((strDate!=jQuery(element).val()) && Date )
			    				date = Date.parse(jQuery(element).val());
			    		}else
			    			//without datepicker expect date in ISO format
			    			date = $.vpmsBindViewModel.StringToDate(ko.selectExtensions.readValue(element));
		    			if(!date){
		    				date = "";
		    				jQuery(element).val("");
		    			}
		    			modelValue(date);
			    	}
			    	else
			    		modelValue(ko.selectExtensions.readValue(element));	
			    });	    	
		    },
		    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
		    	var newValue = ko.utils.unwrapObservable(valueAccessor());	        
		        if(viewModel.format && viewModel.format.indexOf("number")>=0 && jQuery.numbers){
		        	//Don't format numbers on jQuery mobile (it will use HTML5 input fields)
		        	if (!$.vpmsBindViewModel.isMobile)
		        		newValue=jQuery.numbers.toString(newValue, viewModel.settings);
		        	if (newValue != ko.selectExtensions.readValue(element))
		        		ko.selectExtensions.writeValue(element, newValue);
		    	}
		    	else
		    	if(viewModel.format && viewModel.format.indexOf("date")>=0){
		    		if (jQuery.datepicker && !$.vpmsBindViewModel.isMobile){
		    			var dateFormat = jQuery(element).datepicker( "option", "dateFormat" );
		    			//dateFormat not defined (datepicker not inited) - always process
		    			if (!dateFormat || jQuery(element).val() != jQuery.datepicker.formatDate(dateFormat, newValue)) {
				    		//On first load datepicker is not yet initialized
				    		if (jQuery.datepicker._getInst(element))
				    			$(element).datepicker('setDate', newValue);
				    		else
				    			$(element).data('date_value', newValue);
		    			}
		    		} else {
		    			if (newValue != ko.selectExtensions.readValue(element))
			    			//without datepicker write date in ISO format
			    			ko.selectExtensions.writeValue(element, $.vpmsBindViewModel.DateToString(newValue));
		    		}
		    	}				
		    	else {
		    		if (newValue != ko.selectExtensions.readValue(element))
		    			ko.selectExtensions.writeValue(element, newValue);
		    	}
		    }
	};	
	ko.bindingHandlers.text = {
		    'update': function (element, valueAccessor) {
		        var value = ko.utils.unwrapObservable(valueAccessor());
		        if ((value === null) || (value === undefined))
		            value = "";
		        element.innerHTML = $.vpmsBindViewModel.htmlEncode(value);
		    }
	};
	ko.bindingHandlers.readonly = {
			'update' : function(element, valueAccessor) {
				var value = ko.utils.unwrapObservable(valueAccessor());
				if (value) {
					element.setAttribute("readonly", "readonly");
			    }  else {
			        element.removeAttribute("readonly");
			    }  
			} 	
	};	
}
var wWidth = $(window).width();
var wHeight = $(window).height();
jQuery(document).ready(function($){	
	$(window).resize(function() {
		var newwWidth = $(window).width();
		var newwHeight = $(window).height();		
		if(newwWidth!=wWidth || newwHeight!=wHeight){		
			$.vpmsBindViewModel.adjustElementsWidth($("." + $.vpmsBindViewModel.options.containerClass ));
			wWidth=newwWidth;
			wHeight=newwHeight;
		}
	});
});

//datepicker addon hideOnSelect
//hideOnSelect=false used for manually set focus to input and close Date picker on select date
if (jQuery.datepicker && !$.vpmsBindViewModel.isMobile){
	jQuery.datepicker._defaults.hideOnSelect = true;
	
	//Override the _selectDate method
	//Basically, I just copied the method from the
	//plugin source and added/edited some lines
	jQuery.datepicker._selectDate =  function(id, dateStr) {
		 var target = $(id);
		 var inst = this._getInst(target[0]);
		 dateStr = (dateStr !== null ? dateStr : this._formatDate(inst));
		 if (inst.input)
		     inst.input.val(dateStr);
		 this._updateAlternate(inst);
		 var onSelect = this._get(inst, 'onSelect');
		 var hideOnSelect = this._get(inst, 'hideOnSelect');
		
		 if (onSelect)
		     onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]);  // trigger custom callback
		 else if (inst.input)
		     inst.input.trigger('change'); // fire the change event
		 if (inst.inline || !hideOnSelect)
		     this._updateDatepicker(inst);
		 else {
		     this._hideDatepicker();
		     this._lastInput = inst.input[0];
		     if (typeof(inst.input[0]) != 'object')
		         inst.input.focus(); // restore focus
		     this._lastInput = null;
		
		 }
	};
}

//If a specific behavior is necessary in function checks can be made based on model widget type or id
$.vpmsBindViewModel.pushProperty({
	id: 'Background_ID',
	defaultValue: "white",
	applyBehavior : function (model){
		var propertyId = this.id;
		var color = model[propertyId]!==undefined ? model[propertyId]() : model.settings[propertyId];
		if(color){
			var el = document.getElementById(model.id());
			el.setAttribute("style","background-color:" + color +";");
		}
	}
});

$.vpmsBindViewModel.tmplScriptType = "text/x-kendo-template";
//redefine model.template to model._template, kendo makes all fields observable except that starting with "_", template should not be observable 
$.vpmsBindViewModel.widgets_kendo_previousInitModel = $.vpmsBindViewModel.initModel;
$.vpmsBindViewModel.initModel = function(model, containerId){
	if(model.template)
		model._template = model.template;
	model.template=undefined;
	if($.vpmsBindViewModel.widgets_kendo_previousInitModel)
		$.vpmsBindViewModel.widgets_kendo_previousInitModel(model, containerId);
	return model;
};
$.vpmsBindViewModel.applyBindings = function(container){
	var kendoModel = $(container).data('kendoModel');
	var $product = $("<div class=\"vpms-product-root\"><div id=\"vpms-kendo-container\" class=\"vpms-" + kendoModel.model.widget + "-widget\" data-template=\"" + kendoModel.model.widget + "\" data-bind=\"source: model\"></div></div>");
	$(container).append($product);
	kendo.bind($(container), kendoModel);
};
$.vpmsBindViewModel.initModelObservable = function(model, container){
	var kendoModel = kendo.observable({model:model});
	$(container).data('kendoModel', kendoModel);
	return kendoModel.model;
};
$.vpmsBindViewModel.initModelItem = function(item, defaultValue){
	if(item!=undefined)
		return item;
	else
		return defaultValue;
};
$.vpmsBindViewModel.initModel_parent = function(item, defaultValue){
	//kendo has it's own parent() function
};
$.vpmsBindViewModel.getAccordionOptions = function(active){
	var result = {
		autoHeight : false,
		header: ".vpms-accordion-element-header"
	};
	if(active)
		result.active = active;
	return result;			
};
$.vpmsBindViewModel.getElementSettings = function(element){
	var $element = $(element); 
	if($element.length>0 && $element[0].kendoBindingTarget){
		return $element[0].kendoBindingTarget.source.settings;
	}	
};
$.vpmsBindViewModel.getParent = function(model){
	if(model.parent){
		var parent = model.parent();
		if(parent){
			if(parent.widget)
				return parent;
			else
				return $.vpmsBindViewModel.getParent(parent);
		}
	}
};
$.vpmsBindViewModel.unwrapObservable = function(model, name){
	if(!name)
		return model;
	else{
		if(model.get)
			return model.get(name);
		else
			return model[name];
	}
};
$.vpmsBindViewModel.updateModelItem = function(model, name, value){
	if(model[name] == value)
		return;
	if(model.set)
		model.set(name, value);
	else
		model[name] = value;
};
$.vpmsBindViewModel.dependentObservable = function(model, functionName, read, write){
	model[functionName] = function(){
				return read();
	};
};
$.vpmsBindViewModel.initArrayElement = function(element){
	return {wrapdata:element};
};
$.vpmsBindViewModel.getArrayElement = function(element){
	if(element.wrapdata)
		return element.wrapdata;
	else
		return element;
};		
$.vpmsBindViewModel.getGroupTemplate = function(model, getOriginal){
	if(model._template){
		if($.isFunction(model._template))
			return model._template();
		else{
			if(getOriginal)
				return model._template;
			else
				return $.vpmsBindViewModel.cloneObject(model._template);
		}
	}
	else
		return undefined;
};

var jquery_vpmsBindViewModel_widgets_kendo_web_afterRenderInit = $.vpmsBindViewModel.afterRenderInit;
$.vpmsBindViewModel.afterRenderInit = function($this){
	if(jquery_vpmsBindViewModel_widgets_kendo_web_afterRenderInit)
		jquery_vpmsBindViewModel_widgets_kendo_web_afterRenderInit($this);
	//remove class on init - afterRender will be called for whole container anyway
	$this.find(".vpms-afterRender").removeClass("vpms-afterRender");	
};

var jquery_vpmsBindViewModel_widgets_kendo_web_afterRender = $.vpmsBindViewModel.afterRender;
$.vpmsBindViewModel.afterRender = function($this, containerId){
	if(jquery_vpmsBindViewModel_widgets_kendo_web_afterRender)
		jquery_vpmsBindViewModel_widgets_kendo_web_afterRender($this, containerId);
	if($this.hasClass("vpms-treeview-tab"))
		$this.treeview('changed');
}


kendo.data.binders.enableui = kendo.data.Binder.extend({
    refresh: function() {
        var that = this,
            value = that.bindings["enableui"].get();
                
		var $element = jQuery(that.element);
        if (value && that.element.disabled){
			if($element.hasClass("ui-button") || $element.hasClass("ui-btn-hidden"))
				$element.button("enable");
			else
				that.element.removeAttribute("disabled");
		}
        else if ((!value) && (!that.element.disabled)){
			if($element.hasClass("ui-button") || $element.hasClass("ui-btn-hidden"))
				$element.button("disable");
			else
				that.element.disabled = true;
		}        
    }
});

kendo.data.binders.textui = kendo.data.Binder.extend({
    refresh: function() {
        var that = this,
            value = that.bindings["textui"].get();

        
    	if ((value === null) || (value === undefined))
            value = "";
		var $element = jQuery(that.element);
		if($element.children().length==0)
			typeof that.element.innerText == "string" ? that.element.innerText = value : that.element.textContent = value;			
		else
			$element.children().each(function(){
				typeof this.innerText == "string" ? this.innerText = value : this.textContent = value;			
			});                
    }
});

kendo.data.binders.typedValue = kendo.data.Binder.extend({
    init: function(element, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, element, bindings, options);

        var that = this;
        //listen for the change event of the element
        $(that.element).on("change", function() {
            that.change(); //call the change function
        });
    },
    refresh: function() {
        var that = this,
        	newValue = that.bindings["typedValue"].get(),
        	viewModel = that.bindings["typedValue"].source,
        	element = this.element;
        
        if(viewModel.format && viewModel.format.indexOf("number")>=0 && jQuery.numbers){
        	//Don't format numbers on jQuery mobile (it will use HTML5 input fields)
        	if (!$.vpmsBindViewModel.isMobile)
        		newValue=jQuery.numbers.toString(newValue, viewModel.settings);
        	if (newValue != $(element).val())
        		$(element).val(newValue);
    	}
    	else
    	if(viewModel.format && viewModel.format.indexOf("date")>=0){
    		if (jQuery.datepicker && !$.vpmsBindViewModel.isMobile){
    			var dateFormat = jQuery(element).datepicker( "option", "dateFormat" );
    			//dateFormat not defined (datepicker not inited) - always process
    			if (!dateFormat || jQuery(element).val() != jQuery.datepicker.formatDate(dateFormat, newValue)) {
		    		//On first load datepicker is not yet initialized
		    		if (jQuery.datepicker._getInst(element))
		    			$(element).datepicker('setDate', newValue);
		    		else
		    			$(element).data('date_value', newValue);
    			}
    		} else {
    			//without datepicker write date in ISO format
    			$(element).val($.vpmsBindViewModel.DateToString(newValue));
    		}
    	}				
    	else {
    		$(element).val(newValue);
    	}
    },
    change: function() {
        var elementValue = this.element.value,
        	viewModel = this.bindings["typedValue"].source,
        	element = this.element;
        
		//TODO handle all types
        if(viewModel.format && viewModel.format.indexOf("number")>=0 && jQuery.numbers){	            	
            //Don't format numbers on jQuery mobile (it will use HTML5 input fields) 
            if (!$.vpmsBindViewModel.isMobile) 
            	elementValue=jQuery.numbers.toNumber(elementValue, viewModel.settings);
            this.bindings["typedValue"].set(elementValue);
    	}
    	else
    	if(viewModel.format && viewModel.format.indexOf("date")>=0){
    		var date = null;
    		if (jQuery.datepicker && !$.vpmsBindViewModel.isMobile){
    			date = jQuery(element).datepicker('getDate');
    			var strDate = jQuery.datepicker.formatDate(jQuery(element).datepicker( "option", "dateFormat" ), date);
    			//forgiving format
    			if((strDate!=jQuery(element).val()) && Date )
    				date = Date.parse(jQuery(this.element).val());
    		}else
    			//without datepicker expect date in ISO format
    			date = $.vpmsBindViewModel.StringToDate(this.bindings["typedValue"].get());
			if(!date){
				date = "";
				jQuery(element).val("");
			}
			this.bindings["typedValue"].set(date);
    	}
    	else
    		this.bindings["typedValue"].set(elementValue);
    }	         
});

kendo.data.binders.css = kendo.data.Binder.extend({
	  init: function (target, bindings, options) {
	    kendo.data.Binder.fn.init.call(this, target, bindings, options);

	    // get list of class names from our complex binding path object
	    this._lookups = [];
	    for (var key in this.bindings.css.path) {
	      this._lookups.push({
	        key: key,
	        path: this.bindings.css.path[key]
	      });
	    }
	  },

	  refresh: function () {
	    var lookup,
	    value;

	    for (var i = 0; i < this._lookups.length; i++) {
	      lookup = this._lookups[i];

	      // set the binder's path to the one for this lookup,
	      // because this is what .get() acts on.
	      this.bindings.css.path = lookup.path;
	      value = this.bindings.css.get();

	      // add or remove CSS class based on if value is truthy
	      if (value) {
	        $(this.element).addClass(lookup.key);
	      } else {
	        $(this.element).removeClass(lookup.key);
	      }
	    }
	  }
	});

//element widget
$.vpmsBindViewModel.pushTemplate({
	name: 'element',
	template: '\
			<div class="#if(data.wrapdata.container){#vpms-container#}else{#vpms-element#}##if(data.wrapdata.settings && data.wrapdata.settings.span){# vpms-span#=data.wrapdata.settings.span##}#" data-bind="visible: wrapdata.relevant">\
				#if(data.wrapdata.hasLabel){#\
					<div class="ui-widget vpms-label-widget#if(!data.wrapdata.labeled){# vpms-notlabeled#}##if(data.wrapdata.settings){if(data.wrapdata.settings.css){# #=data.wrapdata.settings.css##} if(data.wrapdata.settings.labelAlign){# vpms-align-#=data.wrapdata.settings.labelAlign##}}#" data-bind="source:wrapdata" data-template="label"></div>\
				#}#\
				#if(data.wrapdata.widget && jQuery.vpmsBindViewModel.templateExists(data.wrapdata.widget)){#\
					<div class="ui-widget vpms-#=data.wrapdata.widget#-widget ui-corner-all#if(!data.wrapdata.labeled){# vpms-notlabeled#}##if(data.wrapdata.settings){ if(data.wrapdata.settings.css){# #=data.wrapdata.settings.css##} if(data.wrapdata.settings.align){# vpms-align-#=data.wrapdata.settings.align##}}#" data-template="#=data.wrapdata.widget#" data-bind="source: wrapdata"></div>\
				#}#\
			</div>\
			'
});

//button widget - defaults: isElement=false
//options: id, caption, enable, visible click (function)
$.vpmsBindViewModel.pushTemplate({
	name: 'button',
	template: '<div class="vpms-button-widget" data-bind="visible:visible">\
				<button type="button" data-bind="enableui:enable, attr:{id:id, tooltip:tooltip}, textui:caption, click:click"></button>\
			</div>',
	initModel: function(model){
		model.hasLabel = false;
		if(model.isElement==undefined)
			model.isElement = true;
		if(model.setValue==undefined)
			model.setValue="1";

		if(!model.pos) model.pos = "left";
		if(!model.name) model.name = "btn." + model.role;
		if(!model.role){
			if($.vpmsBindViewModel.roleBindings[model.name])
				model.role = model.name;
			else
				model.role = 'default';
		}
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "btn." + model.role);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");

		var initButtonItem = function(model, name, init, defaultValue){
			if(model[name] && !jQuery.isFunction(model[name]))
				defaultValue = model[name];
			//by priority - function, role
			if(model[name] && jQuery.isFunction(model[name])){
				model["_" + name] = model[name];
			}else
			if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role][name]){
				model["_" + name] = function(){
					return $.vpmsBindViewModel.roleBindings[this.role][name].call($.vpmsBindViewModel.roleBindings[model.role], this); 
				};
			}
			if(init && jQuery.isFunction(init)){
				init(model);
			}
			model[name] = defaultValue;			
		}
		initButtonItem(model, "caption", function(model){
			if(!model._caption && !model.isElement){
				//if not element - get button's caption from resources
				model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
				var caption = $.vpmsBindViewModel.element(model.captions, model.id, false);
				if(caption==false){
					caption  = $.vpmsBindViewModel.initModel_caption({name:model.id, caption:model.caption});
					model.captions.push(caption);
				}
				model._caption = function(){
					var caption = $.vpmsBindViewModel.element(this.captions, this.id, false);
					if(caption!=false)
						return caption.caption;
					else
						return _model.caption;
				};
			}
		}, model.name);
		initButtonItem(model, "visible", function(model){
			if(!model._visible && model.isElement){
				model._visible = function(){
					return this.relevant;
				};
			}
		}, true);
		initButtonItem(model, "enable", function(model){
			if(!model._enable && model.isElement){
				model._enable = function(){
					return !this.disabled;
				};
			}
		}, true);
		//click
		if(model.click)
			model._click = model.click;
		model.click = function(event){
			//set value
			jQuery.vpmsBindViewModel.updateModelItem(this, "value", this.setValue);
			//call user defined function if set
			if(this._click)
				this._click(event);
			else
				if(this.role && $.vpmsBindViewModel.roleBindings[this.role] && $.vpmsBindViewModel.roleBindings[this.role]['click']){
					var $container = $(event.target).closest("." + jQuery.vpmsBindViewModel.options.containerClass);
					var rootmodel = $container.data('model');
					if(!rootmodel.actions)
						rootmodel.actions = {};
					var action = {};
					//action to be filled with parameters
					rootmodel.actions[this.role] = action;
					if(!rootmodel.actionsModels)
						rootmodel.actionsModels = {};
					//action button to pass in updater
					rootmodel.actionsModels[this.role] = this;
					$.vpmsBindViewModel.roleBindings[this.role]['click'].call($.vpmsBindViewModel.roleBindings[this.role], event.target, this, rootmodel, action);
				}
		}
		if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role]['initModel']){
			$.vpmsBindViewModel.roleBindings[model.role]['initModel'].call($.vpmsBindViewModel.roleBindings[model.role], model);
		}
	},
	updateModel: function(model, data){	
		if(jQuery.isFunction(model._caption))
			jQuery.vpmsBindViewModel.updateModelItem(model, "caption", model._caption());
		if(jQuery.isFunction(model._visible))
			jQuery.vpmsBindViewModel.updateModelItem(model, "visible", model._visible());
		if(jQuery.isFunction(model._enable))
			jQuery.vpmsBindViewModel.updateModelItem(model, "enable", model._enable());			
		if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role]['updateModel']){
			$.vpmsBindViewModel.roleBindings[model.role]['updateModel'].call(this, model, data);
		}		
	}	
});

//enable, visible, click, update
jQuery.vpmsBindViewModel.roleBindings['next'] = {
	'enable' : function(model){
		var widget = $.vpmsBindViewModel.getParentHasItem(model, "nextPage");
		if(widget)
			return (widget.nextPage()!==undefined);
		else
			return false;
	},
	'click' : function(element, model, rootmodel, action){
		var widget = $.vpmsBindViewModel.getParentHasItem(model, "nextPage");
		if(widget){
			var id = widget.nextPage();
			widget.nextPage(id);
		}
	},
	'updateModel' : function(model, data){
		//postpone updating "enable" since selectedPage is set in Pages after updating buttons
		if(jQuery.isFunction(model._enable)){
			setTimeout(function(){
				jQuery.vpmsBindViewModel.updateModelItem(model, "enable", model._enable());
			}, 1);
		}
	}
};

jQuery.vpmsBindViewModel.roleBindings['previous'] = {
	'enable' : function(model){
		var widget = $.vpmsBindViewModel.getParentHasItem(model, "prevPage"); 			
		if(widget)
			return (widget.prevPage()!==undefined);
		else
			return false;
	},
	'click' : function(element, model, rootmodel, action){
		var widget = $.vpmsBindViewModel.getParentHasItem(model, "prevPage"); 			
		if(widget){
			var id = widget.prevPage();
			widget.prevPage(id);
		}
	},
	'updateModel' : function(model, data){
		//postpone updating "enable" since selectedPage is set in Pages after updating buttons
		if(jQuery.isFunction(model._enable)){
			setTimeout(function(){
				jQuery.vpmsBindViewModel.updateModelItem(model, "enable", model._enable());
			}, 1);
		}
	}
};

jQuery.vpmsBindViewModel.roleBindings['save'] = {
	'click' : function(element, model, rootmodel, action){
		jQuery(element).vpmsBindViewModel("updateModel");
	}
};

jQuery.vpmsBindViewModel.roleBindings['cancel'] = {
	'click' : function(element, model, rootmodel, action){
		jQuery(element).vpmsBindViewModel("changeModel");
	}
};

jQuery.vpmsBindViewModel.roleBindings['pdf'] = {
	'initModel' : function(model){
		if(!model.settings) model.settings={};
		model.settings.ignoreDisabled = true;
	},		
	'click' : function(element, model, rootmodel, action){
		jQuery(element).vpmsBindViewModel("updateModel");
	},
	'update' : function(model, rootmodel, action){
		//if button still enabled
		var container = jQuery('#' + rootmodel.containerId); 
		if(model){
			if(model.enable==undefined || model.enable)
				$.vpmsBindViewModel.openPdf(container);
		}else{
			//actually should not happen : if pdf button  is not set open pdf anyway?
		}
	}
};

jQuery.vpmsBindViewModel.roleBindings['default'] = {
	'click' : function(element, model, rootmodel, action){
		jQuery.vpmsBindViewModel.updateVPMSModel(jQuery(element).closest("." + $.vpmsBindViewModel.options.containerClass), true);
	}
};

//text widget - defaults: isElement=false
$.vpmsBindViewModel.pushTemplate({
	name: 'text',
	template: '<div class="vpms-text-widget-text" data-bind="attr: {id: id}, text:displayValue"></div>',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = false;	
		if(!model.name) model.name = "text";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.text = $.vpmsBindViewModel.initModelItem(model.text, "");
		model.displayValue = $.vpmsBindViewModel.initModelItem(model.text, "");
	},
	updateModel: function(model, data){
		if(model.isElement)
			$.vpmsBindViewModel.updateModelItem(model, 'displayValue', model.caption);
	}
});

//image widget - defaults: isElement=false
$.vpmsBindViewModel.pushTemplate({
	name: 'image',
	template: '<img class="vpms-image-widget-image" data-bind="attr: {src:src, tooltip:tooltip, id:id}" style="#if(data.width){#width=#=data.width#;#}##if(data.height){#height=#=data.height#;#}#"/>',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = false;
		if(!model.name) model.name = "image";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, true);
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model._src = model.src
		model.src = $.vpmsBindViewModel.initModelItem(model.src, "");
	},
	updateModel: function(model, data){
		if(!model._src)
			$.vpmsBindViewModel.updateModelItem(model, 'src', model.value);
	}

});

//Border widget - defaults: isGroup=true, composite=false, container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'border',
	template: '<fieldset class="vpms-border-widget-fieldset ui-widget-content ui-corner-all" data-bind="attr: {id: id}">\
				<legend class="vpms-border-widget-caption ui-widget-header">\
					<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: required"><span class="ui-icon ui-icon-bullet"></span></div>\
					<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error #if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: error}">&\\#10033;</span></div>\
					<span data-bind="text: caption, attr:{tooltip:tooltip}"></span>\
				</legend>\
				<div class="vpms-elements-container vpms-#=data.settings.colNumber#-col" data-template="element" data-bind="source:elements"></div>\
				</fieldset>',
	initModel: function(model){
		model.hasLabel = false;
		if(model.isGroup==undefined)
			model.isGroup = true;
		if(!model.isGroup && model.isElement==undefined)
			model.isElement = true;
		if(model.container==undefined)
			model.container = true;
		if(!model.settings)
			model.settings = {};
		if(model.settings.colNumber===undefined)
			model.settings.colNumber = 'no';
		if(!model.name) model.name = "border";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
	}
});

//Label widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'label',
	template: '<span><div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: required"><span class="ui-icon ui-icon-bullet"></span></div>\
				<label class="vpms-label-widget-label" data-bind="attr:{for:id}, text:caption"></label></span>',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = true;
		if(!model.name) model.name = "label";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
	}	
});

//value widget - defaults: isElement=true	
$.vpmsBindViewModel.pushTemplate({
	name: 'value',
	template:  '\
		<div data-bind="css: {vpms-widget_with_error_container:error}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted#if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: error}">&\\#10033;</span></div>\
			<div class="vpms-value-widget-value #=data.format#" data-bind="attr:{id:id, tooltip:tooltip}, text:displayValue"></div>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = true;
		if(model.disabled==undefined)
			model._disabled = true;
		if(!model.name) model.name = "value";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.displayValue = $.vpmsBindViewModel.initModelItem($.vpmsBindViewModel.readFormattedValue(model, model.value), ""); 
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, true);
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
	},
	updateModel: function(model, data){
		$.vpmsBindViewModel.updateModelItem(model, 'displayValue', $.vpmsBindViewModel.readFormattedValue(model, model.value));
	}
});

//Edit widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'edit',
	template: '\
		<div data-bind="css: {vpms-widget_with_error_container:error}">\
			<div class="ui-state-error" data-bind="visible:error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted#if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr:{error:error}">&\\#10033;</span></div>\
			<input class="#=data.format# #if(data.autocommit){#vpms-autocommit#}#" data-bind="events:{change:onChange}, attr:{id:id, name:id, tooltip:tooltip}, typedValue:value, disabled:disabled" #if(data.settings && data.settings.mask){#mask="#=data.settings.mask#"#}# #if(data.settings && data.settings.maxlength){#maxlength="#=data.settings.maxlength#"#}#/>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = true;
		if(!model.name) model.name = "edit";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		if(model.format && model.format.indexOf("date")>=0)
			model.value = $.vpmsBindViewModel.initModelItem(model.value, null);
		else
			model.value = $.vpmsBindViewModel.initModelItem(model.value, "");		
		if(model.generateDisplayValue)
			model.displayValue = $.vpmsBindViewModel.initModelItem($.vpmsBindViewModel.readFormattedValue(model, model.value), "");
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(this, e);
			return true;
		}
	},
	updateModel: function(model, data){
		if(model.generateDisplayValue)
			$.vpmsBindViewModel.updateModelItem(model, 'displayValue', $.vpmsBindViewModel.readFormattedValue(model, model.value));
	}
});
$.vpmsBindViewModel.pushTemplate({
	name: 'file',
	template: '\
		<div data-bind="css: {vpms-widget_with_error_container:error}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted#if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: error}">&\\#10033;</span></div>\
			<label class="vpms-file-widget-name" data-bind="text:value, invisible:settings.upload"></label>\
			<form method="post" enctype="multipart/form-data">\
				<input name="fileContent" type="file" data-bind="events:{change:onChange}, disabled:disabled" #if(!data.settings.upload){#style="display:none"#}#/><input name="quoteId" type="hidden" value=""/><input name="fileNodeId" type="hidden" data-bind="value:settings.fileId"/><input name="fileInstanceId" type="hidden" data-bind="value:id"/>\
			</form>\
			<div class="vpms-file-button" data-bind="visible:settings.download">\
				<div class="vpms-button-widget"><button type="button" data-bind="enableui:downloadEnabled, textui: settings.downloadCaption, click:downloadFile"></button></div>\
			</div>\
			<div class="vpms-file-button" data-bind="visible:settings.upload">\
				<div class="vpms-button-widget"><button type="button" data-bind="enableui:uploadEnabled, textui: settings.uploadCaption, click:uploadFile"></button></div>\
			</div>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = true;
		if(!model.name) model.name = "file";
		if(!model.settings) model.settings = {};
		if(model.settings.upload == undefined)
			model.settings.upload = true;
		if(model.settings.download == undefined)
			model.settings.download = true;
		if(model.settings.downloadCaption == undefined)
			model.settings.downloadCaption = "Download";
		if(model.settings.uploadCaption == undefined)
			model.settings.uploadCaption = "Upload";
		if(model.settings.fileId == undefined)
			model.settings.fileId = model.name;		
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.uploadEnabled = $.vpmsBindViewModel.initModelItem(undefined, false);
		model.downloadEnabled = $.vpmsBindViewModel.initModelItem(undefined, false);
		
		model.onChange = function(e){
			$.vpmsBindViewModel.updateModelItem(this, "uploadEnabled", (jQuery(e.target).val()!=""));
			return true;
		};	
		model.afterUpload = function(form, message){
			var $container = $(form).closest("." + $.vpmsBindViewModel.options.containerClass);
			if(message!=''){
				$.vpmsBindViewModel.dialog($container.attr('id') + '_error_dialog', "Uploading error", message, {width:$container.width()-100});
			}else 
				$.vpmsBindViewModel.updateVPMSModel($(form).closest("." + $.vpmsBindViewModel.options.containerClass), true);
		};
		model.uploadFile = function(event){
			$.vpmsBindViewModel.uploadFile(jQuery(event.target).closest('.vpms-file-widget').find('form'), model);
		};
		model.downloadFile = function(event){
			var $container = $(event.target).closest("." + $.vpmsBindViewModel.options.containerClass);
			$.vpmsBindViewModel.downloadFile($container, this);
		};		
	},
	updateModel: function(model, data){
		$.vpmsBindViewModel.updateModelItem(model, "downloadEnabled", (model.value!="" && model.value!=undefined));
	}
});
//Memo widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'memo',
	template: '\
		<div data-bind="css: {vpms-widget_with_error_container:error}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted#if(!jQuery.vpmsBindViewModel.isMobileAgent)#{ ui-icon ui-icon-alert#}#" data-bind="attr: {error: error}">&\\#10033;</span></div>\
			<textarea #if(data.autocommit)(#class="vpms-autocommit"#}# data-bind="events:{change:onChange}, attr:{id:id, name:id, tooltip:tooltip, rows:rows}, value:value, disabled:disabled" #if(data.settings && data.settings.maxlength){#maxlength="#=data.settings.maxlength#"#}#/>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = true;
		if(model.rows==undefined)
			model.rows = 2;
		if(!model.name) model.name = "memo";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.errorclass = $.vpmsBindViewModel.initModelItem(model.errorclass, "");		
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(this, e);
			return true;
		}
	}	
});

//combobox widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'combobox',
	template: '\
		<div class="#if(data.size>1){#vpms-height-auto#}#" data-bind="css: {vpms-widget_with_error_container:error}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted#if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: error}">&\\#10033;</span></div>\
			<select class="#if(data.autocommit){#vpms-autocommit#}#" data-value-field="_v" data-text-field="_x" data-bind="source:choices, events:{change:onChange}, attr:{id:id, name:id, tooltip:tooltip, size:size},  value:value, disabled:disabled"></select></span>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = true;
		if(model.size==undefined)
			model.size = 1;
		if(!model.name) model.name = "combobox";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(this, e);
			return true;
		}
	},
	updateModel: function(model, data){
		//IE8 disabled Listbox fix: selected item not highlighted  
		if(model.size>1){
			$('#' + model.containerId).find('#' + model.id).each(function(){
				$(this).children('option').removeClass("vpms-combobox-widget-selected-option");
				if($(this).is(":disabled"))
					$(this).children('option:selected').addClass("vpms-combobox-widget-selected-option");
			});
		}
	}
});

//TODO - Autocomplete

//checkbox widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'checkbox',
	template: '\
		<div data-bind="css: {vpms-widget_with_error_container:error}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted#if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: error}">&\\#10033;</span></div>\
			<input class="#if(autocommit){#vpms-autocommit#}#" type="checkbox" data-bind="events:{click:onChange},attr:{id:id, name:id, tooltip:tooltip}, checked:checked, disabled:disabled" />\
			<label data-bind="text:caption, attr:{for:id}"></label>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = true;
		model.format = "boolean";
		model.labeled = false; //label will be built in widget itself
		if(!model.name) model.name = "checkbox";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "0");
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		if($.vpmsBindViewModel.element(model.captions, 'text.yes', false)==false)
			model.captions.push($.vpmsBindViewModel.initModel_caption({name:'text.yes', caption:'yes'}));
		if($.vpmsBindViewModel.element(model.captions, 'text.no', false)==false)
			model.captions.push($.vpmsBindViewModel.initModel_caption({name:'text.no', caption:'no'}));
		model.checked = $.vpmsBindViewModel.initModelItem(model.checked, false);
		
		if(model.generateDisplayValue){
			model.readDisplayValue = function(){
				if(this.checked)
					return $.vpmsBindViewModel.element(this.captions, 'text.yes').caption;
				else
					return $.vpmsBindViewModel.element(this.captions, 'text.no').caption;
			};
			model.displayValue = $.vpmsBindViewModel.initModelItem(model.readDisplayValue(), "");
		};
		model.onChange = function(e){
			if(e.target.checked)
				$.vpmsBindViewModel.updateModelItem(this, "value", "1");
			else
				$.vpmsBindViewModel.updateModelItem(this, "value", "0");
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(this, e);	
			return true;
		};
	},
	updateModel: function(model, data){
		$.vpmsBindViewModel.updateModelItem(model,"checked", model.value=="1");
		if(model.generateDisplayValue)
			model.displayValue(model.readDisplayValue()); 
	}
});
//checkbox widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'checkboxAfterLabel',
	template: '\
	<div data-bind="css: {vpms-widget_with_error_container:error}">\
		<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted#if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: error}">&\\#10033;</span></div>\
		<input class="#if(data.autocommit){#vpms-autocommit#}#" type="checkbox" data-bind="events:{click:onChange}, attr:{id:id, name:id, tooltip:tooltip}, checked:checked, disabled:disabled" />\
	</div>\
	',	
	initModel: function(model){
		var labeled = model.labeled;
		$.vpmsBindViewModel.initModel_widget(model, 'checkbox');
		model.labeled = labeled;
	}
});


//radiogroup widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'radiogroupElement',
	template: '\
	<div class="vpms-radiogroup-item-widget">\
	<input class="#if(parent().parent().autocommit){#vpms-autocommit#}#" type="radio" #if(data._v==data.parent().parent().value){#checked#}# name="#=data.parent().parent().id#" id="#=data.parent().parent().id#val#=_v#" value="#=_v#" data-bind="events:{change:onChange}, checked:value, disabled:disabled" #if(data.parent().parent().tooltip){#tooltip="#=data.parent().parent().tooltip#"#}#/><label for="#=data.parent().parent().id#val#=_v#" data-bind="text:_x"></label>\
	</div>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'radiogroup',
	template: '\
		<div data-bind="css: {vpms-widget_with_error_container:error}, attr: {id: id}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted#if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: error}">&\\#10033;</span></div>\
			<div class="vmps-radiogroup-items vpms-#=data.settings.colNumber#-col" data-template="radiogroupElement" data-bind="source: choices"></div>\
		</div>\
	',
	initModel: function(model){
		if(model.isElement==undefined)
			model.isElement = true;
		if(!model.name) model.name = "radiogroup";
		if(!model.settings)
			model.settings = {};
		if(model.settings.colNumber===undefined)
			model.settings.colNumber = 'no';
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.autocommit = $.vpmsBindViewModel.initModelItem(model.autocommit, false);
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(this, e);
			return true;
		};
	}	
});

$.vpmsBindViewModel.pushTemplate({
	name: 'elementgroup',
	template: '\
		<div data-bind="css: {vpms-widget_with_error_container:error}, attr: {id: id}">\
			<div class="ui-state-error" data-bind="visible: error"><span class="vpms-tooltip-error vpms-tooltip-error-shifted#if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: error}">&\\#10033;</span></div>\
			<div class="vpms-elementgroup-elements">\
				<div class="vpms-elements-container #if(data.settings && data.settings.colNumber){# vpms-#=data.settings.colNumber#-col#}#" data-template="element" data-bind="source:elements"></div>\
			</div>\
		</div>\
	',
	initModel: function(model){
		if(!model.labeled)
			model.hasLabel = false;
		if(model.isGroup==undefined)
			model.isGroup = true;
		if(!model.name) model.name = "elementgroup";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);
	}
});

//accordionElement widget  - defaults container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'accordionElement',
	template: '\
	<div>\
		<div class="vpms-accordion-element-header" #if(parent().parent().nocaption){#style="display:none"#}# data-bind="attr:{id:wrapdata.id, relevant:wrapdata.relevant}, click:selectGroupClick"><div style="display:block;">\
				<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: wrapdata.required"><span class="ui-icon ui-icon-bullet"></span></div>\
				<div class="ui-state-error" data-bind="visible: wrapdata.error"><span class="vpms-tooltip-error #if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: wrapdata.error}">&\\#10033;</span></div>\
				<div class="vpms-accordion-caption" data-bind="text:wrapdata.caption, attr:{tooltip:wrapdata.tooltip}"></div>\
				<div style="clear:both"></div>\
			</div>\
		</div>\
		<div class="vpms-accordion-element" data-bind="attr: {id:wrapdata.id, relevant:wrapdata.relevant}">\
			#if($.vpmsBindViewModel.templateExists(data.wrapdata.widget)){#<div class="vpms-#=data.wrapdata.widget#-widget" data-template="#=data.wrapdata.widget#" data-bind="source:wrapdata"></div>#}#\
			<div style="clear:both"></div>\
			#if(parent().parent().multiple){#\
			<div class="vpms-remove-button" data-bind="visible:mutable">\
				<div class="vpms-button-widget"><button type="button" data-bind="enableui:wrapdata.canRemove, textui: wrapdata.removeCaption, click:removeGroup"></button></div>\
			</div>\
			#}#\
			<div style="clear:both"></div>\
		</div>\
	</div>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'accordion',
	template: '<div class="vpms-accordion-container" data-bind="attr:{id:id}, visible:relevant">\
		<div class="#if(!data.noaccordion){#vpms-accordion#}#" data-template="accordionElement" data-bind="source:groups"></div>\
		#if(data.multiple){#\
			<div class="vpms-add-button" data-bind="visible:mutable">\
				<div class="vpms-button-widget"><button type="button" data-bind="enableui:canAdd, textui: addCaption, click:addGroup"></button></div>\
			</div>\
		#}#\
		</div>\
	',
	initModel: function(model){
		if(!model.name) model.name = "accordion";
		//accordion has no selected Page - get and update all groups
		model.getAllGroupsData = true;
		model.setAllGroupsData = true;
		if(model.onlyRelevant == undefined && model.getAllGroupsData)
			model.onlyRelevant = true;				
		if(model.container==undefined)
			model.container = true;	
		if(model.noaccordion==undefined)
			model.noaccordion = false;
		if(model.nocaption==undefined || !model.noaccordion)
			model.nocaption = false;
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.selectedId = $.vpmsBindViewModel.initModelItem(model.selectedId, "");

		model.afterGroupAdded = function(model, group, data){
			var id = $.vpmsBindViewModel.getArrayElement(group).id;
			var elem = jQuery('#'+id+".vpms-accordion-element, #"+id+".vpms-accordion-element-header");
			if(!$.vpmsBindViewModel.isModelIniting(elem)){
				var $accordion = $(elem).closest('.vpms-accordion');
				if(!$accordion.hasClass('vmps-recreate-accordion-pending')){
					$accordion.addClass('vmps-recreate-accordion-pending');
					var that = model;
					setTimeout(function(){
						var activeIndex = 0; 
						if($accordion.hasClass('ui-accordion')){
							activeIndex = $accordion.accordion('option', 'active');
							$accordion.accordion('destroy');
						}
						if(that._selectedId){
							var accordionHeaders = $accordion.children(".vpms-accordion-element-header");
							activeIndex =accordionHeaders.index(accordionHeaders.filter('#' + that._selectedId));
						}
						$accordion.accordion($.vpmsBindViewModel.getAccordionOptions(activeIndex));
						$accordion.removeClass('vmps-recreate-accordion-pending');
					}, 1);
				}
				setTimeout(function(){$.vpmsBindViewModel.afterRender(elem, model.containerId);}, 1);
			}
		};
		model.beforeGroupRemove = function(group){
			var _group = $.vpmsBindViewModel.getArrayElement(group);
			var elem = jQuery('#' + _group.id + ".vpms-accordion-element-header");
			var $accordion = $(elem).closest('.vpms-accordion');
			var that = this;
			setTimeout(function(){				
				if(that._selectedId){
					$accordion.accordion('option', 'active',jQuery('#' + that._selectedId + ".vpms-accordion-element-header"));
				}
			}, 1);
		};
		if(model.multiple){
			model.mutable = function(){
				return !$.vpmsBindViewModel.unwrapObservable(this,'disabled') && !$.vpmsBindViewModel.unwrapObservable(this, 'staticPageCount');
			};
			model.removeGroup = function(event){				
				jQuery(event.target).vpmsBindViewModel('removeFromGroup', this, $.vpmsBindViewModel.getArrayElement(event.data));
			};
			model.addGroup = function(event){
				if(this.insertAfterSelected){
					var selectedPage = undefined;
					var $accordion = jQuery(event.target).closest('.vpms-accordion-widget').children('.vpms-accordion-container').children('.ui-accordion');
					if($accordion.length>0){
						var active = $accordion.accordion( "option", "active" );
						if(active!=undefined){
							$headers = $accordion.children('.vpms-accordion-element-header');
							if($headers.length>active)
								selectedPage = jQuery($headers[active]).attr('id');
						}
					}
					jQuery(event.target).vpmsBindViewModel('addToGroup', this, selectedPage);
				}
				else
					jQuery(event.target).vpmsBindViewModel('addToGroup', this);				
			};
		}//model.multiple		
		model.selectGroupClick = function(event){
			var group = $.vpmsBindViewModel.getArrayElement(event.data);
			this._selectedId = group.id;
			if(this.insertAfterSelected){
				var caption = $.vpmsBindViewModel.upwrapObservable(group, "addCaption");
				if(caption)
					$.vpmsBindViewModel.updateModelItem(this, "addCaption", caption);
			}
		};
	},
	updateModel: function(model, data){
		if(data.sp)
			model._selectedId = data.sp;
	}
});

//superWidget - defaults container=true, isGroup=true, composite=true
$.vpmsBindViewModel.pushTemplate({
	name: 'superWidget',
	template: '<div class="vpms-elements-container #if(data.settings && data.settings.colNumber){# vpms-#=data.settings.colNumber#-col#}#" data-template="element" data-bind="source:elements"></div>',
	initModel: function(model){
		if(model.container==undefined)
			model.container = true;
		if(!model.settings)
			model.settings = {};
		if(model.settings.colNumber===undefined)
			model.settings.colNumber = 'no';
		if(!model.name) model.name = "superWidget";
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
	}
});

//pages widget - defaults container=true, tabbed=true
$.vpmsBindViewModel.pushTemplate({
	name: 'pagesTabs',
	template: '\
	<li class="ui-state-default ui-corner-top" data-bind="visible:wrapdata.relevant, attr:{id:wrapdata.id}, css: {ui-state-active: isSelected}, click: tabClick">\
		<a class="vpms-tab-link" data-bind="attr: {id: wrapdata.id}">\
			<span class="vpms-tab-link-opened-icon ui-icon ui-icon-carat-1-s"></span>\
			<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: wrapdata.required"><span class="ui-icon ui-icon-bullet"></span></div>\
			<div class="vpms-tab-icon ui-state-error" data-bind="visible: wrapdata.error"><span class="vpms-tooltip-error#if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: wrapdata.error}">&\\#10033;</span></div>\
			<div class="vpms-tab-icon ui-state-error" data-bind="visible: isVisited"><span class="ui-icon ui-icon-check"></span></div>\
			<span class="vpms-tab-link-text" data-bind="text:wrapdata.caption, attr:{tooltip:wrapdata.tooltip}"></span>\
			#if(parent().parent().multiple){#\
				<span class="vpms-pages-remove-button" data-bind="visible: _canRemove, attr:{tooltip:wrapdata.removeCaption}">&\\#10006;</span>\
			#}#\
		</a>\
	</li>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'pagesSections',
	template: '\
		<div class="vpms-pages-section" data-bind="attr: {id: wrapdata.id}, visible:isSelected">\
			#if(jQuery.vpmsBindViewModel.templateExists(data.wrapdata.widget)){#\
				<div class="vpms-#=data.wrapdata.widget#-widget" data-template="#=data.wrapdata.widget#" data-bind="source:wrapdata"></div>\
			#}#\
		</div>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'pages',
	template: '<div>\
	<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" data-bind="visible:relevant">\
		#if(data.tabbed){#\
		<div class="vpms-tabs-widget">\
			<ul class="vpms-tab-menu ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">\
				<span data-template="pagesTabs" data-bind="source:groups"></span>\
				#if(data.multiple){#\
					<li class="ui-state-default ui-corner-top" data-bind="visible:_canAdd, click:addClick"><a class="vpms-tab-link">\
					<span class="vpms-pages-add-button" data-bind="attr:{tooltip:_addCaption}">&\\#10010;</span>\
					</a></li>\
				#}#\
			</ul>\
		</div>\
		#}#\
		<div class="vpms-pages-container">\
			<div class="vpms-clear-float"></div>\
			<div data-template="pagesSections" data-bind="source:groups"></div>\
		</div>\
		<div class="vpms-clear-float"></div>\
	</div>\
	<div class="vpms-elements-container" data-template="element" data-bind="source:elements"></div>\
	</div>\
',
	initModel :function(model){
		if(model.container==undefined)
			model.container = true;
		if(model.getAllGroupsData)
			 model.syncUpdate = false;
		if(model.onlyRelevant == undefined && model.getAllGroupsData)
			model.onlyRelevant = true;				
		if(model.tabbed==undefined)	model.tabbed = true;

		if(!model.name) model.name = "pages";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
	
		model.selectedPage = "";		
		model.selectGroup = function(id){
			if(this.selectedPage == id)
				return;		
			if($.vpmsBindViewModel.isModelUpdating('#' + this.containerId)){
				this.selectGroupUpdateRequest = id;
				return;
			}
			if(!this.syncUpdate){
				$.vpmsBindViewModel.updateModelItem(this, 'selectedPage', id);
				$.vpmsBindViewModel.pushState(this.containerId, this.id, id, this.getAllGroupsData);
			}else{
				//findout group
				var group = undefined;
				for(var i=0; i<this.groups.length; i++){
					var _group = $.vpmsBindViewModel.getArrayElement(this.groups[i]); 
					if(_group.id==id){
						group = _group;
						break;
					}
				}
				//check if there are elements or groups to restore, restore them else act as async update
				if(group && (group._elements || group._groups)){
					$.vpmsBindViewModel.debug("syncUpdate - start bind cached elements");
					var rootModel = $('#' + this.containerId).data('model');
					$.vpmsBindViewModel.blockUI(rootModel, "", true);

					if(group._elements){
						for(var i=0; i<group._elements.length; i++){
							group.elements.push(group._elements[i]);
						}
						group._elements = undefined;
					}
					if(group._groups){
						for(var i=0; i<group._groups.length; i++){
							group.groups.push(group._groups[i]);
						}
						group._groups = undefined;
					}
					$('#' + rootModel.containerId).find(".vpms-pages-section").filter('#' + id).addClass('vpms-afterRender');
					this.requestedPage = id;
					$.vpmsBindViewModel.debug("syncUpdate - end bind cached elements");
					jQuery.vpmsBindViewModel.updateVPMSModel($('#' + this.containerId), true);				
				}else{
					$.vpmsBindViewModel.updateModelItem(this, 'selectedPage', id);
					$.vpmsBindViewModel.pushState(this.containerId,this.id, id, this.getAllGroupsData);
				}
			}
		};
		
		if(!model.nextPage) model.nextPage = function(pageId){			
			//setter
			if(pageId){
				this.selectGroup(pageId);
				return;
			}
			var found = false;
			for(var i=0; i<this.groups.length; i++){
				var group = $.vpmsBindViewModel.getArrayElement(this.groups[i]);
				if(!found){
					found = (group.id == this.selectedPage);
				}else{
					if(group.relevant==undefined || group.relevant)
						return group.id;
				}
			}
		};
		if(!model.prevPage) model.prevPage = function(pageId){
			if(pageId){
				this.selectGroup(pageId);
				return;
			}
			var found = false;
			for(var i=this.groups.length-1; i>=0; i--){
				var group = $.vpmsBindViewModel.getArrayElement(this.groups[i]);
				if(!found){
					found = (group.id == this.selectedPage);
				}else{
					if(group.relevant==undefined || group.relevant)
						return group.id;
				}
			}
		};
			
		model.selectedGroup = function () {			
			for(var i=0; i<this.groups.length; i++){
				var _group = $.vpmsBindViewModel.getArrayElement(this.groups[i]); 
				if(_group.id == this.selectedPage){
					return _group;
				}
			}
		};
		
		model.isSelected = function(dataItem){
			var _group = $.vpmsBindViewModel.getArrayElement(dataItem);
			return ($.vpmsBindViewModel.unwrapObservable(this, "selectedPage") == _group.id); 
		};
		model.isVisited = function(dataItem){
			var _group = $.vpmsBindViewModel.getArrayElement(dataItem);
			return ($.vpmsBindViewModel.unwrapObservable(_group, "visited") && !$.vpmsBindViewModel.unwrapObservable(_group, "error")); 
		};
		model.tabClick = function(event){
			var _group = $.vpmsBindViewModel.getArrayElement(event.data);
			if($(event.target).hasClass('vpms-pages-remove-button'))
				jQuery(event.target).vpmsBindViewModel('removeFromGroup', this, _group);
			else{
				if(this.selectedPage!=_group.id){
					this.selectGroup(_group.id);					
				}else{
					if(!this.getAllGroupsData)
						jQuery.vpmsBindViewModel.updateVPMSModel($('#' + (this.containerId)), true);
				}
			}	
		};
		if(model.multiple){
			model.mutable = function(){
				return this.multiple && !$.vpmsBindViewModel.unwrapObservable(this,'disabled') && !$.vpmsBindViewModel.unwrapObservable(this, 'staticPageCount');
			};			
			model._canAdd = function(){
				return  (this.mutable() && $.vpmsBindViewModel.unwrapObservable(this, "canAdd"));
			};				
			model._canRemove = function(dataItem){
				var _group = $.vpmsBindViewModel.getArrayElement(dataItem);
				return (this.mutable() && $.vpmsBindViewModel.unwrapObservable(_group, "canRemove")); 
			};
			if(model.syncUpdate){
				model.beforeGroupAdded = function(model, modelGroup, data, dataGroup){
					if(model.syncUpdate && data.sp){
						if(data.sp!=dataGroup.id){							
							if(modelGroup.elements.length>0)
								modelGroup._elements = modelGroup.elements.splice(0, modelGroup.elements.length);
							if(modelGroup.groups.length>0)
								modelGroup._groups = modelGroup.groups.splice(0, modelGroup.groups.length);
							//clear all content for not selected page
							dataGroup.k_=undefined;
							dataGroup.e_=undefined;
							dataGroup.g_=undefined;
						}
					}			
				};
			}
			model.afterGroupAdded = function(model, group, data){
				var _group = $.vpmsBindViewModel.getArrayElement(group);
				var elem = jQuery('#' + _group.id);
				if(!$.vpmsBindViewModel.isModelIniting(elem)){
					setTimeout(function(){$.vpmsBindViewModel.afterRender(elem, this.containerId);}, 1);
				}
			};			
			model.addClick = function(event){
				if(this.insertAfterSelected){
					var selectedGroup = this.selectedGroup();
					if(selectedGroup.canAdd)
						jQuery(event.target).vpmsBindViewModel('addToGroup', this, this.selectedPage);
				}
				else
					jQuery(event.target).vpmsBindViewModel('addToGroup', this);
			};			
			model._addCaption = function () {
				var activeGroup = this.selectedGroup();
				if(this.insertAfterSelected && activeGroup)
					return $.vpmsBindViewModel.unwrapObservable(activeGroup, 'addCaption');
				else
					return $.vpmsBindViewModel.unwrapObservable(this, 'addCaption');
			};
		}
	},
	beforeUpdateModel: function(model, data){
		if(data && data.g_){
			//if selected Page comes from server - set it
			if(!data.sp){
				if(model.requestedPage){
					data.sp = model.requestedPage;
				}else{
					//if bbq has selected page - take it
					var tab = $.vpmsBindViewModel.getState(model.containerId, model.id);
					if(tab!==undefined && tab!="")
						data.sp = tab;
				}
			}
			model.requestedPage = undefined;
			//if data.sp is set - check if this page will be relevant after update
			if(data.sp){
				for(var i=0; i<data.g_.length; i++){
					if(data.g_[i].id == data.sp){
						//findout group
						var group=undefined;
						for(var k=0; k<model.groups.length; k++){
							var _group = $.vpmsBindViewModel.getArrayElement(model.groups[k]);
							if(_group.name == data.g_[i]._n){
								group = _group;
								break;
							}
						}
						if(group){
							$.vpmsBindViewModel.updateModelItem(group, 'relevant', (data.g_[i]._r!==undefined?data.g_[i]._r:true))
							if(group.relevant==false)
								data.sp=undefined;
						}else{
							if(!(data.g_[i]._r===undefined || data.g_[i]._r==true))
								data.sp=undefined;
						}
						break;
					}
				}
				//reset selected page
				var resetSelectedPage = (data.sp==undefined);
			}
				
			//if data.sp still not set else take first relevant
			var i=0;
			while(i<data.g_.length && !data.sp){
				if(model.multiple){
					if(data.g_[i]._r===undefined || data.g_[i]._r==true)
						data.sp = data.g_[i].id;
				}else{
					for(var k=0; k<model.groups.length; k++){
						var _group = $.vpmsBindViewModel.getArrayElement(model.groups[k]);
						if(_group.name==data.g_[i]._n){
							$.vpmsBindViewModel.updateModelItem(_group, 'relevant', (data.g_[i]._r!==undefined?data.g_[i]._r:true));
							if(_group.relevant==true)
								data.sp = data.g_[i].id;
							break;
						}
					}
				}
				i++;
			};
			//reset selected page if data.sp was not relevant, but do this only once
			if(model.resetSelectedPage)
				model.resetSelectedPage = undefined;
			else
				if(data.sp && resetSelectedPage){
					model.resetSelectedPage = true;
					var _sp = data.sp;
					data.sp = undefined;
					setTimeout(function(){
						$.vpmsBindViewModel.updateModelItem(model, 'selectedPage', _sp);
					}, 1);
				}
			//if no relevant page - set first as selectedPage - better than get everytime all pages and wait till one will be relevant
			if(!data.sp && data.g_.length>0)
				data.sp = data.g_[0].id;

			//syncUpdate on init - clear content and cache elements for not selected
			if(model.syncUpdate && !model.syncUpdateInited){
				for(var i=0; i<data.g_.length; i++){
					if(data.sp!=data.g_[i].id){
						//cash all groups content for not selected page
						for(var k=0; k<model.groups.length; k++){
							var _group = $.vpmsBindViewModel.getArrayElement(model.groups[k]);
							if(_group.name==data.g_[i]._n){
								if(_group.elements.length>0)
									_group._elements = _group.elements.splice(0, _group.elements.length);
								if(_group.groups.length>0)
									_group._groups = _group.groups.splice(0, _group.groups.length);
								break;
							}
						}
						//clear all content for not selected page
						data.g_[i].k_=undefined;
						data.g_[i].e_=undefined;
						data.g_[i].g_=undefined;
					}
				}
				model.syncUpdateInited = true;
			}
		}		
	},
	updateModel: function(model, data){
		if(data && data.sp){
			$.vpmsBindViewModel.updateModelItem(model, 'selectedPage', data.sp);
			$.vpmsBindViewModel.pushState(model.containerId, model.id, model.selectedPage, true);
		}
		if(!model.setAllGroupsData)
			model.prevSelectedGroup = model.selectedGroup();
		if(model.selectGroupUpdateRequest){
			var _sp = model.selectGroupUpdateRequest;
			model.selectGroupUpdateRequest = undefined;
			setTimeout(function(){				
				model.selectGroup(_sp);
			}, 1);
		}
	}
});
				
//Button Bar widget - defaults: container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'buttonbarleft',
	template: '<span>#if(data.wrapdata.pos=="left"){#\
		<span data-template="button" data-bind="source:wrapdata"></span>\
	#}#</span>'
});
$.vpmsBindViewModel.pushTemplate({
	name: 'buttonbarcenter',
	template: '<span>#if(data.wrapdata.pos=="center"){#\
		<span data-template="button" data-bind="source:wrapdata"></span>\
	#}#</span>'
});
$.vpmsBindViewModel.pushTemplate({
	name: 'buttonbarright',
	template: '<span>#if(data.wrapdata.pos=="right"){#\
		<span data-template="button" data-bind="source:wrapdata"></span>\
	#}#</span>'
});

//Button Bar widget - defaults: container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'buttonbar',
	template: '\
	<div>\
		<div class="vpms-button-bar-left" data-template="buttonbarleft" data-bind="source:elements"></div>\
		<div class="vpms-button-bar-center" data-template="buttonbarcenter" data-bind="source:elements"></div>\
		<div class="vpms-button-bar-right" data-template="buttonbarright" data-bind="source:elements"></div>\
	</div>\
	',	
	initModel :function(model){
		if(model.container==undefined)
			model.container = true;
		if(!model.name) model.name = "buttonbar";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);
	}
});

//product - defaults: container=true
$.vpmsBindViewModel.pushTemplate({
	name : "product",
	template: '\
	<div class="ui-widget">\
		<div class="ui-widget-header ui-corner-all vpms-product-widget-header">\
			<span data-bind="text:caption"></span>\
			#if(data.navigateBack){#<span class="vpms-button-widget" style="float:right; font-size:0.65em"><button data-bind="click:navigateBack">Back</button></span><div style="clear:both;"></div>#}#\
		</div>\
		<div data-template="element" data-bind="source:elements"></div>\
	</div>\
	',
	initModel: function(model){
		if(model.container==undefined)
			model.container = true;
		if(!model.name) model.name = "productWidget";
		model.caption = $.vpmsBindViewModel.initModelItem("", "");		
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
	},
	updateModel: function(model, data){
		var caption = $.vpmsBindViewModel.element(model.captions, 'productCaption');
		if(caption)
			$.vpmsBindViewModel.updateModelItem(model, 'caption', caption.caption);
	},
});

//selectQuote - defaults: container=true
$.vpmsBindViewModel.pushTemplate({
	name : "selectQuoteChoicesWidget",
	template: '<li><a href="\\#" data-bind="text:_x,attr:{name:_v},click:selectProduct"></a></li>'
});

$.vpmsBindViewModel.pushTemplate({
	name : "selectQuote",
	template: '\
	<div>\
		<div class="ui-widget-content ui-corner-all">\
			<ul data-template="selectQuoteChoicesWidget" data-bind="source:products"></ul>\
			<div style="clear:both;"></div>\
		</div>\
		<div class="vpms-footer">\
			<div class="vpms-footer-part">&nbsp;</div>\
			<div class="vpms-footer-part"><div class="vpms-powered-by-text">Powered by&nbsp;</div><div class="vpms-powered-by-img">&nbsp;</div></div>\
			<div class="vpms-version-info vpms-footer-part" style="float:right"><span data-bind="text:versionCaption"></span>&nbsp;<span data-bind="text: version"></span></div>\
		</div>\
	</div>',
	initModel: function(model){
		if(model.container==undefined)
			model.container = true;
		if(!model.name) model.name = "selectQuote";
		model.version = $.vpmsBindViewModel.initModelItem(model.version, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.productKey = $.vpmsBindViewModel.initModelItem(model.settings?model.settings.productKey:undefined, "A_Product_key");
		model.version = $.vpmsBindViewModel.initModelItem(undefined, "");		
		model.versionCaption = $.vpmsBindViewModel.initModelItem(undefined, "Version");		
		model.products = [];		
		model.selectProduct = function(event){
			var productName = jQuery(event.target).attr("name");
			jQuery(event.target).vpmsBindViewModel("changeModel", productName);
		}
	},
	updateModel: function(model, data){
		for(var i=0; i<model.elements.length; i++){
			var _element = $.vpmsBindViewModel.getArrayElement(model.elements[i]);
			if(_element.name==model.productKey)
				 $.vpmsBindViewModel.updateModelItem(model, 'products',  _element.choices);
		}
		var caption = $.vpmsBindViewModel.element(model.captions, 'text.version');
		if(caption)
			$.vpmsBindViewModel.updateModelItem(model, 'versionCaption', caption.caption);
		caption = $.vpmsBindViewModel.element(model.captions, 'contextVersion');
		if(caption)
			$.vpmsBindViewModel.updateModelItem(model, 'version', caption.caption);
	}
});

//treeview widget - defaults: container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'treeviewTabs',
	template: '\
	<li class="vpms-treeview-tab vpms-afterRender" data-bind="visible:wrapdata.relevant, attr:{id:wrapdata.id}">\
		<a class="vpms-clearfix ui-corner-left" data-bind="css: { ui-state-active: isTreeTabSelected }, click: tabClick">\
			<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: wrapdata.required"><span class="ui-icon ui-icon-bullet"></span></div>\
			<div class="ui-state-error" data-bind="visible: wrapdata.error"><span class="vpms-tooltip-error#if(!jQuery.vpmsBindViewModel.isMobileAgent){# ui-icon ui-icon-alert#}#" data-bind="attr: {error: wrapdata.error}">&\\#10033;</span></div>\
			<span class="vpms-treeview-link-text" data-bind="text:wrapdata.caption, attr:{tooltip:wrapdata.tooltip}"></span>\
			#if(data.parent().parent().multiple){#\
			<span class="vpms-treeview-remove-button" data-bind="visible:_canRemove, attr:{tooltip:wrapdata.removeCaption}">&\\#10006;</span>\
			#}#\
			#if(data.wrapdata.multiple){#\
			<span class="vpms-treeview-add-button" data-bind="visible:_canAdd, attr:{tooltip:wrapdata.addCaption}">&\\#10010;</span>\
			#}#\
			#if(jQuery.vpmsBindViewModel.isIe7){#\
			<img style="display:none;float:none;"/>\
			#}#\
		</a>\
		<ul data-template="treeviewTabs" data-bind="source:wrapdata.groups"></ul>\
	</li>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'treeviewSection',
	template: '\
	<div>\
		<div class="vpms-treeview-section vpms-afterRender ui-widget-content" data-bind="attr: {id: wrapdata.id}, visible:isTreeTabSelected">\
			#if(jQuery.vpmsBindViewModel.templateExists(data.wrapdata.widget)){#\
				<div class="vpms-#=data.wrapdata.widget#-widget" data-template="#=data.wrapdata.widget#" data-bind="source:wrapdata"></div>\
			#}#\
		</div>\
		<div data-template="treeviewSection" data-bind="source:wrapdata.groups"></div>\
	</div>\
'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'treeview',
	template: '\
	<div>\
		<div class="vpms-treeview-container ui-widget ui-widget-content">\
			<ul class="vpms-treeview ui-widget-content" data-template="treeviewTabs" data-bind="source:groups"></ul>\
			<div data-template="treeviewSection" data-bind="source:groups"></div>\
			<div class="vpms-clear-float"></div>\
		</div>\
		<div class="vpms-elements-container" data-template="element" data-bind="source:elements"></div>\
	</div>',		
	initModel :function(model){
		model.getGroupsTree = true;
		
		if(model.container==undefined)
			model.container = true;

		if(!model.name) model.name = "treeview";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);		
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);

		model.isTreeTabSelected = function(dataItem){
			var _group = $.vpmsBindViewModel.getArrayElement(dataItem);
			return ($.vpmsBindViewModel.unwrapObservable(this, "selectedPage") ==_group.id); 
			
		};
		
		model.tabClick = function(event){
			var rootmodel = this;
			var _group = $.vpmsBindViewModel.getArrayElement(event.data);
			if($(event.target).hasClass('vpms-treeview-remove-button')){
				var _parentGroup = event.data.parent().parent();
				this.setSelectedPage=_parentGroup.id;
				jQuery(event.target).vpmsBindViewModel('removeFromGroup', _parentGroup, _group);
			}else{
				if($(event.target).hasClass('vpms-treeview-add-button')){
					this.setSelectedPage=_group.id;
					var groupAfterId = undefined;
					var activeGroup = this.selectedGroup();
					if(this.insertAfterSelected && _group.insertAfterSelected!==false && activeGroup && activeGroup.name == _group.name && activeGroup.canAdd===true)
						groupAfterId = activeGroup.id;
					jQuery(event.target).vpmsBindViewModel('addToGroup', _group, groupAfterId);
				}else{
					if(this.selectedPage!=_group.id)
						this.selectGroup(_group.id);
					else
						jQuery(event.target).vpmsBindViewModel("updateModel");
				}
			}
		};
							
		model.mutable = function(dataItem){
			var _group = $.vpmsBindViewModel.getArrayElement(dataItem);
			return _group.multiple && !$.vpmsBindViewModel.unwrapObservable(_group,'disabled') && !$.vpmsBindViewModel.unwrapObservable(_group, 'staticPageCount');
		};			
		model._canAdd = function(dataItem){
			var _group = $.vpmsBindViewModel.getArrayElement(dataItem);
			return  (this.mutable(_group) && $.vpmsBindViewModel.unwrapObservable(_group, "canAdd"));
		};				
		model._canRemove = function(dataItem){
			var _group = $.vpmsBindViewModel.getArrayElement(dataItem);
			var _parent = dataItem.parent().parent();
			return (this.mutable(_parent) && $.vpmsBindViewModel.unwrapObservable(_group, "canRemove")); 
		};
				
		model.getModelGroupById = function(_model, _id){
			if(_model.groups)
				var groupsArray = _model.groups;
			else
				var groupsArray = _model.g_; //data
			for(var i=0; i<groupsArray.length; i++){
				var _group = $.vpmsBindViewModel.getArrayElement(groupsArray[i]);
				if(_group.id == _id)
					return _group;
				else
					if(_group.groups || _group.g_){
						var page = this.getModelGroupById(_group, _id);
						if(page)
							return page;
					}
			}
			return undefined;
		};
		model.getDataModelGroupById = function(_data, _model, _id){
			var groupsArray = undefined;
			if(_model)
				groupsArray = _model.groups;
			for(var i=0; i<_data.g_.length; i++){
				var modelGroup = undefined;
				if((_data.g_[i].id==_id || _data.g_[i].g_) && groupsArray){
					for(var k=0; k<groupsArray.length; k++){
						var _group = $.vpmsBindViewModel.getArrayElement(groupsArray[k]);
						if(_group.name==_data.g_[i]._n){
							modelGroup = _group;
							break;
						}
					}
				}
				if(_data.g_[i].id == _id){
					return {
						data:_data.g_[i],
						model: modelGroup
					};
				}
				else
					if(_data.g_[i].g_){
						var result = this.getDataModelGroupById(_data.g_[i], modelGroup, _id);
						if(result)
							return result;
					}
			}
			return undefined;
		};
		model.updateGroupsTree = function(_model, selectedGroup){
			if(_model.groups){
				var groupsArray = _model.groups;
				for(var i=0; i<groupsArray.length; i++){
					var _group = $.vpmsBindViewModel.getArrayElement(groupsArray[i]);
					if(selectedGroup.id==_group.id)
						return true;
					else
						if(_group.multiple && _group.insertAfterSelected!==false && selectedGroup.name==_group.name){
							if(selectedGroup.addCaption)
								$.vpmsBindViewModel.updateModelItem(_group, "addCaption", selectedGroup.addCaption); 
							return true;
						};
						
					if(this.updateGroupsTree(_group, selectedGroup)===true)
						return true;
				}
			}
		};
		model.selectedPage = "";
		
		model.selectGroup = function(id){
			if(this.selectedPage == id)
				return;		
			if($.vpmsBindViewModel.isModelUpdating('#' + this.containerId)){
				this.selectGroupUpdateRequest = id;
				return;
			}
			if(!this.syncUpdate){
				$.vpmsBindViewModel.updateModelItem(this, "selectedPage", id);
				$.vpmsBindViewModel.pushState(this.containerId, this.id, id);
			}else{
				//findout group
				var group = this.getModelGroupById(this, id);
				//check if there are elements, restore them else act as async update
				//check if there are elements or groups to restore, restore them else act as async update
				if(group && group._elements){
					$.vpmsBindViewModel.debug("syncUpdate - start bind cached elements");
					var rootModel = $('#' + this.containerId).data('model');
					$.vpmsBindViewModel.blockUI(rootModel, "", true);

					if(group._elements){
						for(var i=0; i<group._elements.length; i++){
							group.elements.push(group._elements[i]);
						}
						group._elements = undefined;
					}
					$('#' + rootModel.containerId).find(".vpms-treeview-section").filter('#' + id).addClass('vpms-afterRender');
					this.requestedPage = id;
					$.vpmsBindViewModel.debug("syncUpdate - end bind cached elements");
					jQuery.vpmsBindViewModel.updateVPMSModel($('#' + this.containerId), !this.visitable);				
				}else{
					$.vpmsBindViewModel.updateModelItem(this, "selectedPage", id);
					$.vpmsBindViewModel.pushState(this.containerId, this.id, id);
				}
			}			
		};
		model.selectedGroup = function () {
			return this.getModelGroupById(this,this.selectedPage);
		};		
		//Treeview navigation		
		function findPrevPage(_model, params){
			if(!_model.groups)
				return;
			var groupsArray = $.vpmsBindViewModel.unwrapObservable(_model.groups);
			for(var i=0; i<groupsArray.length; i++){
				var _group = $.vpmsBindViewModel.getArrayElement(groupsArray[i]);
				var _id= $.vpmsBindViewModel.unwrapObservable(_group, "id");
				if(_id==params.id)
					return params.prev;
				else{
					if(_group.relevant==undefined || $.vpmsBindViewModel.unwrapObservable(_group, "relevant"))
						params.prev = _id;
					var result = findPrevPage(_group, params);
					if(result)
						return result;
				}
			}
		};
		function findNextPage(_model, params){
			if(!_model.groups)
				return;
			var groupsArray = $.vpmsBindViewModel.unwrapObservable(_model.groups);			
			for(var i=groupsArray.length-1; i>=0; i--){
				var _group = $.vpmsBindViewModel.getArrayElement(groupsArray[i]);
				var result = findNextPage(_group, params);
				if(result)
					return result;
				var _id= $.vpmsBindViewModel.unwrapObservable(_group, "id");
				if(_id==params.id)
					return params.next;
				else{
					if(_group.relevant==undefined || $.vpmsBindViewModel.unwrapObservable(_group, "relevant"))
						params.next = _id;
				}
			}
		};
		model.prevPage = function(pageId){
			if(pageId){
				this.selectGroup(pageId);
				return;
			}	
			var params = {
				id : $.vpmsBindViewModel.unwrapObservable(this, "selectedPage")
			}
			return findPrevPage(this, params);
		};
		model.nextPage = function(pageId){
			if(pageId){
				this.selectGroup(pageId);
				return;
			}
			var params = {
				id : $.vpmsBindViewModel.unwrapObservable(this, "selectedPage")
			};
			return findNextPage(this, params);
		};			
	},
	beforeUpdateModel: function(model, data){
		if(data && data.g_){
			//1: determine future selected page
			//model.setSelectedPage contains groupId where group was added or removed from
			if(model.setSelectedPage){
				var selectedGroup = model.getModelGroupById(data, model.setSelectedPage);
				if(selectedGroup && selectedGroup.sp)
					data.sp = selectedGroup.sp; 
				model.setSelectedPage = undefined;
			}else
				//model.requestedPage on syncUpdate
				if(model.requestedPage){
					data.sp = model.requestedPage;
					model.requestedPage = undefined;
				}else{
					//otherwise if state has selected page - take it
					var tab = $.vpmsBindViewModel.getState(model.containerId, model.id);
					if(tab!==undefined && tab!="")
						data.sp = tab;
				}
			//2: if future selected page exists - check if this page will be relevant after update
			if(data.sp){
				var dataModelGroup = model.getDataModelGroupById(data, model, data.sp);
				if(dataModelGroup){
					if(dataModelGroup.model){
						if(dataModelGroup.model.relevant!=undefined) $.vpmsBindViewModel.updateModelItem(dataModelGroup.model, "relevant", (dataModelGroup.data._r!==undefined?dataModelGroup.data._r:true));
						if(dataModelGroup.model.relevant!=undefined && dataModelGroup.model.relevant==false)
							data.sp=undefined;
					}else
						if(!(dataModelGroup.data._r===undefined || dataModelGroup.data._r==true))
							data.sp=undefined;
					//reset selected page
					var resetSelectedPage = (data.sp==undefined);
				}
			}
			var groupsArray = model.groups;
			//3: if data.sp still not set else take first relevant on top level
			var i=0;
			while(i<data.g_.length && !data.sp){
				var group;
				if(model.multiple){
					if(data.g_[i]._r===undefined || data.g_[i]._r==true)
						data.sp = data.g_[i].id;
				}else{
					for(var k=0; k<groupsArray.length; k++){
						var group = $.vpmsBindViewModel.getArrayElement(groupsArray[k]);
						if(group.name==data.g_[i]._n){
							if(group.relevant!=undefined) $.vpmsBindViewModel.updateModelItem(group, "relevant", (data.g_[i]._r!==undefined?data.g_[i]._r:true));
							if(group.relevant===undefined || group.relevant)
								data.sp = data.g_[i].id;
							break;
						}
					}
				}
				i++;
			};
			//4: reset selected page if data.sp was not relevant, but do this only once
			if(model.resetSelectedPage)
				model.resetSelectedPage = undefined;
			else
				if(data.sp && resetSelectedPage){
					model.resetSelectedPage = true;
					var _sp = data.sp;
					data.sp = undefined;
					setTimeout(function(){
						$.vpmsBindViewModel.updateModelItem(model, "selectedPage", _sp);
					}, 1);
				}
			//if no relevant page - set first as selectedPage - better than get everytime all pages and wait till one will be relevant
			if(!data.sp && data.g_.length>0)
				data.sp = data.g_[0].id;

			//syncUpdate on init - clear content and cache elements for not selected
			if(model.syncUpdate && !model.syncUpdateInited){
				var clearContentFunction = function(_data, _model, _id){
					var groupsArray = _model.groups;
					for(var i=0; i<_data.g_.length; i++){
						var modelGroup = undefined;
						for(var k=0; k<groupsArray.length; k++){
							var group = $.vpmsBindViewModel.getArrayElement(groupsArray[k]);
							if(group.name==_data.g_[i]._n){
								modelGroup = group;
								break;
							}
						}
						if(_id!=data.g_[i].id){
							//cash all groups content for not selected page
							if(modelGroup && modelGroup.elements.length>0)
								modelGroup._elements = modelGroup.elements.splice(0, modelGroup.elements.length);
							//clear all content for not selected page
							_data.g_[i].e_=undefined;
						}
						if(_data.g_[i].g_)
							clearContentFunction(_data.g_[i], modelGroup, _id);
					}
					
				};
				if(data.sp)
					clearContentFunction(data, model, data.sp);
				model.syncUpdateInited = true;
			}
		}
	},
	updateModel: function(model, data){
		if(data.sp){
			$.vpmsBindViewModel.updateModelItem(model, "selectedPage", data.sp);
			$.vpmsBindViewModel.pushState(model.containerId, model.id, model.selectedPage, true);
			if(model.insertAfterSelected)
				model.updateGroupsTree(model, model.selectedGroup());
			setTimeout(function(){
				jQuery('li#' + model.selectedPage).treeview('changed');
				jQuery('li#' + model.selectedPage).treeview('select'); 
			}, 1);
		}
		if(model.syncUpdate)
			$.vpmsBindViewModel.unblockUI(model);
		if(!model.setAllGroupsData)
			model.prevSelectedGroup = model.selectedGroup();
		if(model.selectGroupUpdateRequest){
			var _sp = model.selectGroupUpdateRequest;
			model.selectGroupUpdateRequest = undefined;
			setTimeout(function(){
				$.vpmsBindViewModel.updateModelItem(model, "selectedPage", _sp);
			}, 1);
		}
	}
});

//table widget  - defaults container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'tableCell',
	template: '\
		<td class="#=wrapdata.parent().parent().parent().parent().parent().parent().contentClass# #if(data.wrapdata.removeColumn){#vpms-table-remove-column#}#" #if(data.wrapdata.span){#span="#=data.wrapdata.span#"#}# #if(data.wrapdata.removeColumn){#data-bind="visible:mutable"#}#>\
			#if(data.wrapdata.removeColumn){#\
			<div data-bind="visible:mutable, attr:{enabled:canRemove}">\
				<div class="vpms-button-widget"><button type="button" data-bind="enableui:wrapdata.canRemove, textui: wrapdata.removeCaption, click:removeGroup"></button></div>\
			</div>#}else{#\
			#if($.vpmsBindViewModel.templateExists(data.wrapdata.widget)){#\
				<div class="vpms-#=data.wrapdata.widget#-widget #if(data.wrapdata.settings &&  data.wrapdata.settings.css){# #=data.wrapdata.settings.css# #}# #if(data.wrapdata.settings && data.wrapdata.settings.align){#vpms-align-#=data.wrapdata.settings.align##}#" data-template="#=data.wrapdata.widget#" data-bind="visible:wrapdata.relevant, source: wrapdata"></div>\
			#}}#\
		</td>\
	'
});

//table widget  - defaults container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'tableRow',
	template: '<tr class="#=parent().parent().contentClass# vpms-afterRender" data-bind="attr: {id:wrapdata.id}, visible:wrapdata.relevant, source:wrapdata.elements" data-template="tableCell"></tr>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'table',
	template: '\
	<div>\
		#if(data.navigation){#\
		<div class="vpms-table-navigation" data-bind="visible:navigation.visible">\
			<span class="vpms-table-navigation-prev"><div class="vpms-button-widget"><button type="button" data-bind="enableui:navigation.hasPrev, textui: navigation.prevCaption, click:navigationPrevPage"></button></div></span>\
			<span class="vpms-table-navigation-label" data-bind="text:navigation.label"></span>\
			<span class="vpms-table-navigation-next"><div class="vpms-button-widget"><button type="button" data-bind="enableui:navigation.hasNext, textui: navigation.nextCaption, click:navigationNextPage"></button></div></span>\
		</div>#}#\
		<div #if(data.settings && data.settings.height){#style="overflow-y:auto;height:#=data.settings.height#"#}#>\
			<table class="#=data.contentClass# vpms-table-widget vpms-single-column" data-bind="attr:{id:id}">\
				<thead>\
					<tr class="#=data.contentClass#">\
						#for(var i=0; i<data.settings.colSettings.length; i++){\
							if(data.settings.colSettings[i].removeColumn){#\
								<th class="#=data.headerClass# vpms-table-remove-column" data-bind="visible:mutable"></th>\
							#}else{#\
								<th class="#=data.headerClass# #if(data.settings.colSettings[i].settings && data.settings.colSettings[i].settings.css){# #=data.settings.colSettings[i].settings.css# #}#" style="#if(data.settings.colSettings[i].settings &&  data.settings.colSettings[i].settings.labelAlign){#text-align:#=data.settings.colSettings[i].settings.labelAlign#;#}##if(data.settings.colSettings[i].settings &&  data.settings.colSettings[i].width){#width:#=data.settings.colSettings[i].settings.width#;#}#">\
									<span data-bind="text:settings.colSettings[#=i#].caption"></span>\
									<div class="vpms-widget-required-icon ui-state-highlight" data-bind="visible: settings.colSettings[#=i#].required"><span class="ui-icon ui-icon-bullet"></span></div>\
								</th>\
						#}}#\
					</tr>\
				</thead>\
				<tbody data-template="tableRow" data-bind="source:groups"></tbody>\
			</table>\
		</div>\
		#if(data.multiple){#\
		<div class="vpms-add-button" data-bind="visible:mutable">\
			<div class="vpms-button-widget"><button type="button" data-bind="enableui:canAdd, textui: addCaption, click:addGroup"></button></div>\
		</div>\
		#}#\
	</div>\
	',
	initModel: function(model){
		if(!model.name) model.name = "table";
		//table has no selected Page - get and update all groups
		model.getAllGroupsData = true;
		if(model.onlyRelevant == undefined && model.getAllGroupsData)
			model.onlyRelevant = true;				
		if(!model.readonly)
			model.setAllGroupsData = true;
		if(model.container==undefined)
			model.container = true;
		if(model.noborder===undefined)
			model.noborder = false;
		if(model.headerClass==undefined)
			model.headerClass=model.noborder?"":"ui-widget-header";
		if(model.contentClass==undefined)
			model.contentClass=model.noborder?"":"ui-widget-content";
		if(model.settings && model.settings.visibleLines){
			var intVisibleLines = parseInt(model.settings.visibleLines);
			if(!isNaN(intVisibleLines) && intVisibleLines>0)
				model.settings.height = (intVisibleLines+1) * 2.6 + "em";
		}

		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		//init columns from template
		if(!model.settings)
			model.settings = {};
		if(!model.settings.colSettings)
			model.settings.colSettings = [];
		//colNames in model root to get column captions from VPMS
		model.colNames = [];
		model.initColumn = function(element, index){
			var colModel = {
				settings: element.settings?$.vpmsBindViewModel.cloneObject(element.settings):{},
				caption : $.vpmsBindViewModel.initModelItem(undefined, element.name),
				required: $.vpmsBindViewModel.initModelItem(undefined, false),
				removeColumn: element.removeColumn 
			};
			if(this.settings.colSettings.length>index)
				this.settings.colSettings[index] = colModel;
			else
				this.settings.colSettings.push(colModel);
			this.colNames.push(element.name);
		};
		model.mutable = function(){
			return !$.vpmsBindViewModel.unwrapObservable(this,'disabled') && !$.vpmsBindViewModel.unwrapObservable(this, 'staticPageCount');
		};
		if(model.multiple){
			model.removeGroup = function(event){
				jQuery(event.target).vpmsBindViewModel('removeFromGroup', this, event.data.parent().parent());
			};
			
			model.addGroup = function(event){
				jQuery(event.target).vpmsBindViewModel('addToGroup', this);
			};
			
			if(model._template && model._template.elements){
				model._template.elements.push({
					name:'removeColumn',
					removeColumn: true,
					isElement: false,
					isGroup: false
				});
			};

			//if model is multiple - get column captions and settings from template
			var template = $.vpmsBindViewModel.getGroupTemplate(model);
			if(template){
				for(var i=0; i<template.elements.length; i++)
					model.initColumn(template.elements[i], i);
			}
		}else{ 
			//else - from first group
			if(model.groups && model.groups.length>0){
				var elementsArray = $.vpmsBindViewModel.getArrayElement(model.groups[0]).elements;
				if(elementsArray)
					for(var i=0; i<elementsArray.length; i++)
						model.initColumn($.vpmsBindViewModel.getArrayElement(elementsArray[i]), i);
			}
		}
		//navigation
		$.vpmsBindViewModel.initGroupsNavigation(model);
		if(model.navigation){
			model.navigation.prevCaption = "<";
			model.navigation.nextCaption = ">";
		}
	},
	updateModel: function(model, data){
		if(model.trigger)
			model.trigger("change",{field: "mutable"});
		if(data){
			if(model.multiple){
				for(var i=0; i<model.settings.colSettings.length; i++){
					if(data.cn_ && data.cn_.length>i)
						$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'caption', data.cn_[i]);
					if(data.cr_ && data.cr_.length>i)
						$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'required', (data.cn_[i]=="1"));
				}
			}else{
				if(model.groups && model.groups.length>0){
					var elementsArray = $.vpmsBindViewModel.getArrayElement(model.groups[0]).elements;
					if(elementsArray)
						for(var i=0; i<model.settings.colSettings.length; i++){
							var _element = $.vpmsBindViewModel.getArrayElement(elementsArray[i]);
							$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'caption', _element.caption);
							$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'required', _element.required);	
						}
				}
			}
		}
		//navigation
		if(model.navigationUpdate){
			model.navigationUpdate(data);
		}
	}
});

//grid widget
//*settings.colModel build from template.elements (including format, choices etc.)
//*setting.navigation = false - if true server navigation loading pages
//*settings.readonly = false - if true not editable grid
//*settings.colNames - array of strings width captions (translatable)
//translatable means: model.settings.colNames = ["grid.firstName"], model.captions = [{name:"grid.firstName", id:"grid.firstName"}] 
//in this case resources translated string with id grid.firstName will be returned.
//*if settings.colNames is not defined, will be filled from template.elements[i].name, is still translatable, example template.elements[i].name="firstName"
//model.captions = [{name:"firstName", id:"grid.firstName"}]
$.vpmsBindViewModel.pushTemplate({
	name: 'grid',
	template: '<table data-bind="attr:{tableId:tableId}"></table>',
	initModel: function(model){
		if(!model.name) model.name = "grid";
		if(!model.settings)
			model.settings = {};
		if(!model.settings.height && model.settings.visibleLines){
			var intVisibleLines = parseInt(model.settings.visibleLines);
			if(!isNaN(intVisibleLines) && intVisibleLines>0)
				model.settings.height = intVisibleLines * 23;
		}
		//grid has no selected Page - get and update all groups
		model.getAllGroupsData = true;
		if(model.onlyRelevant == undefined && model.getAllGroupsData)
			model.onlyRelevant = true;
		if(!model.readonly)
			model.setAllGroupsData = true;
		if(model.container==undefined)
			model.container = true;
		if(model.insertAfterSelected==undefined)
			model.insertAfterSelected=true;
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.tableId = function(){
			return "table_" + this.containerId + jQuery.vpmsBindViewModel.unwrapObservable(this, "id"); 
		}
		model.selectedPage = "";
		model.selectedGroup = function(){
			for(var i=0; i<this.groups.length; i++){
				var group = $.vpmsBindViewModel.getArrayElement(this.groups[i]);
				if(group.id == this.selectedPage){
					return group;
				}
			}
		};
		
		//init columns from template if columns is empty
		if(!model.settings.colNames || model.settings.colNames.length==0){
			model.settings.colNames = [];
			var template = $.vpmsBindViewModel.getGroupTemplate(model, true);
			if(template){
				model.settings.colNames.push("id");
				for(var i=0; i<template.elements.length; i++){
					model.settings.colNames.push(template.elements[i].name);
					if(template.elements[i].generateDisplayValue == undefined)
						template.elements[i].generateDisplayValue = true;
				}
			}
			//copy to model root to get captions from VPMS
			model.colNames = model.settings.colNames;
		}	
		if(model.settings.dynamicLoad){
			if(!model.settings.rowsPerPage){
				if(!model.settings.rowNum)					
					model.settings.rowNum = 20;
				model.settings.rowsPerPage = model.settings.rowNum;
			}
			$.vpmsBindViewModel.initGroupsNavigation(model);
		}else{
			model.selectedGridPage = 1;
		}		
	},
	updateModel: function(model, data){
		function createGrid(table, model){
			var tableId = model.tableId();
			var pagerId = 'pager_' + tableId;
			table.attr('id', tableId);
			table.after('<div id="' + pagerId + '"></div>');
			//create colModel
			if(!model.settings.colModel){
				model.settings.colModel = [];
				var commonSettings = jQuery('#' + model.containerId).data('settings'); 
				var template = $.vpmsBindViewModel.getGroupTemplate(model, true);
				if(template){
					model.settings.colModel.push({name:'id',index:'id', hidden:true});
					for(var i=0; i<template.elements.length; i++){
						var col = {name:template.elements[i].name, index:template.elements[i].name};
						if(template.elements[i].settings){
							jQuery.extend(col, template.elements[i].settings);
						}
						if(template.elements[i].format=='date'){
							col.sorttype = 'date';
							col.datefmt = template.elements[i].settings && template.elements[i].settings.dateFormat?template.elements[i].settings.dateFormat:
											(commonSettings && commonSettings.dateFormat)?commonSettings.dateFormat:
											(commonSettings && commonSettings.locale && commonSettings.locale.dateFormat)?commonSettings.locale.dateFormat:'ISO';
						}else
							if(template.elements[i].format=='number'){
								if(template.elements[i].settings && template.elements[i].settings.integer==true)
									col.sorttype = 'int';
								else
									col.sorttype = 'float';
								col.align = 'right';
							}
						if(template.elements[i].settings && template.elements[i].settings.align){
							col.align = template.elements[i].settings.align;
						}
						if(!template.elements[i].disabled && !model.readonly){
								col.editable=true;
								if(template.elements[i].widget=='checkbox')
									col.edittype='checkbox';
								else
									if($.vpmsBindViewModel.isChoicesWidget(template.elements[i].widget))
										col.edittype='select';
									else
										col.edittype='text';
								//datepicker
								if(template.elements[i].format=='date'){
									var dateOptions = jQuery.vpmsBindViewModel.getDatepickerOptions(template.elements[i].settings, commonSettings);
									col.editoptions = {dataInit: function(elem){
											setTimeout(function(){jQuery(elem).datepicker(dateOptions);}, 10);
										}
									};
								}else
								//numbers
								if(template.elements[i].format=='number'){
									var numbersOptions = jQuery.vpmsBindViewModel.getNumbersOptions(template.elements[i].settings, commonSettings);
									col.editoptions = {dataInit: function(elem){
										jQuery(elem).numbers(numbersOptions);
									}}
								}
						}
						//cellattr
						col.cellattr = function(rowId, cellValue, rawObject, cm, rdata) { 
							if(rawObject["_" + cm.name + "_error"])
								return ' class="ui-state-error ui-state-error-text" title="' + rawObject["_" + cm.name + "_error"] + '"';
							else
								return ' title="' +  rawObject["_" + cm.name + "_tooltip"] + '"';
						}
						model.settings.colModel.push(col);
					}//for template.elements
				}//if(model.template)
			} //if(!model.colModel)	
			//caption
			if(!model.settings.caption){
				model.settings.caption = model.caption?model.caption:model.name;
			}
			//jqGrid settings
			var settings = {
				datatype: "local",
				height: "100%",
				pager: '#'+ pagerId,
				viewrecords:true
			};
			jQuery.extend(settings, model.settings);
			//colnames
			if(data.cn_)
				settings.colNames = data.cn_;
			if(settings.hiddengrid===true){
				model.hidden = true;
			}
			settings.onHeaderClick = function(gridstate){
				if(gridstate=='visible'){
					model.hidden = false;
					jQuery('#' + model.containerId).vpmsBindViewModel("updateModel");
				}else{
					model.hidden = true;
				}
			};
			//server navigation enabled
			if(model.settings.dynamicLoad){
				settings.localReader = {
						root: function(obj) {
							return model.navigation.rows;
						},
						page: function(obj) { 
							return model.navigation.pageNumber; 
						},
						total: function(obj) {
							return model.navigation.totalPages; 
						},
						records: function(obj) { 
							return model.navigation.totalRecords; 
						}
				};
			}
			settings.onPaging = function(pgButton){
				var tableId = model.tableId();
				if(model.settings.dynamicLoad){
					model.navigation.pageNumber = parseInt(jQuery('#' + tableId).getGridParam('page'));				
					jQuery('#' + tableId).vpmsBindViewModel("updateModel");
				}else{
					model.selectedGridPage = parseInt(jQuery('#' + tableId).getGridParam('page'));
				}
			};				
			//editable grid functions
			if(!model.readonly){
				model.updateNavCaptions = function(id){
					var tableId = model.tableId();
					var group = undefined;
					if(id)
						group = jQuery.vpmsBindViewModel.elementById(model.groups, id);
					var $addButton = jQuery('#add_table_' + tableId);
					if($addButton.length>0)
						if(group && model.insertAfterSelected && group.addCaption)
							$addButton.attr('title', group.addCaption);
						else
							$addButton.attr('title', model.addCaption);
					var $removeButton = jQuery('#del_table_' + tableId);
					if($removeButton.length>0)
						if(group && group.removeCaption)
							$removeButton.attr('title', group.removeCaption);
						else
							$removeButton.attr('title', model.removeCaption);
				};
				//reflect changes in grid to viewModel
				var reselectRow = function(id){
					var tableId = model.tableId();
					  if(model.selid){
						  jQuery('#' + tableId).jqGrid('setSelection', model.selid, false);
						  model.updateNavCaptions(model.selid);
						  model.selid=undefined;
					  }else{
						  jQuery('#' + tableId).jqGrid('setSelection', id, false);
						  model.updateNavCaptions(id);
					  }
					  jQuery('#' + tableId).focus();
				};
				var afterSave = function(rowid, response){
					model.lastselid = undefined;
					var tableId = model.tableId();
					var group = jQuery.vpmsBindViewModel.elementById(model.groups, rowid);
					var row = jQuery('#'+ tableId).jqGrid('getRowData', rowid);
					var autocommit = false;
					if(group && row){
						for(var name in row){
							if(name!='id'){
								var element = jQuery.vpmsBindViewModel.element(group.elements, name, false);
								if(element!=false){
									if(element.autocommit==true)
										autocommit = true;
									if(element.widget=="checkbox"){
										var displayValue = element.displayValue;
										var checked = element.checked;
										if(row[name]!=displayValue)
											jQuery.vpmsBindViewModel.updateModelItem(element, 'checked', !checked);
									}else
									if($.vpmsBindViewModel.isChoicesWidget(element.widget)){
										var choicesArray = element.choices;
										for(var i=0;i<choicesArray.length;i++){
											if(choicesArray[i]._x==row[name]){
												jQuery.vpmsBindViewModel.updateModelItem(element, 'value', choicesArray[i]._v);
												break;
											}
										}
									}else
										$.vpmsBindViewModel.writeFormattedValue(element, row[name]);
								}
							}
						}
						if(autocommit){
							//set selectedPage to update only changed row
							model.selectedPage(rowid);
							model.prevSelectedGroup = model.selectedGroup();
							jQuery('#' + tableId).vpmsBindViewModel("updateModel");
							var updatingTimerTimeout = 0;
							var updatingTimer = window.setInterval(function () {
								  if (!model.updating) {
									  window.clearInterval(updatingTimer);
									  reselectRow(rowid);
								  }else
								  if(updatingTimerTimeout>=3000)
									  window.clearInterval(updatingTimer);
								  else
									  updatingTimerTimeout += 200;
							}, 200);
						}
						else
							reselectRow(rowid);
					}
				};
				var afterRestore = function(rowid){
					model.lastselid = undefined;
					reselectRow(rowid);
				}
				var editRow = function(id, e){
					var tableId = model.tableId();
					if(model.lastselid && id!=model.lastselid)
						jQuery('#'+ tableId).jqGrid('saveRow',model.lastselid, false, 'clientArray', undefined, afterSave, afterRestore, afterRestore);
					//wait for model ends updating  (5 seconds timeout)
					var updatingTimerTimeout = 0;
					var updatingTimer = window.setInterval(function () {
							if (!model.updating) {
								window.clearInterval(updatingTimer);
								//get choices && checkbox
								var group = jQuery.vpmsBindViewModel.elementById(model.groups, id);
								if(group){
									var elementsArray = group.elements;
									for(var i=0; i<elementsArray.length; i++){
										var _element = $.vpmsBindViewModel.getArrayElement(elementsArray[i]);
										if(_element.disabled!=undefined){
											jQuery('#'+ tableId).jqGrid('setColProp', _element.name, {editable: !_element.disabled});
										}
										if(jQuery.vpmsBindViewModel.isChoicesWidget(_element.widget)){
											var choices = _element.choices;
											var result = '';
											for(var k=0; k<choices.length; k++)
												result += choices[k]._v + ":" + choices[k]._x + ";";
											if(result.length>0)
												result = result.substring(0, result.length-1);
											jQuery('#'+ tableId).jqGrid('setColProp', _element.name, {editoptions: {value:result} });
										}else
										if(_element.widget=='checkbox'){
											var result = jQuery.vpmsBindViewModel.unwrapObservable(jQuery.vpmsBindViewModel.element(_element.captions, 'text.yes'), "caption") + ":" + jQuery.vpmsBindViewModel.unwrapObservable(jQuery.vpmsBindViewModel.element(_element.captions, 'text.no'), "caption");
											jQuery('#'+ tableId).jqGrid('setColProp', _element.name, {editoptions: {value:result} });
										}
									}
								}
								//reselectRow
								jQuery('#' + tableId).jqGrid('setSelection', id, false);
								//edit row
								jQuery('#' + tableId).jqGrid('editRow',id,true, function(){if(e) setTimeout(function(){jQuery("input, select",e.target).focus();}, 10);
								}, false, 'clientArray', undefined, afterSave, afterRestore, afterRestore);
								model.lastselid=id;
							}else
							if(updatingTimerTimeout>=3000)
								window.clearInterval(updatingTimer);
							else
								updatingTimerTimeout += 200;
					}, 200);
				};
				settings.onSelectRow = function(id){
					var tableId = model.tableId();
					if(model.lastselid && id!=model.lastselid){
						model.selid = id;
						jQuery('#'+ tableId).jqGrid('saveRow',model.lastselid, false, 'clientArray', undefined, afterSave, afterRestore, afterRestore);
					}else
						model.updateNavCaptions(id);
				}
				
				//inline edit - open row for editing on click
				settings.ondblClickRow = function(id, iRow, iCol, e){
					editRow(id, e);
				}//ondblClickRow
				var bindKeysSettings = {"onEnter": function(rowid){ editRow(rowid); }};
			}//!model.readonly
			//createGrid
			table.jqGrid(settings).jqGrid("bindKeys", bindKeysSettings);
			if(!model.readonly && model.staticPageCount!==true){
				//nav grid
				table.jqGrid('navGrid', settings.pager,
					{edit:true,add:true,del:true,search:false,view:false,refresh:false,
						deltitle:model.removeCaption,
						addtitle:model.addCaption,
						addfunc:function(){
							if(model.insertAfterSelected){
								var selectedId = table.getGridParam('selrow');
								if(selectedId)
									var group = jQuery.vpmsBindViewModel.elementById(model.groups, selectedId); 
								if((!group || group.canAdd==undefined || group.canAdd) && (model.canAdd==undefined || model.canAdd))
									table.vpmsBindViewModel('addToGroup', model, selectedId);
							}
							else
								if(model.canAdd==undefined || model.canAdd)
									table.vpmsBindViewModel('addToGroup', model);
						},
						delfunc:function(id){
							var group = jQuery.vpmsBindViewModel.elementById(model.groups, id);
							if(group.canRemove==undefined || group.canRemove)
								table.vpmsBindViewModel('removeFromGroup', model, group);
						},
						editfunc:function(id){
							if(model.lastselid!=id)
								editRow(id, null);
							else
								table.jqGrid('saveRow',model.lastselid, false, 'clientArray', undefined, afterSave, afterRestore, afterRestore);
						}
					},
					{}, // edit options
					{}, // add options
					{} // del options
				);
			}
			model.inited = true;
		}	

		//server side navigation
		if(model.navigationUpdate)
			model.navigationUpdate(data);			
		
		setTimeout(function(){
			jQuery('[tableId="' + $.vpmsBindViewModel.escapeSelector(model.tableId()) + '"]').each(function(){
				var table = jQuery(this);
				var model = table[0].kendoBindingTarget.source;
				if(!model.inited){
					createGrid(table, model);
				}
				//reset selectedpage
				
				$.vpmsBindViewModel.updateModelItem(model, "selectedPage", "");
				//merge data
				var selectedIndex;
				var rows = [];
				var count=0;
				for(var i=0; i<model.groups.length; i++){
					var group = $.vpmsBindViewModel.getArrayElement(model.groups[i]);
					if(group.relevant===false)
						continue;
					var row = {id:group.id};
					for(var k=0; k<group.elements.length; k++){
						var element = $.vpmsBindViewModel.getArrayElement(group.elements[k]);
						if(element.displayValue!=undefined)
							row[element.name] = element.displayValue;
						else
							row[element.name] = element.value;
						row["_" + element.name + "_error"] = element.error;
						var tooltip = element.tooltip;
						row["_" + element.name + "_tooltip"] = tooltip?tooltip:"";
					}
					if(data.sp==row.id)
						selectedIndex = count;
					count++;
					rows.push(row);
				};
				//set table caption and table colnames
				if(data.cn_){
					for(var i=0; i<model.settings.colModel.length; i++)
						if(i<data.cn_.length){
							var css = undefined;
							if(model.settings.colModel[i].labelAlign)
								css = {'text-align':model.settings.colModel[i].labelAlign};
							var colname = data.cn_[i];
							if(data.cr_ && data.cr_[i] =="1")
								colname += '<span class="ui-state-highlight vpms-widget-required-icon"><span class="ui-icon ui-icon-bullet"></span></span>';
							table.setLabel( model.settings.colModel[i].name, colname, css);
						}
				}				
				table.setCaption(model.caption);
				if(model.settings.dynamicLoad){
					model.navigation.rows = rows;
				}
				else{
					table.clearGridData();
					table.setGridParam({data: rows});
					if(selectedIndex){
						var rowNum = table.getGridParam('rowNum');
						table.setGridParam({page: Math.floor(selectedIndex/rowNum)+1});						
					}else{
						table.setGridParam({page: model.selectedGridPage});												
					}
				}				
				table.trigger('reloadGrid');
				//enable-disable edit buttons
				if(model.disabled==true)
					jQuery('#pager_' + table.attr('id')).find('.ui-pg-button').addClass('ui-state-disabled');
				else
					jQuery('#pager_' + table.attr('id')).find('.ui-pg-button').removeClass('ui-state-disabled');
				//select new added row
				if(data.sp){
					setTimeout(function(){table.jqGrid('setSelection', data.sp); table.focus();},100);
				}else
					model.updateNavCaptions();
			});
		}, 1);
	}
});
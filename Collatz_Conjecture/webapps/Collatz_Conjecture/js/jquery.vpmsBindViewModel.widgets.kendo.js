$.vpmsBindViewModel.tmplScriptType = "text/x-kendo-template";
$.vpmsBindViewModel.isPhone = kendo.support.mobileOS && !kendo.support.mobileOS.tablet;
$.vpmsBindViewModel.appMode = kendo.support.mobileOS && kendo.support.mobileOS.appMode;
$.vpmsBindViewModel.useNativeScrolling = kendo.support.mobileOS;
$.vpmsBindViewModel.platform = undefined;
$.vpmsBindViewModel.kendoApplicationContainerId = "vpms-kendo-container";
$.vpmsBindViewModel.wrappedInDevice = !kendo.support.mobileOS;
$.vpmsBindViewModel.options.bindAfterUpdate = true;
$.vpmsBindViewModel.ignoreSingleRootPage = true;
$.vpmsBindViewModel.events = {
		start : kendo.support.mobileOS?"touchstart":"mousedown",
		end   : kendo.support.mobileOS?"touchend":"mouseup",
		move  : kendo.support.mobileOS?"touchmove":"mousemove",
		cancel: kendo.support.mobileOS?"touchleave touchcancel":"mouseout"
};

$.vpmsBindViewModel.widgets_kendo_previousInitModelRoot = $.vpmsBindViewModel.initModelRoot;
$.vpmsBindViewModel.initModelRoot = function(model, containerId, settings){
	if($.vpmsBindViewModel.widgets_kendo_previousInitModelRoot)
		$.vpmsBindViewModel.widgets_kendo_previousInitModelRoot(model, containerId, settings);
	//redefine root widget if pages/treeview in elements
	if(model.widget=="product"){		
		if(model.elements){
			for(var i=0; i<model.elements.length; i++){
				var widget = model.elements[i].widget;
				if(widget=="pages" || widget=="treeview"){
					var pagesWidget = model.elements[i];
					pagesWidget.name = model.name;
					model.elements = $.vpmsBindViewModel.initModelItem(model.undefined, []);
					jQuery.extend(model, pagesWidget);
					model.widget = (widget=="pages")?"pagesTablet":"treeview";
					break;
				}
			}
		}
	}else
	if(model.widget=="pages"){
		model.widget = "pagesTablet";
	}
	//if phone or suppresstabs - convert tree view to pages 
	if(model.widget=="treeview" && ($.vpmsBindViewModel.isPhone || (model.settings && model.settings.suppresstabs))){
		$.vpmsBindViewModel.convertTreeviewToPages(model, "pagesTablet");		
	}
};
$.vpmsBindViewModel.widgets_kendo_previousInitModel = $.vpmsBindViewModel.initModel;
//redefine model.template to model._template, kendo makes all fields observable except that starting with "_", template should not be observable 
$.vpmsBindViewModel.initModel = function(model, containerId){
	//redefine template
	if(model.template)
		model._template = model.template;
	model.template=undefined;
	if($.vpmsBindViewModel.widgets_kendo_previousInitModel)
		$.vpmsBindViewModel.widgets_kendo_previousInitModel(model, containerId);
	//init list view 
	if((model.isListview===undefined || model.isInset===undefined) && model.elements){
		if(model.widget=='elementgroup' && model.labeled){
			model.isListView = false;
		}else
		for(var i=0; i<model.elements.length; i++){
			var element = model.elements[i];
			if(model.elements[i].wrapdata)
				element = model.elements[i].wrapdata;
			var isElement = element.isElement || (element.widget=='elementgroup' && element.labeled); 
			if(model.isListview===undefined && isElement && $.vpmsBindViewModel.templateExists(element.widget))
				model.isListview = true;
			if(model.isInset===undefined && !isElement)
				model.isInset = false;
		}
		if(model.isInset===undefined)
			model.isInset = true;
	}	
	return model;
};

$.vpmsBindViewModel.applyBindings = function(container){
	var kendoModel = $(container).data('kendoModel');
	var $product = $("<div class=\"vpms-product-root\"><div id=\"" + $.vpmsBindViewModel.kendoApplicationContainerId + "\" class=\"vpms-" + kendoModel.model.widget + "-widget\" data-template=\"" + kendoModel.model.widget + "\" data-bind=\"source: model\"></div></div>");
	$(container).append($product);
	kendo.bind($(container), kendoModel, { roles: { button: kendo.mobile.ui.roles.button } });
};
$.vpmsBindViewModel.initModelObservable = function(model, container){
	var kendoModel = kendo.observable({model:model});
	$(container).data('kendoModel', kendoModel);
	return kendoModel.model;
};
$.vpmsBindViewModel.initModelItem = function(item, defaultValue){
	if(item!==undefined)
		return item;
	else
		return defaultValue;
};
$.vpmsBindViewModel.initModel_parent = function(model, parent){
	//kendo has it's own parent() function, for detached introspection we hold _parent link
	model._parent = parent;
};
$.vpmsBindViewModel.getParent = function(model){
	var parent = (model.parent!==undefined)?(jQuery.isFunction(model.parent)?model.parent():model.parent):model._parent;
	if(parent){
		if(parent.widget)
			return parent;
		else
			return $.vpmsBindViewModel.getParent(parent);
	}
};
$.vpmsBindViewModel.unwrapObservable = function(model, name){
	if(!name)
		return model;
	else
		if(model.get)
			return model.get(name);
		else
			return model[name];
};
$.vpmsBindViewModel.updateModelItem = function(model, name, value){
	if($.isFunction(model[name]))
		model[name](value);
	else
		if(model.set)
			model.set(name, value);
		else
			model[name] = value;
};
$.vpmsBindViewModel.dependentObservable = function(model, functionName, read, write){
	model[functionName] = function(){
				return read();
	};
};
$.vpmsBindViewModel.initArrayElement = function(element){
	return {wrapdata:element};
};
$.vpmsBindViewModel.getArrayElement = function(element){
	if(element.wrapdata)
		return element.wrapdata;
	else
		return element;
};		
$.vpmsBindViewModel.getGroupTemplate = function(model){
	if(model._template){
		if($.isFunction(model._template))
			return model._template();
		else
			return $.vpmsBindViewModel.cloneObject(model._template);
	}
	else
		return undefined;
};
$.vpmsBindViewModel.hasParentWidgetItem = function(model, widget, item){
	var _widget = $.vpmsBindViewModel.getParentWidget(model, widget);
	if(_widget)
		return _widget[item]!==undefined;
};
//overwrite core functions
$.vpmsBindViewModel.dialog = function(id, title, content, options){
	var $dialog = $('#' + id);
	if($dialog.length===0){
		var width = (options && options.width)?options.width:"80%";
		var heightStr = (options && options.height)?"height: " +options.height+";":"";
		$dialog = $('\
			<div id="' + id + '" data-role="modalview" style="width: ' + width + ';'+ heightStr +'">\
				<div data-role="header">\
					<div data-role="navbar">\
						<span id="' + id +'_title"></span>\
						<a data-role="button" data-align="right">Close</a>\
					</div>\
				</div>\
				<div data-role="content" class="km-insetcontent"><div class="vpms-dialog-content" style="padding:1em"></div></div>\
			</div>');
		var splitview = $('div[data-role="splitview"]');
		if(splitview.length>0){
			var panes = $(splitview).find('div[data-role="pane"]');
			$(panes[panes.length-1]).append($dialog);
		}else{
			var $mobileContainer = $("#" + $.vpmsBindViewModel.kendoApplicationContainerId);
			$mobileContainer.append($dialog);
		}
		$dialog.kendoMobileModalView();
		$dialog.on($.vpmsBindViewModel.events.end, 'div[data-role="header"] a', function(){
			$dialog.kendoMobileModalView('close');
		});
	}
	$dialog.find('#'+id + "_title").html(title);
	var $content = $dialog.find('.vpms-dialog-content');
	if(typeof content == 'string')
		$content.html(content);
	else{
		$content.empty();
		$content.append(content);
	}
	$dialog.kendoMobileModalView('open');
};
$.vpmsBindViewModel.isMobile = true;
$.vpmsBindViewModel.defaultTransition = "slide";
$.vpmsBindViewModel.changeView = function(viewid, selector, isBack){
	var $pane;
	if(selector)
		$pane = jQuery(selector);
	if(!$pane || $pane.length===0){
		//find pane where this view
		var $view = jQuery('[data-role="view"]').filter('#' + viewid);
		$pane = $view.closest('[data-role="pane"]');
	}
	if($pane && $pane.length>0){
		var pane = $pane.data("kendoMobilePane");
		if(pane){
			pane.navigate('#' + viewid, $.vpmsBindViewModel.defaultTransition + (isBack?" reverse":""));
		}
	}else{
		//no pane found - application navigate
		if($.vpmsBindViewModel.kendoApp)
			$.vpmsBindViewModel.kendoApp.navigate('#' + viewid, $.vpmsBindViewModel.defaultTransition + (isBack?" reverse":""));
	}
};

$.vpmsBindViewModel.adjustElementsWidth = function(){
};
$.vpmsBindViewModel.blockUI = function(model, text, isOverlay){
	if($.vpmsBindViewModel.kendoApp){
		$($.vpmsBindViewModel.kendoApp.element).children('.km-loader').show();
		//cache previous container only if new model initing 
		if(model.init){
			var vpmsRoot = jQuery($.vpmsBindViewModel.kendoApp.element).closest('.vpms-product-root');
			vpmsRoot.addClass("vpms-product-root-cached");
			jQuery('body').append(vpmsRoot);
		}
	}		
	if(model.init)
		jQuery('#' + model.containerId).hide();	
};

$.vpmsBindViewModel.unblockUI = function(model){
	//remove cached container, if new model initing
	if($.vpmsBindViewModel.kendoApp)
		$($.vpmsBindViewModel.kendoApp.element).children('.km-loader').hide();
	jQuery('body').children('.vpms-product-root-cached').remove();
	jQuery('#' + model.containerId).show();
};    

$.vpmsBindViewModel.kendoMobileSwitchInit = function($switch){
	var switchData = $switch.data("kendoMobileSwitch");
	if(switchData){
		switchData.bind("change", function(e){
			$(this.element).trigger('change');
		});
		//disable switch
		var switchWrapper = switchData.wrapper;
		switchWrapper[0].addEventListener($.vpmsBindViewModel.events.start, function(e) { 
			if($(this).closest('.vpms-checkbox-container').attr('_disabled')=="true")
				e.stopPropagation();
		}, true);
	}	
};

$.vpmsBindViewModel.kendoMobileRestoreGenericViews = function($container){
	var containerId=$container.attr('id');
	if(containerId){
		var $pane = $container.closest('[data-role="pane"]');
		var childViews = $pane.find('[data-role="view"][parent="'+ containerId +'"]');
		childViews.addClass("vpms-afterRender");
		//recursive restore views
		childViews.find('.vpms-generic-view-container').each(function(){
			$.vpmsBindViewModel.kendoMobileRestoreGenericViews($(this));
		});
		$container.append(childViews);
	}
};

$.vpmsBindViewModel.kendoMobileViewInit = function(e){
	//fix: switch does not fire input's change event, autocommit does not work
	$(e.view.element).find('.vpms-checkbox-widget input, .vpms-checkboxAfterLabel-widget input').each(function(){
		$.vpmsBindViewModel.kendoMobileSwitchInit($(this));
	});
};

$.vpmsBindViewModel.kendoMobileResetScroller = function(view){
	var $pane = jQuery(view).closest('[data-role="pane"]');
	if($pane.length>0){
		var pane = $pane.data('kendoMobilePane');
		if(pane && pane.view() && pane.view().scroller)
			pane.view().scroller.reset();
	}
};

$.vpmsBindViewModel.kendoMobileScrollTo = function(element){
	var $pane = jQuery(element).closest('[data-role="pane"]');
	if($pane.length>0){
		var pane = $pane.data('kendoMobilePane');
		if(pane && pane.view() && pane.view().scroller){
			var scroller = pane.view().scroller;
			var offset = $(element).offset().top + scroller.scrollTop - $(pane.view().header).height();
			pane.view().scroller.scrollTo(0, -offset);
		}
	}
};

$.vpmsBindViewModel.kendoMobileViewShow = function(e){
	$.vpmsBindViewModel.kendoMobileResetScroller(e.view.element);
};

$.vpmsBindViewModel.getForcePlatformObject = function(){
	if($.vpmsBindViewModel.platform){
		var platform = $.vpmsBindViewModel.platform.split('-')[0];
		var result = {
			name : platform,
			device: platform,
			appMode:$.vpmsBindViewModel.appMode,
			tablet:($.vpmsBindViewModel.isPhone?false:platform)
		};
		result[platform] = true;
		if(platform=="ios")
			result.device = ($.vpmsBindViewModel.isPhone?"iphone":"ipad");
		if(!$.vpmsBindViewModel.isPhone){
			if(platform=="ios") result.tablet = "ipad"; else
			if(platform=="android") result.tablet = "nexus"; else
			if(platform=="blackberry") result.tablet = "playbook";
		}
		return result;
	}
	return undefined;
};
$.vpmsBindViewModel.getMobileApplicationParameters = function(){
	return {
        browserHistory: false,
		platform: $.vpmsBindViewModel.getForcePlatformObject(),
		skin: $.vpmsBindViewModel.platform,
		loading: "<h1>" + 'Please wait...' + "</h1>"
	};
};

$.vpmsBindViewModel.afterRenderInit = function($this){
	var settings = $this.data('settings');
	var mobileApplicationParameters = $.vpmsBindViewModel.getMobileApplicationParameters();
	if($.vpmsBindViewModel.wrappedInDevice){
		if(!$this.parent().hasClass('vpms-device-emulator')){
			$this.parent().append($('<div class="vpms-device-emulator '+ 
					(mobileApplicationParameters.platform?'vpms-' + mobileApplicationParameters.platform.name:'') + ' ' + 
					($.vpmsBindViewModel.isPhone?'':'vpms-tablet')  + '"></div>').append($this));
			$('html').css("font-size", "12px");
		}
	}
	var kendoApplicationContainer = $this.find("#" + $.vpmsBindViewModel.kendoApplicationContainerId);
    //get initially created internal views in root
	var model = $this.data('model');
	if(model.initialViewId){
		var initialView = $this.findAndSelf('#' + model.initialViewId +'[data-role="view"]');
		if(initialView.length>0){
			kendoApplicationContainer.append(initialView);
			initialView.removeClass('vpms-afterRender');
			mobileApplicationParameters.initial = "#" + model.initialViewId;
		}
	} 
	//create new application
    if(kendo.history){
        kendo.history.current = "";
    }
	$.vpmsBindViewModel.kendoApp = new kendo.mobile.Application(kendoApplicationContainer, mobileApplicationParameters);
	$($.vpmsBindViewModel.kendoApp.element).on('change', '.vpms-autocommit', function(){
		$.vpmsBindViewModel.autoCommit(this);
	});
	//iOS Safari bug - native date picker does not call change
	$($.vpmsBindViewModel.kendoApp.element).on('blur', 'input[type="date"]', function(){
		$(this).trigger('change');
	});
	$.vpmsBindViewModel.kendoClickableMoveFunction = function(event){
		var $this = $(this);
		if($this.data('vpmsSelectStart')===true){
			$this.data('vpmsSelectStart', false);
		}
		$this.removeClass('km-state-active');
		$this.unbind($.vpmsBindViewModel.events.move, $.vpmsBindViewModel.kendoClickableMoveFunction);
	};
	$($.vpmsBindViewModel.kendoApp.element).on($.vpmsBindViewModel.events.start, '.vpms-listview.vpms-listview-clickable>li', function(event){
		if($(this).hasClass('km-state-active') || $(this).hasClass('active'))
			return;
		$(':focus').blur(); //make focused edit element commit its value to model
		$(this).addClass('km-state-active');
		$(this).data('vpmsSelectStart', true);
		$(this).bind($.vpmsBindViewModel.events.move, $.vpmsBindViewModel.kendoClickableMoveFunction);
	});
	$($.vpmsBindViewModel.kendoApp.element).on($.vpmsBindViewModel.events.end + ' ' + $.vpmsBindViewModel.events.cancel, '.vpms-listview.vpms-listview-clickable>li', function(){
		if(!$(this).hasClass("vpms-selectable-tab"))
			$(this).removeClass('km-state-active');
		$(this).unbind($.vpmsBindViewModel.events.move, $.vpmsBindViewModel.kendoClickableMoveFunction);
	});
};

$.vpmsBindViewModel.afterRender = function($this, containerId){	
	$this.findAndSelf('.vpms-afterRender').each(function(){
		//move internal created views to closest pane
		//don't init view, it should be inited by $.vpmsBindViewModel.kendoMobileViewInit 
		if($(this).attr('data-role')=="view"){
			var $pane = $(this).closest('[data-role="pane"]');
			if($pane.length>0){
				$pane.append($(this));
				if(!$(this).hasClass('km-view')){
					$(this).kendoMobileView();
				}
				$(this).hide();
			}
		}else{
			//init switch widget only if it is not init
			$(this).findAndSelf('.vpms-checkbox-widget input, .vpms-checkboxAfterLabel-widget input').each(function(){
				var switchData = $(this).data("kendoMobileSwitch");
				if(!switchData){
					$(this).kendoMobileSwitch({ onLabel: $(this).attr('data-on-label'), offLabel: $(this).attr('data-off-label') });
					$.vpmsBindViewModel.kendoMobileSwitchInit($(this));
				}
			});
			$(this).findAndSelf('.vpms-button-widget-button').each(function(){
				var buttonData = $(this).data("kendoMobileButton");
				if(!buttonData){
					$(this).kendoMobileButton();
				}
			});
		}
		//clear render flag
		$(this).removeClass('vpms-afterRender');
	});
	//mask
	if($.fn.mask)
		$this.findAndSelf('.vpms-edit-widget input[mask]').each(function(){
			var mask = $(this).attr('mask');
			if(mask)
				$(this).mask(mask);
		});
	//accordion
	$this.findAndSelf('.vpms-accordion').each(function(){
		var $accordion = $(this);
		var accordion = $accordion.data('accordion');
		if(!accordion){
			accordion = {
				select: function(selector){
					var $li = $(selector);
					if(!$li.hasClass('active')){
						$li.closest('ul').children('li').removeClass('active');
						$li.addClass('active');
					}
				}	
			};
			$accordion.data('accordion', accordion);
			$accordion.bind($.vpmsBindViewModel.events.end, function(e){
				var $li =  jQuery(e.target).closest('li');
				if($li.data('vpmsSelectStart')===true){
					$li.closest('ul').data('accordion').select($li);
					$.vpmsBindViewModel.kendoMobileScrollTo($li);
				}
				$li.data('vpmsSelectStart', false);
			});
			//select first visible
			if($accordion.children('li.active').length===0){
				$li = $(this).children('li[relevant="true"]');
				if($li.length>0)
					accordion.select($li[0]);
			}
		}
	});
};

$.vpmsBindViewModel.widgets_kendo_afterModelUpdate = $.vpmsBindViewModel.afterModelUpdate;
$.vpmsBindViewModel.afterModelUpdate=function(container, settings){
	if($.vpmsBindViewModel.widgets_kendo_afterModelUpdate)
		$.vpmsBindViewModel.widgets_kendo_afterModelUpdate(container, settings);
	//inset listview first and last visible schould be rounded
	$(container).find('.vpms-listview').each(function(){
		$(this).children('li').removeClass('first').removeClass('last');
		$(this).children('li[relevant="true"]:first').addClass('first');
		$(this).children('li[relevant="true"]:last').addClass('last');
	});
	$(container).find('.vpms-afterRender').each(function(){
		$.vpmsBindViewModel.afterRender($(this), $(container).attr('id'));
	});
	//reset scroller of a view
	var panes = $(container).find('[data-role="pane"]');
	for(var i=0; i<panes.length; i++){
		var pane = $(panes[i]).data('kendoMobilePane');
		if(pane && pane.view() && pane.view().scroller){
			var scroller = pane.view().scroller;
			if(scroller._size && scroller._size.height>=$(scroller.scrollElement).height())
				scroller.reset();
		}
	}
	//this flag set to true to prevent multiple change view during single VPMS update call;
	$.vpmsBindViewModel.isMobileViewChanging = false;
};

$.vpmsBindViewModel.startMobileChangeView = function(){
	if($.vpmsBindViewModel.isMobileViewChanging)
		return false;
	else{
		$.vpmsBindViewModel.isMobileViewChanging = true;
		return true;
	}
};

$.vpmsBindViewModel.getCaptionById = function(model, id){
	for(var i=0; i<model.captions.length; i++){
		if(model.captions[i].name==id)
			if(model.get)
				return model.get('captions['+i+'].caption');
			else
				return model.captions[i].caption;
	}
	return model.id;
};

$.vpmsBindViewModel.pushTemplate({
	name: 'listElement',
	template: '#if(!$.vpmsBindViewModel.templateExists(data.wrapdata.widget)){#<li style="display:none" relevant="false"></li>#}else{#\
		<li class="#if(!data.wrapdata.container){#vpms-element#}# vpms-#=data.wrapdata.widget#-element #if(data.wrapdata.labeled!=true){#vpms-notlabeled#}#" data-bind="visible: wrapdata.relevant, attr:{relevant:wrapdata.relevant}">\
			#if(data.wrapdata.hasLabel==true){#<div class="vpms-label-widget #if(data.wrapdata.labeled!=true){#vpms-notlabeled#}# #if(data.wrapdata.settings && data.wrapdata.settings.labelAlign){#vpms-align-#=data.wrapdata.settings.labelAlign##}#" data-bind="source:wrapdata" data-template="label"></div>#}#\
			<div class="vpms-#=data.wrapdata.widget#-widget #if(data.wrapdata.settings &&  data.wrapdata.settings.css){# #=data.wrapdata.settings.css# #}# #if(!data.wrapdata.labeled){#vpms-notlabeled#}# #if(data.wrapdata.settings && data.wrapdata.settings.align){#vpms-align-#=data.wrapdata.settings.align##}#" data-template="#=data.wrapdata.widget#" data-bind="source: wrapdata"></div>\
		<div style="clear:both"></div></li>\
		#}#'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'element',
	template: '#if(!$.vpmsBindViewModel.templateExists(data.wrapdata.widget)){#<div style="display:none"></div>#}else{#\
		<div class="#if(!data.wrapdata.container){#vpms-element#}# vpms-#=data.wrapdata.widget#-element" data-bind="visible: wrapdata.relevant">\
			#if(data.wrapdata.hasLabel==true){#<div class="vpms-label-widget #if(data.wrapdata.labeled!=true){#vpms-notlabeled#}# #if(data.wrapdata.settings && data.wrapdata.settings.labelAlign){#vpms-align-#=data.wrapdata.settings.labelAlign##}#" data-bind="source:wrapdata" data-template="label"></div>#}#\
			<div class="vpms-#=data.wrapdata.widget#-widget #if(data.wrapdata.settings &&  data.wrapdata.settings.css){# #=data.wrapdata.settings.css# #}# #if(!data.wrapdata.labeled){#vpms-notlabeled#}# #if(data.wrapdata.settings && data.wrapdata.settings.align){#vpms-align-#=data.wrapdata.settings.align##}#" data-template="#=data.wrapdata.widget#" data-bind="source: wrapdata"></div>\
		<div style="clear:both"></div></div>\
		#}#'
});

$.vpmsBindViewModel.pushTemplate({
	name : "product",
	template: '\
		<div data-role="view" #if($.vpmsBindViewModel.useNativeScrolling){#data-use-native-scrolling="true"#}# id="#=data.rootViewId()#" data-init="$.vpmsBindViewModel.kendoMobileViewInit" data-show="$.vpmsBindViewModel.kendoMobileViewShow">\
			<div data-role="header">\
				<div data-role="navbar"><span data-bind="text: caption"></span>#if(data.navigateBack){#\
					<a class="nav-button km-back" data-role="button" data-align="left" data-bind="click:navigateBack"><span data-bind="text:backcaption"></span></a>#}##if(data.logo){#\
					<img data-align="right" class="vpms-csc-logo" data-bind="attr:{src:logo}"/>\
				#}#</div>\
			</div>\
			<div data-role="content" class="km-insetcontent">\
				#if(data.isListview){#\
				<ul class="vpms-listview km-listview km-list km-listinset vpms-elements-container" data-template="listElement" data-bind="source: elements"></ul>\
				#}else{#\
				<div class="vpms-elements-container" data-template="element" data-bind="source: elements"></div>\
				#}#\
			</div>\
		</div>\
	',
	initModel: function(model){
		if(model.container===undefined)
			model.container = true;
		if(!model.name) model.name = "product";	
		if(!model.id) model.id = model.name;
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.logo = $.vpmsBindViewModel.initModelItem(model.settings?model.settings.logo:undefined, undefined);
		model.rootViewId = function(){
			return "root_" + $.vpmsBindViewModel.alphanumericSelector(this.name);
		};				
		model.caption = function(){
			return $.vpmsBindViewModel.getCaptionById(this, 'productCaption');
		};
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.captions.push($.vpmsBindViewModel.initModel_caption({name:'btn.back', caption:'Back'}));
		model.backcaption = function(){
			return $.vpmsBindViewModel.getCaptionById(this, 'btn.back');
		};
	}
});

//selectQuote
$.vpmsBindViewModel.pushTemplate({
	name : "selectQuoteChoicesWidget",
	template: '<li data-bind="attr:{id:_v}"><a class="km-listview-link" data-bind="text:_x"></a></li>'
});
$.vpmsBindViewModel.pushTemplate({
	name : "selectQuote",
	template: '\
		<div class="km-listview-wrapper"><ul class="vpms-listview vpms-listview-clickable km-listview km-list km-listinset" data-template="selectQuoteChoicesWidget" data-bind="source: products, events:{' + $.vpmsBindViewModel.events.end + ':selectProduct}"></ul></div>\
	',
	initModel: function(model){
		if(model.container===undefined)
			model.container = true;
		if(!model.name) model.name = "selectQuote";
		if(!model.settings)
			model.settings={};
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.productKey = $.vpmsBindViewModel.initModelItem(model.settings?model.settings.productKey:undefined, "A_Product_key");
		model.products = []	;
		model.selectProduct = function(e){
			var $li =  jQuery(e.target).closest('li');
			if($li.data('vpmsSelectStart')===true)
			{
				var id = $li.attr('id');
				if(id)
					jQuery(e.target).vpmsBindViewModel('changeModel', id);
			}
			$li.data('vpmsSelectStart', false);
		};
	},
	updateModel: function(model, data){
		for(var i=0; i<model.elements.length; i++){
			if(model.elements[i].wrapdata.name==model.productKey)
				 $.vpmsBindViewModel.updateModelItem(model, 'products',  $.vpmsBindViewModel.unwrapObservable(model.elements[i].wrapdata, 'choices'));
		}
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'button',
	template: '#if(data.wrapdata && data.wrapdata.barButton || data.barButton){#<a class="vpms-button-widget" data-icon="#=data.wrapdata.icon#" data-bind="events:{' + $.vpmsBindViewModel.events.start + ':wrapdata.startClick}, css:{km-state-active:wrapdata.enable}, attr:{isEnabled:wrapdata.enable}, visible:wrapdata.visible, text:wrapdata.caption, click:wrapdata.click"></a>#}else{#\
				<a class="vpms-button-widget-button" data-role="button" data-icon="#=data.wrapdata?data.wrapdata.icon:data.icon#" data-bind="enabled:wrapdata.enable, visible:wrapdata.visible, click:wrapdata.click"><span class="km-text" data-bind="text:wrapdata.caption"></span></a>#}#\
	',
	initModel: function(model){
		model.hasLabel = false;
		if(model.isElement===undefined)
			model.isElement = true;
		if(model.setValue===undefined)
			model.setValue="1";
			
		if(!model.pos) model.pos = "left";
		if(!model.name) model.name = "btn." + model.role;
		if(!model.role){
			if($.vpmsBindViewModel.roleBindings[model.name])
				model.role = model.name;
			else
				model.role = 'default';
		}		
		if(!model.icon){
			if($.vpmsBindViewModel.roleBindings[model.role]) 
				model.icon = $.vpmsBindViewModel.roleBindings[model.role].icon;
			if(!model.icon)
				model.icon = model.role;
		}
		if(model.barButton===undefined)
			model.barButton = false;
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "btn." + model.role);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		var initButtonItem = function(model, name, init, defaultValue){
			if(model[name] && !jQuery.isFunction(model[name]))
				defaultValue = model[name];
			//by priority - function, role
			if(model[name] && jQuery.isFunction(model[name])){
				model["_" + name] = model[name];
			}else
			if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role][name]){
				model["_" + name] = function(){
					return $.vpmsBindViewModel.roleBindings[this.role][name].call($.vpmsBindViewModel.roleBindings[model.role], this); 
				};
			}
			if(init && jQuery.isFunction(init)){
				init(model);
			}
			model[name] = defaultValue;			
		};
		initButtonItem(model, "caption", function(model){
			if(!model._caption && !model.isElement){
				//if not element - get button's caption from resources
				model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
				var caption = $.vpmsBindViewModel.element(model.captions, model.id, false);
				if(caption===false){
					caption  = $.vpmsBindViewModel.initModel_caption({name:model.id, caption:model.caption});
					model.captions.push(caption);
				}
				model._caption = function(){
					var _model = $.vpmsBindViewModel.getArrayElement(this);
					var caption = $.vpmsBindViewModel.element(_model.captions, _model.id, false);
					if(caption!==false)
						return caption.caption;
					else
						return _model.caption;
				};
			}
		}, model.name);
		initButtonItem(model, "visible", function(model){
			if(!model._visible && model.isElement){
				model._visible = function(){
					var _model = $.vpmsBindViewModel.getArrayElement(this);
					return _model.relevant;
				};
			}
		}, true);
		initButtonItem(model, "enable", function(model){
			if(!model._enable && model.isElement){
				model._enable = function(){
					var _model = $.vpmsBindViewModel.getArrayElement(this);
					return !_model.disabled;
				};
			}
		}, true);
		//backwards compatibility
		model.isEnabled = function(value){
			var _model = $.vpmsBindViewModel.getArrayElement(this);
			if(value)
				jQuery.vpmsBindViewModel.updateModelItem(_model, "enable", value);
			else
				return jQuery.vpmsBindViewModel.unwrapObservable(_model, "enable");
		};
		//click - prevent execution if button is disabled
		model.startClick = function(event){
			var _model = $.vpmsBindViewModel.getArrayElement(this);
			if(!_model.enable)
				return false;			
		};
		//click
		if(model.click)
			model._click = model.click;
		model.click = function(event){
			var _model = $.vpmsBindViewModel.getArrayElement(this);
			if(!_model.enable)
				return;
			//set value
			jQuery.vpmsBindViewModel.updateModelItem(_model, "value", _model.setValue);
			//call user defined function if set
			if(_model._click)
				_model._click(event);
			else
				if(_model.role && $.vpmsBindViewModel.roleBindings[_model.role] && $.vpmsBindViewModel.roleBindings[_model.role].click){
					var $container = $(event.target).closest("." + jQuery.vpmsBindViewModel.options.containerClass);
					var rootmodel = $container.data('model');
					if(!rootmodel.actions)
						rootmodel.actions = {};
					var action = {};
					//action to be filled with parameters
					rootmodel.actions[_model.role] = action;
					if(!rootmodel.actionsModels)
						rootmodel.actionsModels = {};
					//action button to pass in updater
					rootmodel.actionsModels[_model.role] = _model;
					$.vpmsBindViewModel.roleBindings[_model.role].click(event.target, _model, rootmodel, action);
				}
		};
		if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role].initModel){
			$.vpmsBindViewModel.roleBindings[model.role].initModel(model);
		}
	},
	updateModel: function(model, data){	
		if(jQuery.isFunction(model._caption))
			jQuery.vpmsBindViewModel.updateModelItem(model, "caption", model._caption());
		if(jQuery.isFunction(model._visible))
			jQuery.vpmsBindViewModel.updateModelItem(model, "visible", model._visible());
		if(jQuery.isFunction(model._enable))
			jQuery.vpmsBindViewModel.updateModelItem(model, "enable", model._enable());			
		if(model.role && $.vpmsBindViewModel.roleBindings[model.role] && $.vpmsBindViewModel.roleBindings[model.role].updateModel){
			$.vpmsBindViewModel.roleBindings[model.role].updateModel(model, data);
		}
		//trigger change by enabled for bar buttons - kendo bar widget removes km-state-active
		if(model.barButton && model.trigger){			
			model.trigger("change",{field: "enable"});
		}
	}	
});

//enable, visible, click, update
jQuery.vpmsBindViewModel.roleBindings.save = {
		'enable' : function(model){
			return !$.vpmsBindViewModel.unwrapObservable(model,'disabled');
		},
		'click' : function(element, model, rootmodel, action){
			jQuery(element).vpmsBindViewModel("updateModel");
		},
		'icon' : 'save'
};

jQuery.vpmsBindViewModel.roleBindings.cancel = {
		'enable' : function(model){
			return !$.vpmsBindViewModel.unwrapObservable(model,'disabled');	
		},
		'click' : function(element, model, rootmodel, action){
			jQuery(element).vpmsBindViewModel("changeModel");
		},
		'icon' : 'trash'
};

jQuery.vpmsBindViewModel.roleBindings.pdf = {
		'initModel' : function(model){
			if(!model.settings) model.settings={};
			model.settings.ignoreDisabled = true;
		},		
		'click' : function(element, model, rootmodel, action){
			jQuery(element).vpmsBindViewModel("updateModel");
		},
		'update' : function(model, rootmodel, action){
			//if button still enabled
			var container = jQuery('#' + rootmodel.containerId); 
			if(model){
				if(($.isFunction(model.enable) && model.enable()===true) || model.enable===true)
					$.vpmsBindViewModel.openPdf(container);
				}else{
					//actually should not happen : if pdf button  is not set open pdf anyway?
				}
		},
		'icon' : 'bookmarks'
};
//enable, visible, click, update
jQuery.vpmsBindViewModel.roleBindings.next = {
		'visible' : function(model){
			return jQuery.vpmsBindViewModel.roleBindings.next.enable(model) || jQuery.vpmsBindViewModel.roleBindings.previous.enable(model);
		},		
		'enable' : function(model){
			var widget = $.vpmsBindViewModel.getParentHasItem(model, "nextPage");
			if(widget)
				return (widget.nextPage()!==undefined);
			else
				return false;
		},
		'click' : function(element, model, rootmodel, action){
			var widget = $.vpmsBindViewModel.getParentHasItem(model, "nextPage");
			if(widget){
				var id = widget.nextPage();
				widget.nextPage(id);
			}			
		},
		'updateModel' : function(model, data){
			//postpone updating "enable" and "visible" since selectedPage is set in Pages after updating buttons
			if(jQuery.isFunction(model._enable) || jQuery.isFunction(model._visible)){
				setTimeout(function(){
					if(jQuery.isFunction(model._enable))
						jQuery.vpmsBindViewModel.updateModelItem(model, "enable", model._enable());
					if(jQuery.isFunction(model._visible))
						jQuery.vpmsBindViewModel.updateModelItem(model, "visible", model._visible());
				}, 1);
			}
		}
};
jQuery.vpmsBindViewModel.roleBindings.previous = {
		'visible' : function(model){
			return jQuery.vpmsBindViewModel.roleBindings.next.enable(model) || jQuery.vpmsBindViewModel.roleBindings.previous.enable(model);
		},		
		'enable' : function(model){
			var widget = $.vpmsBindViewModel.getParentHasItem(model, "prevPage");
			if(widget)
				return (widget.prevPage()!==undefined);
			else
				return false;			
		},
		'click' : function(element, model, rootmodel, action){
			var widget = $.vpmsBindViewModel.getParentHasItem(model, "prevPage");
			if(widget){
				var id = widget.prevPage();
				widget.prevPage(id);
			}			
		},
		'updateModel' : function(model, data){
			//postpone updating "enable" and "visible" since selectedPage is set in Pages after updating buttons
			if(jQuery.isFunction(model._enable) || jQuery.isFunction(model._visible)){
				setTimeout(function(){
					if(jQuery.isFunction(model._enable))
						jQuery.vpmsBindViewModel.updateModelItem(model, "enable", model._enable());
					if(jQuery.isFunction(model._visible))
						jQuery.vpmsBindViewModel.updateModelItem(model, "visible", model._visible());
				}, 1);
			}
		}
};
jQuery.vpmsBindViewModel.roleBindings['default'] = {
		'enable' : function(model){
			return !$.vpmsBindViewModel.unwrapObservable(model,'disabled');
		},
		'click' : function(element, model, rootmodel, action){
			jQuery.vpmsBindViewModel.updateVPMSModel(jQuery(element).closest("." + $.vpmsBindViewModel.options.containerClass), true);
		}
};

//Pages for Tablet
$.vpmsBindViewModel.pushTemplate({
	name: 'pagesTablet',
	template: '\
		#if(data.isSplitView){#\
		<div data-role="splitview" id="#=data.tabletViewId()#">\
			<div data-role="pane" id="left-pane">\
		#}#\
			<div data-role="view" #if($.vpmsBindViewModel.useNativeScrolling){#data-use-native-scrolling="true"#}# id="#=data.rootViewId()#" #if(!data.isSplitView){#data-show="$.vpmsBindViewModel.kendoMobileViewShow"#}#>\
					<div data-role="header">\
						<div data-role="navbar">\
							<span data-bind="text:caption"></span>#if(data.navigateBack){#\
							<a class="nav-button km-back" data-role="button" data-align="left" data-bind="click:navigateBack"><span data-bind="text:backcaption"></span></a>#}##if(!data.isSplitView && data.logo){#\
							<img data-align="right" class="vpms-csc-logo" data-bind="attr:{src:logo}"/>\
						#}#</div>\
					</div>\
					<div data-role="content" class="km-insetcontent">\
						<div class="km-listview-wrapper"><ul class="vpms-listview vpms-listview-clickable km-listview km-list km-listinset" data-template="pagesTabletTabs" data-bind="source:groups, events:{' + $.vpmsBindViewModel.events.end + ':onSelectGroup}"></ul></div>\
					</div>\
					#if(data.buttons){#<div data-role="footer">\
						<div data-role="tabstrip" class="vpms-buttonbar-widget" data-bind="source:buttons" data-template="button">\
						</div>\
					</div>#}#\
					#if(!data.isSplitView){#\
					<div class="vpms-generic-view-container" data-template="pagesTabletSections" data-bind="source:groups, attr:{id:id}"></div>\
					#}#\
				</div>\
		#if(data.isSplitView){#\
			</div>\
			<div data-role="pane" id="right-pane" data-template="pagesTabletSections" data-initial="#=data.getSectionViewId(data.selectedPage)#" data-bind="source:groups"></div>\
		</div>\
		#}#\
	',
	initModel: function(model){
		if(model.container===undefined)
			model.container = true;
		if(!model.name) model.name = "pagesTablet";
		if(!model.id) model.id = model.name;
		if(!model.settings)
			model.settings = {};
		model.logo = $.vpmsBindViewModel.initModelItem(model.settings?model.settings.logo:undefined, undefined);
		model.isSplitView = !$.vpmsBindViewModel.isPhone && !model.settings.suppresstabs;
		model.caption = function(){
			return $.vpmsBindViewModel.getCaptionById(this, 'productCaption');
		};
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.captions.push($.vpmsBindViewModel.initModel_caption({name:'btn.back', caption:'Back'}));
		model.backcaption = function(){
			return $.vpmsBindViewModel.getCaptionById(this, 'btn.back');
		};
		model.cancelQuote = function(e){
			jQuery(e.target).vpmsBindViewModel('changeModel');
		};
		model.selectedPage = "";
		model.getSectionViewId = function(id){
			if(id)
				return "section_" + $.vpmsBindViewModel.alphanumericSelector(id);
			else
				return "";
		};		
		model.tabletViewId = function(){
			return "tablet_" + $.vpmsBindViewModel.alphanumericSelector(this.name);
		};		
		model.rootViewId = function(){
			return "root_" + $.vpmsBindViewModel.alphanumericSelector(this.name);
		};		
		model.sectionViewId = function(dataItem){
			var _group = (dataItem)?$.vpmsBindViewModel.getArrayElement(dataItem):this;
			return this.getSectionViewId($.vpmsBindViewModel.unwrapObservable(_group, "id"));
		};
		model.isSelected = function(dataItem){
			var _group = $.vpmsBindViewModel.getArrayElement(dataItem);
			return ($.vpmsBindViewModel.unwrapObservable(this, "selectedPage") == _group.id); 
		};
		model.tabIconClass = function(dataItem){
			if(dataItem){
				var _group = $.vpmsBindViewModel.getArrayElement(dataItem);				
				return ($.vpmsBindViewModel.unwrapObservable(_group, "error")?"km-icon km-warning":$.vpmsBindViewModel.unwrapObservable(_group, "visited")?"km-icon km-check":"km-icon km-notvisited");
			}
		};				
		model.selectedGroup = function(){
			var selectedId = $.vpmsBindViewModel.unwrapObservable(this, 'selectedPage');
			for(var i=0; i<this.groups.length; i++){
				if( $.vpmsBindViewModel.unwrapObservable(this.groups[i].wrapdata, 'id') == selectedId){
					return this.groups[i].wrapdata;
				}
			}
		};	
		model.nextPage = function(pageId){
			//setter
			if(pageId){
				this.selectGroupUpdate(pageId);
				return;
			}			
			var found = (this.selectedPage==this.id || !this.selectedPage);
			for(var i=0; i<this.groups.length; i++){
				var group = $.vpmsBindViewModel.getArrayElement(this.groups[i]);
				if(!found){
					found = (group.id == this.selectedPage);
				}else{
					if(group.relevant===undefined || group.relevant)
						return group.id;
				}
			}
		};
		if(!model.prevPage) model.prevPage = function(pageId){
			//setter
			if(pageId){
				this.selectGroupUpdate(pageId);
				return;
			}				
			var found = false;
			for(var i=this.groups.length-1; i>=0; i--){
				var group = $.vpmsBindViewModel.getArrayElement(this.groups[i]);
				if(!found){
					found = (group.id == this.selectedPage);
				}else{
					if(group.relevant===undefined || group.relevant)
						return group.id;
				}
			}
		};
		if((model.settings.suppresstabs && model.navigateBack) || ($.vpmsBindViewModel.isPhone && !model.settings.suppresstabs)){
			model.sectionsNavigateBack = function(event){
				if(this.settings.suppresstabs)
					this.navigateBack(event);
				else{
					$.vpmsBindViewModel.pushState(this.containerId, this.id, "", true);
					var $pane = $(event.target).closest('[data-role="pane"]');
					$.vpmsBindViewModel.changeView(this.rootViewId(), $pane, true);
					jQuery(event.target).vpmsBindViewModel('updateModel');
				}
			};
		}		
		model.selectGroup = function(id){
			if(this.selectedPage == id)
				return;
			$.vpmsBindViewModel.updateModelItem(this,'selectedPage', id);
			$.vpmsBindViewModel.pushState(this.containerId, this.id, id, true);
			var rootModel = $('#' + this.containerId).data('model');
			if(id && !rootModel.init){
				$.vpmsBindViewModel.changeView(this.getSectionViewId(id));
			}
		};
		model.selectGroupUpdate = function(id){
			//To prevent changing view if view change already has been requested
			if(!$.vpmsBindViewModel.startMobileChangeView())
				return;
			//To deffer changing of a view if autocommit field being processed
			if($.vpmsBindViewModel.isModelUpdating('#' + this.containerId)){
				this.selectGroupUpdateRequest = id;
				return;
			}
			if(!this.syncUpdate){
				this.selectGroup(id);
			}else{
				var group, i;
				for(i=0; i<this.groups.length; i++)
					if(this.groups[i].wrapdata.id == id){
						group = this.groups[i].wrapdata;
						break;
					}
				//check if there are elements or groups to restore, restore them else act as async update
				if(group && (group._elements || group._groups)){
					var rootModel = $('#' + this.containerId).data('model');
					$.vpmsBindViewModel.blockUI(rootModel);
					$.vpmsBindViewModel.debug("syncUpdate - start bind cached elements", true);
					var elementsHasMutated=false, groupsHasMutated=false;
					if(group._elements){
						for(i=0; i<group._elements.length; i++)
							group.elements.push(group._elements[i]);
						group._elements = undefined;
						elementsHasMutated = true;
					}
					if(group._groups){
						for(i=0; i<group._groups.length; i++)
							group.groups.push(group._groups[i]);
						group._groups = undefined;
						groupsHasMutated = true;
					}
					this.requestedPage = id;
					$('#' + rootModel.containerId).find('#' + this.getSectionViewId(id)).find('[data-role="content"]').addClass('vpms-afterRender');
					$.vpmsBindViewModel.debug("syncUpdate - end bind cached elements");
				}else{
					this.selectGroup(id);
				}
			}
			jQuery('#' + this.containerId).vpmsBindViewModel('updateModel');
		};
		model.onSelectGroup = function(e){
			var $li =  jQuery(e.target).closest('li');
			if($li.data('vpmsSelectStart')===true){
				var id = $li.attr('id');
				if(id){
					this.selectGroupUpdate(id);
				}
			}
			$li.data('vpmsSelectStart', false);
		};
		//buttons : find buttonBar widget, elements - buttons for tabstrip, remove next and previous
		if(model.elements){
			for(var i=0; i<model.elements.length; i++){
				if(model.elements[i].widget=="buttonbar"){
					for(var j=model.elements[i].elements.length-1; j>=0; j--){
						model.elements[i].elements[j].barButton = true;
					}
					model.buttons=function(){
						for(var i=0; i<this.elements.length; i++){
							if(this.elements[i].wrapdata.widget=="buttonbar"){
								if(this.get)
									return this.get("elements[" +i +"].wrapdata.elements");
								else
									return this.elements[i].wrapdata.elements;
							}
						}
						return [];
					};
					break;
				}
			}
		}
	},
	beforeUpdateModel: function(model, data){
		if(data){
			var resetSelectedPage = false;
			var i, k, group;
			//if selected Page comes from server - set it 
			if(!data.sp){
				if(model.requestedPage){
					data.sp = model.requestedPage;
				}else{
					//get selected tab from state
					var tab = $.vpmsBindViewModel.getState(model.containerId, model.id);
					if(tab!==undefined && tab!=="")
						data.sp = tab;
				}
			}
			model.requestedPage = undefined;
			//if data.sp is set - check if this page will be relevant after update
			if(data.sp){
				for(i=0; i<data.g_.length; i++){
					if(data.g_[i].id == data.sp){
						//findout group
						group=undefined;
						for(k=0; k<model.groups.length; k++){
							var g = $.vpmsBindViewModel.getArrayElement(model.groups[k]);
							if(g.name == data.g_[i]._n){
								group = g;
								break;
							}
						}
						if(group){
							if(group.relevant!==undefined)
								$.vpmsBindViewModel.updateModelItem(group, 'relevant', (data.g_[i]._r!==undefined?data.g_[i]._r:true));
							if(group.relevant!==undefined && group.relevant===false)
								data.sp=undefined;
						}else{
							if(!(data.g_[i]._r===undefined || data.g_[i]._r===true))
								data.sp=undefined;
						}
						break;
					}
				}
				//reset selected page
				resetSelectedPage = (data.sp===undefined);
			}			
			//if data.sp still not set take first relevant, but not for the Phone!!!
			i=0;		
			if(data.g_ && !data.sp && (!$.vpmsBindViewModel.isPhone || model.settings.suppresstabs)){
				while(i<data.g_.length && !data.sp){
					if(model.multiple){
						if(data.g_[i]._r===undefined || data.g_[i]._r===true)
							data.sp = data.g_[i].id;
					}else{
						for(k=0; k<model.groups.length; k++){
							group = $.vpmsBindViewModel.getArrayElement(model.groups[k]);
							if(group.name==data.g_[i]._n){
								if(group.relevant!==undefined)
									$.vpmsBindViewModel.updateModelItem(group, 'relevant', (data.g_[i]._r!==undefined?data.g_[i]._r:true));
								if(group.relevant===undefined || group.relevant===true)
									data.sp = data.g_[i].id;
								break;
							}
						}
					}
					i++;
				}
			}
			//reset selected page if data.sp was not relevant, but do this only once
			if(model.resetSelectedPage)
				model.resetSelectedPage = undefined;
			else
				if(data.sp && resetSelectedPage){
					model.resetSelectedPage = true;
					var _sp = data.sp;
					data.sp = undefined;
					setTimeout(function(){
						model.selectGroupUpdate(_sp);
					}, 1);
				}
			
			//syncUpdate on init - clear content and cache elements for not selected
			if(model.syncUpdate && !model.syncUpdateInited){ 
				for(i=0; i<data.g_.length; i++){
					if(data.sp!=data.g_[i].id){
						//cash all groups content for not selected page
						for(k=0; k<model.groups.length; k++){
							group = $.vpmsBindViewModel.getArrayElement(model.groups[k]);
							if(group.name==data.g_[i]._n){
								if(group.elements.length>0)
									group._elements = group.elements.splice(0, group.elements.length);
								if(group.groups.length>0)
									group._groups = group.groups.splice(0, group.groups.length);
								break;
							}
						}
						//clear all content for not selected page
						data.g_[i].k_=undefined;
						data.g_[i].e_=undefined;
						data.g_[i].g_=undefined;
					}
				}
				model.syncUpdateInited = true;
			}
			
			if(model.settings.suppresstabs && data.sp){
				model.initialViewId = model.getSectionViewId(data.sp);
			}
		}
	},
	updateModel: function(model, data){
		model.selectGroup(data.sp?data.sp:"");
		if(!model.setAllGroupsData)
			model.prevSelectedGroup = model.selectedGroup();
		if(model.selectGroupUpdateRequest){
			var _sp = model.selectGroupUpdateRequest;
			model.selectGroupUpdateRequest = undefined;
			setTimeout(function(){
				model.selectGroupUpdate(_sp);
			}, 1);
		}
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'pagesTabletTabs',
	template: '\
		<li class="vpms-pages-tab vpms-selectable-tab" data-bind="visible:wrapdata.relevant, attr:{id:wrapdata.id, relevant:wrapdata.relevant}, css: {km-state-active: isSelected}"><a class="km-listview-link">\
			<span data-bind="attr:{class:tabIconClass}"></span>\
			<span data-bind="text:wrapdata.caption"></span>&nbsp;\
		</a></li>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'pagesTabletSections',
	template: '\
	<div data-role="view" #if($.vpmsBindViewModel.useNativeScrolling){#data-use-native-scrolling="true"#}# class="#if(!$.vpmsBindViewModel.getParentWidget(data, "pagesTablet").isSplitView){#vpms-afterRender#}#" data-bind="attr:{id:sectionViewId}" data-init="$.vpmsBindViewModel.kendoMobileViewInit" data-show="$.vpmsBindViewModel.kendoMobileViewShow">\
		<div data-role="header">\
			<div data-role="navbar">\
				<span data-bind="text:wrapdata.caption"></span>&nbsp;#if($.vpmsBindViewModel.hasParentWidgetItem(data, "pagesTablet", "sectionsNavigateBack")){#\
				<a class="nav-button km-back" data-align="left" data-role="button" data-bind="click:sectionsNavigateBack"><span data-bind="text:backcaption"></span></a>#}##if($.vpmsBindViewModel.hasParentWidgetItem(data, "pagesTablet", "logo")){#\
				<img data-align="right" class="vpms-csc-logo" data-bind="attr:{src:logo}"/>\
			#}#</div>\
		</div>\
		<div data-role="content" class="km-insetcontent">\
			<div class="vpms-pages-group-error ui-state-error" data-bind="visible:wrapdata.error, text:wrapdata.error"></div>\
			<div class="vpms-#=wrapdata.widget#-widget #if(data.wrapdata.settings &&  data.wrapdata.settings.css){# #=data.wrapdata.settings.css# #}#"   data-template="${wrapdata.widget}" data-bind="source: wrapdata"></div>\
		</div>\
		#if(!$.vpmsBindViewModel.getParentWidget(data, "pagesTablet").isSplitView && $.vpmsBindViewModel.hasParentWidgetItem(data, "pagesTablet", "buttons")){#<div data-role="footer">\
		<div data-role="tabstrip" class="vpms-buttonbar-widget" data-bind="source:buttons" data-template="button"></div>\
		</div>#}#\
	</div>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'superWidget',
	template: '\
		#if(data.isListview){#\
		<ul class="km-listview km-list vpms-listview km-listinset vpms-elements-container" data-template="listElement" data-bind="source: elements"></ul>\
		#}else{#\
		<div class="vpms-elements-container#if(data.settings && data.settings.colNumber){# vpms-#=data.settings.colNumber#-col#}#" data-template="element" data-bind="source: elements"></div>\
		#}#\
	',
	initModel: function(model){
		if(model.container===undefined)
			model.container = true;
		if(!model.settings)
			model.settings = {};
		if(!model.name) model.name = "superWidget";
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
	}
});	

$.vpmsBindViewModel.pushTemplate({
	name: 'border',
	template: '\
		<ul class="km-listview km-listgroupinset">\
			<li>\
				<div class="km-group-title vpms-border-title">\
					<span class="ui-state-required" data-bind="visible:required">&bull;</span>\
					<span data-bind="text:caption"></span>\
				</div>\
				<div class="vpms-border-group-error ui-state-error" data-bind="visible:wrapdata.error, text:wrapdata.error"></div>\
				#if(data.isListview){#\
				<ul class="km-list vpms-listview vpms-elements-container" data-template="listElement" data-bind="source: elements"></ul>\
				#}else{#\
				<div class="vpms-elements-container" data-template="element" data-bind="source: elements"></div>\
				#}#\
			</li>\
		</ul>\
	',
	initModel: function(model){
		model.hasLabel = false;
		if(model.isGroup===undefined)
			model.isGroup = true;
		if(!model.isGroup && model.isElement===undefined)
			model.isElement = true;
		if(model.container===undefined)
			model.container = true;
		if(!model.name) model.name = "border";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'label',
	template: '\
		<label class="vpms-label" data-bind="attr:{for:id}">\
			#if(data.labeled){#<span data-bind="text:caption"></span>&nbsp;#}#\
			<span class="ui-state-required" data-bind="visible:required">&bull;</span>\
		</label>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(!model.name) model.name = "label";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'value',
	template: '\
		<div>\
			<div class="vpms-value-widget-value #if(data.format){##=data.format##}#" data-bind="attr:{id:id}"><div data-bind="text:displayValue"></div></div>\
			<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
		</div>',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(model.disabled===undefined)
			model._disabled = true;
		if(!model.name) model.name = "value";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, true);
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.displayValue = $.vpmsBindViewModel.initModelItem(model.displayValue, "");
	},
	updateModel: function(model, data){
		$.vpmsBindViewModel.updateModelItem(model, 'displayValue', $.vpmsBindViewModel.readFormattedValue(model, $.vpmsBindViewModel.unwrapObservable(model, 'value')));
	}
});

//text widget - defaults: isElement=false
$.vpmsBindViewModel.pushTemplate({
	name: 'text',
	template: '<div class="vpms-text-widget-text" data-bind="attr:{id:id}"><div data-bind="text:displayValue"></div></div>',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = false;
		if(!model.name) model.name = "text";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.text = $.vpmsBindViewModel.initModelItem(model.text, "");
		model.displayValue = $.vpmsBindViewModel.initModelItem($.vpmsBindViewModel.unwrapObservable(model, 'text'), "");
	},
	updateModel: function(model, data){
		if(model.isElement)
			$.vpmsBindViewModel.updateModelItem(model, 'displayValue', $.vpmsBindViewModel.unwrapObservable(model, 'caption'));
	}	
});

$.vpmsBindViewModel.pushTemplate({
	name: 'edit',
	template: '\
	<span>\
		<input #if(data.settings){# settings="#=JSON.stringify(data.settings)#"#}# #if(data.settings && data.settings.mask){# mask="#=data.settings.mask#"#}# #if(data.settings && data.settings.maxlength){# maxlength="#=data.settings.maxlength#"#}# type="#=format#" class="#if(data.autocommit){#vpms-autocommit#}#" data-bind="events:{change:onChange}, attr:{id:id, name:id}, disabled:disabled, value:editableValue" />\
		<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
	</span>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(!model.name) model.name = "edit";
		model.originalFormat = model.format;
		if(!model.format){
			if(model.settings && model.settings.mask) 
				model.format = "text";
			else if(model.settings && model.settings.inputType)
				model.format = model.settings.inputType;
		} 
		if(model.format=="number")
			model.format="tel";
		if(!model.format)
			model.format = "text";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.editableValue = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.displayValue = $.vpmsBindViewModel.initModelItem(model.displayValue, "");
		model.onChange = function(e){
			if(this._onChangeStarted)
				return;
			this._onChangeStarted = true;
			try{
				if(this.format && this.format.indexOf("date")>=0){
					var date = $.vpmsBindViewModel.StringToDate($.vpmsBindViewModel.unwrapObservable(this, 'editableValue'));
					if(!date)
						date = "";
					$.vpmsBindViewModel.updateModelItem(this, 'value',  date);
				}else{
					if(!(this.settings && this.settings.mask) && this.originalFormat == "number" && jQuery.numbers){
						var value = jQuery.numbers.toNumber($.vpmsBindViewModel.unwrapObservable(this, 'editableValue'), this.numberSettings);
						$.vpmsBindViewModel.updateModelItem(this, 'editableValue', jQuery.numbers.toString(value, this.numberSettings));
						$.vpmsBindViewModel.updateModelItem(this, 'value', value);
					}else
						$.vpmsBindViewModel.updateModelItem(this, 'value',  $.vpmsBindViewModel.unwrapObservable(this, 'editableValue'));
				}
				if($.vpmsBindViewModel.onModelValueChange)
					$.vpmsBindViewModel.onModelValueChange(e.data, e);
			}finally{
				this._onChangeStarted = false;
			}
		};
	},
	updateModel: function(model, data, settings){
		var value = $.vpmsBindViewModel.unwrapObservable(model, 'value');
		if(value instanceof Date)
			$.vpmsBindViewModel.updateModelItem(model, 'editableValue', $.vpmsBindViewModel.DateToString(value));
		else if(!(model.settings && model.settings.mask) && model.originalFormat=="number" && jQuery.numbers){
			model.numberSettings = $.vpmsBindViewModel.getNumbersOptions(model.settings, settings);
			$.vpmsBindViewModel.updateModelItem(model, 'editableValue', jQuery.numbers.toString(value, model.numberSettings));
		}
		else
			$.vpmsBindViewModel.updateModelItem(model, 'editableValue', value);
		$.vpmsBindViewModel.updateModelItem(model, 'displayValue', $.vpmsBindViewModel.readFormattedValue(model, $.vpmsBindViewModel.unwrapObservable(model, 'value')));
	}
});

//Memo widget - defaults isElement=true
$.vpmsBindViewModel.pushTemplate({
	name: 'memo',
	template: '\
		<span>\
			<textarea class="#if(data.autocommit){#vpms-autocommit#}#" data-bind="events:{change:onChange}, attr:{id:id, name:id, rows:rows}, disabled:disabled, value:value" />\
			<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
		</span>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(model.rows===undefined)
			model.rows = 2;
		if(!model.name) model.name = "memo";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(e.data, e);
		};
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'combobox',
	template: '\
		<span>\
			<select class="#if(data.autocommit){#vpms-autocommit#}#" data-value-field="_v" data-text-field="_x" data-bind="events:{change:onChange}, attr:{id:id, name:id}, source:choices, value:value, disabled:disabled"></select>\
			<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
		</span>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(model.size===undefined)
			model.size = 1;
		if(!model.name) model.name = "combobox";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(e.data, e);
		};
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'checkbox',
	template: '\
		<span class="vpms-checkbox-container" data-bind="attr:{_disabled:disabled}">\
			<input class="#if(data.autocommit){#vpms-autocommit#}#" data-role="switch" data-on-label="YES" data-off-label="NO" data-bind="events:{change:onChange}, checked:checked, disabled:disabled" />\
			<label class="vpms-label" data-bind="text:caption, attr:{for:id, _disabled:disabled}"></label>&nbsp;\
			<div class="ui-state-error" data-bind="attr:{writeDependent:writeDependent}, visible:error, text:error"></div>\
		</span>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		model.labeled = false; //label will be built in widget itself
		model.format = "boolean";
		if(!model.name) model.name = "checkbox";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "0");
		model.checked = $.vpmsBindViewModel.initModelItem(model.checked, (model.value=="1"));
		model.writeDependent = function(){
			if($.vpmsBindViewModel.unwrapObservable(this, "checked")===true)
				$.vpmsBindViewModel.updateModelItem(this, "value", "1");
			else
				$.vpmsBindViewModel.updateModelItem(this, "value", "0");
		};
		if(model.generateDisplayValue){
			model.displayValue = function(){
				if($.vpmsBindViewModel.unwrapObservable(this, 'checked'))
					return $.vpmsBindViewModel.unwrapObservable($.vpmsBindViewModel.element(this.captions, 'text.yes'), 'caption');
				else
					return $.vpmsBindViewModel.unwrapObservable($.vpmsBindViewModel.element(this.captions, 'text.no'), 'caption');
			};
		}
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(e.data, e);
		};
	},
	updateModel: function(model, data, settings){
		$.vpmsBindViewModel.updateModelItem(model, 'checked', ($.vpmsBindViewModel.unwrapObservable(model, 'value')=="1"));
	}
});


$.vpmsBindViewModel.pushTemplate({
	name: 'radiogroupElement',
	template: '\
		<li><label class="km-listview-label vpms-label">\
			<span data-bind="text:_x"></span>\
			<input class="km-widget km-icon km-check #if(parent().parent().autocommit){#vpms-autocommit#}#" #if(data._v==data.parent().parent().value){#checked#}# type="radio" name="#=parent().parent().id#" value="#=_v#" data-bind="events:{change:onChange}, checked:value"/>\
		</label></li>\
	'
});

//TODO Error: wrapdata is not defined? checked binding
$.vpmsBindViewModel.pushTemplate({
	name: 'radiogroup',
	template: '\
		<span>\
			<ul class="km-list km-listview km-listinset" data-template="radiogroupElement" data-bind="source: choices"></ul>\
			<div class="ui-state-error" data-bind="visible:error, text:error"></div>\
		</span>\
	',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = true;
		if(!model.name) model.name = "radiogroup";
		if(!model.settings)
			model.settings = {};
		if(model.settings.colNumber===undefined)
			model.settings.colNumber = 'no';
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.autocommit = $.vpmsBindViewModel.initModelItem(model.autocommit, false);
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.onChange = function(e){
			if($.vpmsBindViewModel.onModelValueChange)
				$.vpmsBindViewModel.onModelValueChange(e.data.parent().parent(), e);
		};
	}
});

//accordionElement widget  - defaults container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'accordionElement',
	template: '\
		<li class="vpms-accordion-item vpms-afterRender" data-bind="visible:wrapdata.relevant, attr:{id:wrapdata.id, relevant:wrapdata.relevant}">\
			#if(!parent().parent().settings.nocaption){#<a class="vpms-accordion-element-header#if(!parent().parent().settings.noaccordion){# km-listview-link#}#">\
				<span class="km-icon km-warning" data-bind="visible:wrapdata.error"></span>\
				<span class="ui-state-required" data-bind="visible:wrapdata.required">&bull;</span>\
				<span data-bind="text:wrapdata.caption"></span>&nbsp;\
			</a>#}#\
			<div class="vpms-accordion-element-content">\
				<div class="vpms-accordion-group-error ui-state-error" data-bind="visible:wrapdata.error, text:wrapdata.error"></div>\
				#if(data.wrapdata.widget=="superWidget"){\
					if(data.wrapdata.isListview){#\
						<ul class="km-listview km-list km-listinset vpms-listview vpms-elements-container" data-template="listElement" data-bind="source: wrapdata.elements"></ul>\
					#}else{#\
						<div class="vpms-elements-container" data-template="element" data-bind="source: wrapdata.elements"></div>\
					#}#\
				#}else{#\
					<ul class="km-list vpms-${wrapdata.widget}-widget" data-bind="source:wrapdata" data-template="${wrapdata.widget}"></ul>\
				#}#\
				#if(parent().parent().multiple){#\
				<div class="vpms-remove-button" data-bind="visible:mutable">\
					<a class="vpms-button-widget-button" data-role="button" id="remove" data-bind="enabled:wrapdata.canRemove, click:parent().parent().removeClick"><span class="km-text" data-bind="text:wrapdata.removeCaption"></span></a>\
				</div>\
				#}#\
			</div>\
		</li>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'accordion',
	template: '\
		<div class="vpms-accordion-container" data-bind="attr:{id:id}, visible:relevant">\
			<ul class="#if(!data.settings.noaccordion){#vpms-accordion vpms-listview-clickable #}#vpms-listview km-listview km-list km-listinset" data-template="accordionElement" data-bind="source:groups"></ul>\
			#if(data.multiple){#\
				<div class="vpms-add-button" data-bind="visible:mutable">\
					<a class="vpms-button-widget-button" data-role="button" id="add" data-bind="enabled:canAdd, click:addClick"><span class="km-text" data-bind="text:addCaption"></span></a>\
				</div>\
			#}#\
		</div>\
	',
	initModel: function(model){
		if(!model.name) model.name = "accordion";
		//accordion has no selected Page - get and update all groups
		model.getAllGroupsData = true;
		model.setAllGroupsData = true;
		if(model.container===undefined)
			model.container = true;
		if(!model.settings) model.settings={};
		if(model.settings.noaccordion===undefined)
			model.settings.noaccordion = false;
		if(model.settings.nocaption===undefined || !model.settings.noaccordion)
			model.settings.nocaption = false;
		
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.mutable = function(){
			return !$.vpmsBindViewModel.unwrapObservable(this,'disabled') && !$.vpmsBindViewModel.unwrapObservable(this, 'staticPageCount');
		};
		if(model.multiple){
			if(!model.addClick) model.addClick = function(event){
				if(event.data.canAdd)
					jQuery(event.target).vpmsBindViewModel('addToGroup', event.data);
			};
			
			if(!model.removeClick) model.removeClick = function(event){
				if(event.data.wrapdata.canRemove)
					jQuery(event.target).vpmsBindViewModel('removeFromGroup', event.data.parent().parent(), event.data.wrapdata);
			};
		}
	},
	updateModel: function(model, data, settings){
		if(model.trigger)
			model.trigger("change",{field: "mutable"});
		//set active - data.sp or if nothing selected select first visible
		if(!model.noaccordion)
			setTimeout(function(){
				var $ul = $('#' + model.containerId).find('.vpms-accordion-container#' + model.id).children('ul');
				if($ul.length>0){
					var accordion = $ul.data("accordion");
					if(accordion){
						var $li;
						if(data.sp){
							$li = $ul.children('li#' + data.sp);
							if($li.length>0 && $li.is(':visible'))
								accordion.select($li);
						}
						if($ul.children('li.active').length===0){
							$li = $ul.children('li[relevant="true"]');
							if($li.length>0)
								accordion.select($li[0]);
						}
					}
				}
			}, 1);
	}
});
$.vpmsBindViewModel.pushTemplate({
	name: 'pages',
	template: '\
		<div data-bind="attr:{id:id, vpmsgenericviewname:id}, visible:relevant">\
			<div class="km-listview-wrapper">\
				<ul class="vpms-listview vpms-listview-clickable km-listview km-list km-listinset" data-template="pagesTabs" data-bind="source:groups, events:{' + $.vpmsBindViewModel.events.end + ':onSelectPagesGroup}, attr:{id:id}"></ul>\
			</div>\
			#if(data.multiple){#\
			<div class="vpms-add-button" data-bind="visible:canAdd">\
				<a class="vpms-button-widget-button" data-role="button" id="add" data-bind="click:addClick"><span class="km-text" data-bind="text:addCaption"></span></a>\
			</div>\
			#}#\
			<div class="vpms-pages-sections vpms-generic-view-container" data-template="pagesSections" data-bind="source:groups, attr:{id:genericContainerId}"></div>\
		</div>\
	',
	initModel: function(model){
		if(!model.name) model.name = "pages";
		//pages has no selected Page - get and update all groups
		if(model.onlyRelevant === undefined && model.getAllGroupsData)
			model.onlyRelevant = true;		
		if(model.container===undefined)
			model.container = true;
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		model.captions.push($.vpmsBindViewModel.initModel_caption({name:'btn.back', caption:'Back'}));
		model.backcaption = function(){
			return $.vpmsBindViewModel.getCaptionById(this, 'btn.back');
		};	
		model.mutable = function(){
			return !$.vpmsBindViewModel.unwrapObservable(this,'disabled') && !$.vpmsBindViewModel.unwrapObservable(this, 'staticPageCount');
		};
		model.selectedPage = "";
		model.getSectionViewId = function(id){
			return "pagesSection_" + $.vpmsBindViewModel.alphanumericSelector(id);
		};		
		model.sectionViewId = function(dataItem){
			var _group = (dataItem)?$.vpmsBindViewModel.getArrayElement(dataItem):this;
			return this.getSectionViewId($.vpmsBindViewModel.unwrapObservable(_group, "id"));
		};
		model.genericContainerId = function(){
			return "generic_" + $.vpmsBindViewModel.unwrapObservable(this, "id");
		};		
		if(!model.selectedGroup) model.selectedGroup = function () {
			for(var i=0; i<this.groups.length; i++){
				if(this.groups[i].wrapdata.id == this.selectedPage){
					return this.groups[i].wrapdata;
				}
			}
		};
		model.navigateBackBeforeUpdate = function(event){
			if(!$.vpmsBindViewModel.startMobileChangeView())
				return;
			var viewId= jQuery('[vpmsgenericviewname="' + this.id + '"]').closest('[data-role="view"]').attr("id");
			this.selectGroup("");
			//$.vpmsBindViewModel.pushState(this.containerId, this.id, "", true);
			var $pane = $(event.target).closest('[data-role="pane"]');
			$.vpmsBindViewModel.changeView(viewId, $pane, true);
			this.ignoreSelectedPageFromServer = true;
		};
		model.navigateBack = function(event){
			this.navigateBackBeforeUpdate(event);
			$.vpmsBindViewModel.updateVPMSModel(jQuery('#' + this.containerId), true);
		};
		if(model.multiple){
			if(!model.addClick) model.addClick = function(event){
				if(event.data.canAdd)
					jQuery(event.target).vpmsBindViewModel('addToGroup', event.data);
			};
			
			if(!model.removeClick) model.removeClick = function(event){
				var parentModel = $.vpmsBindViewModel.getArrayElement(event.data.parent().parent());
				var group = $.vpmsBindViewModel.getArrayElement(event.data.wrapdata);
				if(group.canRemove){
				parentModel.navigateBackBeforeUpdate(event);
					jQuery(event.target).vpmsBindViewModel('removeFromGroup', parentModel, group);
				}
			};
		}
		model.beforeGroupRemove = function(group){
			$.vpmsBindViewModel.kendoMobileRestoreGenericViews(jQuery('[id="'+ this.genericContainerId() +'"].vpms-generic-view-container'));
		};
		model.tabIconClass = function(dataItem){
			if(dataItem){
				var _group = $.vpmsBindViewModel.getArrayElement(dataItem);				
				return ($.vpmsBindViewModel.unwrapObservable(_group, "error")?"km-icon km-warning":($.vpmsBindViewModel.unwrapObservable(_group, "visited")?"km-icon km-check":"km-icon km-notvisited"));
			}
		};		
		model.selectGroup = function(id, ignoreSelectedPage){
			if(ignoreSelectedPage || id != $.vpmsBindViewModel.unwrapObservable(this, 'selectedPage')){
				$.vpmsBindViewModel.updateModelItem(this, 'selectedPage', id);
				$.vpmsBindViewModel.pushState(this.containerId, this.id, id, true);
				if(id){
					var viewId = this.getSectionViewId(id);
					setTimeout(function(){ 
						$.vpmsBindViewModel.changeView(viewId); 
					}, 1);
				}
			}
		};
		model.onSelectPagesGroup = function(e){
			var $li =  jQuery(e.target).closest('li');
			if($li.data('vpmsSelectStart')===true){
				var id = $li.attr('id');
				if(id){
					if(!$.vpmsBindViewModel.startMobileChangeView())
						return;
					this.requestedPage = id;
					jQuery.vpmsBindViewModel.updateVPMSModel($('#' + this.containerId), true);
				}
			}
			$li.data('vpmsSelectStart', false);
		};
	},
	updateModel: function(model, data){
		if(model.requestedPage){
			model.selectGroup(model.requestedPage, true);
			model.requestedPage=undefined;
		}else{
			var tab = $.vpmsBindViewModel.getState(model.containerId, model.id);
			model.selectGroup(model.ignoreSelectedPageFromServer?"":data.sp?data.sp:tab?tab:"");
			model.ignoreSelectedPageFromServer = undefined;
		}
		if(!model.setAllGroupsData)
			model.prevSelectedGroup = model.selectedGroup();
		if(model.trigger)
			model.trigger("change",{field: "mutable"});
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'pagesTabs',
	template: '\
		<li class="vpms-pages-tab" data-bind="visible:wrapdata.relevant, attr:{id:wrapdata.id}"><a class="km-listview-link">\
			<span data-bind="attr:{class:tabIconClass}"></span>\
			<span class="vpms-pages-title" data-bind="text:wrapdata.caption"></span>&nbsp;\
		</a></li>'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'pagesSections',
	template: '\
		<div data-role="view" #if($.vpmsBindViewModel.useNativeScrolling){#data-use-native-scrolling="true"#}# class="vpms-afterRender" data-bind="attr:{id:sectionViewId, parent:genericContainerId}" data-init="$.vpmsBindViewModel.kendoMobileViewInit" data-show="$.vpmsBindViewModel.kendoMobileViewShow">\
			<div data-role="header">\
				<div data-role="navbar">\
					<a class="nav-button km-back" data-align="left" data-role="button" data-bind="click:navigateBack"><span data-bind="text:backcaption"></span></a>\
					<span data-bind="text:wrapdata.caption"></span>&nbsp;\
				</div>\
			</div>\
			<div data-role="content" class="km-insetcontent">\
				<div class="vpms-pages-group-error ui-state-error" data-bind="visible:wrapdata.error, text:wrapdata.error"></div>\
				<div class="vpms-#=wrapdata.widget#-widget #if(data.wrapdata.settings &&  data.wrapdata.settings.css){# #=data.wrapdata.settings.css# #}#"   data-template="${wrapdata.widget}" data-bind="source: wrapdata"></div>	\
				#if(parent().parent().multiple){#\
				<div class="vpms-remove-button" data-bind="visible:wrapdata.canRemove">\
					<a class="vpms-button-widget-button" data-role="button" id="remove" data-bind="click:parent().parent().removeClick"><span class="km-text" data-bind="text:wrapdata.removeCaption"></span></a>\
				</div>\
				#}#\
				<div>&nbsp;</div>\
			</div>\
		</div>\
	'
});
//table widget
$.vpmsBindViewModel.pushTemplate({
	name: 'tableCell',
	template: '\
		<td #if(data.wrapdata.span){#span="#=data.wrapdata.span#"#}# #if(data.wrapdata.removeColumn){#class="vpms-table-remove-column" data-bind="visible:mutable"#}#>\
			#if(data.wrapdata.removeColumn){#\
			<div data-bind="visible:mutable, events:{'+ $.vpmsBindViewModel.events.end +':removeClick}">\
				<span class="km-icon km-trash ui-state-required" data-bind="attr:{enabled:canRemove}"></span>\
			</div>#}else{#\
			#if($.vpmsBindViewModel.templateExists(data.wrapdata.widget)){#\
				<div class="vpms-#=data.wrapdata.widget#-widget #if(data.wrapdata.settings &&  data.wrapdata.settings.css){# #=data.wrapdata.settings.css# #}# #if(data.wrapdata.settings && data.wrapdata.settings.align){#vpms-align-#=data.wrapdata.settings.align##}#" data-template="#=data.wrapdata.widget#" data-bind="visible:wrapdata.relevant, source: wrapdata"></div>\
			#}}#\
		</td>\
	'
});

//table widget  - defaults container=true
$.vpmsBindViewModel.pushTemplate({
	name: 'tableRow',
	template: '<tr class="vpms-afterRender" data-bind="attr: {id:wrapdata.id}, visible:wrapdata.relevant, source:wrapdata.elements" data-template="tableCell"></tr>\
	'
});

$.vpmsBindViewModel.pushTemplate({
	name: 'table',
	template: '\
	<div>\
		#if(data.navigation){#\
		<div class="vpms-table-navigation" data-bind="visible:navigation.visible">\
			<a class="vpms-button-widget-button" data-role="button" id="prev" data-icon="previous" data-bind="enabled:navigation.hasPrev, click:navigationPrevPage"><span class="km-text"></span></a>\
			<span class="vpms-table-navigation-label" data-bind="text:navigation.label"></span>\
			<a class="vpms-button-widget-button" data-role="button" id="next" data-icon="next" data-bind="enabled:navigation.hasNext, click:navigationNextPage"><span class="km-text"></span></a>\
		</div>#}#\
		<table class="vpms-table-widget #=data.settings.contentClass#" data-bind="attr:{id:id}">\
			#if(!data.settings.noheader){#\
			<thead>\
				<tr>\
					#for(var i=0; i<data.settings.colSettings.length; i++){\
						if(data.settings.colSettings[i].removeColumn){#\
							<th class="vpms-table-remove-column" data-bind="visible:mutable"></th>\
						#}else{#\
							<th class="#if(data.settings.colSettings[i].settings &&  data.settings.colSettings[i].settings.css){# #=data.settings.colSettings[i].settings.css# #}#" style="#if(data.settings.colSettings[i].settings &&  data.settings.colSettings[i].settings.labelAlign){#text-align:#=data.settings.colSettings[i].settings.labelAlign#;#}##if(data.settings.colSettings[i].settings &&  data.settings.colSettings[i].width){#width:#=data.settings.colSettings[i].settings.width#;#}#">\
								<span data-bind="text:settings.colSettings[#=i#].caption"></span>\
								<span class="ui-state-required" data-bind="visible:settings.colSettings[#=i#].required">&bull;</span>\
							</th>\
					#}}#\
				</tr>\
			</thead>\
			#}#\
			<tbody data-template="tableRow" data-bind="source:groups"></tbody>\
		</table>\
		#if(data.multiple){#\
		<div class="vpms-add-button" data-bind="visible:mutable">\
			<a class="vpms-button-widget-button" data-role="button" id="add" data-bind="enabled:canAdd, click:addClick"><span class="km-text" data-bind="text:addCaption"></span></a>\
		</div>\
		#}#\
	</div>\
	',
	initModel: function(model){
		if(!model.name) model.name = "table";
		//table has no selected Page - get and update all groups
		model.getAllGroupsData = true;
		if(model.onlyRelevant === undefined && model.getAllGroupsData)
			model.onlyRelevant = true;				
		if(!model.readonly)
			model.setAllGroupsData = true;
		if(model.container===undefined)
			model.container = true;
		if(!model.settings)
			model.settings={};
		if(model.settings.noborder===undefined)
			model.settings.noborder = false;
		if(model.settings.contentClass===undefined)
			model.settings.contentClass=model.settings.noborder?"":"vpms-table-bordered";
		if(model.settings.noheader===undefined)
			model.settings.noheader = false;		
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.groups = $.vpmsBindViewModel.initModelItem(model.groups, []);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []); 
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);
		//init columns from template
		if(!model.settings)
			model.settings = {};
		if(!model.settings.colSettings)
			model.settings.colSettings = [];
		//colNames in model root to get column captions from VPMS
		model.colNames = [];
		model.initColumn = function(element, index){
			var colModel = {
				settings: element.settings?$.vpmsBindViewModel.cloneObject(element.settings):{},
				caption : $.vpmsBindViewModel.initModelItem(undefined, element.name),
				required: $.vpmsBindViewModel.initModelItem(undefined, false),
				removeColumn: element.removeColumn 
			};
			if(this.settings.colSettings.length>index)
				this.settings.colSettings[index] = colModel;
			else
				this.settings.colSettings.push(colModel);
			this.colNames.push(element.name);
		};
		model.mutable = function(){
			return !$.vpmsBindViewModel.unwrapObservable(this,'disabled') && !$.vpmsBindViewModel.unwrapObservable(this, 'staticPageCount');
		};
		var i;
		if(model.multiple){
			if(model._template && model._template.elements)
				model._template.elements.push({
					name:'removeColumn',
					removeColumn: true,
					isElement: false,
					isGroup: false
				});
			//if model is multiple - get column captions and settings from template
			var template = $.vpmsBindViewModel.getGroupTemplate(model);
			if(template)
				for(i=0; i<template.elements.length; i++)
					model.initColumn(template.elements[i], i);
			if(!model.addClick) model.addClick = function(event){
				if(event.data.canAdd)
					jQuery(event.target).vpmsBindViewModel('addToGroup', event.data);
			};
			
			if(!model.removeClick) model.removeClick = function(event){
				var group = event.data.parent().parent();
				if(group.canRemove)
					jQuery(event.target).vpmsBindViewModel('removeFromGroup', group.parent().parent().parent(), group);
			};
			
		}else{ 
			//else - from first group
			var groupsArray =  $.vpmsBindViewModel.unwrapObservable(model.groups);
			if(groupsArray && groupsArray.length>0){
				var elementsArray = $.vpmsBindViewModel.unwrapObservable(groupsArray[0].elements);
				if(elementsArray)
					for(i=0; i<elementsArray.length; i++)
						model.initColumn(elementsArray[i], i);
			}
		}
		//navigation
		$.vpmsBindViewModel.initGroupsNavigation(model);
	},
	updateModel: function(model, data){
		if(model.trigger)
			model.trigger("change",{field: "mutable"});
		if(data){
			var i;
			if(model.multiple){
				for(i=0; i<model.settings.colSettings.length; i++){
					if(data.cn_ && data.cn_.length>i)
						$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'caption', data.cn_[i]);
					if(data.cr_ && data.cr_.length>i)
						$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'required', (data.cn_[i]=="1"));
				}
			}else{
				var groupsArray =  $.vpmsBindViewModel.unwrapObservable(model.groups);
				if(groupsArray && groupsArray.length>0){
					var elementsArray = $.vpmsBindViewModel.unwrapObservable(groupsArray[0].elements);
					if(elementsArray)
						for(i=0; i<model.settings.colSettings.length; i++){
							$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'caption', $.vpmsBindViewModel.unwrapObservable(elementsArray[i], 'caption'));
							$.vpmsBindViewModel.updateModelItem(model.settings.colSettings[i], 'required', $.vpmsBindViewModel.unwrapObservable(elementsArray[i], 'required'));	
						}
				}
			}
		}
		//navigation
		if(model.navigationUpdate)
			model.navigationUpdate(data);
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'grid',
	template: function(){
		var t = $.vpmsBindViewModel.getTemplate('table');
		if(t){
			if($.isFunction(t.template))
				return t.template();
			else
				return t.template;
		}
		else
			return '<p>grid template is not defined</p>';
	},
	initModel: function(model){
		$.vpmsBindViewModel.initModel_widget(model, 'table');
	},
	updateModel: function(model, data, settings){
		$.vpmsBindViewModel.updateModel_widget(model, data, settings, 'table');
	}
});

//Treeview
$.vpmsBindViewModel.pushTemplate({
	name: 'treeviewTabs',
	template: '\
		<li class="vpms-pages-tab vpms-selectable-tab" data-bind="visible:wrapdata.relevant, attr:{id:wrapdata.id, relevant:wrapdata.relevant}, css: {km-state-active: isSelected}"><a class="km-listview-link">\
			<span data-bind="attr:{class:tabIconClass}"></span>\
			<span data-bind="text:wrapdata.caption"></span>&nbsp;\
			#if(data.wrapdata.multiple){#\
			<a class="km-widget km-rowinsert km-detail" data-role="detailbutton" data-style="rowinsert" data-bind="visible:wrapdata.canAdd"></a>\
			#}#\
			#if(data.parent().parent().multiple){#\
			<a class="km-widget km-rowdelete km-detail" data-role="detailbutton" data-style="rowdelete" data-bind="visible:wrapdata.canRemove"></a>\
			#}#\
		</a>\
		#if(data.wrapdata.groups){#\
			<div data-role="view" #if($.vpmsBindViewModel.useNativeScrolling){#data-use-native-scrolling="true"#}# class="vpms-afterRender" data-bind="attr:{id:treeViewId}">\
				<div data-role="header">\
					<div data-role="navbar">\
						<span data-bind="text:wrapdata.caption"></span>\
						<a class="nav-button km-back" data-role="button" data-align="left" data-bind="click:treeNavigateBack"><span data-bind="text:backcaption"></span></a>#if($.vpmsBindViewModel.isPhone && $.vpmsBindViewModel.hasParentWidgetItem(data, "treeview", "logo")){#\
						<img data-align="right" class="vpms-csc-logo" data-bind="attr:{src:logo}"/>#}#\
					</div>\
				</div>\
				<div data-role="content" class="km-insetcontent">\
					<div class="km-listview-wrapper"><ul class="vpms-listview vpms-listview-clickable km-listview km-list km-listinset" data-template="treeviewTabs" data-bind="source:wrapdata.groups, events:{' + $.vpmsBindViewModel.events.end + ':onSelectGroup}"></ul></div>\
				#if(data.wrapdata.multiple){#\
					<div class="vpms-add-button" data-bind="visible:isMutable">\
						<a class="vpms-button-widget-button" data-role="button" id="add" data-bind="enabled:wrapdata.canAdd, click:addGroupClick"><span class="km-text" data-bind="text:wrapdata.addCaption"></span></a>\
					</div>\
				#}#\
				</div>\
				#if($.vpmsBindViewModel.hasParentWidgetItem(data, "treeview", "buttons")){#<div data-role="footer">\
					<div data-role="tabstrip" class="vpms-buttonbar-widget" data-bind="source:buttons" data-template="button"></div>\
				</div>#}#\
			</div>\
		#}#\
		</li>'
});
$.vpmsBindViewModel.pushTemplate({
	name: 'treeviewSections',
	template: '\
	<div data-role="view" #if($.vpmsBindViewModel.useNativeScrolling){#data-use-native-scrolling="true"#}# class="vpms-afterRender" data-bind="attr:{id:treeSectionViewId, parent:parentGenericContainerId}" data-init="$.vpmsBindViewModel.kendoMobileViewInit" data-show="$.vpmsBindViewModel.kendoMobileViewShow">\
		<div data-role="header">\
			<div data-role="navbar">\
				<span data-bind="text:wrapdata.caption"></span>&nbsp;#if($.vpmsBindViewModel.isPhone){#\
				<a class="nav-button km-back" data-align="left" data-role="button" data-bind="click:sectionsNavigateBack">Back</a>#}##if($.vpmsBindViewModel.hasParentWidgetItem(data, "treeview", "logo")){#\
				<img data-align="right" class="vpms-csc-logo" data-bind="attr:{src:logo}"/>\
			#}#</div>\
		</div>\
		<div data-role="content" class="km-insetcontent">\
			<div class="vpms-pages-group-error ui-state-error" data-bind="visible:wrapdata.error, text:wrapdata.error"></div>\
			#if($.vpmsBindViewModel.templateExists(data.wrapdata.widget)){#\
				<div class="vpms-#=wrapdata.widget#-widget #if(data.wrapdata.settings &&  data.wrapdata.settings.css){# #=data.wrapdata.settings.css# #}#"   data-template="${wrapdata.widget}" data-bind="source: wrapdata"></div>\
			#}#\
			#if(data.wrapdata.groups){#\
				<div class="vpms-generic-view-container" data-template="treeviewSections" data-bind="attr:{id:genericContainerId}, source:wrapdata.groups"></div>\
			#}#\
		</div>\
		#if($.vpmsBindViewModel.isPhone && $.vpmsBindViewModel.hasParentWidgetItem(data, "treeview", "buttons")){#<div data-role="footer">\
		<div data-role="tabstrip" class="vpms-buttonbar-widget" data-bind="source:buttons" data-template="button"></div>\
		</div>#}#\
	</div>\
	'
});
$.vpmsBindViewModel.pushTemplate({
	name: 'treeview',
	template: '\
		#if(!$.vpmsBindViewModel.isPhone){#\
		<div data-role="splitview" id="#=data.tabletViewId()#">\
			<div data-role="pane" id="left-pane" data-initial="#=data.treeViewId()#">\
		#}#\
			<div data-role="view" #if($.vpmsBindViewModel.useNativeScrolling){#data-use-native-scrolling="true"#}# data-bind="attr:{id:treeViewId}" #if($.vpmsBindViewModel.isPhone){#data-show="$.vpmsBindViewModel.kendoMobileViewShow"#}#>\
					<div data-role="header">\
						<div data-role="navbar">\
							<span data-bind="text:caption"></span>#if(data.navigateBack){#\
							<a class="nav-button km-back" data-role="button" data-align="left" data-bind="click:navigateBack"><span data-bind="text:backcaption"></span></a>#}##if($.vpmsBindViewModel.isPhone && data.logo){#\
							<img data-align="right" class="vpms-csc-logo" data-bind="attr:{src:logo}"/>\
						#}#</div>\
					</div>\
					<div data-role="content" class="km-insetcontent">\
						<div class="km-listview-wrapper"><ul class="vpms-listview vpms-listview-clickable km-listview km-list km-listinset" data-template="treeviewTabs" data-bind="source:groups, events:{' + $.vpmsBindViewModel.events.end + ':onSelectGroup}"></ul></div>\
					</div>\
					#if(data.buttons){#<div data-role="footer">\
						<div data-role="tabstrip" class="vpms-buttonbar-widget" data-bind="source:buttons" data-template="button">\
						</div>\
					</div>#}#\
					#if($.vpmsBindViewModel.isPhone){#\
					<div class="vpms-generic-view-container" data-template="treeviewSections" data-bind="attr:{id:genericContainerId}, source:groups"></div>\
					#}#\
				</div>\
		#if(!$.vpmsBindViewModel.isPhone){#\
			</div>\
			<div data-role="pane" id="right-pane" data-initial="#=data.treeSectionViewId()#">\
				<div data-role="view" #if($.vpmsBindViewModel.useNativeScrolling){#data-use-native-scrolling="true"#}# data-bind="attr:{id:treeSectionViewId}" #if($.vpmsBindViewModel.isPhone){#data-show="$.vpmsBindViewModel.kendoMobileViewShow"#}#>\
					<div data-role="header">\
						<div data-role="navbar">\
							<span data-bind="text:caption"></span>#if(data.logo){#\
							<img data-align="right" class="vpms-csc-logo" data-bind="attr:{src:logo}"/>\
						#}#</div>\
					</div>\
					<div data-role="content" class="km-insetcontent">\
						<div class="vpms-generic-view-container" data-template="treeviewSections" data-bind="attr:{id:genericContainerId}, source:groups"></div>\
					</div>\
			</div>\
		</div>\
		#}#\
	',
	initModel: function(model){
		model.getGroupsTree = true;
		if(model.container===undefined)
			model.container = true;
		if(!model.name) model.name = "treeview";
		if(!model.id) model.id = model.name;
		if(!model.settings)
			model.settings={};
		model.logo = $.vpmsBindViewModel.initModelItem(model.settings?model.settings.logo:undefined, undefined);
		model.captions = $.vpmsBindViewModel.initModelItem(model.captions, []);	
		model.caption = function(){
			return $.vpmsBindViewModel.getCaptionById(this, 'productCaption');
		};
		model.captions.push($.vpmsBindViewModel.initModel_caption({name:'btn.back', caption:'Back'}));
		model.backcaption = function(){
			return $.vpmsBindViewModel.getCaptionById(this, 'btn.back');
		};
		
		model.cancelQuote = function(e){
			jQuery(e.target).vpmsBindViewModel('changeModel');
		};
		
		//getters
		model.getTreeViewId = function(id){
			if(id)			
				return "tree_" + $.vpmsBindViewModel.alphanumericSelector(id);
			else
				return "";
		};
		model.getSectionViewId = function(id){
			if(id)
				return "section_" + $.vpmsBindViewModel.alphanumericSelector(id);
			else
				return "";
		};
		model.getGenericContainerId = function(id){
			return "generic_" + $.vpmsBindViewModel.alphanumericSelector(id);
		};
		model.getModelGroupById = function(_model, _id){
			if(_model.id == _id)
				return _model;
			var groupsArray = (_model.groups)?_model.groups:_model.g_;
			if(groupsArray){
				for(var i=0; i<groupsArray.length; i++){
					var _group = $.vpmsBindViewModel.getArrayElement(groupsArray[i]);
					var page = this.getModelGroupById(_group, _id);
					if(page)
						return page;
				}
			}
			return undefined;
		};
		//binding functions
		model.tabletViewId = function(){
			return "tablet_" + $.vpmsBindViewModel.alphanumericSelector(this.name);
		};		
		model.treeViewId = function(dataItem){
			var _group = (dataItem)?$.vpmsBindViewModel.getArrayElement(dataItem):this;
			return this.getTreeViewId($.vpmsBindViewModel.unwrapObservable(_group, "id"));
		};
		model.treeSectionViewId = function(dataItem){
			var _group = (dataItem)?$.vpmsBindViewModel.getArrayElement(dataItem):this;
			return this.getSectionViewId($.vpmsBindViewModel.unwrapObservable(_group, "id"));
		};
		model.genericContainerId = function(dataItem){
			var _group = (dataItem)?$.vpmsBindViewModel.getArrayElement(dataItem):this;
			return this.getGenericContainerId($.vpmsBindViewModel.unwrapObservable(_group, "id"));
		};
		model.parentGenericContainerId = function(dataItem){
			var _group = (dataItem)?$.vpmsBindViewModel.getArrayElement(dataItem):this;
			return this.getGenericContainerId($.vpmsBindViewModel.unwrapObservable(_group.parent().parent().parent(), "id"));
		};		
		model.isSelected = function(dataItem){
			var _group = $.vpmsBindViewModel.getArrayElement(dataItem);
			var result = ($.vpmsBindViewModel.unwrapObservable(this, "selectedPage") == _group.id);
			return result; 
		};
		model.tabIconClass = function(dataItem){
			if(dataItem){
				var _group = $.vpmsBindViewModel.getArrayElement(dataItem);
				return ($.vpmsBindViewModel.unwrapObservable(_group, "error")?"km-icon km-warning":($.vpmsBindViewModel.unwrapObservable(_group, "visited")?"km-icon km-check":"km-icon km-notvisited"));
			}
		};
		model.isMutable = function(dataItem){
			if(dataItem){
				var _group = $.vpmsBindViewModel.getArrayElement(dataItem);				
				return !$.vpmsBindViewModel.unwrapObservable(_group,'disabled') && !$.vpmsBindViewModel.unwrapObservable(_group, 'staticPageCount');
			}
		};
		model.addGroupClick = function(e){
			var _group = $.vpmsBindViewModel.getArrayElement(e.data);
			this.setSelectedPageFromGroup = _group;
			if(_group.canAdd)
				jQuery(e.target).vpmsBindViewModel('addToGroup', _group, this.selectedPage);			
		};
		model.onSelectGroup = function(e){
			if($(e.target).hasClass("km-detail")){
				var _group = $.vpmsBindViewModel.getArrayElement(e.target.kendoBindingTarget.source);
				if($(e.target).hasClass("km-rowinsert")){
					jQuery(e.target).vpmsBindViewModel('addToGroup', _group);
					this.setSelectedPageFromGroup = _group;
				}else{
					if($(e.target).hasClass("km-rowdelete")){
						var _parent = _group.parent().parent().parent();
						this.setSelectedPageFromGroup = _parent;
						jQuery(e.target).vpmsBindViewModel('removeFromGroup', _parent, _group);
					}
				}
			}else{
				var $li =  jQuery(e.target).closest('li');
				if($li.data('vpmsSelectStart')===true){
					var id = $li.attr('id');
					if(id){
						this.selectGroupUpdate(id);
					}
				}
				$li.data('vpmsSelectStart', false);
			}
		};		
		//selecting page routines
		model.selectedPage = "";	
		model.selectedGroup = function(){
			return this.getModelGroupById(this, this.selectedPage); 
		};		
		model.selectGroup = function(id){
			var _model = this;
			if(_model.selectedPage == id)
				return;
			if(!id)
				id=_model.id;
			var _group = _model.getModelGroupById(_model, id);
			if(!_group)
				return;
			$.vpmsBindViewModel.updateModelItem(_model,'selectedPage', id);
			$.vpmsBindViewModel.pushState(_model.containerId, _model.id, id, true);
			var rootModel = $('#' + _model.containerId).data('model');
			setTimeout(function(){
				if(_group.groups && _group.groups.length>0){
					$.vpmsBindViewModel.changeView(_model.getTreeViewId(id));
				}else{
					if($.vpmsBindViewModel.isPhone)
						$.vpmsBindViewModel.changeView(_model.getSectionViewId(id));
					else
						$.vpmsBindViewModel.changeView(_model.getTreeViewId(_group.parent().parent().parent().id));
				}
				if(!$.vpmsBindViewModel.isPhone)
					$.vpmsBindViewModel.changeView(_model.getSectionViewId(id));
			}, 1);
		};				
		model.selectGroupUpdate = function(id){
			//To prevent changing view if view change already has been requested
			if(!$.vpmsBindViewModel.startMobileChangeView())
				return;
			//To deffer changing of a view if autocommit field being processed
			if($.vpmsBindViewModel.isModelUpdating('#' + this.containerId)){
				this.selectGroupUpdateRequest = id;
				return;
			}
			if(!this.syncUpdate){
				this.selectGroup(id);
			}else{
				var group = this.getModelGroupById(this, id);
				//check if there are elements or groups to restore, restore them else act as async update
				if(group && group._elements){
					var rootModel = $('#' + this.containerId).data('model');
					$.vpmsBindViewModel.blockUI(rootModel);
					$.vpmsBindViewModel.debug("syncUpdate - start bind cached elements", true);
					for(var i=0; i<group._elements.length; i++)
						group.elements.push(group._elements[i]);
					group._elements = undefined;
					this.requestedPage = id;
					$('#' + rootModel.containerId).find('#' + this.getSectionViewId(id)).find('[data-role="content"]').addClass('vpms-afterRender');
					$.vpmsBindViewModel.debug("syncUpdate - end bind cached elements");
				}else{
					this.selectGroup(id);
				}
			}
			jQuery('#' + this.containerId).vpmsBindViewModel('updateModel');
		};		
		model.treeNavigateBack = function(event){
			var parent = event.data.parent().parent();
			this.selectGroupUpdate(parent.id);
		};
		if($.vpmsBindViewModel.isPhone){
			model.sectionsNavigateBack = function(event){
				var group = $.vpmsBindViewModel.getArrayElement(event.data);
				this.selectGroupUpdate(group.parent().parent().parent().id);
			};
		}			
		//restoring views before removing groups routines
		model.restoreTreeViewGenericViews = function(group){
			var _group = $.vpmsBindViewModel.getArrayElement(group);
			var $li = jQuery('[id="'+ _group.id +'"].vpms-pages-tab');
			var $view = jQuery('[id="'+ model.getTreeViewId(_group.id) +'"]');
			if($li.length>0 && $view.length>0){
				$li.append($view);
			}
			if(_group.groups){
				for(var i=0; i<_group.groups.length; i++){
					this.restoreTreeViewGenericViews(_group.groups[i]);
				}
			}			
		};
		var initMultipleGroups = function(_model){
			if(_model.groups){
				for(var i=0; i<_model.groups.length; i++){
					var _group = $.vpmsBindViewModel.getArrayElement(_model.groups[i]);
					if(_group.multiple){
						_group.beforeGroupRemove = function(group){
							$.vpmsBindViewModel.kendoMobileRestoreGenericViews(jQuery('[id="'+ model.getGenericContainerId(this.id) +'"].vpms-generic-view-container'));
							model.restoreTreeViewGenericViews(group);
						};
						_group.afterGroupAdded = function(__model, __group, __data){
							initMultipleGroups(__group);
						};						
					}else{
						initMultipleGroups(_group);
					}
				}
			}
		};	
		initMultipleGroups(model);	
		//Treeview navigation
		function findPrevPage(_model, params){
			if(_model.id == params.id){
				return params.prev;
			}else{
				if(_model.relevant===undefined || _model.relevant)
					params.prev = _model.id;				
			}
			if(!_model.groups)
				return;
			for(var i=0; i<_model.groups.length; i++){
				var _group = $.vpmsBindViewModel.getArrayElement(_model.groups[i]);
				var result = findPrevPage(_group, params);
				if(result)
					return result;
			}
		}
		function findNextPage(_model, params){
			if(_model.id == params.id){
				params.found = true;
			}
			if(!_model.groups)
				return;
			for(var i=0; i<_model.groups.length; i++){
				var _group = $.vpmsBindViewModel.getArrayElement(_model.groups[i]);
				if(params.found){
					if(_group.relevant===undefined || _group.relevant)
						return _group.id;					
				}
				var result = findNextPage(_group, params);
				if(result)
					return result;
			}
		}
		model.prevPage = function(pageId){
			if(pageId){
				this.selectGroupUpdate(pageId);
				return;
			}				
			var params = {
				id : this.selectedPage?this.selectedPage:this.id
			};
			return findPrevPage(this, params);
		};
		model.nextPage = function(pageId){
			if(pageId){
				this.selectGroupUpdate(pageId);
				return;
			}				
			var params = {
				id : this.selectedPage?this.selectedPage:this.id
			};
			return findNextPage(this, params);
		};

		//buttons : find buttonBar widget, elements - buttons for tabstrip, remove next and previous
		if(model.elements){
			for(var i=0; i<model.elements.length; i++){
				if(model.elements[i].widget=="buttonbar"){
					for(var j=model.elements[i].elements.length-1; j>=0; j--){
						model.elements[i].elements[j].barButton = true;
					}
					model.buttons=function(){
						for(var i=0; i<this.elements.length; i++){
							if(this.elements[i].wrapdata.widget=="buttonbar"){
								if(this.get)
									return this.get("elements[" +i +"].wrapdata.elements");
								else
									return this.elements[i].wrapdata.elements;
							}
						}
						return [];
					};
					break;
				}
			}
		}
	},
	beforeUpdateModel: function(model, data){
		if(data){
			//model.setSelectedPageFromGroup contains multiple group add or remove operation performed
			var dataGroup, tab;
			if(model.setSelectedPageFromGroup){
				dataGroup = model.getModelGroupById(data, model.setSelectedPageFromGroup.id);
				if(dataGroup && dataGroup.sp)
					data.sp = dataGroup.sp; 				
				model.setSelectedPageFromGroup = undefined;
			}
			//if selected Page comes from server - set it 
			if(!data.sp){
				if(model.requestedPage){
					data.sp = model.requestedPage;
				}else{
					//get selected tab from state
					tab = $.vpmsBindViewModel.getState(model.containerId, model.id);
					if(tab!==undefined && tab!=="")
						data.sp = tab;
				}
			}
			model.requestedPage = undefined;
			//if data.sp is set - check if this page will be relevant after update
			if(data.sp){
				dataGroup = model.getModelGroupById(data, data.sp);
				if(dataGroup && !(dataGroup._r===undefined || dataGroup._r===true))
					data.sp = undefined;
			}else{
				//if data.sp still not set take first relevant but only if it has no child groups, but not for the Phone!!!	
				if(!model.multiple && data.g_ && !$.vpmsBindViewModel.isPhone){
					var i=0; var done=false;
					while(i<data.g_.length && !done){
						for(var k=0; k<model.groups.length; k++){
							var group = $.vpmsBindViewModel.getArrayElement(model.groups[k]);
							if(group.name==data.g_[i]._n){
								if(group.relevant!==undefined){
										$.vpmsBindViewModel.updateModelItem(group, 'relevant', (data.g_[i]._r!==undefined?data.g_[i]._r:true));
								}
								if(group.relevant===undefined || group.relevant===true){
									if(!group.multiple && (!group.groups || group.groups.length===0 )){
										data.sp = data.g_[i].id;
									}
									done = true;
									break;
								}
							}
						}
						i++;
					}
						
				}
			}
			//syncUpdate on init - clear content and cache elements for not selected
			if(model.syncUpdate && !model.syncUpdateInited){
				var clearContentFunction = function(_data, _model, _id){
					var groupsArray = _model.groups;
					for(var i=0; i<_data.g_.length; i++){
						var modelGroup = undefined;
						for(var k=0; k<groupsArray.length; k++){
							var group = $.vpmsBindViewModel.getArrayElement(groupsArray[k]);
							if(group.name==_data.g_[i]._n){
								modelGroup = group;
								break;
							}
						}
						if(_id!=data.g_[i].id){
							//cash all groups content for not selected page
							if(modelGroup && modelGroup.elements.length>0)
								modelGroup._elements = modelGroup.elements.splice(0, modelGroup.elements.length);
							//clear all content for not selected page
							_data.g_[i].e_=undefined;
						}
						if(_data.g_[i].g_)
							clearContentFunction(_data.g_[i], modelGroup, _id);
					}
					
				};
				clearContentFunction(data, model, data.sp);
				model.syncUpdateInited = true;
			}			
		}
	},
	updateModel: function(model, data){
		model.selectGroup(data.sp?data.sp:"");
		if(!model.setAllGroupsData){
			var selectedGroup = model.selectedGroup();
			if(selectedGroup != model)
				model.prevSelectedGroup = selectedGroup;
			else
				model.prevSelectedGroup = undefined;
		}			
			
		if(model.selectGroupUpdateRequest){
			var _sp = model.selectGroupUpdateRequest;
			model.selectGroupUpdateRequest = undefined;
			setTimeout(function(){
				model.selectGroupUpdate(_sp);
			}, 1);
		}
	}
});


$.vpmsBindViewModel.pushTemplate({
	name: 'elementgroup',
	template: '\
			#if(data.isListview){#\
			<ul class="km-listview km-list vpms-listview km-listinset vpms-elements-container" data-template="listElement" data-bind="source: elements"></ul>\
			#}else{#\
			<span>\
				<div class="vpms-elements-container#if(data.settings && data.settings.colNumber){# vpms-#=data.settings.colNumber#-col#}#" data-template="element" data-bind="source: elements"></div>\
				#if(data.labeled){#<div class="ui-state-error" data-bind="visible:error, text:error">#=data.error#</div>#}#\
			</span>\
			#}#\
	',
	initModel: function(model){
		if(!model.labeled)
			model.hasLabel = false;
		if(model.isGroup===undefined)
			model.isGroup = true;
		if(model.container===undefined)
			model.container = true;
		if(!model.settings)
			model.settings = {};
		if(!model.name) model.name = "elementgroup";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, model.name);
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, false);
		model.elements = $.vpmsBindViewModel.initModelItem(model.elements, []);	
	},
	updateModel: function(model, data, settings){
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'autocomplete',
	template: function(){
		var t = $.vpmsBindViewModel.getTemplate('combobox');
		if(t){
			if($.isFunction(t.template))
				return t.template();
			else
				return t.template;
		}
		else
			return '<p>autocomplete template is not defined</p>';
	},
	initModel: function(model){
		$.vpmsBindViewModel.initModel_widget(model, 'combobox');
	},
	updateModel: function(model, data, settings){
		$.vpmsBindViewModel.updateModel_widget(model, data, settings, 'combobox');
	}
});

$.vpmsBindViewModel.pushTemplate({
	name: 'checkboxAfterLabel',
	template: function(){
		var t = $.vpmsBindViewModel.getTemplate('checkbox');
		if(t){
			if($.isFunction(t.template))
				return t.template();
			else
				return t.template;
		}
		else
			return '<p>checkboxAfterLabel template is not defined</p>';
	},
	initModel: function(model){
		$.vpmsBindViewModel.initModel_widget(model, 'checkbox');
	},
	updateModel: function(model, data, settings){
		$.vpmsBindViewModel.updateModel_widget(model, data, settings, 'checkbox');
	}
});

//image widget - defaults: isElement=false
$.vpmsBindViewModel.pushTemplate({
	name: 'image',
	template: '<img class="vpms-image-widget-image" data-bind="attr: {src:src, id:id}" #if(data.width){#width="#=data.width#"#}# #if(data.height){#height="#=data.height#"#}#/>',
	initModel: function(model){
		if(model.isElement===undefined)
			model.isElement = false;
		if(!model.name) model.name = "image";
		model.id = $.vpmsBindViewModel.initModelItem(model.id, "");
		model.caption = $.vpmsBindViewModel.initModelItem(model.caption, "");
		model.relevant = $.vpmsBindViewModel.initModelItem(model.relevant, true);
		model.required = $.vpmsBindViewModel.initModelItem(model.required, false);
		model.error = $.vpmsBindViewModel.initModelItem(model.error, "");
		model.value = $.vpmsBindViewModel.initModelItem(model.value, "");
		model.disabled = $.vpmsBindViewModel.initModelItem(model.disabled, true);
		model.tooltip = $.vpmsBindViewModel.initModelItem(model.tooltip, "");
		model._src = model.src;
		model.src = $.vpmsBindViewModel.initModelItem("", "");
	},
	updateModel: function(model, data, settings){
		var src = model._src;
		if(model.isElement){
			src = $.vpmsBindViewModel.unwrapObservable(model, 'value');			
		}
		$.vpmsBindViewModel.updateModelItem(model, 'src', (settings.resurl?(settings.resurl + "/"):"") + src);
	}
});
$.vpmsBindViewModel.models.push({
 name: 'ID_Collatz_Conjecture',
 model: function() { return   {name: 'ID_Collatz_Conjecture', widget: 'pages', settings:{suppresstabs:true},
  elements: [
     {name: 'pagesbuttons', widget:'buttonbar',
     elements: [
        {name: 'Save', caption: 'Save', widget:'button', pos:'left', role:'save', isElement:false},
        {name: 'Previous', caption: 'Previous', widget:'button', pos:'right', role:'previous', isElement:false},
        {name: 'Next', caption: 'Next', widget:'button', pos:'right', role:'next', isElement:false}
     ]
     }
  ],
  groups: [
     {name: 'PAGE_Collatz_Conjecture', widget: 'superWidget', settings:{colNumber:1,css:'bold-caption '},
     elements: [
        {name: 'SEC_Collatz_Conjecture_Input', widget: 'border', container:false, settings:{colNumber:1,css:'bold '},
        elements: [
           {name: 'fld_Number_INT', widget:'edit', format:'number', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}
        ]
        },
        {name: 'SEC_Collatz_Conjecture_Output', widget: 'border', container:false, settings:{colNumber:1,css:'bold '},
        elements: [
           {name: 'fld_Steps_INT', widget:'edit', format:'number', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}},
           {name: 'fld_Values_VEC', widget:'memo', labeled:true, autocommit:true, rows:'15', container:false, settings:{labelAlign:'left'}},
           {name: 'fld_Maximum_Value_INT', widget:'edit', format:'number', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}
        ]
        }
     ]
     }
  ]
  };}
});
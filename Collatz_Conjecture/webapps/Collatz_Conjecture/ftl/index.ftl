<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<#if RequestParameters.platform??>
<meta name="viewport" content="width=device-width, initial-scale=1">
</#if>
<title>VP/MS Styler - Collatz_Conjecture</title>
		
<#if RequestParameters.platform??>
<#include "_header_ios.ftl">
<#else>
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/jquery__default/jquery-ui.css'}" type="text/css" />
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/ui.jqgrid.css'}" type="text/css" />
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/jquery.treeview.css'}" type="text/css" />
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/all.css'}" type="text/css" />
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/custom.css'}" type="text/css" />

<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery-ui.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/json2.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jookie.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/grid.locale-en.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.jqGrid.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/excanvas.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.cookie-modified.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.numbers.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/amplify.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.ba-bbq.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.tmpl.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/knockout-1.2.1.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/date.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.treeview.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.custom.properties.js'}"></script> <!-- custom properties' template definitions -->
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.widgets.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.custom.widgets.js'}"></script> <!-- custom widgets' template definitions -->
</#if>


<script type="text/javascript">		

var styler_ui_model = "ID_Collatz_Conjecture";

jQuery(document).ready(function($){
	var quoteId = "${quoteId!''}";

	<#if RequestParameters.debug??>
		jQuery("#debugQuoteInput").attr("value", quoteId);
		jQuery("#debugModelInput").attr("value", styler_ui_model);
		jQuery("#debugContainer").show();
	</#if>

	// product
	var settings = {
		defaultModel: styler_ui_model,
		quoteId: quoteId
    };
	$('#styler').vpmsBindViewModel(settings);
});
</script>

</head>
<body>
	<div id="styler" url="${springMacroRequestContext.getContextPath() + '/process'}" resurl="${springMacroRequestContext.getContextPath() + '/images'}" pdfurl="pdf.pdf"></div>		
	<script id="ID_Collatz_Conjecture" type="text/html">
	<div vpms_element="{name: 'ID_Collatz_Conjecture', widget: 'pages', settings:{suppresstabs:true}}">
		<div vpms_group="{name: 'PAGE_Collatz_Conjecture', widget: 'superWidget', settings:{colNumber:1,css:'bold-caption '}}">
			<div vpms_element="{name: 'SEC_Collatz_Conjecture_Input', widget: 'border', container:false, settings:{colNumber:1,css:'bold '}}">
				<div vpms_element="{name: 'fld_Number_INT', widget:'edit', format:'number', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
			</div>
			
			<div vpms_element="{name: 'SEC_Collatz_Conjecture_Output', widget: 'border', container:false, settings:{colNumber:1,css:'bold '}}">
				<div vpms_element="{name: 'fld_Steps_INT', widget:'edit', format:'number', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Values_VEC', widget:'memo', labeled:true, autocommit:true, rows:'15', container:false, settings:{labelAlign:'left'}}"></div>
				
				<div vpms_element="{name: 'fld_Maximum_Value_INT', widget:'edit', format:'number', labeled:true, autocommit:true, container:false, settings:{labelAlign:'left',align:'left'}}"></div>
			</div>
		</div>
		<div vpms_element="{name: 'pagesbuttons', widget:'buttonbar'}">
			<div vpms_element="{name: 'Save', caption: 'Save', widget:'button', pos:'left', role:'save', isElement:false}"></div>
			<div vpms_element="{name: 'Previous', caption: 'Previous', widget:'button', pos:'right', role:'previous', isElement:false}"></div>
			<div vpms_element="{name: 'Next', caption: 'Next', widget:'button', pos:'right', role:'next', isElement:false}"></div>
		</div>
	</div>
	</script>
	<!-- debug section: enable debug with ?debug attached to URL after quote ID --> 
	<div id="debugContainer" style="display:none;">
		<a href="#" onclick="if(jQuery('#debug').is(':visible')) jQuery('#debug').hide(); else jQuery('#debug').show();">Debug</a> 
		<div id="debug" style="display:none;">
			<form method="post" action="${springMacroRequestContext.getContextPath() + '/debug'}" enctype="multipart/form-data">
				<input id="debugQuoteInput" type="hidden" name="quoteId"/> 
				<input id="debugModelInput" type="hidden" name="modelName"/>
				<p><button type="submit" name="action" value="getState">Get state</button></p>
				<p><button type="submit" name="action" value="setState">Set state</button> File: <input type="file" name="vpmsState" /> </p>
			</form>
		</div>
	</div>
</body>

</html>

<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>Dynamic Log4J Control</title>
</head>
<body>
<h1>Dynamic Log4J Control</h1>
<form>
<table class="default">
<tr><th>Level</th><th>Logger</th><th>Set New Level</th></tr>
<tr><td>${rootLogger.level}</td><td>${rootLogger.name}</td><td>
    <#list ["DEBUG", "INFO", "WARN", "ERROR", "OFF"] as level>
        <a href="${springMacroRequestContext.getContextPath()+'/log4j?log=&level='+level}">${level}</a>
    </#list>
</td></tr>
<#list rootLogger.loggerRepository.currentLoggers as logger>
	<#if showAll || (((logger.level!"") != "") && (logger.level.syslogEquivalent > -1)) >    
        <tr><td>${logger.level!""}</td><td>${logger.name}</td><td>
        <#list ["TRACE", "DEBUG", "INFO", "WARN", "ERROR", "OFF"] as level>
            <a href="${springMacroRequestContext.getContextPath()+'/log4j?log='+logger.name+'&level='+level}">${level}</a>
        </#list>
        </td></tr>
    </#if>
</#list>
<tr><td></td><td><input type="text" name="log"/></td><td>
<select name="level">
    <#list ["TRACE", "DEBUG", "INFO", "WARN", "ERROR", "OFF"] as level>
        <option>${level}</option>
    </#list>
</select> <input type="submit" value="Add New Logger"/></td></tr>
</table>
</form>
Show <a href="${springMacroRequestContext.getContextPath()+'/log4j?showAll=true'}">all known loggers</a>
<p>Download
    <#list rootLogger.allAppenders as appender>
      	<#if appender.class.name=="org.apache.log4j.RollingFileAppender" || appender.class.name=="org.apache.log4j.FileAppender">
      		<a href="${springMacroRequestContext.getContextPath()+'/log4j?download='+appender.name}">${appender.name}</a>&nbsp;        	
    	</#if>            	
    </#list> 
    <a href="${springMacroRequestContext.getContextPath()+'/log4j?download=all'}">all</a>&nbsp;
</p>    
</body>
</html>
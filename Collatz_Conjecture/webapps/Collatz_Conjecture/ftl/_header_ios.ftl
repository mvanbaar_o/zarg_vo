<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/kendo/kendo.mobile.all.min.css'}" type="text/css" />
<link rel="stylesheet" href="${springMacroRequestContext.getContextPath()+'/css/jquery.vpmsBindViewModel.kendo.css'}" type="text/css" />

<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/kendo.mobile.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/json2.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jookie.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.maskedinput.min.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.cookie-modified.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.numbers.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.js'}"></script>
<script type="text/javascript" src="${springMacroRequestContext.getContextPath()+'/js/jquery.vpmsBindViewModel.widgets.kendo.js'}"></script>

<style type="text/css">
	body, html{
		margin:0;
		height:100%;
		width:100%;
	}
</style>

<script type="text/javascript">
<#if RequestParameters.platform??>
	$.vpmsBindViewModel.platform = "${RequestParameters.platform}";
</#if>
<#if RequestParameters.phone??>
	$.vpmsBindViewModel.isPhone = true;
</#if>
</script>
